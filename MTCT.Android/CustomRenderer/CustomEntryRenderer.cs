﻿using Android.Content;
using Android.Graphics.Drawables;
using Android.Text;
using MTCT.Droid.CustomRenderer;
using MTCT.Extension;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EntryBox), typeof(CustomEntryRenderer))]
[assembly: ExportRenderer(typeof(PickerBox), typeof(CustomPickerRenderer))]
[assembly: ExportRenderer(typeof(EditorBox), typeof(CustomEditorRenderer))]
[assembly: ExportRenderer(typeof(Xamarin.Forms.Button), typeof(ButtonCustomRenderer))]
[assembly: ExportRenderer(typeof(Label), typeof(LabelCustomRenderer))]
[assembly: ExportRenderer(typeof(ExtDatePicker), typeof(DatePickerExtension))]
[assembly: ExportRenderer(typeof(ExtTimePicker), typeof(TimePickerExtension))]
[assembly: ExportRenderer(typeof(DateTimePickerBox), typeof(DateTimeEntryBoxRender))]
namespace MTCT.Droid.CustomRenderer
{
    public class CustomEntryRenderer : EntryRenderer
    {
        public CustomEntryRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                string fontFamily = e.NewElement?.FontFamily;
                //if (!string.IsNullOrEmpty(fontFamily))
                //{
                //   // var label = (TextView)Control; // for example
                //   // Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, fontFamily + ".otf");
                //   // label.Typeface = font;
                //}
                //GradientDrawable gd = new GradientDrawable();
                //gd.SetColor(global::Android.Graphics.Color.Transparent);
                //this.Control.SetBackgroundDrawable(gd);
                //this.Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.SetHintTextColor(ColorStateList.ValueOf(global::Android.Graphics.Color.White));


                var nativeedittextfield = (Android.Widget.EditText)this.Control;
                GradientDrawable gd = new GradientDrawable();
                gd.SetCornerRadius(0);
                gd.SetColor(Android.Graphics.Color.Transparent);
                nativeedittextfield.Background = gd;
            }
        }
    }

    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                string fontFamily = e.NewElement?.FontFamily;
                //if (!string.IsNullOrEmpty(fontFamily))
                //{
                //   // var label = (TextView)Control; // for example
                //   // Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, fontFamily + ".otf");
                //   // label.Typeface = font;
                //}
                GradientDrawable gd = new GradientDrawable();
                gd.SetColor(global::Android.Graphics.Color.Transparent);
                this.Control.SetBackgroundDrawable(gd);
                this.Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.SetHintTextColor(ColorStateList.ValueOf(global::Android.Graphics.Color.White));
            }
        }
    }

    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                string fontFamily = e.NewElement?.FontFamily;
                //if (!string.IsNullOrEmpty(fontFamily))
                //{
                //   // var label = (TextView)Control; // for example
                //   // Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, fontFamily + ".otf");
                //   // label.Typeface = font;
                //}
                GradientDrawable gd = new GradientDrawable();
                gd.SetColor(global::Android.Graphics.Color.Transparent);
                this.Control.SetBackgroundDrawable(gd);
                this.Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.SetHintTextColor(ColorStateList.ValueOf(global::Android.Graphics.Color.White));
            }
        }
    }

    public class ButtonCustomRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            var button = this.Control;
            button.SetAllCaps(false);

            string fontDamily = e.NewElement?.FontFamily;
            //if (!string.IsNullOrEmpty(fontDamily))
            //{
            //    var label = (TextView)Control; // for example
            //    Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, fontDamily + ".otf");
            //    label.Typeface = font;

            //}
        }
    }

    public class LabelCustomRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            string fontFamily = e.NewElement?.FontFamily;
            //if (!string.IsNullOrEmpty(fontFamily))
            //{
            //    var label = (TextView)Control; // for example
            //    Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, fontFamily + ".otf");
            //    label.Typeface = font;
            //}
        }
    }

    public class DatePickerExtension : DatePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.DatePicker> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                return;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null)
            {
                return;
            }

            var nativeEditTextField = Control;
            GradientDrawable gd = new GradientDrawable();
            gd.SetCornerRadius(0);

            gd.SetStroke(0, Android.Graphics.Color.Transparent);
            gd.SetColor(Android.Graphics.Color.Transparent);
            //gd.SetStroke(3, Android.Graphics.Color.ParseColor("#66FFFFFF"));
            gd.SetColor(Android.Graphics.Color.Transparent);

            nativeEditTextField.Background = gd;
            Control.SetPadding(0, 0, 0, 0);
            Control.Gravity = Android.Views.GravityFlags.CenterVertical;
        }
        /// <summary>
        /// Setzt den Datumswert oder den NullText
        /// </summary>
        /// <param name="customDatePicker">Das PCL Control</param>
    }

    public class TimePickerExtension : TimePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TimePicker> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                return;
            }

            var nativeEditTextField = Control;
            GradientDrawable gd = new GradientDrawable();
            gd.SetCornerRadius(0);
            gd.SetColor(Android.Graphics.Color.Transparent);
            gd.SetStroke(0, Android.Graphics.Color.Transparent);
            gd.SetColor(Android.Graphics.Color.Transparent);

            nativeEditTextField.Background = gd;
            Control.SetPadding(5, 0, 5, 0);
            Control.Gravity = Android.Views.GravityFlags.CenterVertical;

            //string fontDamily = e.NewElement?.FontFamily;
            //if (!string.IsNullOrEmpty(fontDamily))
            //{
            //    var label = (TextView)Control; // for example
            //    Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, fontDamily + ".ttf");
            //    label.Typeface = font;
            //}
            //this.Control.TextFormatted = "hh:mm tt";
        }


    }

    public class DateTimeEntryBoxRender : EntryRenderer
    {
        public DateTimeEntryBoxRender()
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            string fontFamily = e.NewElement?.FontFamily;
            //if (!string.IsNullOrEmpty(fontFamily))
            //{
            //    var label = (TextView)Control; // for example
            //    Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, fontFamily + ".ttf");
            //    label.Typeface = font;
            //}

            var nativeedittextfield = (Android.Widget.EditText)this.Control;
            GradientDrawable gd = new GradientDrawable();
            gd.SetCornerRadius(0);
            gd.SetColor(Android.Graphics.Color.Transparent);
            nativeedittextfield.Background = gd;

            Control.SetPadding(0, 0, 0, 0);
            Control.Gravity = Android.Views.GravityFlags.CenterVertical;

        }
    }
}