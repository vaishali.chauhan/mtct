﻿using System;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using MTCT.Droid.InterfaceService;
using MTCT.Interface;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(BarcodeManager1))]
namespace MTCT.Droid.InterfaceService
{
    public class BarcodeManager1 : IBarcodeManager
    {
        public byte[] GetByteArray(string parameter)
        {
            byte[] bitmap = null;
            try
            {
                var barcodeWrite = new ZXing.Mobile.BarcodeWriter
                {
                    Format = ZXing.BarcodeFormat.CODE_128,
                    Options = new ZXing.Common.EncodingOptions
                    {
                        Width = 400,
                        Height = 80,
                        Margin = 0,
                       
                    }
                };
                var barcode = barcodeWrite.Write(parameter);
                MemoryStream str = new MemoryStream();
                barcode.Compress(Bitmap.CompressFormat.Png, 0, str);
                bitmap = str.ToArray();
            }
            catch (Exception ex)
            {

            }
            return bitmap;
        }
    }

}