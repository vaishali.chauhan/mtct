﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MTCT.Droid.InterfaceService;
using MTCT.Interface;
using MTCT.Models;
using Java.Util;
using Xamarin.Forms;
using Zebra.Sdk.Printer.Discovery;

[assembly: Dependency(typeof(BluetoothDroid))]

namespace MTCT.Droid.InterfaceService
{
    public class BluetoothDroid : IBluetooth
    {

        public bool IsBluetoothEnabled()
        {
            using (BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter)
            {
                if (bluetoothAdapter != null)
                {
                    return bluetoothAdapter.IsEnabled;
                }
                else
                {
                    return false;
                }
            }
        }

        public async Task<List<DiscoveredPrinter>> GetPrinters()
        {
            BluetoothDiscoveryHandler discoveryHandler = new BluetoothDiscoveryHandler();

            await Task.Run(() =>
            {

                BluetoothDiscoverer.FindPrinters(Android.App.Application.Context, discoveryHandler);

                while (!discoveryHandler.DiscoveryComplete)
                {
                    Thread.Sleep(10);
                }

            });

            return discoveryHandler.DiscoveredPrinters;
        }

        private class BluetoothDiscoveryHandler : DiscoveryHandler
        {

            public void DiscoveryError(string message)
            {
                Console.WriteLine($"An error occurred during discovery: {message}.");
                DiscoveryComplete = true;
            }

            public void DiscoveryFinished()
            {
                DiscoveryComplete = true;
            }

            public void FoundPrinter(DiscoveredPrinter printer)
            {
                DiscoveredPrinters.Add(printer);
            }

            public bool DiscoveryComplete { get; private set; } = false;

            public List<DiscoveredPrinter> DiscoveredPrinters { get; } = new List<DiscoveredPrinter>();
        }

        private BluetoothSocket bluetoothSocket { get; set; }

        public List<BLDevice> GetBluetoothDevices()
        {
            using (BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter)
            {
                var btdevice = bluetoothAdapter?.BondedDevices.Select(i => new BLDevice { Name = i.Name, Address = i.Address }).ToList();
                return btdevice;
            }
        }


        public void ConnectBluetooth(string deviceName, string deviceAddress)
        {
            using (BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter)
            {
                BluetoothDevice device = (from bd in bluetoothAdapter?.BondedDevices
                                          where (bd?.Name == deviceName && bd?.Address == deviceAddress)
                                          select bd).FirstOrDefault();

                bluetoothSocket = device?.CreateRfcommSocketToServiceRecord(UUID.FromString("00001101-0000-1000-8000-00805f9b34fb"));
                bluetoothSocket?.Connect();
            }
        }

        public async Task<bool> Print(byte[] printContent)
        {
            try
            {
                await Task.Run(() =>
                {
                    bluetoothSocket?.OutputStream.Write(printContent, 0, printContent.Length);
                });

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public void DisconnectBluetooth()
        {
            bluetoothSocket?.OutputStream.Close();
            bluetoothSocket?.OutputStream.Dispose();
            bluetoothSocket.Close();
            bluetoothSocket.Dispose();
        }


        public ZPLImage GetZPLImageCommand(byte[] imageBytes)
        {
            Bitmap bitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
            return Utils.getZplCode(bitmap, true);
        }
    }

    public class Utils
    {
        public static ZPLImage getZplCode(Bitmap bitmap, bool addHeaderFooter)
        {
            ZPLConverter zp = new ZPLConverter();
            zp.compressHex = true;
            zp.blackLimit = 50;
            Bitmap grayBitmap = toGrayScale(bitmap);
            return zp.convertFromImage(grayBitmap, addHeaderFooter);
        }

        public static Bitmap toGrayScale(Bitmap bmpOriginal)
        {
            int width, height;
            height = bmpOriginal.Height;
            width = bmpOriginal.Width;

            Bitmap grayScale = Bitmap.CreateBitmap(width, height, Bitmap.Config.Argb8888);
            Canvas c = new Canvas(grayScale);
            Paint paint = new Paint();
            ColorMatrix cm = new ColorMatrix();
            cm.SetSaturation(0);
            ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
            paint.SetColorFilter(f);
            c.DrawBitmap(bmpOriginal, 0, 0, paint);

            int width2 = grayScale.Width;
            int height2 = grayScale.Height;

            int[] pixels = new int[width2 * height2];
            grayScale.GetPixels(pixels, 0, width2, 0, 0, width2, height2);

            // Iterate over height
            for (int y = 0; y < height2; y++)
            {
                int offset = y * height2;
                // Iterate over width
                for (int x = 0; x < width2; x++)
                {
                    int pixel = grayScale.GetPixel(x, y);
                    int lowestBit = pixel & 0xff;
                    if (lowestBit < 128)
                        grayScale.SetPixel(x, y, Android.Graphics.Color.Black);
                    else
                        grayScale.SetPixel(x, y, Android.Graphics.Color.White);
                }
            }

            return grayScale;
        }
    }

    public class ZPLConverter
    {
        public int blackLimit = 380;
        private int total;
        private int widthBytes;
        public bool compressHex = false;
        private Dictionary<int, string> mapCode = new Dictionary<int, string>
        {
            {1, "G"},
            {2, "H"},
            {3, "I"},
            {4, "J"},
            {5, "K"},
            {6, "L"},
            {7, "M"},
            {8, "N"},
            {9, "O"},
            {10, "P"},
            {11, "Q"},
            {12, "R"},
            {13, "S"},
            {14, "T"},
            {15, "U"},
            {16, "V"},
            {17, "W"},
            {18, "X"},
            {19, "Y"},
            {20, "g"},
            {40, "h"},
            {60, "i"},
            {80, "j"},
            {100, "k"},
            {120, "l"},
            {140, "m"},
            {160, "n"},
            {180, "o"},
            {200, "p"},
            {220, "q"},
            {240, "r"},
            {260, "s"},
            {280, "t"},
            {300, "u"},
            {320, "v"},
            {340, "w"},
            {360, "x"},
            {380, "y"},
            {400, "z"}
        };

        public ZPLImage convertFromImage(Bitmap image, bool addHeaderFooter)
        {
            string hexAscii = createBody(image);
            if (compressHex)
            {
                hexAscii = encodeHexAscii(hexAscii);
            }

            string zplCode = "^GFA," + total + "," + total + "," + widthBytes + ", " + hexAscii;

            if (addHeaderFooter)
            {
                string header = "^XA " + "^FO0,0^GFA," + total + "," + total + "," + widthBytes + ", ";
                string footer = "^FS" + "^XZ";
                zplCode = header + zplCode + footer;
            }


            return new ZPLImage { ByteCount = total, BytesPerRow = widthBytes, Data = hexAscii };
        }

        private string createBody(Bitmap bitmapImage)
        {
            StringBuilder sb = new StringBuilder();
            int height = bitmapImage.Height;
            int width = bitmapImage.Width;
            int rgb, red, green, blue, index = 0;
            char[] auxBinaryChar = { '0', '0', '0', '0', '0', '0', '0', '0' };
            widthBytes = width / 8;
            if (width % 8 > 0)
            {
                widthBytes = (((int)(width / 8)) + 1);
            }
            else
            {
                widthBytes = width / 8;
            }
            this.total = widthBytes * height;
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    rgb = bitmapImage.GetPixel(w, h);
                    red = (rgb >> 16) & 0x000000FF;
                    green = (rgb >> 8) & 0x000000FF;
                    blue = (rgb) & 0x000000FF;
                    char auxChar = '1';
                    int totalColor = red + green + blue;
                    if (totalColor > blackLimit)
                    {
                        auxChar = '0';
                    }
                    auxBinaryChar[index] = auxChar;
                    index++;
                    if (index == 8 || w == (width - 1))
                    {
                        sb.Append(fourByteBinary(new String(auxBinaryChar)));
                        auxBinaryChar = new char[] { '0', '0', '0', '0', '0', '0', '0', '0' };
                        index = 0;
                    }
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }

        private string fourByteBinary(string binaryStr)
        {
            int dec = Java.Lang.Integer.ParseInt(binaryStr, 2);
            if (dec > 15)
            {
                return Java.Lang.Integer.ToString(dec, 16).ToUpper();
            }
            else
            {
                return "0" + Java.Lang.Integer.ToString(dec, 16).ToUpper();
            }
        }

        private string encodeHexAscii(string code)
        {
            int maxlinea = widthBytes * 2;
            StringBuilder sbCode = new StringBuilder();
            StringBuilder sbLinea = new StringBuilder();
            String previousLine = null;
            int counter = 1;
            char aux = code[0];
            bool firstChar = false;
            for (int i = 1; i < code.Length; i++)
            {
                if (firstChar)
                {
                    aux = code[i];
                    firstChar = false;
                    continue;
                }
                if (code[i] == '\n')
                {
                    if (counter >= maxlinea && aux == '0')
                    {
                        sbLinea.Append(",");
                    }
                    else if (counter >= maxlinea && aux == 'F')
                    {
                        sbLinea.Append("!");
                    }
                    else if (counter > 20)
                    {
                        int multi20 = (counter / 20) * 20;
                        int resto20 = (counter % 20);
                        sbLinea.Append(mapCode[multi20]);
                        if (resto20 != 0)
                        {
                            sbLinea.Append(mapCode[resto20]).Append(aux);
                        }
                        else
                        {
                            sbLinea.Append(aux);
                        }
                    }
                    else
                    {
                        sbLinea.Append(mapCode[counter]).Append(aux);
                    }
                    counter = 1;
                    firstChar = true;
                    if (sbLinea.ToString().Equals(previousLine))
                    {
                        sbCode.Append(":");
                    }
                    else
                    {
                        sbCode.Append(sbLinea.ToString());
                    }
                    previousLine = sbLinea.ToString();
                    sbLinea.Length = 0;
                    continue;
                }
                if (aux == code[i])
                {
                    counter++;
                }
                else
                {
                    if (counter > 20)
                    {
                        int multi20 = (counter / 20) * 20;
                        int resto20 = (counter % 20);
                        sbLinea.Append(mapCode[multi20]);
                        if (resto20 != 0)
                        {
                            sbLinea.Append(mapCode[resto20]).Append(aux);
                        }
                        else
                        {
                            sbLinea.Append(aux);
                        }
                    }
                    else
                    {
                        sbLinea.Append(mapCode[counter]).Append(aux);
                    }
                    counter = 1;
                    aux = code[i];
                }
            }
            return sbCode.ToString();
        }
    }
}