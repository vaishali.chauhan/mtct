﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MTCT.Droid.InterfaceService;
using MTCT.Interface;
using Xamarin.Forms;

[assembly: Dependency(typeof(CloseApponBackButton))]
namespace MTCT.Droid.InterfaceService
{
    public class CloseApponBackButton : ICloseAppOnBackButton
    {
        public void CloseApp()
        {
            try
            {
                var activity = (Activity)Forms.Context;
                activity.FinishAffinity();
            }
            catch (System.Exception ex)
            {
                //Utility.DisplayInfo.ErrorMessage(ex.Message);
            }
        }
    }
}