﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MTCT.Droid.InterfaceService;
using MTCT.Interface;
using Android.App;
using Android.Content;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(LocationZ))]
namespace MTCT.Droid.InterfaceService
{
    public class LocationZ : ILocSettings
    {
        public void OpenSettings()
        {
            LocationManager LM = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);


            if (LM.IsProviderEnabled(LocationManager.GpsProvider) == false)
            {


                Context ctx = Forms.Context;
                ctx.StartActivity(new Intent(Android.Provider.Settings.ActionLocationSourceSettings));


            }
            else
            {
                //this is handled in the PCL
            }
        }


    }
}