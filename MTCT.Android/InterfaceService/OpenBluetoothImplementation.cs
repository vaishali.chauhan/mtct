﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MTCT.Droid.InterfaceService;
using MTCT.Interface;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;

[assembly: Dependency(typeof(OpenBluetoothImplementation))]
namespace MTCT.Droid.InterfaceService
{
    public class OpenBluetoothImplementation : IOpenBluetoothPage
    {
        public void OpenBluetoothSettingPage()
        {
            Intent intentOpenBluetoothSettings = new Intent();
            intentOpenBluetoothSettings.SetAction(Android.Provider.Settings.ActionBluetoothSettings);
            Context ctx = Forms.Context;
            ctx.StartActivity(intentOpenBluetoothSettings);
        }
    }
}