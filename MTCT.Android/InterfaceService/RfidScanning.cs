﻿using Com.Sdgsystems.Idengine.Api;
using Com.Sdgsystems.Idengine.Rfid.Api;
using MTCT.Droid.InterfaceService;
using MTCT.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

[assembly: Dependency(typeof(RfidScanning))]

namespace MTCT.Droid.InterfaceService
{
    public class RfidScanning : IRfidScanning
    {
        DeviceManager deviceManager;
        Com.Sdgsystems.Idengine.Api.Device device;
        RfidReader rfidReader;
        App my_application = new App();

        public RfidScanning()
        {
            deviceManager = new DeviceManager(Android.App.Application.Context);
        }

        public bool ConnectBarcodeDevice(string devicename)
        {
            device = deviceManager.OpenDevice(deviceManager.AvailableDevices.FirstOrDefault(a => a.Name == devicename));
            return deviceManager.IsConnected;
        }

        public IList<string> GetDevices()
        {
            List<string> lstData = new List<string>();

            var data = deviceManager.AvailableDevices.Where(x => x.DeviceType.Contains("RFID")).Select(i => i.Name).ToList();
            return data;
        }

        public void ScanDevice(string devicename)
        {
            try
            {
                var deviceInfo = deviceManager.AvailableDevices.FirstOrDefault(a => a.Name == devicename);
                if (deviceInfo != null)
                {
                    connectToDevice(deviceInfo);
                    rfidReader.StartScan();
                }
            }
            catch (Exception ex)
            {
                //Toast.MakeText(Android.App.Application.Context, ex.Message, ToastLength.Long).Show();
            }
        }

        protected void connectToDevice(IDeviceInfo si)
        {
            try
            {
                if (rfidReader != null) rfidReader.Close();
                rfidReader = deviceManager.OpenRfidReader(si);
                rfidReader.SetCallback(new CustomCallBack());
            }
            catch (DeviceException e)
            {
                //Toast.MakeText(Android.App.Application.Context, "Could not connect to scanner " + si, ToastLength.Long).Show();
                //Toast.MakeText(Android.App.Application.Context, "Could not connect to " + si, ToastLength.Long).Show();
            }
        }
    }

    public class CustomCallBack : DeviceCallback
    {
        App my_application = new App();
        LogUtils logUtils = new LogUtils();
        public CustomCallBack()
        {
            logUtils.Log("In CustomCallBack Constructor");
        }

        public override void OnDeviceResponse(IDeviceResponse response)
        {
            base.OnDeviceResponse(response);
            try
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    logUtils.Log("OnDeviceResponse Start");
                    if (response != null)
                    {
                        logUtils.Log("response found");
                        if (response is IDeviceDataResponse)
                        {
                            IDeviceDataResponse data = (IDeviceDataResponse)response;

                            string dataString = data.DataString;
                            logUtils.Log("data.DataString - " + data.DataString);
                            String subType = data.DataSubType;
                            logUtils.Log("data.DataSubType - " + data.DataSubType);
                            String text = "Data: <b>" + dataString
                                    + "</b><br>Type: <b>"
                                    + subType + "</b><br>";
                            logUtils.Log("text - " + text);
                            MessagingCenter.Send<App, string>(my_application, "ScanBarcode", dataString);
                        }
                        else if (response is IScanFailedResponse)
                        {
                            logUtils.Log("Scan failed");
                        }
                        else if (response is IScanStoppedResponse)
                        {
                            logUtils.Log("Scan stopped");
                        }
                    }
                    else
                    {
                        logUtils.Log("response not found");
                    }
                    logUtils.Log("OnDeviceResponse End");
                });
            }
            catch (Exception ex)
            {
                logUtils.Log("Exception - " + ex.StackTrace);
            }
        }
    }
}