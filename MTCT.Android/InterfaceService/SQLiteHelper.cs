﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MTCT.Droid.InterfaceService;
using MTCT.Interface;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteHelper))]
namespace MTCT.Droid.InterfaceService
{

    public class SQLiteHelper : ISQLiteConnection
    {
        public string sqliteFilename = "MTCT.sqlite";
        public SQLiteConnection GetConnection()
        {
            try
            {
                string databasePath = string.Empty;
                var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                if (!Directory.Exists(documentsPath))
                {
                    Directory.CreateDirectory(documentsPath);
                }
                databasePath = Path.Combine(documentsPath, "MTCT.sqlite");
                CopyDatabaseIfNotExists(databasePath);
                var connection = new SQLiteConnection(databasePath);
                return connection;
            }
            catch (Exception ex)
            {
                var exception = ex.Message;
                return null;
            }
        }

        private static void CopyDatabaseIfNotExists(string dbPath)
        {
            try
            {
                if (!File.Exists(dbPath))
                {
                    using (var br = new BinaryReader(Android.App.Application.Context.Assets.Open("MTCT.sqlite")))
                    {
                        using (var bw = new BinaryWriter(new FileStream(dbPath, FileMode.Create)))
                        {
                            byte[] buffer = new byte[2048];
                            int length = 0;
                            while ((length = br.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                bw.Write(buffer, 0, length);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
        }
    }
}