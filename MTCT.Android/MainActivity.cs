﻿
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Java.Security;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using Xamarin;
using Xamarin.Forms.Platform.Android;

namespace MTCT.Droid
{
    [Activity(Label = "MTCT", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            FormsMaps.Init(this, savedInstanceState);
            Popup.Init(this, savedInstanceState);
            ZXing.Net.Mobile.Forms.Android.Platform.Init();
            Acr.UserDialogs.UserDialogs.Init(this);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            //Intent intent = new Intent();
            //intent.SetFlags(ActivityFlags.NewTask);
            //Window.SetStatusBarColor(ColorExtensions.ToAndroid(Xamarin.Forms.Color.FromHex("#004311")));
            CameraPermission();
            GetPermission();
            LoadApplication(new App());
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public async override void OnBackPressed()
        {
            if (Rg.Plugins.Popup.Popup.SendBackPressed(base.OnBackPressed))
            {
                await PopupNavigation.PopAsync(false);
            }
            else
            {
                //PopupNavigation.PopAllAsync();
            }
        }

        public async Task CameraPermission()
        {
            try
            {
                string version = Android.OS.Build.VERSION.Release;                         //Android Version No like 4.4.4 etc... 
                var mver = string.Format("{0:0.00}", version.Substring(0, 3));
                double ver = Convert.ToDouble(mver);
                if (ver >= 5.0)
                {
                    var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Camera);
                    if (status != PermissionStatus.Granted)
                    {
                        var results = await CrossPermissions.Current.RequestPermissionsAsync(Plugin.Permissions.Abstractions.Permission.Camera);
                        //Best practice to always check that the key exists
                        if (results.ContainsKey(Plugin.Permissions.Abstractions.Permission.Camera))
                        {
                            status = results[Plugin.Permissions.Abstractions.Permission.Camera];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        //public void BluetoothPermission()
        //{
        //    Intent intent = new Intent();
        //    intent.AddFlags(ActivityFlags.NewTask);
        //    intent.SetData(Android.Net.Uri.Parse(baseUrl + @"name_of_apk"));
        //    Android.App.Application.Context.StartActivity(intent);
        //}

        void GetPermission()
        {
            //var name = Android.OS.Build.VERSION.SdkInt;     //Android Version Name like Kitkate etc... 
            //string version = Android.OS.Build.VERSION.Release;    //Android Version No like 4.4.4 etc... 
            //double ver = ConvertStringToDouble(version);

            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                const string AccessFineLocationpermission = Manifest.Permission.AccessFineLocation;
                const string AccessCoarseLocationpermission = Manifest.Permission.AccessCoarseLocation;
                const string AccessLocationExtraCommandspermission = Manifest.Permission.AccessLocationExtraCommands;
                const string AccessMockLocationpermission = Manifest.Permission.AccessMockLocation;
                const string AccessNetworkStatepermission = Manifest.Permission.AccessNetworkState;
                const string ChangeWifiStatepermission = Manifest.Permission.ChangeWifiState;
                const string Internetpermission = Manifest.Permission.Internet;
                const string Camerapermission = Manifest.Permission.Camera;
                const string ReadExternalStoragepermission = Manifest.Permission.ReadExternalStorage;
                const string WriteExternalStoragepermission = Manifest.Permission.WriteExternalStorage;
                const string Bluetooth = Manifest.Permission.Bluetooth;
                const string BluetoothAdmin = Manifest.Permission.BluetoothAdmin;
                const string BluetoothPrivileged = Manifest.Permission.BluetoothPrivileged;

                if (CheckSelfPermission(AccessFineLocationpermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(AccessCoarseLocationpermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(AccessLocationExtraCommandspermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(AccessMockLocationpermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(AccessNetworkStatepermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(ChangeWifiStatepermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(Internetpermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(Camerapermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(ReadExternalStoragepermission) != (int)Android.Content.PM.Permission.Granted
                   || CheckSelfPermission(WriteExternalStoragepermission) != (int)Android.Content.PM.Permission.Granted)
                {
                    RequestPermissions(new string[]  {
                        Manifest.Permission.AccessFineLocation,
                        Manifest.Permission.AccessCoarseLocation,
                        Manifest.Permission.AccessLocationExtraCommands,
                        Manifest.Permission.AccessMockLocation,
                        Manifest.Permission.AccessNetworkState,
                        Manifest.Permission.ChangeWifiState,
                        Manifest.Permission.Internet,
                        Manifest.Permission.Camera,
                        Manifest.Permission.ReadExternalStorage,
                        Manifest.Permission.WriteExternalStorage,
                },
                101);
                }

                if (CheckSelfPermission(BluetoothAdmin) != (int)Android.Content.PM.Permission.Granted
                                    || CheckSelfPermission(Bluetooth) != (int)Android.Content.PM.Permission.Granted
                                    || CheckSelfPermission(BluetoothPrivileged) != (int)Android.Content.PM.Permission.Granted)
                {
                    RequestPermissions(new string[]
                    {
                                              Manifest.Permission.Bluetooth,
                                              Manifest.Permission.BluetoothAdmin,
                                              Manifest.Permission.BluetoothPrivileged,
                    }, 1);
                }
            }
        }
        public static double ConvertStringToDouble(string inputString)
        {
            if (inputString.Split('.').Length > 1)
                inputString = inputString.Substring(0, inputString.IndexOf('.', inputString.IndexOf('.') + 1));
            return double.Parse(inputString);
        }
    }
}