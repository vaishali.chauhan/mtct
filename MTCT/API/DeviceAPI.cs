﻿using MTCT.Models.APIModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class DeviceAPI
    {
        public async Task<EqTpSoResponseModel> SaveDevices(List<Devices> deviceListner)
        {
            EqTpSoResponseModel deviceModel = new EqTpSoResponseModel();
            try
            {
                using (HttpClientFactory httpClientFactory = new HttpClientFactory(token: Settings.Token))
                {
                    var requestJson = JsonConvert.SerializeObject(deviceListner);
                    var response = await httpClientFactory.PostAsync(EndPointsUrl.POST_DEVICES, requestJson);
                    var json = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == System.Net.HttpStatusCode.OK ||
                            response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        deviceModel = JsonConvert.DeserializeObject<EqTpSoResponseModel>(json);
                        deviceModel.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            SaveDevices(deviceListner);
                        }
                    }
                    else
                    {
                        deviceModel.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("SaveDevices/DeviceAPI Exception:" + ex.Message);
            }
            return deviceModel;
        }
    }
}
