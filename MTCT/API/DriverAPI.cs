﻿using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class DriverAPI
    {
        public async Task<DriverModelDetails> GetAllDrivers()
        {
            DriverModelDetails driverListner = new DriverModelDetails();
            try
            {
                using (var hcf = new HttpClientFactory(token: Settings.Token))
                {
                    var response = await hcf.client.GetAsync(EndPointsUrl.GET_DRIVERS);
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        driverListner.mDriverModels = JsonConvert.DeserializeObject<List<DriverModel>>(responseJson);
                        driverListner.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            GetAllDrivers();
                        }
                    }
                    else
                    {
                        driverListner.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("DriverAPI/GetAllDrivers Exception:" + ex.Message);
            }
            return driverListner;
        }

        public async Task<DriverModelDetails> GetDriverByAppRefCode(string driverAppRefCode)
        {
            DriverModelDetails driver = new DriverModelDetails();
            try
            {
                using (var hcf = new HttpClientFactory(token: Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_DRIVER_APP_REF_CODE, driverAppRefCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        driver.mDriverModels = JsonConvert.DeserializeObject<List<DriverModel>>(responseJson);
                        driver.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            GetDriverByAppRefCode(driverAppRefCode);
                        }
                    }
                    else
                    {
                        driver.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("DriverAPI/GetDriverByAppRefCode Exception:" + ex.Message);
            }
            return driver;
        }


       
    }
}
