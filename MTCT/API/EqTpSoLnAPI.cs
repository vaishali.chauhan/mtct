﻿using MTCT.Models.APIModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class EqTpSoLnAPI
    {
        public async Task<EqTpSoResponseModel> SaveEqTpSoLn(List<EqTpSoLnModel> eqTpSoLnModel)
        {
            EqTpSoResponseModel EqTpSoModel = new EqTpSoResponseModel();
            try
            {
                using (HttpClientFactory httpClientFactory = new HttpClientFactory(token:Settings.Token))
                {
                    var requestJson = JsonConvert.SerializeObject(eqTpSoLnModel);
                    var response = await httpClientFactory.PostAsync(EndPointsUrl.POST_MTCTSOLN, requestJson);
                    var json = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == System.Net.HttpStatusCode.OK ||
                            response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        EqTpSoModel = JsonConvert.DeserializeObject<EqTpSoResponseModel>(json);
                        EqTpSoModel.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            SaveEqTpSoLn(eqTpSoLnModel);
                        }
                    }
                    else
                    {
                        EqTpSoModel.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("SaveEqTpSoLn/EqTpSoLnAPI Exception:" + ex.Message);
            }
            return EqTpSoModel;
        }
    }
}
