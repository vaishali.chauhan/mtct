﻿using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class EqptAPI
    {
        public async Task<EqtpResponse> GetEqtpByEqtpCode(string eqtpCode)
        {
            EqtpResponse mEqtpResponse = new EqtpResponse();
            try
            {
                using (var hcf = new HttpClientFactory(token: Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_MTCT_BY_MTCTCODE, eqtpCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        mEqtpResponse = JsonConvert.DeserializeObject<EqtpResponse>(responseJson);
                        mEqtpResponse.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            await GetEqtpByEqtpCode(eqtpCode);
                        }
                    }
                    else
                    {
                        mEqtpResponse.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("EqptAPI/GetEqtpByEqtpCode Exception:" + ex.Message);
            }
            return mEqtpResponse;
        }

        public async Task<DeviceListner> GetAllDevices()
        {
            DeviceListner deviceListner = new DeviceListner();
            try
            {
                using (var hcf = new HttpClientFactory(token:Settings.Token))
                {
                    var response = await hcf.client.GetAsync(EndPointsUrl.GET_DEVICES);
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        deviceListner.mDevices = JsonConvert.DeserializeObject<List<Devices>>(responseJson);
                        deviceListner.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            await GetAllDevices();
                        }
                    }
                    else
                    {
                        deviceListner.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("FlstAPI/GetAllDevices Exception:" + ex.Message);
            }
            return deviceListner;
        }

        public async Task<AppDevice> GetDeviceByDeviceCode(string deviceCode)
        {
            AppDevice mDevices = new AppDevice();
            try
            {
                using (var hcf = new HttpClientFactory(token:Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_DEVICE_BY_DEVICECODE, deviceCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        mDevices = JsonConvert.DeserializeObject<AppDevice>(responseJson);
                        mDevices.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            GetDeviceByDeviceCode(deviceCode);
                        }
                    }
                    else
                    {
                        mDevices.IsSuccess = false;
                       // DisplayInfo.ErrorMessage("Device Code is Wrong.");
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("FlstAPI/GetDeviceByDeviceCode Exception:" + ex.Message);
            }
            return mDevices;
        }

        public async Task<FLSTSResponse> GetFlstByFlstCode(string flstCode)
        {
            FLSTSResponse flst = new FLSTSResponse();
            try
            {
                using (var hcf = new HttpClientFactory(token:Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_FLST_BY_FLSTCODE, flstCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        flst = JsonConvert.DeserializeObject<FLSTSResponse>(responseJson);
                        flst.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            GetFlstByFlstCode(flstCode);
                        }
                    }
                    else
                    {
                        flst.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("FlstAPI/GetFlstByFlstCode Exception:" + ex.Message);
            }
            return flst;
        }
    }
}
