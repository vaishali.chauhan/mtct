﻿using MTCT.Models.APIModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace MTCT.API
{
    public class EqtpHaulerAPI
    {
        public async Task<EqtpHaulerDetail> GetEqtpHaulersByCode(string eqtpHaulerCode)
        {
            EqtpHaulerDetail haulerDetail = new EqtpHaulerDetail();
            try
            {
                using (var hcf = new HttpClientFactory(token: Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_Hauler_By_Id, eqtpHaulerCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        haulerDetail = JsonConvert.DeserializeObject<EqtpHaulerDetail>(responseJson);
                        haulerDetail.IsSuccess = true;
                    }
                    else
                    {
                        haulerDetail.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("EqtpHaulerAPI/GetEqtpHaulersByCode Exception:" + ex.Message);
            }
            return haulerDetail;
        }

        public async Task<List<EqtpHaulers>> GetAllEqtpHaulers(string appRefCode)
        {
            List<EqtpHaulers> haulerDetail = new List<EqtpHaulers>();
            try
            {
                using (var hcf = new HttpClientFactory(token: Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_Hauler_By_RefCode, appRefCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        haulerDetail = JsonConvert.DeserializeObject<List<EqtpHaulers>>(responseJson);
                        //haulerDetail.IsSuccess = true;
                    }
                    else
                    {
                        //haulerDetail.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("EqtpHaulerAPI/GetAllEqtpHaulers Exception:" + ex.Message);
            }
            return haulerDetail;
        }
    }
}
