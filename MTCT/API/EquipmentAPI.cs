﻿using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class EquipmentAPI
    {
        public async Task<EquipmentModelDetail> GetAllEquipment()
        {
            EquipmentModelDetail equipmentListner = new EquipmentModelDetail();
            try
            {
                using (var hcf = new HttpClientFactory(token:Settings.Token))
                {
                    var response = await hcf.client.GetAsync(EndPointsUrl.GET_EQUIPMENTS);
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        equipmentListner.mEquipmentModels = JsonConvert.DeserializeObject<List<EquipmentModel>>(responseJson);
                        equipmentListner.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            GetAllEquipment();
                        }
                    }
                    else
                    {
                        equipmentListner.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("EquipmentAPI/GetAllEquipment Exception:" + ex.Message);
            }
            return equipmentListner;
        }

        public async Task<EquipmentModelDetail> GetEquipmentByRefCode(string driverAppRefCode)
        {
            EquipmentModelDetail equipment = new EquipmentModelDetail();
            try
            {
                using (var hcf = new HttpClientFactory(token:Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_EQUIPMENT_APP_REF_CODE, driverAppRefCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        equipment.mEquipmentModels = JsonConvert.DeserializeObject<List<EquipmentModel>>(responseJson);
                        equipment.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            GetEquipmentByRefCode(driverAppRefCode);
                        }
                    }
                    else
                    {
                        equipment.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("EquipmentAPI/GetEquipmentByRefCode Exception:" + ex.Message);
            }
            return equipment;
        }
    }
}
