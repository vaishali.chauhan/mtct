﻿using MTCT.Models.APIModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class FlstShiftAPI
    {
        public async Task<EqTpSoResponseModel> SaveOpenOrder(List<EqTpSoModel> eqtpsoModel)
        {
            EqTpSoResponseModel eqtpsoResponseModel = new EqTpSoResponseModel();
            try
            {
                using (HttpClientFactory httpClientFactory = new HttpClientFactory(token: Settings.Token))
                {
                    var requestJson = JsonConvert.SerializeObject(eqtpsoModel);
                    // requestJson = requestJson.Replace(".0", "");

                    var response = await httpClientFactory.PostAsync(EndPointsUrl.POST_FLSTSHIFT, requestJson);
                    var json = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == System.Net.HttpStatusCode.OK ||
                            response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        eqtpsoResponseModel = JsonConvert.DeserializeObject<EqTpSoResponseModel>(json);
                        eqtpsoResponseModel.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            SaveOpenOrder(eqtpsoModel);
                        }
                    }
                    else
                    {
                        eqtpsoResponseModel.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("SaveOpenOrder/FlstShiftAPI Exception:" + ex.Message);
            }
            return eqtpsoResponseModel;
        }

        public async Task<EqTpSoResponseModel> SaveCloseShift(OrderCloseModelDetail flstShiftModel)
        {
            EqTpSoResponseModel shiftModel = new EqTpSoResponseModel();
            try
            {
                using (HttpClientFactory httpClientFactory = new HttpClientFactory(token: Settings.Token))
                {
                    foreach (var flstShift in flstShiftModel.eqTpSo)
                    {
                        flstShift.eqtpsoOpeningOdometerImagePath = flstShift.eqtpsoOpeningOdometerImage;
                        flstShift.eqtpsoOpeningSignaturePath = flstShift.eqtpsoOpeningSignature;

                        flstShift.eqtpsoClosingOdometerImagePath = flstShift.eqtpsoClosingOdometerImage;
                        flstShift.eqtpsoClosingSignaturePath = flstShift.eqtpsoClosingSignature;

                        flstShift.eqtpsoInterfaceDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    }

                    var requestJson = JsonConvert.SerializeObject(flstShiftModel);
                    // requestJson = requestJson.Replace(".0", "");

                    var response = await httpClientFactory.PostAsync(EndPointsUrl.POST_FLSTCLOSESHIFT, requestJson);
                    var json = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == System.Net.HttpStatusCode.OK ||
                            response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        shiftModel = JsonConvert.DeserializeObject<EqTpSoResponseModel>(json);
                        shiftModel.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            await SaveCloseShift(flstShiftModel);
                        }
                    }
                    else
                    {
                        shiftModel.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("SaveCloseShift/FlstShiftAPI Exception:" + ex.Message);
            }
            return shiftModel;
        }

    }
}
