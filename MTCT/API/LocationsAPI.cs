﻿using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class LocationsAPI
    {
        public async Task<LocationDetails> GetEqtLocations(string eqTpLocationAppRefCode)
        {
            LocationDetails locationsDetails = new LocationDetails();
            try
            {
                using (var hcf = new HttpClientFactory(token: Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_Location, eqTpLocationAppRefCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        locationsDetails.Locations = JsonConvert.DeserializeObject<List<Locations>>(responseJson);
                        locationsDetails.IsSuccess = true;
                    }
                    else
                    {
                        locationsDetails.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("EqtpHaulerAPI/GetEqtpHaulers Exception:" + ex.Message);
            }
            return locationsDetails;
        }
    }
}
