﻿using MTCT.Models.APIModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class LoginAPI
    {
        public async Task<UserResponse> Authentication(LoginRequiest loginRequest)
        {
            UserResponse mUserResponse = new UserResponse();
            try
            {
                using (var hcf = new HttpClientFactory())
                {
                    string requestJson = JsonConvert.SerializeObject(loginRequest);
                    var response = await hcf.PostAsync(EndPointsUrl.USER_LOGIN, requestJson);
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        mUserResponse = JsonConvert.DeserializeObject<UserResponse>(responseJson);
                        if (!string.IsNullOrEmpty(responseJson))
                        {
                            mUserResponse.IsSuccess = true;
                            mUserResponse.successMessage = EndPointsMessage.LoginSuccess;
                        }
                        else
                        {
                            mUserResponse.IsSuccess = false;
                            mUserResponse.errorMessage = EndPointsMessage.LoginError;
                        }
                    }
                    else
                    {
                        mUserResponse.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("LoginAPI/Authentication Exception:" + ex.Message);
            }
            return mUserResponse;
        }
    }
}
