﻿using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class ProjectAPI
    {
        public async Task<ProjectDetails> GetProjects(string eqtpHaulerCode)
        {
            ProjectDetails projectDetails = new ProjectDetails();
            try
            {
                using (var hcf = new HttpClientFactory(token: Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_Projects, eqtpHaulerCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        projectDetails.ProjectDetail = JsonConvert.DeserializeObject<List<ProjectDetail>>(responseJson);
                        projectDetails.IsSuccess = true;
                    }
                    else
                    {
                        projectDetails.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("EqtpHaulerAPI/GetEqtpHaulers Exception:" + ex.Message);
            }
            return projectDetails;
        }
    }
}
