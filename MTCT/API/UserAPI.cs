﻿using MTCT.Models.APIModels;
using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.API
{
    public class UserAPI
    {
        public async Task<UserModelDetail> GetAllUsers()
        {
            UserModelDetail userModelDetail = new UserModelDetail();
            try
            {
                using (var hcf = new HttpClientFactory(token:Settings.Token))
                {
                    var response = await hcf.client.GetAsync(EndPointsUrl.GET_USERS);
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        userModelDetail.mUserModels = JsonConvert.DeserializeObject<List<UserModel>>(responseJson);
                        userModelDetail.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            GetAllUsers();
                        }
                    }
                    else
                    {
                        userModelDetail.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("UserAPI/GetAllUsers Exception:" + ex.Message);
            }
            return userModelDetail;
        }

        public async Task<UserModelDetail> GetuserByAppRefCode(string useAppRefCode)
        {
            UserModelDetail user = new UserModelDetail();
            try
            {
                using (var hcf = new HttpClientFactory(token:Settings.Token))
                {
                    var response = await hcf.client.GetAsync(string.Format(EndPointsUrl.GET_USERS_APP_REF_CODE, useAppRefCode));
                    var responseJson = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        user.mUserModels = JsonConvert.DeserializeObject<List<UserModel>>(responseJson);
                        user.IsSuccess = true;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        var isTokenUpdate = await Common.sessionExpired();
                        if (isTokenUpdate)
                        {
                            GetuserByAppRefCode(useAppRefCode);
                        }
                    }
                    else
                    {
                        user.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("UserAPI/GetuserByAppRefCode Exception:" + ex.Message);
            }
            return user;
        }
    }
}
