﻿using MTCT.Interface;
using MTCT.Services;
using MTCT.Utilties;
using MTCT.Views.MasterPages;
using SQLite;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace MTCT
{
    public partial class App : Application
    {
        public static SQLiteConnection dbConn;

        public App()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            App.dbConn = DependencyService.Get<ISQLiteConnection>().GetConnection();
            MainPage = new NavigationPage(new Views.SplashScreen.SplashScreenPage());
        }

        public static async Task<bool> CheckInternetConnection(Action method = null)
        {
            bool isConnect = false;
            try
            {
                if (!Common.HasInternetConnection())
                {
                    isConnect = false;

                    var isAgain = await App.Current.MainPage.DisplayAlert("No Internet Connection", "Please, check internet connection on your smartphone to use the application! ", "Try Again", "CANCELAR");
                    if (isAgain)
                    {
                        isConnect = await CheckInternetConnection(method);
                    }
                }
                else
                {
                    if (method != null)
                        method();

                    isConnect = true;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                // Utilities.Utility.DisplayErrorMessage("App.Xaml: CheckInternetConnection" + ex.Message);
            }
            return isConnect;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected async override void OnResume()
        {
            try
            {
                await new MasterMenuPage().GetDeviceList();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("App/OnResume: " + ex.Message);
            }
        }
    }
}
