using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("Roboto-Black.otf", Alias = "RobotoBlack")]
[assembly: ExportFont("Roboto-Bold.otf", Alias = "RobotoBold")]
[assembly: ExportFont("Roboto-Medium.otf", Alias = "RobotoMedium")]
[assembly: ExportFont("Roboto-Regular.otf", Alias = "RobotoRegular")]