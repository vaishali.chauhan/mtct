﻿namespace MTCT.DatabaseService
{
    public class DatabaseHelper
    {
        #region Flst
        public static string FuelStation = "Select * from Flst";
        public static string InsertFlst = "Insert into Flst(FlstId,FlstCode,FlstDesc,FlstDeviceCode,FlstShiftCounter,FlstReceiptsCounter,FlstIssuesCounter,FlstAdjustmentsCounter,FlstTransfersCounter,FlstIssuesPrintCopies,FlstShiftPrintCopies,PrintFormat) Values(?,?,?,?,?,?,?,?,?,?,?,?)";
        public static string UpdateFlst = "Update Flst set FlstDeviceCode=?,FlstShiftCounter=?,FlstReceiptsCounter=?,FlstIssuesCounter=?,FlstAdjustmentsCounter=?,FlstTransfersCounter=?,FlstIssuesPrintCopies=?,FlstShiftPrintCopies=?,PrintFormat=? where FlstId=?";
        public static string UpdateFlstDesc = "Update Flst set FlstDesc=? where FlstId =?";
        public static string FlstIsssueRefno = "Select FlstIssuesCounter from Flst Order By FlstIssuesCounter DESC LIMIT 1";
        public static string FlstShiftRefno = "Select FlstShiftCounter from Flst Order By FlstShiftCounter DESC LIMIT 1";
        public static string FlstIsssueCounter = "Update Flst set FlstIssuesCounter=? where FlstId =?";
        public static string FlstShiftCounter = "Update Flst set FlstShiftCounter=? where FlstId =?";
        public static string FlstShiftConfiguration = "Update Flst set FlstIssuesCounter=? where FlstId =?";
        public static string FlstPrintCount = "Update Flst set FlstIssuesPrintCopies=? where FlstId =?";
        public static string FlstCode = "Select * from Flst where FlstCode = ?";
        #endregion

        #region Eqtp
        public static string EqtpStation = "Select * from Eqtp";
        public static string InsertEqtp = "Insert into Eqtp(EqtpId,EqtpCode,EqtpName,EqtpRfidEnabled,EqtpActiveStatus,EqtpOrderCounterNo,EqtpTransportCounterNo,EqtpOrderPrint,EqtpTransportPrint,EqtpDeviceCode,PrintFormat,EqtpSignEnabled) Values(?,?,?,?,?,?,?,?,?,?,?,?)";
        public static string UpdateEqtp = "Update Eqtp set EqtpOrderCounterNo=?,EqtpTransportCounterNo=?,EqtpOrderPrint=?,EqtpTransportPrint=?,EqtpDeviceCode=?,PrintFormat=?, EqtpSignEnabled=? where EqtpId=?";
        public static string EqtpOrderCounter = "Update Eqtp set EqtpOrderCounterNo=? where EqtpId =?";
        public static string EqtpTransportCounter = "Update Eqtp set EqtpTransportCounterNo=? where EqtpId =?";
        public static string DeleteEqtp = "Delete from Eqtp where Id=?";
        #endregion

        #region AppConfig
        public static string InsertAppConfig = "Insert into AppConfig(AppConfigId,AppConfigApiUrl,AppConfigUserName,AppConfigPassword) Values(?,?,?,?)";
        public static string UpdateAppConfig = "Update AppConfig set AppConfigApiUrl=?,AppConfigUserName=?,AppConfigPassword=? where AppConfigId=?";
        public static string GetAppConfig = "Select * from AppConfig";
        #endregion

        #region User
        public static string User = "Select * from User";
        public static string Login = "Select * from User where UserCode=? and UserPassword=?";
        public static string DeleteUser = "Delete from User where Id=?";
        public static string InsertUser = "Insert into User(UserId,UserName,UserEmail,UserPassword,UserRole,UserCode,appuserAppRefCode,appuserActiveStatus) Values(?,?,?,?,?,?,?,?)";
        #endregion

        #region Driver
        public static string Driver = "Select * from Driver";
        public static string SearchDriver = "Select * from Driver where DriverName like '%{0}%' OR DriverCode like '%{0}%'";
        public static string DeleteDriver = "Delete from Driver where Id=?";
        public static string InsertDriver = "Insert into Driver(DriverId,DriverCode,DriverName,DriverActiveStatus,driverAppRefCode) Values(?,?,?,?,?)";
        #endregion

        #region Haulers
        public static string Hauler = "Select * from EqTpHaulers";
        public static string GetHaulerByEqtphaulerCode = "Select * from EqTpHaulers Where EqtphaulerCode=?";
        public static string SearchHauler = "Select * from EqTpHaulers where EqtphaulerCode like '%{0}%' OR EqtphaulerDesc like '%{0}%'";
        public static string DeleteHauler = "Delete from EqTpHaulers where Id=?";
        public static string InsertHauler = "Insert into EqTpHaulers (EqtphaulerId,EqtphaulerCode,EqtphaulerDesc,EqtphaulerAppRefCode,EqtphaulerActiveStatus) Values(?,?,?,?,?)";
        public static string UpdateHaulerDesc = "Update EqTpHaulers set EqtphaulerDesc=? where EqtphaulerId =?";
        public static string UpdateHauler = "Update EqTpHaulers set EqtphaulerId=?,EqtphaulerCode=?,EqtphaulerDesc=?,EqtphaulerAppRefCode=?,EqtphaulerActiveStatus=? where EqtphaulerId=?";
        #endregion

        #region Trailers
        public static string Trailer = "Select * from EqTpTrailers";
        public static string SearchTrailer = "Select * from EqTpTrailers where EqtptrailerCode like '%{0}%' OR EqtptrailerDesc like '%{0}%'";
        public static string DeleteTrailer = "Delete from EqTpTrailers where Id=?";
        public static string GerTrailerById = "Select * from EqTpTrailers where EqtphaulerId=?";
        public static string GerTrailerByEqtptrailerCode = "Select * from EqTpTrailers where EqtptrailerCode=?";
        public static string InsertTrailer = "Insert into EqTpTrailers(EqtptrailerId,EqtphaulerId,EqtptrailerCode,EqtptrailerDesc,EqtptrailerActiveStatus) Values(?,?,?,?,?)";
        #endregion

        #region Location
        public static string Location = "Select * from Locations";
        public static string SearchLocation = "Select * from Locations where EqtplocationCode like '%{0}%'";
        public static string DeleteLocation = "Delete from Locations where Id=?";
        public static string InsertLocation = "Insert into Locations(EqtplocationId,EqtplocationCode,EqtplocationAppRefCode,EqtplocationActiveStatus) Values(?,?,?,?)";
        #endregion

        #region Project
        public static string Project = "Select * from Project";
        public static string SearchProject = "Select * from Project where ProjectCode like '%{0}%'";
        public static string DeleteProject = "Delete from Project where Id=?";
        public static string InsertProject = "Insert into Project(ProjectId,ProjectCode,ProjectDesc,ProjectAppRefCode,ProjectActiveStatus) Values(?,?,?,?,?)";
        #endregion

        #region ProjectActivity
        public static string ProjectActivity = "Select * from ProjectActivity ";
        public static string ProjectActivityByProjectId = "Select * from ProjectActivity Where projectId = {0}";
        public static string SearchProjectActivity = "Select * from ProjectActivity where projactCode like '%{0}%'";
        public static string DeleteProjectActivity = "Delete from ProjectActivity where Id=?";
        public static string InsertProjectActivity = "Insert into ProjectActivity (projActId,projectId,projactCode,projactDesc,projactActiveStatus) Values(?,?,?,?,?)";
        #endregion

        #region Equipment
        public static string Equipment = "Select * from Equipment";
        //public static string SearchEquipment = "Select * from Equipment where EquipmentRFID like '%{0}%' And EquipmentCode like '%{0}%' And EquipmentDesc like '%{0}%'";
        public static string SearchEquipment = "Select * from Equipment where EquipmentRFID like '%{0}%' OR EquipmentCode like '%{0}%' OR EquipmentDesc like '%{0}%'";
        public static string DeleteEquipment = "Delete from Equipment where Id=?";
        public static string InsertEquipment = "Insert into Equipment(EquipmentId,EquipmentCode,EquipmentDesc,EquipmentLicensePlate,EquipmentRFID,EquipmentFltankCapacity,EquipmentActiveStatus,equipmentAppRefCode) Values(?,?,?,?,?,?,?,?)";
        #endregion

        #region FlstFuel
        public static string GetAllFlstFuel = "Select * from FlstFuel order by FlstfuelPumpCode asc";
        public static string InsertFlstFuel = "Insert into FlstFuel(FlstfuelId,FlstId,FlstfuelPumpCode,FlstfuelFuelCode,FlstfuelActiveStatus) Values(?,?,?,?,?)";
        //  public static string DeleteFlstFuel = "Delete From FlstFuel where FlstFuelId=?";
        public static string DeleteFlstFuel = "Delete From FlstFuel where FlstId=?";
        public static string DeleteFlstFuelById = "Delete From FlstFuel where Id=?";
        #endregion

        #region EqTpSo
        public static string GetShift = "Select * from EqTpSo";
        public static string StartShiftInsert = "Insert into EqTpSo(EqTpSoId,eqtpsoEqtpCode,eqtpsoDeviceCode,eqtpsoEqtphaulerCode,eqtpsoEqtptrailerCode,eqtpsoRefno,eqtpsoDriverCode,eqtpsoDriverName,eqtpsoUserCode,eqtpsoUserName,eqtpsoOpeningDate,eqtpsoOpeningLocation,eqtpsoOpeningOdometer,eqtpsoOpeningOdometerImage,eqtpsoOpeningLongitude,eqtpsoOpeningLatitude,eqtpsoOpeningNotes,eqtpsoOpeningSignature,eqtpsoStatus,eqtpsoInterfaceStatus,eqtpsoInterfaceDate) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        public static string StartUpdateOrder = "Update EqTpSo set eqtpsoClosingDate=?,eqtpsoClosingLocation=?,eptpsoClosingOdometer=?,eqtpsoClosingOdometerImage=?,eqtpsoClosingLongitude=?,eqtpsoClosingLatitude=?,eqtpsoClosingNotes=?,eqtpsoClosingSignature=?,eqtpsoStatus=?,eqtpsoInterfaceStatus=?,eqtpsoInterfaceDate=? where EqTpSoId=?";
        public static string StartShiftUpdate = "Update EqTpSo set eqtpsoInterfaceStatus=?,IsServer=? where EqTpSoId=?";
        public static string EqTpSoOrder = "Select * from EqTpSo where eqtpsoStatus=?";
        public static string GetShiftbyFlstShiftRefno = "Select * from EqTpSo Where eqtpsoRefno=?";

        public static string StartShiftRefno = "Select FlstShiftRefno from FlstShift Order By FlstShiftRefno DESC LIMIT 1";
        //Close Shift
        public static string CloseShiftInsert = "Update FlstShift set FlstshiftStatus=?,FlstShiftClosingDate=?,FlstshiftClosingPumpCounter1=?,FlstshiftClosingPumpCounter2=?,FlstshiftClosingPumpCounter3=?,FlstshiftClosingPumpImage1=?,FlstshiftClosingPumpImage2=?,FlstshiftClosingPumpImage3=?,FlstShiftClosingLongitude=?,FlstShiftClosingLatitude=?,FlstShiftClosingNotes=?,FlstShiftClosingSignature=?,FlstshiftClosingUserCode=?,FlstshiftClosingUserName=?,FlstshiftFromIssuesRefno=?,FlstshiftToIssuesRefno=?,FlstshiftIssuesDif1=?,FlstshiftIssuesDif2=?,FlstshiftIssuesDif3=?,FlstshiftIssuesProcessed=?,FlstshiftIssuesVoid=?,FlstshiftIssuesVoidRefno=?,FlstshiftNetPumpCounter1=?,FlstshiftNetPumpCounter2=?,FlstshiftNetPumpCounter3=?,FlstshiftIssuesPump1=?,FlstshiftIssuesPump2=?,FlstshiftIssuesPump3=? where FlstshiftId =?";
        #endregion

        #region EqTpSoLn
        public static string GetEqTpSoLnByRefno = "Select * from EqTpSoLn where eqtpsolnRefno=?";
        public static string GetAllEqTpSoLns = "Select * from EqTpSoLn";
        public static string OrderTransportInsert = "Insert into EqTpSoLn(EqTpSoLnId,eqtpsolnDeviceCode,eqtpsolnEqtphaulerCode,eqtpsolnEqtptrailerCode,eqtpsolnEqtpsoRefno,eqtpsolnRefno,eqtpsolnBarcode,eqtpsolnSecurityCode,eqtpsolnEquipmentRfid,eqtpsolnEquipmentCode,eqtpsolnEquipmentDesc,eqtpsolnEquipmentLicensePlate,eqtpsolnProjectCode,eqtpsolnProjectDesc,eqtpsolnProjactCode,eqtpsolnProjactDesc,eqtpsolnLoadingDatetime,eqtpsolnLoadingLocation,eqtpsolnLoadingOdometer,eqtpsolnLoadingOdometerImage,eqtpsolnLoadingLongitude,eqtpsolnLoadingLatitude,eqtpsolnLoadingNotes,eqtpsolnLoadingSignature,eqtpsolnStatus,eqtpsolnInterfaceStatus,eqtpsolnInterfaceDate) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        public static string UpdateIssuesInterface = "Update EqTpSoLn set eqtpsolnInterfaceStatus=? where EqTpSoLnId=?";
        public static string GetAllFlstIsById = "Select * from EqTpSoLn where eqtpsolnEqtpsoRefno=?";
        public static string UpdateTransportStatus = "Update EqTpSoLn set eqtpsolnStatus=? where Id=?";
       
        public static string GetAllFlstIsRefno = "Select * from FlstIs Where FlstisShiftRefno=?";
        public static string ShiftIssuesUpdate = "Update FlstIs set FlstisId=?, FlstId=?,FlstisRefno=?,FlstisShiftRefno=?,FlstisDate=?,FlstisEquipmentRfid=?,FlstisEquipmentCode=?,FlstisEquipmentDesc=?,FlstisDriverName=?,FlstisDriverCode=?,FlstisOdhrmeter=?,FlstisQuantity=?,FlstisPumpCode=?,FlstisFuelCode=?,FlstisPumpImage=?,FlstisNote=?,FlstisSignature=?,FlstisLongitude=?,FlstisLatitude=?,FlstisStatus=?,FlstisInterfaceStatus=?,FlstisFlstCode=?,FlstisDeviceCode=?,FlstisEquipmentLicensePlate=?,FlstisBarcode=?,FlstisSecurityCode=? where Id=?";
        public static string GetFlstIs = "Select * from FlstIs where FlstisId=?";
        #endregion

        #region Device
        public static string InsertDevice = "Insert into AppDevices(AppdeviceId,AppdeviceCode,AppdeviceDesc,AppdeviceAppRefCode,AppdeviceInuse,AppdeviceSecurityCheck) Values(?,?,?,?,?,?)";
        public static string GetDevice = "Select * from AppDevices";
        public static string UpdateDeviceInUse = "Update AppDevices set AppdeviceInuse=? where Id=?";
        //public static string UpdateConfiguration = "Update Flst set ConfigShiftRefnoCounter=?,ConfigRefno=? where ConfigId=?";
        #endregion

        // Configuration
        public static string GetConfiguration = "Select * from Flst WHERE FlstId={0}";
        public static string GetIssuePrint = "Select * from Flst WHERE FlstIssuesPrintCopies={0}";
        public static string InsertConfiguration = "Update Flst set FlstDeviceCode=?,FlstShiftCounter=?,FlstIssuesPrintCopies=?,FlstShiftPrintCopies=? where FlstId=?";
    }
}
