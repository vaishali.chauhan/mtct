﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Interface
{
    public interface IBarcodeManager
    {
        byte[] GetByteArray(string parameter);
    }
}
