﻿using MTCT.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MTCT.Interface
{
    public interface IBluetooth
    {
        bool IsBluetoothEnabled();

        List<BLDevice> GetBluetoothDevices();

        void ConnectBluetooth(string deviceName, string deviceAddress);

        Task<bool> Print(byte[] content);

        void DisconnectBluetooth();

        ZPLImage GetZPLImageCommand(byte[] imageBytes);
    }
}
