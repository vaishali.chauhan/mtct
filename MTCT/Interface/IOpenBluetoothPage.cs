﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Interface
{
    public interface IOpenBluetoothPage
    {
        void OpenBluetoothSettingPage();
    }
}
