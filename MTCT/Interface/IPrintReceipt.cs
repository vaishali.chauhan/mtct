﻿using MTCT.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.Interface
{
    public interface IPrintReceipt
    {
        //Task<string> PrintReceiptMethod(Receipt receipt, AppKey appKey);
        Task<bool> PrintReceiptMethod(string bluetoothAddress, Receipt receiptString);

        Task<bool> PrintReceiptMethod(string bluetoothAddress, Receipt receiptString,string barcode);

        Task<bool> PrintOrderReceipt(string bluetoothAddress, Receipt receiptString, List<string> barcodes);

        Task<List<BluetoothList>> BluetoothList();
    }
}
