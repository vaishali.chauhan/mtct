﻿using System.Collections.Generic;

namespace MTCT.Interface
{
    public interface IRfidScanning
    {
        IList<string> GetDevices();
        bool ConnectBarcodeDevice(string devicename);
        void ScanDevice(string devicename);
    }
}
