﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Interface
{
    public interface ISQLiteConnection
    {
        SQLiteConnection GetConnection();
    }
}
