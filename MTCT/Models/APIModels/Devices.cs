﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{
    public class Devices : Response
    {
        public int appdeviceId { get; set; }
        public string appdeviceCode { get; set; }
        public string appdeviceDesc { get; set; }
        public string appdeviceAppRefCode { get; set; }
        public bool appdeviceInuse { get; set; }
        public string appdeviceSecurityCheck { get; set; }
    }

    public class DeviceListner : Response
    {
        public List<Devices> mDevices { get; set; }
        public DeviceListner()
        {
            mDevices = new List<Devices>();
        }
    }
}
