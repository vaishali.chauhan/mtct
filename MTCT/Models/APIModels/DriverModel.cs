﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{
    public class DriverModel
    {
        public int driverId { get; set; }
        public string driverCode { get; set; }
        public string driverName { get; set; }
        public string driverAppRefCode { get; set; }
        public bool driverActiveStatus { get; set; }
    }

    public class DriverModelDetails :Response
    {
        public List<DriverModel> mDriverModels { get; set; }
        public DriverModel mDriver { get; set; }
    }
}
