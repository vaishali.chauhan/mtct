﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{

    public class EqTpSoModel
    {
        public string eqtpsoEqtpCode { get; set; }
        public string eqtpsoDeviceCode { get; set; }
        public string eqtpsoEqtphaulerCode { get; set; }
        public string eqtpsoEqtptrailerCode { get; set; }
        public string eqtpsoRefno { get; set; }
        public string eqtpsoDriverCode { get; set; }
        public string eqtpsoDriverName { get; set; }
        public string eqtpsoUserCode { get; set; }
        public string eqtpsoUserName { get; set; }
        public Nullable<DateTime> eqtpsoOpeningDate { get; set; }
        public string eqtpsoOpeningLocation { get; set; }
        public decimal eqtpsoOpeningOdometer { get; set; }
        public string eqtpsoOpeningOdometerImage { get; set; }
        public string eqtpsoOpeningOdometerImagePath { get; set; }
        public string eqtpsoOpeningLongitude { get; set; }
        public string eqtpsoOpeningLatitude { get; set; }
        public string eqtpsoOpeningNotes { get; set; }
        public string eqtpsoOpeningSignature { get; set; }
        public string eqtpsoOpeningSignaturePath { get; set; }
        public Nullable<DateTime> eqtpsoClosingDate { get; set; }
        public string eqtpsoClosingLocation { get; set; }
        public decimal eptpsoClosingOdometer { get; set; }
        public string eqtpsoClosingOdometerImage { get; set; }
        public string eqtpsoClosingOdometerImagePath { get; set; }
        public string eqtpsoClosingLongitude { get; set; }
        public string eqtpsoClosingLatitude { get; set; }
        public string eqtpsoClosingNotes { get; set; }
        public string eqtpsoClosingSignature { get; set; }
        public string eqtpsoClosingSignaturePath { get; set; }
        public string eqtpsoStatus { get; set; }
        public string eqtpsoInterfaceStatus { get; set; }
        public Nullable<DateTime> eqtpsoInterfaceDate { get; set; }
    }

    public class OrderCloseModelDetail
    {
        public List<EqTpSoModel> eqTpSo;
        public List<EqTpSoLnModel> eqTpSoLn;
        public OrderCloseModelDetail()
        {
            eqTpSo = new List<EqTpSoModel>();
            eqTpSoLn = new List<EqTpSoLnModel>();
        }
    }

    public class OrderOpenModelDetail : Response
    {
        public List<EqTpSoModel> mFlstShiftModel;
        public List<OrderCloseModelDetail> flstCloseShiftModelDetails;
        public OrderOpenModelDetail()
        {
            mFlstShiftModel = new List<EqTpSoModel>();
            flstCloseShiftModelDetails = new List<OrderCloseModelDetail>();
        }
    }
}
