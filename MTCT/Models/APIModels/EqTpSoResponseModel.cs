﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{

    public class PassedRecords
    {
        public int count { get; set; }
        public List<string> entityCodes { get; set; }
    }

    public class Detail
    {
        public string entityCode { get; set; }
        public string message { get; set; }
    }

    public class FailedRecords
    {
        public int count { get; set; }
        public List<Detail> details { get; set; }
    }

    public class EqTpSoResponseModel : Response
    {
        public int totalRecordsCount { get; set; }
        public PassedRecords passedRecords { get; set; }
        public FailedRecords failedRecords { get; set; }
    }
}
