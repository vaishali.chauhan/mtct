﻿using System.Collections.Generic;

namespace MTCT.Models.APIModels
{
    public class EqtpHaulerDetail : Response
    {
        public LocalModels.EqTpHaulers EqTpHaulers { get; set; }
        public List<LocalModels.EqTpTrailers> EqTpTrailers { get; set; }

        public EqtpHaulerDetail()
        {
            EqTpTrailers = new List<LocalModels.EqTpTrailers>();
        }
    }
}
