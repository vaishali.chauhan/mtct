﻿namespace MTCT.Models.APIModels
{
    public class EqtpHaulers : Response
    {
        public int eqtphaulerId { get; set; }
        public string eqtphaulerCode { get; set; }
        public string eqtphaulerDesc { get; set; }
        public string eqtphaulerAppRefCode { get; set; }
        public bool eqtphaulerActiveStatus { get; set; }
    }
}
