﻿namespace MTCT.Models.APIModels
{
    public class EqtpResponse : Response
    {
        public int eqtpId { get; set; }
        public string eqtpCode { get; set; }
        public string eqtpName { get; set; }
        public bool eqtpRfidEnabled { get; set; }
        public bool eqtpActiveStatus { get; set; }
    }
}
