﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{
    public class EquipmentModel
    {
        public int equipmentId { get; set; }
        public string equipmentCode { get; set; }
        public string equipmentDesc { get; set; }
        public string equipmentLicensePlate { get; set; }
        public string equipmentRfid { get; set; }
        public double equipmentFltankCapacity { get; set; }
        public string equipmentAppRefCode { get; set; }
        public bool equipmentActiveStatus { get; set; }
    }

    public class EquipmentModelDetail : Response
    {
        public List<EquipmentModel> mEquipmentModels { get; set; }
        public EquipmentModel mEquipment { get; set; }
    }
}
