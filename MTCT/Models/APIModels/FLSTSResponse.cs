﻿using System.Collections.Generic;

namespace MTCT.Models.APIModels
{
    public class FlSt
    {
        public int flstId { get; set; }
        public string flstCode { get; set; }
        public string flstDesc { get; set; }
        public bool flstRfidEnabled { get; set; }
        public bool flstActiveStatus { get; set; }
    }

    public class FlStFuel
    {
        public int flstfuelId { get; set; }
        public int flstId { get; set; }
        public string flstfuelFuelCode { get; set; }
        public string flstfuelPumpCode { get; set; }
        public bool flstfuelActiveStatus { get; set; }
    }

    public class FLSTSResponse : Response
    {
        public FlSt flSt { get; set; }
        public List<FlStFuel> flStFuel { get; set; }

    }

    public class FLSTSResponseDetail : Response
    {
        public List<FLSTSResponse> mFLSTSResponse { get; set; }
    }
}
