﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{

    public class FlstIsModel
    {
        public string flstisFlstCode { get; set; }
        public string flstisDeviceCode { get; set; }
        public string flstisShiftRefno { get; set; }
        public string flstisRefno { get; set; }
        public DateTime flstisDate { get; set; }
        public string flstisBarcode { get; set; }
        public string flstisSecurityCode { get; set; }
        public string flstisLongitude { get; set; }
        public string flstisLatitude { get; set; }
        public string flstisDriverCode { get; set; }
        public string flstisDriverName { get; set; }
        public string flstisEquipmentRfid { get; set; }
        public string flstisEquipmentCode { get; set; }
        public string flstisEquipmentDesc { get; set; }
        public string flstisEquipmentLicensePlate { get; set; }
        public decimal flstisOdhrmeter { get; set; }
        public string flstisPumpCode { get; set; }
        public string flstisFuelCode { get; set; }
        public decimal flstisQuantity { get; set; }
        public string flstisNotes { get; set; }
        public string flstisSignature { get; set; }
        public string flstisPumpImage { get; set; }
        public string flstisStatus { get; set; }
        public string flstisInterfaceStatus { get; set; }
    }
}

