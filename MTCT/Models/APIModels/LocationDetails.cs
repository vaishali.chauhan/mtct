﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{
    public class LocationDetails: Response
    {
        public List<Models.LocalModels.Locations> Locations { get; set; }

        public LocationDetails()
        {
            Locations = new List<LocalModels.Locations>();
        }
    }
}
