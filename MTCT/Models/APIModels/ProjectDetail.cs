﻿using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MTCT.Models.APIModels
{

    public class ProjectDetail : INotifyPropertyChanged
    {
        public Project project { get; set; }

        public List<ProjectActivity> projAct { get; set; }

        //Extra
        public double projActHeight { get; set; }
        private bool _IsDisplayprojAct;
        public bool IsDisplayprojAct
        {
            get
            {
                return _IsDisplayprojAct;
            }
            set
            {
                _IsDisplayprojAct = value;
                OnPropertyChanged("IsDisplayprojAct");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public ProjectDetail()
        {
            projAct = new List<ProjectActivity>();
        }
    }

    public class ProjectDetails : Response
    {
        public List<ProjectDetail> ProjectDetail { get; set; }

        public ProjectDetails()
        {
            ProjectDetail = new List<ProjectDetail>();
        }
    }
}
