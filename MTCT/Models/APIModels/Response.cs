﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{
    public class Response
    {
        public bool IsSuccess { get; set; }
        public string errorMessage { get; set; }
        public string successMessage { get; set; }
    }
}
