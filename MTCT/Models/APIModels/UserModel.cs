﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{

    public class UserModel
    {
        public int appuserId { get; set; }
        public string appuserCode { get; set; }
        public string appuserName { get; set; }
        public string appuserEmail { get; set; }
        public string appuserPassword { get; set; }
        public string appuserRole { get; set; }
        public string appuserAppRefCode { get; set; }
        public bool appuserActiveStatus { get; set; }
    }

    public class UserModelDetail : Response
    {
        public List<UserModel> mUserModels { get; set; }
    }

}
