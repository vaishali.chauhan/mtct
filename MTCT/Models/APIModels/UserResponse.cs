﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.APIModels
{
    public class UserResponse : Response
    {
        public string token { get; set; }
        public string expiration { get; set; }
    }
}
