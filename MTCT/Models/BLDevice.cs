﻿namespace MTCT.Models
{
    public class BLDevice
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public bool IsConnected { get; set; }
    }
}
