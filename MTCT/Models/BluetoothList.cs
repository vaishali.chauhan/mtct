﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models
{
    public class BluetoothList
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }

    public class Receipt
    {
        public StringBuilder ReceiptString { get; set; }
        public StringBuilder ReceiptString1 { get; set; }
        public StringBuilder ReceiptString2 { get; set; }


        public StringBuilder OrderReceiptString { get; set; }
        public List<StringBuilder> TransactionReceiptString { get; set; }
        public StringBuilder ClosingReceiptString { get; set; }
    }

    public class AppKey
    {
        public int Id { get; set; }
        public string KeyName { get; set; }
        public string KeyAddress { get; set; }
    }


    public class BitmapData
    {
        public BitArray Dots
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public int Width
        {
            get;
            set;
        }
    }

}
