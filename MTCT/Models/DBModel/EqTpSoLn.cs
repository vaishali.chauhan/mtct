﻿using SQLite;
using System;

namespace MTCT.Models.DBModel
{
    public class EqTpSoLn
    {
        [AutoIncrement, PrimaryKey, NotNull]
        public int Id { get; set; }
        public int EqTpSoLnId { get; set; }
        public string eqtpsolnEqtpCode { get; set; }
        public string eqtpsolnDeviceCode { get; set; }
        public string eqtpsolnEqtphaulerCode { get; set; }
        public string eqtpsolnEqtptrailerCode { get; set; }
        public string eqtpsolnEqtpsoRefno { get; set; }
        public string eqtpsolnRefno { get; set; }
        public string eqtpsolnBarcode { get; set; }
        public string eqtpsolnSecurityCode { get; set; }
        public string eqtpsolnEquipmentRfid { get; set; }
        public string eqtpsolnEquipmentCode { get; set; }
        public string eqtpsolnEquipmentDesc { get; set; }
        public string eqtpsolnEquipmentLicensePlate { get; set; }
        public string eqtpsolnProjectCode { get; set; }
        public string eqtpsolnProjectDesc { get; set; }
        public string eqtpsolnProjactCode { get; set; }
        public string eqtpsolnProjactDesc { get; set; }
        public Nullable<DateTime> eqtpsolnLoadingDatetime { get; set; }
        public string eqtpsolnLoadingLocation { get; set; }
        public decimal eqtpsolnLoadingOdometer { get; set; }
        public string eqtpsolnLoadingOdometerImage { get; set; }
        public string eqtpsolnLoadingLongitude { get; set; }
        public string eqtpsolnLoadingLatitude { get; set; }
        public string eqtpsolnLoadingNotes { get; set; }
        public string eqtpsolnLoadingSignature { get; set; }
        public Nullable<DateTime> eqtpsolnUnloadingDatetime { get; set; }
        public string eqtpsolnUnloadingLocation { get; set; }
        public decimal eqtpsolnUnloadingOdometer { get; set; }
        public string eqtpsolnUnloadingOdometerImage { get; set; }
        public string eqtpsolnUnloadingLongitude { get; set; }
        public string eqtpsolnUnloadingLatitude { get; set; }
        public string eqtpsolnUnloadingNotes { get; set; }
        public string eqtpsolnUnLoadingSignature { get; set; }
        public string eqtpsolnStatus { get; set; }
        public string eqtpsolnInterfaceStatus { get; set; }
        public DateTime eqtpsolnInterfaceDate { get; set; }

        public EqTpSoLn()
        {
            eqtpsolnUnloadingLocation = string.Empty;
            eqtpsolnUnloadingOdometerImage = string.Empty;
            eqtpsolnUnloadingLongitude = string.Empty;
            eqtpsolnUnloadingLatitude = string.Empty;
            eqtpsolnUnloadingNotes = string.Empty;
            eqtpsolnUnLoadingSignature = string.Empty;
            eqtpsolnUnloadingOdometer = 0;
        }

    }
}
