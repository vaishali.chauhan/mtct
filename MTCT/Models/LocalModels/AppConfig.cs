﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class AppConfig
    {
        [AutoIncrement, PrimaryKey, NotNull]
        public int Id { get; set; }
        public int AppConfigId { get; set; }
        public string AppConfigCode { get; set; }
        public string AppConfigDesc { get; set; }
        public string AppConfigApiUrl { get; set; }
        public string AppConfigUserName { get; set; }
        public string AppConfigPassword { get; set; }
    }
}
