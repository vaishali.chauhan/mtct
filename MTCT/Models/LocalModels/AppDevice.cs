﻿using MTCT.Models.APIModels;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class AppDevice : Response
    {
        [AutoIncrement, PrimaryKey, NotNull]
        public int Id { get; set; }
        public int AppdeviceId { get; set; }
        public string AppdeviceCode { get; set; }
        public string AppdeviceDesc { get; set; }
        public string AppdeviceAppRefCode { get; set; }
        public bool AppdeviceInuse { get; set; }
        public string AppdeviceSecurityCheck { get; set; }
    }
}
