﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class Configuration
    {
        public int ConfigId { get; set; }
        public int FlstId { get; set; }
        public int UserId { get; set; }
        public string ConfigDeviceId { get; set; }
        public string ConfigShiftRefnoCounter { get; set; }
        public string ConfigRefno { get; set; }
    }
}
