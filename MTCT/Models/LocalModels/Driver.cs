﻿using MTCT.Models.APIModels;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class Driver : Response
    {
        [AutoIncrement, PrimaryKey, NotNull]
        public int Id { get; set; }
        public int DriverId { get; set; }
        public string DriverCode { get; set; }
        public string DriverName { get; set; }
        public bool DriverActiveStatus { get; set; }
        public string driverAppRefCode { get; set; }

        // Extra
        public string DisplayDriver
        {
            get
            {
                return DriverName + " - " + DriverCode;
            }
        }
    }

    public class DriverListner : Response
    {
        public List<Driver> Drivers { get; set; }
        public DriverListner()
        {
            Drivers = new List<Driver>();
        }
    }
}
