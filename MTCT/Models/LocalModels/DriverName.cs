﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class DriverName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DriverNameLists
    {
        public List<DriverName> mDriverNames = new List<DriverName>();
        public DriverNameLists()
        {
            mDriverNames = new List<DriverName>()
            {
                new DriverName{Id =1,Name="Sean J. Bolden"},
                new DriverName{Id =2,Name="Antonio S. Brock"},
                new DriverName{Id =3,Name="Alvin A. Murphy"},
                new DriverName{Id =4,Name="James R. Saunders"},
                new DriverName{Id =5,Name="Joseph K. Stenson"},
                new DriverName{Id =6,Name="Joseph M. Gonzales"},
                new DriverName{Id =7,Name="Dennis B. Wikins"},
            };
        }
    }
}
