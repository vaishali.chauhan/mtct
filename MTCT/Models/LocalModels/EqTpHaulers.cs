﻿using SQLite;

namespace MTCT.Models.LocalModels
{
    public class EqTpHaulers
    {
        [AutoIncrement, PrimaryKey, NotNull]
        public int Id { get; set; }
        public int EqtphaulerId { get; set; }
        public string EqtphaulerCode { get; set; }
        public string EqtphaulerDesc { get; set; }
        public string EqtphaulerAppRefCode { get; set; }
        public bool EqtphaulerActiveStatus { get; set; }

        public string DisplayHauler
        {
            get
            {
                return EqtphaulerCode + " " + EqtphaulerDesc;
            }
        }

        //public string DisplayHauler
        //{
        //    get
        //    {
        //        return EqtphaulerDesc + "(" + EqtphaulerCode + ")";
        //    }
        //}
    }
}
