﻿using System;

namespace MTCT.Models.LocalModels
{
    public class EqTpSo
    {
        public int Id { get; set; }
        public int EqTpSoId { get; set; }
        public string eqtpsoEqtpCode { get; set; }
        public string eqtpsoDeviceCode { get; set; }
        public string eqtpsoEqtphaulerCode { get; set; }
        public string eqtpsoEqtptrailerCode { get; set; }
        public string eqtpsoRefno { get; set; }
        public string eqtpsoDriverCode { get; set; }
        public string eqtpsoDriverName { get; set; }
        public string eqtpsoUserCode { get; set; }
        public string eqtpsoUserName { get; set; }
        public DateTime eqtpsoOpeningDate { get; set; }
        public string eqtpsoOpeningLocation { get; set; }
        public decimal eqtpsoOpeningOdometer { get; set; }
        public string eqtpsoOpeningOdometerImage { get; set; }
        public string eqtpsoOpeningLongitude { get; set; }
        public string eqtpsoOpeningLatitude { get; set; }
        public string eqtpsoOpeningNotes { get; set; }
        public string eqtpsoOpeningSignature { get; set; }
        public DateTime eqtpsoClosingDate { get; set; }
        public string eqtpsoClosingLocation { get; set; }
        public decimal eptpsoClosingOdometer { get; set; }
        public string eqtpsoClosingOdometerImage { get; set; }
        public string eqtpsoClosingLongitude { get; set; }
        public string eqtpsoClosingLatitude { get; set; }
        public string eqtpsoClosingNotes { get; set; }
        public string eqtpsoClosingSignature { get; set; }
        public string eqtpsoStatus { get; set; }
        public string eqtpsoInterfaceStatus { get; set; }
        public DateTime eqtpsoInterfaceDate { get; set; }

        public bool IsServer { get; set; }
    }
}
