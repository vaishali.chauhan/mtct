﻿using MTCT.Models.DBModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace MTCT.Models.LocalModels
{
    public class EqTpSoLnLocal : EqTpSoLn, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ImageSource _IsVoidImage { get; set; } = "iconNon.png";
        public ImageSource IsVoidImage
        {
            get
            {
                return _IsVoidImage;
            }
            set
            {
                _IsVoidImage = value;
                OnPropertyChanged("IsVoidImage");
            }
        }
        public bool _IsVoided { get; set; }
        public bool IsVoided
        {
            get
            {
                return _IsVoided;
            }
            set
            {
                _IsVoided = value;
                OnPropertyChanged("IsVoided");
            }
        }

        public Color txtColorDescarga
        {
            get
            {
                if (!string.IsNullOrEmpty(eqtpsolnUnloadingLocation))
                    return (Color)App.Current.Resources["FuelBlack"];
                else
                    return Color.Red;
            }
        }
        public Color txtColorFechaDescarga
        {
            get
            {
                if (eqtpsolnUnloadingDatetime.HasValue)
                    return (Color)App.Current.Resources["FuelBlack"];
                else
                    return Color.Red;
            }
        }

    }




    //public class EqTpSoLnLists
    //{
    //    public List<EqTpSoLn> mEqTpSoLns = new List<EqTpSoLn>();
    //    public EqTpSoLnLists()
    //    {
    //        mEqTpSoLns = new List<EqTpSoLn>()
    //        {
    //            new EqTpSoLn{eqtpsolnRefno="000001",eqtpsolnEquipmentRfid="00000000000121212",eqtpsolnEquipmentCode="0003",eqtpsolnProjectCode="0001",eqtpsolnProjactCode="0001",eqtpsolnEquipmentDesc="Retro",eqtpsolnProjectDesc="Juan Perez",eqtpsolnProjactDesc="Juan Perez",eqtpsolnLoadingLocation="ITABo",eqtpsolnLoadingDatetime=DateTime.Now,eqtpsolnUnloadingLocation="ITABO",eqtpsolnUnloadingDatetime=DateTime.Now,eqtpsolnStatus="Procesado",eqtpsolnInterfaceStatus="Pendiente",eqtpsolnInterfaceDate=DateTime.Now},

    //        };
    //    }
    //}
}
