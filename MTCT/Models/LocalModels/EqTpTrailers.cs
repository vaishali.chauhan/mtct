﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class EqTpTrailers
    {
        public int Id { get; set; }
        public int EqtptrailerId { get; set; }
        public int EqtphaulerId { get; set; }
        public string EqtptrailerCode { get; set; }
        public string EqtptrailerDesc { get; set; }
        public bool EqtptrailerActiveStatus { get; set; }

        public string DisplayTrailer
        {
            get
            {
                return EqtptrailerCode + " " + EqtptrailerDesc;
            }
        }

        //public string DisplayTrailer
        //{
        //    get
        //    {
        //        return EqtptrailerDesc + "(" + EqtptrailerCode + ")";
        //    }
        //}
    }
}
