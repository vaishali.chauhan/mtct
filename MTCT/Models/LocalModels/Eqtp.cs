﻿using SQLite;

namespace MTCT.Models.LocalModels
{
    public class Eqtp
    {
        [AutoIncrement, PrimaryKey, NotNull]
        public int Id { get; set; }
        public int EqtpId { get; set; }
        public string EqtpCode { get; set; }
        public string EqtpName { get; set; }
        public bool EqtpRfidEnabled { get; set; }
        public bool EqtpActiveStatus { get; set; }
        public int EqtpOrderCounterNo { get; set; }
        public int EqtpTransportCounterNo { get; set; }
        public int EqtpOrderPrint { get; set; }
        public int EqtpTransportPrint { get; set; }
        public int EqtpDeviceCode { get; set; }
        public string PrintFormat { get; set; }
        public bool EqtpSignEnabled { get; set; }
    }
}
