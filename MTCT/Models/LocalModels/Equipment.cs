﻿using MTCT.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class Equipment : Response
    {
        public int Id { get; set; }
        public int EquipmentId { get; set; }
        public string EquipmentCode { get; set; }
        public string EquipmentDesc { get; set; }
        public string EquipmentLicensePlate { get; set; }
        public string EquipmentRFID { get; set; }
        public double EquipmentFltankCapacity { get; set; }
        public bool EquipmentActiveStatus { get; set; }
        public string equipmentAppRefCode { get; set; }

        // Extra
        public string DisplayEquipment
        {
            get
            {
                return EquipmentCode + " " + EquipmentDesc + " " + EquipmentRFID;
            }
        }
    }

    public class EquipmentListner : Response
    {
        public List<Equipment> Equipments { get; set; }
        public EquipmentListner()
        {
            Equipments = new List<Equipment>();
        }
    }
}
