﻿using MTCT.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class Flst : Response
    {
        public int Id { get; set; }
        public int FlstId { get; set; }
        public string FlstCode { get; set; }
        public string FlstDesc { get; set; }
        public string FlstDeviceCode { get; set; }
        public int FlstShiftCounter { get; set; }
        public int FlstReceiptsCounter { get; set; }
        public int FlstIssuesCounter { get; set; }
        public int FlstAdjustmentsCounter { get; set; }
        public int FlstTransfersCounter { get; set; }
        public int FlstIssuesPrintCopies { get; set; }
        public int FlstShiftPrintCopies { get; set; }
        public string PrintFormat { get; set; }

        // Extra
        public bool appdeviceInuse { get; set; }
    }

    public class FlstListner : Response
    {
        public List<Flst> Flsts { get; set; }
        public FlstListner()
        {
            Flsts = new List<Flst>();
        }
    }
}
