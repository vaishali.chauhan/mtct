﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class FlstFuel
    {
        public int Id { get; set; }
        public int FlstfuelId { get; set; }
        public int FlstId { get; set; }
        public string FlstfuelPumpCode { get; set; }
        public string FlstfuelFuelCode { get; set; }
        public bool FlstfuelActiveStatus { get; set; }

        // Extra
        public string DisplayFlstFuel
        {
            get
            {
                return FlstfuelPumpCode + " - " + FlstfuelFuelCode;
            }
        }

    }
}
