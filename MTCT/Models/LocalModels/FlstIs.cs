﻿using MTCT.Utilties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace MTCT.Models.LocalModels
{
    public class FlstIs : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public int FlstisId { get; set; }

        //  [JsonIgnore]

        public int FlstId { get; set; }
        public string FlstisFlstCode { get; set; }
        public string FlstisDeviceCode { get; set; }
        public string FlstisShiftRefno { get; set; }
        public string FlstisRefno { get; set; }
        public DateTime FlstisDate { get; set; }
        public string FlstisLongitude { get; set; }
        public string FlstisLatitude { get; set; }
        public string FlstisDriverCode { get; set; }
        public string FlstisDriverName { get; set; }
        public string FlstisEquipmentRfid { get; set; }
        public string FlstisEquipmentCode { get; set; }
        public string FlstisEquipmentDesc { get; set; }
        public string FlstisEquipmentLicensePlate { get; set; }
        public decimal FlstisOdhrmeter { get; set; }
        public string FlstisPumpCode { get; set; }
        public string FlstisFuelCode { get; set; }
        public decimal FlstisQuantity { get; set; }
        public string FlstisStatus { get; set; }
        public string FlstisInterfaceStatus { get; set; }
        public string FlstisNote { get; set; }
        public string FlstisSignature { get; set; }
        public string FlstisPumpImage { get; set; }
        public string FlstisBarcode { get; set; }
        public string FlstisSecurityCode { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        // Extra
        //  public Color _StatusColor { get; set; }
        public Color StatusColor
        {
            get
            {
                if (FlstisStatus == EqTpSoLnStatus.Procesado.ToString())
                {
                    return Color.FromHex("#053CAA");
                }
                else
                {
                    return Color.FromHex("#DD0803");
                }
            }
            //set
            //{
            //    _StatusColor = value;
            //    OnPropertyChanged("StatusColor");
            //}
        }

        public bool _IsVoided { get; set; }
        public bool IsVoided
        {
            get
            {
                return _IsVoided;
            }
            set
            {
                _IsVoided = value;
                OnPropertyChanged("IsVoided");
            }
        }

        public string DisplayGel
        {
            get
            {
                //return FlstisPumpCode + " - " + FlstisFuelCode + " " + string.Format("{0:00.00}", FlstisQuantity.ToDecimalString() + "M");
                if (FlstisQuantity == 0)
                    return FlstisPumpCode + " - " + FlstisFuelCode + " " + "0.00";
                else
                    return FlstisPumpCode + " - " + FlstisFuelCode + " " + Convert.ToDecimal(string.Format("{0:0.00}", FlstisQuantity)).ToDecimalString();
            }
        }

        public string DisplayKm
        {
            get
            {
                if (FlstisOdhrmeter == 0)
                    return "0.00";
                else
                    return Convert.ToDecimal(string.Format("{0:0.00}", FlstisOdhrmeter)).ToDecimalString();
                //return string.Format("{0:00.00}", FlstisOdhrmeter.ToDecimalString() + "M");
            }
        }

        public ImageSource _IsVoidImage { get; set; } = "iconNon.png";
        public ImageSource IsVoidImage
        {
            get
            {
                return _IsVoidImage;
            }
            set
            {
                _IsVoidImage = value;
                OnPropertyChanged("IsVoidImage");
            }
        }

    }
}
