﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class Locations
    {
        public int Id { get; set; }
        public int EqtplocationId { get; set; }
        public string EqtplocationCode { get; set; }
        public string EqtplocationAppRefCode { get; set; }
        public bool EqtplocationActiveStatus { get; set; }
    }
}
