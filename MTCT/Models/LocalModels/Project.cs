﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class Project
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectDesc { get; set; }
        public string ProjectAppRefCode { get; set; }
        public bool ProjectActiveStatus { get; set; }

        public string DisplayProject
        {
            get
            {
                return ProjectCode + " " + ProjectDesc;
            }
        }
    }
}
