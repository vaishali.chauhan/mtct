﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MTCT.Models.LocalModels
{
    public class ProjectActivity
    {
        public int Id { get; set; }
        public int projActId { get; set; }
        public int projectId { get; set; }
        public string projactCode { get; set; }
        public string projactDesc { get; set; }
        public bool projactActiveStatus { get; set; }


        // extra

        [JsonIgnore]
        public string DisplayStatus
        {
            get
            {
                if (projactActiveStatus)
                    return "Activo";
                else
                    return "Inactivo";

            }
        }

        [JsonIgnore]
        public string DisplayProjact
        {
            get
            {
                return projactCode + " " + projactDesc;
            }
        }

        [JsonIgnore]
        public Xamarin.Forms.Color BGColor
        {
            get
            {
                if (projactActiveStatus)
                    return Color.Green;
                else
                    return Color.Red;

            }
        }

    }
}
