﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Models.LocalModels
{
    public class User
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public string UserRole { get; set; }
        public string UserCode { get; set; }
        public bool appuserActiveStatus { get; set; }
        public string appuserAppRefCode { get; set; }
    }
}
