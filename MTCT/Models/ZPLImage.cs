﻿namespace MTCT.Models
{
    public class ZPLImage
    {
        public int ByteCount { get; set; }
        public int BytesPerRow { get; set; }
        public string Data { get; set; }
    }
}
