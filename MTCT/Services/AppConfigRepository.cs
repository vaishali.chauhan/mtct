﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTCT.Services
{
    public class AppConfigRepository
    {
        public List<AppConfig> GetAllAppConfig()
        {
            List<AppConfig> appConfig = new List<AppConfig>();
            appConfig = App.dbConn.Query<AppConfig>(DatabaseHelper.GetAppConfig, new object[] { });
            return appConfig;
        }

        public bool SaveAppConfig(AppConfig mAppConfig)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertAppConfig,
                new object[]
                {
                    mAppConfig.AppConfigId,
                    mAppConfig.AppConfigApiUrl,
                    mAppConfig.AppConfigUserName,
                    mAppConfig.AppConfigPassword,
                }) == 1 ? true : false;
            return success;
        }

        public bool UpdateAppConfig(AppConfig mAppConfig)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateAppConfig,
                new object[]
                {
                    mAppConfig.AppConfigApiUrl,
                    mAppConfig.AppConfigUserName,
                    mAppConfig.AppConfigPassword,
                    mAppConfig.AppConfigId,
                }) == 1 ? true : false;
            return success;
        }

        public AppConfig GetAppConfig()
        {
            return App.dbConn.Query<AppConfig>(DatabaseHelper.GetAppConfig, new object[] { }).FirstOrDefault();
        }
    }
}
