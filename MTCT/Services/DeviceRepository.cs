﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace MTCT.Services
{
    public class DeviceRepository
    {
        public bool SaveDevice(AppDevice mAppDevice)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertDevice,
                new object[]
                {
                        mAppDevice.AppdeviceId,
                        mAppDevice.AppdeviceCode,
                        mAppDevice.AppdeviceDesc,
                        mAppDevice.AppdeviceAppRefCode,
                        mAppDevice.AppdeviceInuse,
                        mAppDevice.AppdeviceSecurityCheck,
                }) == 1 ? true : false;
            return success;
        }

        public bool UpdateDevice(AppDevice mAppDevice)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateDeviceInUse,
                new object[]
                {
                        mAppDevice.AppdeviceInuse,
                        mAppDevice.Id
                }) == 1 ? true : false;

            return success;
        }

        public AppDevice GetDevice()
        {
            return App.dbConn.Query<AppDevice>(DatabaseHelper.GetDevice, new object[] { }).FirstOrDefault();
        }

        public Flst GetIssuesPrintCopy(int flstIssuesPrint)
        {
            return App.dbConn.Query<Flst>(string.Format(DatabaseHelper.GetIssuePrint, flstIssuesPrint), new object[] { }).FirstOrDefault();
        }
    }
}
