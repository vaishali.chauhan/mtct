﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTCT.Services
{
    public class DriverRepository
    {
        public List<Driver> GetDrivers()
        {
            List<Driver> driver = new List<Driver>();
            driver = App.dbConn.Query<Driver>(DatabaseHelper.Driver, new object[] { });
            return driver;
        }

        public List<Driver> GetDriverName(string searchValue)
        {
            List<Driver> driver = new List<Driver>();
            driver = App.dbConn.Query<Driver>(string.Format(DatabaseHelper.SearchDriver, searchValue), new object[] { });
            return driver;
        }

        public int DeleteDriver(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteDriver, new object[] { id });
            return success;
        }
        public bool SaveDriver(Driver mDriver)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertDriver,
                new object[]
                {
                    mDriver.DriverId,
                    mDriver.DriverCode,
                    mDriver.DriverName,
                    mDriver.DriverActiveStatus,
                  mDriver.driverAppRefCode,
                }) == 1 ? true : false;
            return success;
        }
    }
}
