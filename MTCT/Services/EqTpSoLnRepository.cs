﻿using MTCT.DatabaseService;
using MTCT.Models.DBModel;
using MTCT.Models.LocalModels;
using System.Collections.Generic;
using System.Linq;

namespace MTCT.Services
{
    public class EqTpSoLnRepository
    {

        public bool SaveEqtpSoLn(EqTpSoLn eqTpSoLn)
        {
            int returnResult = 0;
            if (eqTpSoLn.Id > 0)
                returnResult = App.dbConn.Update(eqTpSoLn);
            else
                returnResult = App.dbConn.Insert(eqTpSoLn);

            return returnResult > 0 ? true : false;
        }

        public bool SaveEqTpSoLns(EqTpSoLnLocal eqTpSoLn)
        {
            bool success = false;
            if (eqTpSoLn.Id == 0)
            {
                success = App.dbConn.Execute(DatabaseHelper.OrderTransportInsert,
                    new object[]
                    {
                        eqTpSoLn.EqTpSoLnId,
                        eqTpSoLn.eqtpsolnDeviceCode,
                        eqTpSoLn.eqtpsolnEqtphaulerCode,
                        eqTpSoLn.eqtpsolnEqtptrailerCode,
                        eqTpSoLn.eqtpsolnEqtpsoRefno,
                        eqTpSoLn.eqtpsolnRefno,
                        eqTpSoLn.eqtpsolnBarcode,
                        eqTpSoLn.eqtpsolnSecurityCode,
                        eqTpSoLn.eqtpsolnEquipmentRfid,
                        eqTpSoLn.eqtpsolnEquipmentCode,
                        eqTpSoLn.eqtpsolnEquipmentDesc,
                        eqTpSoLn.eqtpsolnEquipmentLicensePlate,
                        eqTpSoLn.eqtpsolnProjectCode,
                        eqTpSoLn.eqtpsolnProjectDesc,
                        eqTpSoLn.eqtpsolnProjactCode,
                        eqTpSoLn.eqtpsolnProjactDesc,
                        eqTpSoLn.eqtpsolnLoadingDatetime,
                        eqTpSoLn.eqtpsolnLoadingLocation,
                        eqTpSoLn.eqtpsolnLoadingOdometer,
                        eqTpSoLn.eqtpsolnLoadingOdometerImage,
                        eqTpSoLn.eqtpsolnLoadingLongitude,
                        eqTpSoLn.eqtpsolnLoadingLatitude,
                        eqTpSoLn.eqtpsolnLoadingNotes,
                        eqTpSoLn.eqtpsolnLoadingSignature,
                        eqTpSoLn.eqtpsolnStatus,
                        eqTpSoLn.eqtpsolnInterfaceStatus,
                        eqTpSoLn.eqtpsolnInterfaceDate,

                    }) == 1 ? true : false;
            }
            //else
            //{
            //    success = App.dbConn.Execute(DatabaseHelper.ShiftIssuesUpdate,
            //        new object[]
            //        {
            //         eqTpSoLn.EqTpSoLnId,
            //            eqTpSoLn.eqtpsolnDeviceCode,
            //            eqTpSoLn.eqtpsolnEqtphaulerCode,
            //            eqTpSoLn.eqtpsolnEqtptrailerCode,
            //            eqTpSoLn.eqtpsolnEqtpsoRefno,
            //            eqTpSoLn.eqtpsolnRefno,
            //            eqTpSoLn.eqtpsolnBarcode,
            //            eqTpSoLn.eqtpsolnSecurityCode,
            //            eqTpSoLn.eqtpsolnEquipmentRfid,
            //            eqTpSoLn.eqtpsolnEquipmentCode,
            //            eqTpSoLn.eqtpsolnEquipmentDesc,
            //            eqTpSoLn.eqtpsolnEquipmentLicensePlate,
            //            eqTpSoLn.eqtpsolnProjectCode,
            //            eqTpSoLn.eqtpsolnProjectDesc,
            //            eqTpSoLn.eqtpsolnProjactCode,
            //            eqTpSoLn.eqtpsolnProjactDesc,
            //            eqTpSoLn.eqtpsolnLoadingDatetime,
            //            eqTpSoLn.eqtpsolnLoadingLocation,
            //            eqTpSoLn.eqtpsolnLoadingOdometer,
            //            eqTpSoLn.eqtpsolnLoadingOdometerImage,
            //            eqTpSoLn.eqtpsolnLoadingLongitude,
            //            eqTpSoLn.eqtpsolnLoadingLatitude,
            //            eqTpSoLn.eqtpsolnLoadingNotes,
            //            eqTpSoLn.eqtpsolnLoadingSignature,
            //            eqTpSoLn.eqtpsolnStatus,
            //            eqTpSoLn.eqtpsolnInterfaceStatus,
            //            eqTpSoLn.eqtpsolnInterfaceDate,
            //        eqTpSoLn.Id,
            //        }) == 1 ? true : false;
            //}
            return success;
        }

        public bool UpdateStatus(EqTpSoLnLocal eqTpSoLn)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateTransportStatus,
                new object[]
                {
                        eqTpSoLn.eqtpsolnStatus,
                        eqTpSoLn.Id
                }) == 1 ? true : false;

            return success;
        }

        public bool UpdateInterfaceStatus(string interfaceStatus, int id)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateIssuesInterface,
                new object[]
                {
                        interfaceStatus,
                        id
                }) == 1 ? true : false;

            return success;
        }

        public EqTpSoLnLocal GetEqTpSoLnIsLast()
        {
            return App.dbConn.Query<EqTpSoLnLocal>(DatabaseHelper.GetAllEqTpSoLns, new object[] { }).LastOrDefault();
        }

        public FlstIs GetFlstIsLastRefno(string shiftRefno)
        {
            return App.dbConn.Query<FlstIs>(DatabaseHelper.GetAllFlstIsRefno, new object[] { shiftRefno }).LastOrDefault();
        }

        public FlstIs GetFlstIsFirstRefno(string shiftRefno)
        {
            return App.dbConn.Query<FlstIs>(DatabaseHelper.GetAllFlstIsRefno, new object[] { shiftRefno }).FirstOrDefault();
        }

        //public FlstIs GetFlstIsFirst()
        //{
        //    return App.dbConn.Query<FlstIs>(DatabaseHelper.GetAllFlstIs, new object[] { }).FirstOrDefault();
        //}

        public FlstIs GetFlstIsById(int flstIsId)
        {
            return App.dbConn.Query<FlstIs>(DatabaseHelper.GetFlstIs, new object[] { flstIsId }).FirstOrDefault();
        }

        //public FlstIs GetFlstIsById1(string FlstisRefno)
        //{
        //    return App.dbConn.Query<FlstIs>(DatabaseHelper.GetEqTpSoLnByRefno, new object[] { FlstisRefno }).FirstOrDefault();
        //}

        public EqTpSoLnLocal GetEqTpSoLnByRefno(string eqtpsolnRefno)
        {
            return App.dbConn.Query<EqTpSoLnLocal>(DatabaseHelper.GetEqTpSoLnByRefno, new object[] { eqtpsolnRefno }).FirstOrDefault();
        }

        public List<EqTpSoLnLocal> GetAllEqTpSoLns()
        {
            List<EqTpSoLnLocal> eqTpSoLn = new List<EqTpSoLnLocal>();
            eqTpSoLn = App.dbConn.Query<EqTpSoLnLocal>(DatabaseHelper.GetAllEqTpSoLns, new object[] { });
            return eqTpSoLn;
        }

        public List<EqTpSoLnLocal> GetAllIssuesByShift(string orderrefno)
        {
            List<EqTpSoLnLocal> eqTpSoLns = new List<EqTpSoLnLocal>();
            eqTpSoLns = App.dbConn.Query<EqTpSoLnLocal>(DatabaseHelper.GetAllFlstIsById, new object[] { orderrefno });
            return eqTpSoLns;
        }
    }
}
