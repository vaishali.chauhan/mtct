﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System.Collections.Generic;
using System.Linq;
namespace MTCT.Services
{
    public class EqptHaulerRepository
    {
        public List<EqTpHaulers> GetEqTpHaulers()
        {
            List<EqTpHaulers> eqTpHaulers = new List<EqTpHaulers>();
            eqTpHaulers = App.dbConn.Query<EqTpHaulers>(DatabaseHelper.Hauler, new object[] { });
            return eqTpHaulers;
        }

        public EqTpHaulers GetEqTpHauler()
        {
            return App.dbConn.Query<EqTpHaulers>(DatabaseHelper.Hauler, new object[] { }).FirstOrDefault();
        }

        public EqTpHaulers GetHaulerByEqtphaulerCode(string haulerCode)
        {
            return App.dbConn.Query<EqTpHaulers>(DatabaseHelper.GetHaulerByEqtphaulerCode, new object[] { haulerCode }).FirstOrDefault();
        }

        public bool UpdateHaulerDescbyHaulerCode(string desc, int id)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateHaulerDesc,
                new object[]
                {
                    desc,
                    id
                }) == 1 ? true : false;
            return success;
        }

        public List<EqTpHaulers> GetEqTpHaulersbyName(string searchValue)
        {
            List<EqTpHaulers> driver = new List<EqTpHaulers>();
            driver = App.dbConn.Query<EqTpHaulers>(string.Format(DatabaseHelper.SearchHauler, searchValue), new object[] { });
            return driver;
        }

        public int DeleteEqTpHaulers(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteHauler, new object[] { id });
            return success;
        }

        public bool SaveEqTpHaulers(EqTpHaulers mEqTpHaulers)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertHauler,
                new object[]
                {
                    mEqTpHaulers.EqtphaulerId,
                    mEqTpHaulers.EqtphaulerCode,
                    mEqTpHaulers.EqtphaulerDesc,
                    mEqTpHaulers.EqtphaulerAppRefCode,
                    mEqTpHaulers.EqtphaulerActiveStatus
                }) == 1 ? true : false;
            return success;
        }

        public bool UpdateEqTpHauler(EqTpHaulers mEqTpHaulers)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateHauler,
                new object[]
                {
                    mEqTpHaulers.EqtphaulerCode,
                    mEqTpHaulers.EqtphaulerDesc,
                    mEqTpHaulers.EqtphaulerAppRefCode,
                    mEqTpHaulers.EqtphaulerActiveStatus,
                    mEqTpHaulers.EqtphaulerId
                }) == 1 ? true : false;
            return success;
        }
    }
}
