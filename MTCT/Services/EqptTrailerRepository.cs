﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MTCT.Services
{
    public class EqptTrailerRepository
    {
        public List<EqTpTrailers> GetEqTpTrailers()
        {
            List<EqTpTrailers> eqTpHaulers = new List<EqTpTrailers>();
            eqTpHaulers = App.dbConn.Query<EqTpTrailers>(DatabaseHelper.Trailer, new object[] { });
            return eqTpHaulers;
        }

        public List<EqTpTrailers> GetEqTpTrailersByHaulerId(int haulerId)
        {
            List<EqTpTrailers> eqTpHaulers = new List<EqTpTrailers>();
            eqTpHaulers = App.dbConn.Query<EqTpTrailers>(DatabaseHelper.GerTrailerById, new object[] { haulerId });
            return eqTpHaulers;
        }

        public EqTpTrailers GerTrailerByEqtptrailerCode(string trailerCode)
        {
            var mEqTpTrailers = App.dbConn.Query<EqTpTrailers>(DatabaseHelper.GerTrailerByEqtptrailerCode, new object[] { trailerCode }).FirstOrDefault();
            return mEqTpTrailers;
        }

        public List<EqTpTrailers> GetEqTpTrailersbyName(string searchValue)
        {
            List<EqTpTrailers> driver = new List<EqTpTrailers>();
            driver = App.dbConn.Query<EqTpTrailers>(string.Format(DatabaseHelper.SearchTrailer, searchValue), new object[] { });
            return driver;
        }

        public int DeleteEqTpTrailers(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteTrailer, new object[] { id });
            return success;
        }
        public bool SaveEqTpTrailers(EqTpTrailers mEqTpHaulers)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertTrailer,
                new object[]
                {
                    mEqTpHaulers.EqtptrailerId,
                    mEqTpHaulers.EqtphaulerId,
                    mEqTpHaulers.EqtptrailerCode,
                    mEqTpHaulers.EqtptrailerDesc,
                    mEqTpHaulers.EqtptrailerActiveStatus,
                }) == 1 ? true : false;
            return success;
        }
    }
}
