﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System.Collections.Generic;
using System.Linq;

namespace MTCT.Services
{
    public class EqtpRepository
    {
        public List<Eqtp> GetEqTps()
        {
            List<Eqtp> eqTps = new List<Eqtp>();
            eqTps = App.dbConn.Query<Eqtp>(DatabaseHelper.EqtpStation, new object[] { });
            return eqTps;
        }

        public Eqtp GetEqTp()
        {
            return App.dbConn.Query<Eqtp>(DatabaseHelper.EqtpStation, new object[] { }).FirstOrDefault();
        }

        public bool SaveEqTp(Eqtp mEqTp)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertEqtp,
                new object[]
                {
                    mEqTp.EqtpId,
                    mEqTp.EqtpCode,
                    mEqTp.EqtpName,
                    mEqTp.EqtpRfidEnabled,
                    mEqTp.EqtpActiveStatus,
                    mEqTp.EqtpOrderCounterNo,
                    mEqTp.EqtpTransportCounterNo,
                    mEqTp.EqtpOrderPrint,
                    mEqTp.EqtpTransportPrint,
                    mEqTp.EqtpDeviceCode,
                    mEqTp.PrintFormat,
                    mEqTp.EqtpSignEnabled
                }) == 1 ? true : false;
            return success;
        }

        public bool UpdateEqTp(Eqtp mEqTp)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateEqtp,
                new object[]
                {
                    mEqTp.EqtpOrderCounterNo,
                    mEqTp.EqtpTransportCounterNo,
                    mEqTp.EqtpOrderPrint,
                    mEqTp.EqtpTransportPrint,
                    mEqTp.EqtpDeviceCode,
                    mEqTp.PrintFormat,
                    mEqTp.EqtpSignEnabled,
                    mEqTp.EqtpId
                }) == 1 ? true : false;
            return success;
        }

        public int UpdateEqtpCounter(Eqtp mEqTp)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.EqtpOrderCounter, new object[] { mEqTp.EqtpOrderCounterNo, mEqTp.EqtpId });
            return success;
        }

        public int UpdateEqtpTransportCounter(Eqtp mEqTp)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.EqtpTransportCounter, new object[] { mEqTp.EqtpTransportCounterNo, mEqTp.EqtpId });
            return success;
        }

        public int DeleteEqtp(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteEqtp, new object[] { id });
            return success;
        }
    }
}
