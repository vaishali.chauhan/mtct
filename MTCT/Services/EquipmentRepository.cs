﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTCT.Services
{
    public class EquipmentRepository
    {
        public List<Equipment> GetEquipments()
        {
            List<Equipment> equipment = new List<Equipment>();
            equipment = App.dbConn.Query<Equipment>(DatabaseHelper.Equipment, new object[] { });
            return equipment;
        }

        public List<Equipment> GetEquipmentRFID(string euipmentRFID,string euipmentCode,string euipmentName)
        {
            List<Equipment> equipment = new List<Equipment>();
            equipment = App.dbConn.Query<Equipment>(string.Format(DatabaseHelper.SearchEquipment, euipmentRFID, euipmentCode, euipmentName), new object[] { });
            return equipment;
        }

        public List<Equipment> GetEquipmentRFID(string searchValue)
        {
            List<Equipment> equipment = new List<Equipment>();
            equipment = App.dbConn.Query<Equipment>(string.Format(DatabaseHelper.SearchEquipment, searchValue), new object[] { });
            return equipment;
        }

        public List<EqTpHaulers> GetEquipmentHaulers()
        {
            List<EqTpHaulers> haulers = new List<EqTpHaulers>();
            haulers = App.dbConn.Query<EqTpHaulers>(DatabaseHelper.Hauler, new object[] { });
            return haulers;
        }

        public List<EqTpHaulers> GetEquipmentHaulers(string searchValue)
        {
            List<EqTpHaulers> haulers = new List<EqTpHaulers>();
            haulers = App.dbConn.Query<EqTpHaulers>(string.Format(DatabaseHelper.SearchHauler, searchValue), new object[] { });
            return haulers;
        }

        public List<EqTpTrailers> GetEquipmentTrailers()
        {
            List<EqTpTrailers> trailers = new List<EqTpTrailers>();
            trailers = App.dbConn.Query<EqTpTrailers>(DatabaseHelper.Trailer, new object[] { });
            return trailers;
        }
        public List<EqTpTrailers> GetEquipmentTrailers(string searchValue)
        {
            List<EqTpTrailers> haulers = new List<EqTpTrailers>();
            haulers = App.dbConn.Query<EqTpTrailers>(string.Format(DatabaseHelper.SearchTrailer, searchValue), new object[] { });
            return haulers;
        }
        public int DeleteEquipment(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteEquipment, new object[] { id });
            return success;
        }
        public bool SaveEquipment(Equipment mEquipment)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertEquipment,
                new object[]
                {
                    mEquipment.EquipmentId,
                    mEquipment.EquipmentCode,
                    mEquipment.EquipmentDesc,
                    mEquipment.EquipmentLicensePlate,
                    mEquipment.EquipmentRFID,
                    mEquipment.EquipmentFltankCapacity,
                    mEquipment.EquipmentActiveStatus,
                    mEquipment.equipmentAppRefCode,
                }) == 1 ? true : false;
            return success;
        }
    }
}
