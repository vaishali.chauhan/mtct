﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTCT.Services
{
    public class FlstFuelRepository
    {
        public List<FlstFuel> GetFlstFuels()
        {
            List<FlstFuel> flstFuel = new List<FlstFuel>();
            flstFuel = App.dbConn.Query<FlstFuel>(DatabaseHelper.GetAllFlstFuel, new object[] { }).ToList();
            return flstFuel;
        }

        public bool SaveFlstFuel(FlstFuel mFlstFuel)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertFlstFuel,
                new object[]
                {
                    mFlstFuel.FlstfuelId,
                    mFlstFuel.FlstId,
                    mFlstFuel.FlstfuelPumpCode,
                    mFlstFuel.FlstfuelFuelCode,
                    mFlstFuel.FlstfuelActiveStatus
                }) == 1 ? true : false;
            return success;
        }
        public int DeleteFlstFuelByFlstId(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteFlstFuel, new object[] { id });
            return success;
        } 
        public int DeleteFlstFuelById(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteFlstFuelById, new object[] { id });
            return success;
        }
    }
}
