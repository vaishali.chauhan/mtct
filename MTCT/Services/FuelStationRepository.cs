﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System.Collections.Generic;
using System.Linq;

namespace MTCT.Services
{
    public class FuelStationRepository
    {
        public List<Flst> GetAllFlst()
        {
            List<Flst> flst = new List<Flst>();
            flst = App.dbConn.Query<Flst>(DatabaseHelper.FuelStation, new object[] { });
            return flst;
        }

        public bool SaveFlst(Flst mFlst)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertFlst,
                new object[]
                {
                    mFlst.FlstId,
                    mFlst.FlstCode,
                    mFlst.FlstDesc,
                    mFlst.FlstDeviceCode,
                    mFlst.FlstShiftCounter,
                    mFlst.FlstReceiptsCounter,
                    mFlst.FlstIssuesCounter,
                    mFlst.FlstAdjustmentsCounter,
                    mFlst.FlstTransfersCounter,
                    mFlst.FlstIssuesPrintCopies,
                    mFlst.FlstShiftPrintCopies,
                    mFlst.PrintFormat
                }) == 1 ? true : false;
            return success;
        }

        public bool UpdateFlst(Flst mFlst)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateFlst, 
                new object[] 
                { 
                    mFlst.FlstDeviceCode, 
                    mFlst.FlstShiftCounter, 
                    mFlst.FlstReceiptsCounter, 
                    mFlst.FlstIssuesCounter, 
                    mFlst.FlstAdjustmentsCounter, 
                    mFlst.FlstTransfersCounter, 
                    mFlst.FlstIssuesPrintCopies, 
                    mFlst.FlstShiftPrintCopies, 
                    mFlst.PrintFormat,
                    mFlst.FlstId 
                }) == 1 ? true : false;
            return success;
        }

        public bool UpdateFlstDescbyFlstCode(string desc, int id)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.UpdateFlstDesc, 
                new object[]
                { 
                    desc, 
                    id 
                }) == 1 ? true : false;
            return success;
        }

        public Flst GetFuelCode(string flstcode)
        {
            return App.dbConn.Query<Flst>(DatabaseHelper.FlstCode, new object[] { flstcode }).FirstOrDefault();
        }

        public Flst GetFuelStation()
        {
            return App.dbConn.Query<Flst>(DatabaseHelper.FuelStation, new object[] { }).FirstOrDefault();
        }

        public Flst GetFlst()
        {
            return App.dbConn.Query<Flst>(DatabaseHelper.FuelStation, new object[] { }).LastOrDefault();
        }

        public Flst GetFlstIssuesRefno()
        {
            return App.dbConn.Query<Flst>(DatabaseHelper.FlstIsssueRefno, new object[] { }).FirstOrDefault();
        }

        public Flst GetFlstShiftRefno()
        {
            return App.dbConn.Query<Flst>(DatabaseHelper.FlstShiftRefno, new object[] { }).FirstOrDefault();
        }

        public int UpdateFlstShiftCounter(Flst mFlst)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.FlstShiftCounter, new object[] { mFlst.FlstShiftCounter, mFlst.FlstId });
            return success;
        }

        public int UpdateFlstIssuesCounter(Flst mFlst)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.FlstIsssueCounter, new object[] { mFlst.FlstIssuesCounter, mFlst.FlstId });
            return success;
        }
        //public bool UpdateFlstIssuesCounter(string flstIssuesCounter, int id)
        //{
        //    bool success = false;
        //    success = App.dbConn.Execute(DatabaseHelper.FlstIsssueCounter,
        //        new object[]
        //        {
        //                flstIssuesCounter,
        //                id
        //        }) == 1 ? true : false;

        //    return success;
        //}

        public int UpdateFlstIssuesPrint(Flst mFlst)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.FlstPrintCount, new object[] { mFlst.FlstIssuesPrintCopies, mFlst.FlstId });
            return success;
        }
    }
}
