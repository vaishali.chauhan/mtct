﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTCT.Services
{
    public class LocationRepository
    {
        public List<Locations> GetLocationss()
        {
            List<Locations> locations = new List<Locations>();
            locations = App.dbConn.Query<Locations>(DatabaseHelper.Location, new object[] { });
            return locations;
        }

        public List<Locations> GetLocationsbyName(string searchValue)
        {
            List<Locations> driver = new List<Locations>();
            driver = App.dbConn.Query<Locations>(string.Format(DatabaseHelper.SearchLocation, searchValue), new object[] { });
            return driver;
        }

        public int DeleteLocations(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteLocation, new object[] { id });
            return success;
        }

        public bool SaveLocations(Locations mLocations)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertLocation,
                new object[]
                {
                    mLocations.EqtplocationId,
                    mLocations.EqtplocationCode,
                    mLocations.EqtplocationAppRefCode,
                    mLocations.EqtplocationActiveStatus,
                }) == 1 ? true : false;
            return success;
        }
    }
}
