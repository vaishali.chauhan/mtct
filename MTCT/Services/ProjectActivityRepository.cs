﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTCT.Services
{
    public class ProjectActivityRepository
    {
        public List<ProjectActivity> GetProjectActivity()
        {
            List<ProjectActivity> projectsActivity = new List<ProjectActivity>();
            projectsActivity = App.dbConn.Query<ProjectActivity>(DatabaseHelper.ProjectActivity, new object[] { });
            return projectsActivity;
        }

        public List<ProjectActivity> GetProjectActivityByProjectId(int projectId)
        {
            List<ProjectActivity> projectsActivity = new List<ProjectActivity>();
            projectsActivity = App.dbConn.Query<ProjectActivity>(string.Format(DatabaseHelper.ProjectActivityByProjectId, projectId), new object[] { });
            return projectsActivity;
        }


        public List<ProjectActivity> GetProjectActivitybyCode(string searchValue)
        {
            List<ProjectActivity> projectsActivity = new List<ProjectActivity>();
            projectsActivity = App.dbConn.Query<ProjectActivity>(string.Format(DatabaseHelper.SearchProjectActivity, searchValue), new object[] { });
            return projectsActivity;
        }

        public int DeleteProjectActivity(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteProjectActivity, new object[] { id });
            return success;
        }
        public bool SaveProjectActivity(ProjectActivity mProject)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertProjectActivity,
                new object[]
                {
                    mProject.projActId,
                    mProject.projectId,
                    mProject.projactCode,
                    mProject.projactDesc,
                    mProject.projactActiveStatus,
                }) == 1 ? true : false;
            return success;
        }
    }
}
