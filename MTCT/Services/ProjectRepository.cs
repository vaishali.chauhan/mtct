﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System.Collections.Generic;

namespace MTCT.Services
{
    public class ProjectRepository
    {
        public List<Project> GetProjects()
        {
            List<Project> projects = new List<Project>();
            projects = App.dbConn.Query<Project>(DatabaseHelper.Project, new object[] { });
            return projects;
        }

        public List<Project> GetProjectbyName(string searchValue)
        {
            List<Project> projects = new List<Project>();
            projects = App.dbConn.Query<Project>(string.Format(DatabaseHelper.SearchProject, searchValue), new object[] { });
            return projects;
        }

        public int DeleteProjects(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteProject, new object[] { id });
            return success;
        }
        public bool SaveProjects(Project mProject)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertProject,
                new object[]
                {
                    mProject.ProjectId,
                    mProject.ProjectCode,
                    mProject.ProjectDesc,
                    mProject.ProjectAppRefCode,
                    mProject.ProjectActiveStatus,
                }) == 1 ? true : false;
            return success;
        }
    }
}
