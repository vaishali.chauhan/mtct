﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System.Collections.Generic;
using System.Linq;

namespace MTCT.Services
{
    public class ShiftRepository
    {
        public bool SaveStartShift(EqTpSo mEqTpSo)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.StartShiftInsert,
                new object[]
                {
                    mEqTpSo.EqTpSoId,
                    mEqTpSo.eqtpsoEqtpCode,
                    mEqTpSo.eqtpsoDeviceCode,
                    mEqTpSo.eqtpsoEqtphaulerCode,
                    mEqTpSo.eqtpsoEqtptrailerCode,
                    mEqTpSo.eqtpsoRefno,
                    mEqTpSo.eqtpsoDriverCode,
                    mEqTpSo.eqtpsoDriverName,
                    mEqTpSo.eqtpsoUserCode,
                    mEqTpSo.eqtpsoUserName,
                    mEqTpSo.eqtpsoOpeningDate,
                    mEqTpSo.eqtpsoOpeningLocation,
                    mEqTpSo.eqtpsoOpeningOdometer,
                    mEqTpSo.eqtpsoOpeningOdometerImage,
                    mEqTpSo.eqtpsoOpeningLongitude,
                    mEqTpSo.eqtpsoOpeningLatitude,
                    mEqTpSo.eqtpsoOpeningNotes,
                    mEqTpSo.eqtpsoOpeningSignature,
                    mEqTpSo.eqtpsoStatus,
                    mEqTpSo.eqtpsoInterfaceStatus,
                    mEqTpSo.eqtpsoInterfaceDate
                }) == 1 ? true : false;
            return success;
        }

        public bool UpdateInterfaceStatus(string interfaceCode, int id, bool isServer)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.StartShiftUpdate,
                new object[]
                {
                        interfaceCode,
                        isServer,
                        id,
                }) == 1 ? true : false;
            return success;
        }

        public bool SaveCloseOrder(EqTpSo mEqTpSo)
        {
            bool success = false;
            //var mShift = GetStartShift();
            if (mEqTpSo != null)
            {
                success = App.dbConn.Execute(DatabaseHelper.StartUpdateOrder,
                   new object[]
                   {
                        mEqTpSo.eqtpsoClosingDate,
                        mEqTpSo.eqtpsoClosingLocation,
                        mEqTpSo.eptpsoClosingOdometer,
                        mEqTpSo.eqtpsoClosingOdometerImage,
                        mEqTpSo.eqtpsoClosingLongitude,
                        mEqTpSo.eqtpsoClosingLatitude,
                        mEqTpSo.eqtpsoClosingNotes,
                        mEqTpSo.eqtpsoClosingSignature,
                        mEqTpSo.eqtpsoStatus,
                        mEqTpSo.eqtpsoInterfaceStatus,
                        mEqTpSo.eqtpsoInterfaceDate,
                        mEqTpSo.EqTpSoId
                   }) == 1 ? true : false;
            }
            return success;
        }

        public EqTpSo GetFlstShiftRefno()
        {
            return App.dbConn.Query<EqTpSo>(DatabaseHelper.StartShiftRefno, new object[] { }).FirstOrDefault();
        }

        public EqTpSo GetStartShift()
        {
            return App.dbConn.Query<EqTpSo>(DatabaseHelper.GetShift, new object[] { }).LastOrDefault();
        }

        public EqTpSo GetStartShiftByFlstshiftRefno(string FlstshiftRefno)
        {
            return App.dbConn.Query<EqTpSo>(DatabaseHelper.GetShiftbyFlstShiftRefno, new object[] { FlstshiftRefno }).LastOrDefault();
        }

        public List<EqTpSo> GetAllEqTpSo()
        {
            List<EqTpSo> flStShifts = new List<EqTpSo>();
            flStShifts = App.dbConn.Query<EqTpSo>(DatabaseHelper.GetShift, new object[] { });
            return flStShifts;
        }

        public EqTpSo GetOpenOrder(string status)
        {
            return App.dbConn.Query<EqTpSo>(DatabaseHelper.EqTpSoOrder, new object[] { status }).FirstOrDefault();
        }
    }
}
