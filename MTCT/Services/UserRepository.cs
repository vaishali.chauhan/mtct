﻿using MTCT.DatabaseService;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTCT.Services
{
    public class UserRepository
    {
        public User Login(string userCode, string password)
        {
            return App.dbConn.Query<User>(DatabaseHelper.Login, new object[] { userCode, password }).FirstOrDefault();
        }

        public List<User> GetAllUser()
        {
            List<User> user = new List<User>();
            user = App.dbConn.Query<User>(DatabaseHelper.User, new object[] { });
            return user;
        }

        public int DeleteUser(int id)
        {
            int success = 0;
            success = App.dbConn.Execute(DatabaseHelper.DeleteUser, new object[] { id });
            return success;
        }
        public bool SaveUser(User mUser)
        {
            bool success = false;
            success = App.dbConn.Execute(DatabaseHelper.InsertUser,
                new object[]
                {
                    mUser.UserId,
                    mUser.UserName,
                    mUser.UserEmail,
                    mUser.UserPassword,
                    mUser.UserRole,
                    mUser.UserCode,
                    mUser.appuserAppRefCode,
                    mUser.appuserActiveStatus
                }) == 1 ? true : false;
            return success;
        }
    }
}
