﻿using MTCT.API;
using MTCT.Models;
using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Services;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MTCT.Utilties
{
    public static class Common
    {

        public static MasterDetailPage masterDetail { get; set; }

        //public static string Token { get; set; } = string.Empty;
        public static string LanguageCode { get; set; } = "es";
        public static string OpenStatus { get; set; } = "Open";
        public static string CloseStatus { get; set; } = "Closed";
        public static string VoidStatus { get; set; } = "Void";

        public static bool isBluetoothRemember { get; set; } = false;
        public static string Bluetoothaddress { get; set; }
        public static BluetoothList bluetoothList { get; set; }

        public static AppKey mAppkey { get; set; }

        public static string barcode { get; set; }

        public static string ConvertSpainishToEnglish(string name)
        {
            if (name == EqTpSoLnStatus.Anulado.ToString())
                return "Voided";
            else if (name == EqTpSoLnStatus.Procesado.ToString())
                return "Processed";
            else if (name == InterfaceStatus.Pendiente.ToString())
                return "Pending";
            else if (name == InterfaceStatus.Completado.ToString())
                return "Completed";
            else
                return string.Empty;
        }

        public static User mUser = new User();
        public static Flst mFlst = new Flst();
        public static Eqtp mEqtp = new Eqtp();
        public static List<Models.APIModels.FlSt> mFlsts = new List<Models.APIModels.FlSt>();
        public static EqTpSo mFlStShifts = new EqTpSo();
        public static string userRole;

        public static bool HasInternetConnection()
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                return false;
            }
        }

        public async static Task<bool> sessionExpired()
        {
            bool isTokenUpdate = false;
            AppConfigRepository appConfigRepository = new AppConfigRepository();
            LoginRequiest loginRequiest = new LoginRequiest();
            LoginAPI loginAPI = new LoginAPI();

            var mConfigId = appConfigRepository.GetAppConfig();
            if (mConfigId != null)
            {
                loginRequiest.username = mConfigId.AppConfigUserName;
                loginRequiest.password = mConfigId.AppConfigPassword;

                var response = await loginAPI.Authentication(loginRequiest);
                if (response != null)
                {
                    isTokenUpdate = true;
                    Settings.Token = response.token;
                }
            }
            return isTokenUpdate;
        }
    }

    public enum UserRole
    {
        Admin,
        User
    }

    public enum EqTpSoLnStatus
    {
        Procesado,
        Anulado
    }

    public enum InterfaceStatus
    {
        Pendiente,
        Completado
    }
}
