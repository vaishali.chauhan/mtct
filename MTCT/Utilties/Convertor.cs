﻿using MTCT.Models.APIModels;
using MTCT.Models.DBModel;
using MTCT.Models.LocalModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace MTCT.Utilties
{
    public static class Convertor
    {
        public static string ToDecimalString(this decimal value)
        {
            return value.ToString("", CultureInfo.InvariantCulture);
        }

        public static string ToDoubleString(this double value)
        {
            return value.ToString("", CultureInfo.InvariantCulture);
        }


        public static EqTpSoLnModel ToEqTpSoLnModel(this EqTpSoLn mEqTpSoLn)
        {
            EqTpSoLnModel eqTpSoLnModel = new EqTpSoLnModel();
            eqTpSoLnModel.eqtpsolnEqtpCode = mEqTpSoLn.eqtpsolnEqtpCode;
            eqTpSoLnModel.eqtpsolnDeviceCode = mEqTpSoLn.eqtpsolnDeviceCode;
            eqTpSoLnModel.eqtpsolnEqtphaulerCode = mEqTpSoLn.eqtpsolnEqtphaulerCode;
            eqTpSoLnModel.eqtpsolnEqtptrailerCode = mEqTpSoLn.eqtpsolnEqtptrailerCode;
            eqTpSoLnModel.eqtpsolnEqtpsoRefno = mEqTpSoLn.eqtpsolnEqtpsoRefno;
            eqTpSoLnModel.eqtpsolnRefno = mEqTpSoLn.eqtpsolnRefno;
            eqTpSoLnModel.eqtpsolnBarcode = mEqTpSoLn.eqtpsolnBarcode;
            eqTpSoLnModel.eqtpsolnSecurityCode = mEqTpSoLn.eqtpsolnSecurityCode;
            eqTpSoLnModel.eqtpsolnEquipmentRfid = mEqTpSoLn.eqtpsolnEquipmentRfid;
            eqTpSoLnModel.eqtpsolnEquipmentCode = mEqTpSoLn.eqtpsolnEquipmentCode;
            eqTpSoLnModel.eqtpsolnEquipmentDesc = mEqTpSoLn.eqtpsolnEquipmentDesc;
            eqTpSoLnModel.eqtpsolnEquipmentLicensePlate = mEqTpSoLn.eqtpsolnEquipmentLicensePlate;
            eqTpSoLnModel.eqtpsolnProjectCode = mEqTpSoLn.eqtpsolnProjectCode;
            eqTpSoLnModel.eqtpsolnProjectDesc = mEqTpSoLn.eqtpsolnProjectDesc;
            eqTpSoLnModel.eqtpsolnProjactCode = mEqTpSoLn.eqtpsolnProjactCode;
            eqTpSoLnModel.eqtpsolnProjactDesc = mEqTpSoLn.eqtpsolnProjactDesc;
            eqTpSoLnModel.eqtpsolnLoadingDatetime = Convert.ToDateTime(mEqTpSoLn.eqtpsolnLoadingDatetime);
            eqTpSoLnModel.eqtpsolnLoadingLocation = mEqTpSoLn.eqtpsolnLoadingLocation;
            eqTpSoLnModel.eqtpsolnLoadingLongitude = mEqTpSoLn.eqtpsolnLoadingLongitude;
            eqTpSoLnModel.eqtpsolnLoadingLatitude = mEqTpSoLn.eqtpsolnLoadingLatitude;
            eqTpSoLnModel.eqtpsolnLoadingOdometer = mEqTpSoLn.eqtpsolnLoadingOdometer;
            eqTpSoLnModel.eqtpsolnLoadingOdometerImage = mEqTpSoLn.eqtpsolnLoadingOdometerImage;
            eqTpSoLnModel.eqtpsolnLoadingNotes = mEqTpSoLn.eqtpsolnLoadingNotes;
            eqTpSoLnModel.eqtpsolnLoadingSignature = mEqTpSoLn.eqtpsolnLoadingSignature;
            eqTpSoLnModel.eqtpsolnStatus = Common.ConvertSpainishToEnglish(mEqTpSoLn.eqtpsolnStatus);
            eqTpSoLnModel.eqtpsolnInterfaceStatus = Common.ConvertSpainishToEnglish(InterfaceStatus.Completado.ToString()); 
            eqTpSoLnModel.eqtpsolnInterfaceDate = mEqTpSoLn.eqtpsolnInterfaceDate;
            if (mEqTpSoLn.eqtpsolnUnloadingDatetime != null && mEqTpSoLn.eqtpsolnUnloadingDatetime != DateTime.MinValue)
                eqTpSoLnModel.eqtpsolnUnloadingDatetime = Convert.ToDateTime(mEqTpSoLn.eqtpsolnUnloadingDatetime);
            eqTpSoLnModel.eqtpsolnUnloadingLocation = mEqTpSoLn.eqtpsolnUnloadingLocation;
            eqTpSoLnModel.eqtpsolnUnloadingLongitude = mEqTpSoLn.eqtpsolnUnloadingLongitude;
            eqTpSoLnModel.eqtpsolnUnloadingLatitude = mEqTpSoLn.eqtpsolnUnloadingLatitude;
            eqTpSoLnModel.eqtpsolnUnloadingOdometer = mEqTpSoLn.eqtpsolnUnloadingOdometer;
            eqTpSoLnModel.eqtpsolnUnloadingOdometerImage = mEqTpSoLn.eqtpsolnUnloadingOdometerImage;
            eqTpSoLnModel.eqtpsolnUnloadingNotes = mEqTpSoLn.eqtpsolnUnloadingNotes;
            eqTpSoLnModel.eqtpsolnUnLoadingSignature = mEqTpSoLn.eqtpsolnUnLoadingSignature;
            return eqTpSoLnModel;
        }

    }
}
