﻿using Acr.UserDialogs;
using MTCT.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MTCT.Utilties
{
    public static class DisplayInfo
    {
        public static void InfoMessage(string successMessage)
        {
            UserDialogs.Instance.Toast(new ToastConfig(successMessage)
            {
                BackgroundColor = Color.Black,
                MessageTextColor = Color.White,
                Duration = TimeSpan.FromSeconds(3),
            });
        }

        public static void ErrorMessage1(string errormessage)
        {
            UserDialogs.Instance.Toast(new ToastConfig(errormessage)
            {
                BackgroundColor = Color.FromHex("#FF3333"),
                Duration = TimeSpan.FromSeconds(6),
            });
        }
        public static void ErrorMessage(string errormessage)
        {
            UserDialogs.Instance.Toast(new ToastConfig(errormessage)
            {
                BackgroundColor = Color.FromHex("#FF3333"),
                Duration = TimeSpan.FromSeconds(3),
            });
        }
        public static void SuccessMessage(string successmessage)
        {
            UserDialogs.Instance.Toast(new ToastConfig(successmessage)
            {
                BackgroundColor = Color.FromHex("#37996a"),
                Duration = TimeSpan.FromSeconds(1.5),
            });
        }



        public static async System.Threading.Tasks.Task<bool> BluetoothPermission()
        {
            bool isConnect = false;
            var myAction = await App.Current.MainPage.DisplayAlert("Bluetooth", "Please turn on bluetooth", "OK", "CANCELAR");
            if (myAction)
            {
                if (Device.RuntimePlatform == global::Xamarin.Forms.Device.Android)
                {
                    DependencyService.Get<IOpenBluetoothPage>().OpenBluetoothSettingPage();
                    isConnect = true;
                }
            }
            else
            {

            }
            return isConnect;
        }

        public static void ShowLoader(string message = "Loading...")
        {
            UserDialogs.Instance.ShowLoading(message);
        }

        public static void HideLoader()
        {
            UserDialogs.Instance.HideLoading();
        }

    }
}
