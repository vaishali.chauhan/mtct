﻿namespace MTCT.Utilties
{
    public class EndPointsMessage
    {
        #region Success
        public const string LoginSuccess = "Autentica con éxito.";
        public const string Success = "Guardar con éxito.";
        public const string OrderSuccess = "Orden fue abierto exitosamente.";
        public const string OrderSuccessLocal = "Orden guardado localmente.";
        public const string OrderCloseSuccess = "Orden fue cerrada exitosamente.";
        public const string TransportSuccess = "Transporte fue grabado exitosamente.";
        public const string DownloadSuccess = "Descarga de equipo grabada exitosamente.";
        public const string TransportSuccessLocal = "Transporte fue grabado localmente.";
        public const string SyncronizationSuccess = "Sincronización completada.";
        #endregion

        #region Error
        public const string LoginError = "Usuario o Password invalido.";
        public const string Validation = "Campos son requeridos.";
        public const string StartOrderError = "Orden no Grabar.";
        public const string CloseOrderError = "Orden no fue cerrado.";
        public const string ShiftTransportError = "Transporte Despacho no Grabar.";
        public const string Error = "Algo salió mal.";
        public const string SyncronizationError = "Sincronización no completada.";
        public const string DeviceCode = "El código de dispositivo es requerido.";
        public const string Transport = "Transporte de Equipo es requerido.";
        public const string URL = "URL es requerido.";
        public const string URLMsg = "URL es requerido.";
        public const string UserMsg = "Usuario es requerido.";
        public const string PasswordMsg = "Contraseña es requerido.";


        public const string DispenserCounter1 = "Dispensador es requerido.";
        public const string DispenserCounter2 = "Dispensador es requerido.";
        public const string DispenserCounter3 = "Dispensador es requerido.";
        public const string Notes = "Notas son requeridas.";
        public const string Signature = "Firma es requerida.";
        public const string RFID = "Rfid es requerido.";
        public const string ProjectCode = "Proyecto es requerido.";
        public const string Projact = "Partida es requerido.";
        public const string Fule = "Dispensador es requerido.";
        public const string Qty = "Cantidad es requerida.";
        public const string Picture = "Foto es requerida";
        public const string Location = "Localización es requerida.";
        public const string Hauler = "Hauler es requerida.";
        public const string Trailer = "Cola es requerida.";
        public const string OpeningDate = "Fecha Apertura es requerida.";
        public const string CloseingDate = "Fecha Cierre es requerida.";
        public const string UnLoadingDate = "Fecha es requerida.";
        public const string Driver = "Código del Chofer es requerida.";
        public const string Odometer = "Kilometraje es requerido.";
        public const string OdometerNotLess = "El Kilometraje no puede ser menor que el Kilometraje de carga.";
        #endregion

        #region Notes
        public const string IssueInfo = "Primero comience el turno. Entonces puedes guardar expedir.";
        public const string DeviceStatus = "El dispositivo no está disponible.";
        public const string StatusVoid = "El estado es nulo, no se puede guardar la descarga.";
        #endregion
    }
}
