﻿namespace MTCT.Utilties
{
    public static class EndPointsUrl
    {
        #region Base Address
       // public const string SandBoxBaseAddress = "http://aasmobile.eastus2.cloudapp.azure.com/AASMobile/api/";
        public static string SandBoxBaseAddress = "";
        #endregion

        #region Login
        public const string USER_LOGIN = "api/Authenticate/login";
        #endregion

        #region Flst
        public const string GET_FLSTS = "api/flst";
        public const string GET_FLST_BY_FLSTCODE = "api/flst/{0}";
        #endregion

        #region Eqtp
        public const string GET_MTCTS = "api/eqtp";
        public const string GET_MTCT_BY_MTCTCODE = "api/eqtp/{0}";
        #endregion

        #region Device
        public const string GET_DEVICE_BY_DEVICECODE = "api/appdevice/{0}";
        public const string GET_DEVICES = "api/appdevice";
        public const string POST_DEVICES = "api/appdevice";
        #endregion

        #region Driver
        public const string GET_DRIVERS = "api/driver";
        public const string GET_DRIVER_APP_REF_CODE = "api/driver/GetByAppRefCode/{0}";
        #endregion

        #region Hauler
        public const string GET_Hauler_By_Id = "api/eqtphaulers/{0}";
        public const string GET_Hauler_By_RefCode = "api/eqtphaulers/GetByAppRefCode/{0}";
        #endregion
					
        #region Locations
        public const string GET_Location = "api/eqtplocations/GetByAppRefCode/{0}";
        #endregion

        #region Projects
        public const string GET_Projects = "api/project/GetByAppRefCode/{0}";
        #endregion

        #region Equipment
        public const string GET_EQUIPMENTS = "api/equipment";
        public const string GET_EQUIPMENT_APP_REF_CODE = "api/equipment/GetByAppRefCode/{0}";
        #endregion

        #region User
        public const string GET_USERS = "api/appuser"; 
        public const string GET_USERS_APP_REF_CODE = "api/appuser/GetByAppRefCode/{0}";
        #endregion
        
        #region EqTpSo
        public const string POST_FLSTSHIFT = "api/eqtpso";
        public const string POST_FLSTCLOSESHIFT = "api/eqtpso/eqtpsoClosing";
        #endregion

        #region EqTpSoLn
        public const string POST_MTCTSOLN = "api/eqtpsoln";
        #endregion
    }
}
