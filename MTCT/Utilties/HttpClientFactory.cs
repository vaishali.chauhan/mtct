﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MTCT.Utilties
{
    public class HttpClientFactory : IDisposable
    {
        public HttpClient client;
        public HttpClientFactory(string baseUrl = null, string token = null, string mediaType = null, string deviceToken = null)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 5, 0);

            if (baseUrl == null)
            {
                client.BaseAddress = new Uri(Settings.Url);
            }
            else
            {
                client.BaseAddress = new Uri(baseUrl);
            }

            if (mediaType == null)
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
            else
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));
            }

            if (token != null)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
        }
        public async Task<HttpResponseMessage> PostAsync(string controllerName, string srlzRequest)
        {
            return await client.PostAsync(controllerName, new StringContent(srlzRequest, Encoding.UTF8, "application/json"));
        }
        public void Dispose()
        {
            //GC.Collect();
        }
    }
}
