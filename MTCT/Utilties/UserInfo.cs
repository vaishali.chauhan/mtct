﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MTCT.Utilties
{
    public class UserInfo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public string token;
        public string Token
        {
            get { return Token; }
            set
            {
                token = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Token"));
                Settings.Token = value;
            }
        }

        public string url;
        public string Url
        {
            get { return Url; }
            set
            {
                url = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Url"));
                Settings.Url = value;
            }
        }
    }
    public class Settings
    {
        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }

        private const string TokenKey = "token_key";
        private const string UrlKey = "url_key";
        private static readonly string SettingsDefault = string.Empty;
        private static readonly string RememberDefault = string.Empty;

        public static string Token
        {
            get { return AppSettings.GetValueOrDefault(TokenKey, SettingsDefault); }
            set { AppSettings.AddOrUpdateValue(TokenKey, value); }
        }

        public static string Url
        {
            get { return AppSettings.GetValueOrDefault(UrlKey, RememberDefault); }
            set { AppSettings.AddOrUpdateValue(UrlKey, value); }
        }
    }
}
