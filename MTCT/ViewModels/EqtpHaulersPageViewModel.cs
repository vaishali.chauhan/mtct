﻿using MTCT.Models.LocalModels;
using MTCT.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MTCT.ViewModels
{
    public class EqtpHaulersPageViewModel : ViewModelBase
    {
        private ObservableCollection<EqTpHaulers> _haulers;
        public ObservableCollection<EqTpHaulers> Haulers
        {
            get => _haulers;
            set
            {
                _haulers = value;
                OnPropertyChanged(nameof(Haulers));
            }
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                GetHauler();
                OnPropertyChanged(nameof(SearchText));

            }
        }

        EquipmentRepository _equipmentRepo;
        public EqtpHaulersPageViewModel()
        {
            _equipmentRepo = new EquipmentRepository();

            LoadData();
        }

        private void LoadData()
        {
            try
            {
                var mEquipmentHaulers = _equipmentRepo.GetEquipmentHaulers();
                if (mEquipmentHaulers != null)
                {
                    Haulers = new ObservableCollection<EqTpHaulers>(mEquipmentHaulers.OrderBy(x => x.EqtphaulerCode));
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("LoadData/EquipmentHaulersPopup" + ex.Message);
            }
        }

        private void GetHauler()
        {
            try
            {
                var mEquipmentHaulers = _equipmentRepo.GetEquipmentHaulers(SearchText);
                if (mEquipmentHaulers != null)
                {
                    Haulers = new ObservableCollection<EqTpHaulers>(mEquipmentHaulers);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetHauler/EquipmentHaulersPopup" + ex.Message);
            }
        }

    }
}
