﻿using MTCT.Models.LocalModels;
using MTCT.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MTCT.ViewModels
{
    public class EqtpTrailersPageViewModel : ViewModelBase
    {
        private ObservableCollection<EqTpTrailers> _trailer;
        public ObservableCollection<EqTpTrailers> Trailers
        {
            get => _trailer;
            set
            {
                _trailer = value;
                OnPropertyChanged(nameof(Trailers));
            }
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                GetTrailer();
                OnPropertyChanged(nameof(SearchText));
            }
        }

        EquipmentRepository _equipmentRepo;
        public EqtpTrailersPageViewModel()
        {
            _equipmentRepo = new EquipmentRepository();

            LoadData();    
        }

        private void LoadData()
        {
            try
            {
                var mEquipmentTrailers = _equipmentRepo.GetEquipmentTrailers();
                if (mEquipmentTrailers != null)
                {
                    Trailers = new ObservableCollection<EqTpTrailers>(mEquipmentTrailers.OrderBy(x => x.EqtptrailerCode));
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("LoadData/EquipmentTrailersPopup" + ex.Message);
            }
        }
        private void GetTrailer()
        {
            try
            {
                var mEquipmentTrialers = _equipmentRepo.GetEquipmentTrailers(SearchText);
                if (mEquipmentTrialers != null)
                {
                    Trailers = new ObservableCollection<EqTpTrailers>(mEquipmentTrialers);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetTrailer/EquipmentTrailersPopup" + ex.Message);
            }
        }
    }
}
