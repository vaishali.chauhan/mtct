﻿using MTCT.API;
using MTCT.Interface;
using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddDevicePage : ContentPage
    {
        AppDevice appDevice = new AppDevice();
        string mDeviceCode = string.Empty;
        List<string> mFormat = new List<string>();
        private bool signEnabled = true;

        #region API
        LoginAPI loginAPI;
        EqptAPI eqptAPI;
        DriverAPI driverAPI;
        DeviceAPI deviceAPI;
        EqtpHaulerAPI eqtpHaulerAPI;
        LocationsAPI locationsAPI;
        ProjectAPI projectAPI;
        EquipmentAPI equipmentAPI;
        UserAPI userAPI;
        #endregion

        #region Repository
        EqtpRepository eqtpRepository;
        DeviceRepository deviceRepository;
        FuelStationRepository fuelStationRepository;
        FlstFuelRepository flstFuelRepository;
        DriverRepository driverRepository;
        EqptHaulerRepository eqptHaulerRepository;
        EqptTrailerRepository eqptTrailerRepository;
        EquipmentRepository equipmentRepository;
        ProjectRepository projectRepository;
        ProjectActivityRepository projectActivityRepository;
        LocationRepository locationRepository;
        UserRepository userRepository;
        AppConfigRepository appConfigRepository;
        #endregion

        #region Constructor
        public AddDevicePage()
        {
            InitializeComponent();
            InitializeObject();
            BindPrintFormat();
            pkPrintFormat.SelectedIndex = 0;
        }
        #endregion

        #region Methods
        public void InitializeObject()
        {
            try
            {
                eqptAPI = new EqptAPI();
                loginAPI = new LoginAPI();
                driverAPI = new DriverAPI();
                equipmentAPI = new EquipmentAPI();
                userAPI = new UserAPI();
                deviceAPI = new DeviceAPI();
                eqtpHaulerAPI = new EqtpHaulerAPI();
                locationsAPI = new LocationsAPI();
                eqtpRepository = new EqtpRepository();
                deviceRepository = new DeviceRepository();
                fuelStationRepository = new FuelStationRepository();
                flstFuelRepository = new FlstFuelRepository();
                driverRepository = new DriverRepository();
                eqptHaulerRepository = new EqptHaulerRepository();
                eqptTrailerRepository = new EqptTrailerRepository();
                projectAPI = new ProjectAPI();
                locationRepository = new LocationRepository();
                equipmentRepository = new EquipmentRepository();
                projectRepository = new ProjectRepository();
                projectActivityRepository = new ProjectActivityRepository();
                userRepository = new UserRepository();
                appConfigRepository = new AppConfigRepository();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeObject/AddDevicePage Exception: " + ex.Message);
            }
        }

        public void BindPrintFormat()
        {
            mFormat.Clear();
            mFormat.Add("Imprimir con Logo y Código de Barra (Zebra)");
            mFormat.Add("Imprimir con Logo y Código de Barra");
            mFormat.Add("Imprimir sin Logo y Código de Barra");
            pkPrintFormat.ItemsSource = mFormat.ToList();
            pkPrintFormat.SelectedIndex = 0;
        }

        public async Task<bool> GetToken()
        {
            bool isSave = false;
            try
            {
                LoginRequiest loginRequiest = new LoginRequiest();

                Settings.Url = txtURL.Text;
                loginRequiest.username = txtUserName.Text;
                loginRequiest.password = txtPassword.Text;
                if (await App.CheckInternetConnection())
                {
                    var response = await loginAPI.Authentication(loginRequiest);

                    if (response.IsSuccess && response.IsSuccess)
                    {
                        isSave = true;
                        Settings.Token = response.token;
                    }
                    else
                    {
                        Utilties.DisplayInfo.ErrorMessage("Por favor entre datos de autorización correctos");
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("AddDevicePage/GetToken Exception: " + ex.Message);
            }
            return isSave;
        }

        public bool ValidatedFields()
        {
            bool isValid = false;
            try
            {
                if (string.IsNullOrEmpty(txtAppRefCode.Text))
                {
                    txtAppRefCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Transport);
                }
                else if (string.IsNullOrEmpty(txtDeviceCode.Text))
                {
                    txtDeviceCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.DeviceCode);
                }
                else if (string.IsNullOrEmpty(txtURL.Text))
                {
                    txtURL.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.URLMsg);
                }
                else if (string.IsNullOrEmpty(txtUserName.Text))
                {
                    txtUserName.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.UserMsg);
                }
                else if (string.IsNullOrEmpty(txtPassword.Text))
                {
                    txtPassword.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.PasswordMsg);
                }
                else
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ValidatedFields/AddDevicePage Exception: " + ex.Message);
            }
            return isValid;
        }

        public async Task<bool> SaveEqtp(string eqtpAppCode)
        {
            bool isSave = false;
            try
            {
                var apiEqtp = await eqptAPI.GetEqtpByEqtpCode(eqtpAppCode);
                if (apiEqtp != null && apiEqtp.eqtpId > 0)
                {
                    var mEqtps = eqtpRepository.GetEqTps();
                    if (mEqtps != null && mEqtps.Count() > 0)
                    {
                        foreach (var eqtp in mEqtps)
                        {
                            eqtpRepository.DeleteEqtp(eqtp.Id);
                        }
                    }

                    Eqtp mEqtp = new Eqtp();
                    int orderCounter = 0;
                    int transportCounter = 0;
                    int orderPrint = 0;
                    int trasnportPrint = 0;

                    if (!string.IsNullOrEmpty(txtShiftCounter.Text))
                        orderCounter = Convert.ToInt32(txtShiftCounter.Text);
                    if (!string.IsNullOrEmpty(txtTransportCounter.Text))
                        transportCounter = Convert.ToInt32(txtTransportCounter.Text);
                    if (!string.IsNullOrEmpty(txtOrderPrint.Text))
                        orderPrint = Convert.ToInt32(txtOrderPrint.Text);
                    if (!string.IsNullOrEmpty(txtTransportPrint.Text))
                        trasnportPrint = Convert.ToInt32(txtTransportPrint.Text);

                    mEqtp.EqtpId = apiEqtp.eqtpId;
                    mEqtp.EqtpCode = apiEqtp.eqtpCode;
                    mEqtp.EqtpName = apiEqtp.eqtpName;
                    mEqtp.EqtpRfidEnabled = apiEqtp.eqtpRfidEnabled;
                    mEqtp.EqtpActiveStatus = apiEqtp.eqtpActiveStatus;
                    mEqtp.EqtpOrderCounterNo = orderCounter;
                    mEqtp.EqtpTransportCounterNo = transportCounter;
                    mEqtp.EqtpOrderPrint = orderPrint;
                    mEqtp.EqtpTransportPrint = trasnportPrint;
                    mEqtp.EqtpDeviceCode = Convert.ToInt32(txtDeviceCode.Text);
                    mEqtp.EqtpSignEnabled = signEnabled;

                    if (pkPrintFormat.SelectedIndex == 0)
                    {
                        mEqtp.PrintFormat = "Format Zebra";
                    }
                    else if (pkPrintFormat.SelectedIndex == 1)
                    {
                        mEqtp.PrintFormat = "Format 1";
                    }
                    else if (pkPrintFormat.SelectedIndex == 2)
                    {
                        mEqtp.PrintFormat = "Format 2";
                    }
                    var iseqtp = eqtpRepository.SaveEqTp(mEqtp);

                    AppConfig appConfig = new AppConfig();
                    var mConfigId = appConfigRepository.GetAllAppConfig();
                    if (mConfigId.Count() > 0 && mConfigId != null)
                    {
                        appConfig.AppConfigId = mConfigId.LastOrDefault().AppConfigId + 1;
                    }
                    else
                    {
                        appConfig.AppConfigId = 1;
                    }
                    appConfig.AppConfigApiUrl = txtURL.Text;
                    appConfig.AppConfigUserName = txtUserName.Text;
                    appConfig.AppConfigPassword = txtPassword.Text;
                    var mAppConfig = appConfigRepository.SaveAppConfig(appConfig);

                    isSave = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSave;
        }

        public async Task<bool> SaveUser(string AppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiUser = await userAPI.GetuserByAppRefCode(AppRefCode);
                if (apiUser != null)
                {
                    var mUsers = userRepository.GetAllUser();
                    if (mUsers != null && mUsers.Count() > 0)
                    {
                        foreach (var mUser in mUsers)
                        {
                            userRepository.DeleteUser(mUser.Id);
                        }
                    }

                    if (apiUser.mUserModels != null)
                    {
                        foreach (var mUserModel in apiUser.mUserModels.Where(x => x.appuserActiveStatus == true))
                        {
                            User mUser = new User()
                            {
                                UserId = mUserModel.appuserId,
                                UserName = mUserModel.appuserName,
                                UserEmail = mUserModel.appuserEmail,
                                UserPassword = mUserModel.appuserPassword,
                                UserRole = mUserModel.appuserRole,
                                UserCode = mUserModel.appuserCode,
                                appuserActiveStatus = mUserModel.appuserActiveStatus,
                                appuserAppRefCode = mUserModel.appuserAppRefCode,
                            };
                            var mUserInsert = userRepository.SaveUser(mUser);
                        }
                    }
                    isSave = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSave;
        }

        public async Task<bool> SaveDriver(string AppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiDriver = await driverAPI.GetDriverByAppRefCode(AppRefCode);
                if (apiDriver != null)
                {
                    var mDrivers = driverRepository.GetDrivers();
                    if (mDrivers != null && mDrivers.Count() > 0)
                    {
                        foreach (var mDriver in mDrivers)
                        {
                            driverRepository.DeleteDriver(mDriver.Id);
                        }
                    }

                    foreach (var mDriverModel in apiDriver.mDriverModels.Where(x => x.driverActiveStatus == true))
                    {
                        Driver mDriver = new Driver()
                        {
                            DriverId = mDriverModel.driverId,
                            DriverCode = mDriverModel.driverCode,
                            DriverName = mDriverModel.driverName,
                            DriverActiveStatus = mDriverModel.driverActiveStatus,
                            driverAppRefCode = mDriverModel.driverAppRefCode,
                        };
                        driverRepository.SaveDriver(mDriver);
                    }
                    isSave = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSave;
        }

        public async Task<bool> SaveHaulers(string AppRefCode)
        {
            bool isSave = false;
            try
            {
                var allEqtpHauler = await eqtpHaulerAPI.GetAllEqtpHaulers(AppRefCode);
                if (allEqtpHauler != null && allEqtpHauler.Count > 0)
                {
                    var mEqtpHaulers = eqptHaulerRepository.GetEqTpHaulers();
                    if (mEqtpHaulers != null && mEqtpHaulers.Count() > 0)
                    {
                        foreach (var mHauler in mEqtpHaulers)
                        {
                            var result = eqptHaulerRepository.DeleteEqTpHaulers(mHauler.Id);
                        }
                    }

                    var mEqTpTrailers = eqptTrailerRepository.GetEqTpTrailers();
                    if (mEqTpTrailers != null && mEqTpTrailers.Count() > 0)
                    {
                        foreach (var mTrailer in mEqTpTrailers)
                        {
                            var result = eqptTrailerRepository.DeleteEqTpTrailers(mTrailer.Id);
                        }
                    }

                    foreach (var eqtpHaulers in allEqtpHauler)
                    {
                        var apiEqtpHaulers = await eqtpHaulerAPI.GetEqtpHaulersByCode(eqtpHaulers.eqtphaulerCode);
                        if (apiEqtpHaulers != null && apiEqtpHaulers.EqTpHaulers != null && apiEqtpHaulers.EqTpHaulers.EqtphaulerId > 0)
                        {
                            if (apiEqtpHaulers.EqTpHaulers != null && apiEqtpHaulers.EqTpHaulers.EqtphaulerId > 0)
                            {
                                var result = eqptHaulerRepository.SaveEqTpHaulers(apiEqtpHaulers.EqTpHaulers);
                            }

                            if (apiEqtpHaulers.EqTpTrailers != null && apiEqtpHaulers.EqTpTrailers.Count > 0)
                            {
                                foreach (var eqTpTrailers in apiEqtpHaulers.EqTpTrailers.Where(x => x.EqtptrailerActiveStatus == true))
                                {
                                    var result = eqptTrailerRepository.SaveEqTpTrailers(eqTpTrailers);
                                }
                            }
                            isSave = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSave;
        }

        public async Task<bool> SaveEquipment(string AppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiEquipment = await equipmentAPI.GetEquipmentByRefCode(AppRefCode);
                if (apiEquipment != null)
                {
                    var mEquipments = equipmentRepository.GetEquipments();
                    if (mEquipments != null && mEquipments.Count() > 0)
                    {
                        foreach (var mEquipment in mEquipments)
                        {
                            equipmentRepository.DeleteEquipment(mEquipment.Id);
                        }
                    }
                }
                foreach (var mEquipmentModel in apiEquipment.mEquipmentModels.Where(x => x.equipmentActiveStatus == true))
                {
                    Equipment mEquipments = new Equipment()
                    {
                        EquipmentId = mEquipmentModel.equipmentId,
                        EquipmentCode = mEquipmentModel.equipmentCode,
                        EquipmentDesc = mEquipmentModel.equipmentDesc,
                        EquipmentLicensePlate = mEquipmentModel.equipmentLicensePlate,
                        EquipmentRFID = mEquipmentModel.equipmentRfid,
                        EquipmentFltankCapacity = mEquipmentModel.equipmentFltankCapacity,
                        EquipmentActiveStatus = mEquipmentModel.equipmentActiveStatus,
                        equipmentAppRefCode = mEquipmentModel.equipmentAppRefCode,
                    };
                    equipmentRepository.SaveEquipment(mEquipments);
                }

                //Equipment mEquipmentsA = new Equipment()
                //{
                //    EquipmentId = 100001,
                //    EquipmentCode = "718037858494",
                //    EquipmentDesc = "Hauler BT-2515TURBO",
                //    EquipmentLicensePlate = "L12345",
                //    EquipmentRFID = "718037858494",
                //    EquipmentFltankCapacity = 25,
                //    EquipmentActiveStatus = true,
                //    equipmentAppRefCode = "FLST - JAGUA",
                //};
                //equipmentRepository.SaveEquipment(mEquipmentsA);
                isSave = true;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("SaveEquipment/AddDevicePage Exception: " + ex.Message);
            }
            return isSave;
        }

        public async Task<bool> SaveLocations(string AppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiLocationDetail = await locationsAPI.GetEqtLocations(AppRefCode);
                if (apiLocationDetail.IsSuccess)
                {
                    isSave = true;
                    if (apiLocationDetail != null
                    && apiLocationDetail.Locations.Count > 0)
                    {
                        isSave = true;
                        var mLocationss = locationRepository.GetLocationss();
                        if (mLocationss != null && mLocationss.Count() > 0)
                        {
                            foreach (var mLocations in mLocationss)
                            {
                                locationRepository.DeleteLocations(mLocations.Id);
                            }
                        }

                        foreach (var item in apiLocationDetail.Locations.Where(x => x.EqtplocationActiveStatus == true))
                        {
                            locationRepository.SaveLocations(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("SaveEquipment/AddDevicePage Exception: " + ex.Message);
            }
            return isSave;
        }

        public async Task<Dictionary<bool, string>> SaveProject(string AppRefCode)
        {
            Dictionary<bool, string> valuePairs = new Dictionary<bool, string>();
            try
            {
                var apiProjectDetails = await projectAPI.GetProjects(AppRefCode);
                if (apiProjectDetails != null
                    && apiProjectDetails.ProjectDetail.Count > 0)
                {
                    var mProjects = projectRepository.GetProjects();
                    if (mProjects != null && mProjects.Count() > 0)
                    {
                        foreach (var mProject in mProjects)
                        {
                            projectRepository.DeleteProjects(mProject.Id);
                        }
                    }

                    var mProjectActivitys = projectActivityRepository.GetProjectActivity();
                    if (mProjectActivitys != null && mProjectActivitys.Count() > 0)
                    {
                        foreach (var mProjectActivity in mProjectActivitys)
                        {
                            projectActivityRepository.DeleteProjectActivity(mProjectActivity.Id);
                        }
                    }

                    foreach (var ProjectDetail in apiProjectDetails.ProjectDetail.Where(x => x.project.ProjectActiveStatus == true))
                    {
                        projectRepository.SaveProjects(ProjectDetail.project);
                        if (ProjectDetail.projAct != null)
                        {
                            foreach (var projectActivity in ProjectDetail.projAct.Where(x => x.projactActiveStatus == true))
                            {
                                projectActivityRepository.SaveProjectActivity(projectActivity);
                            }
                        }
                    }
                    valuePairs.Add(true, "");
                }
                else
                {
                    valuePairs.Add(false, "Please enter valid Transport code.");
                }
            }
            catch (Exception ex)
            {
                valuePairs.Add(false, "SaveEquipment/AddDevicePage Exception: " + ex.Message);
                //Utilties.DisplayInfo.ErrorMessage("SaveEquipment/AddDevicePage Exception: " + ex.Message);
            }
            return valuePairs;
        }
        #endregion

        #region Events
        private async void BtnSave_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (ValidatedFields())
                {
                    bool isSuccess = true;
                    await Task.Run(() => Utilties.DisplayInfo.ShowLoader());
                    await Task.Delay(1 * 500);
                    if (await GetToken())
                    {
                        var responseAPI = await eqptAPI.GetDeviceByDeviceCode(txtDeviceCode.Text);
                        if (responseAPI.IsSuccess)
                        {
                            //if (responseAPI.AppdeviceInuse == true)
                            //{
                            //    await DisplayAlert("", EndPointsMessage.DeviceStatus, "Ok");
                            //}
                            //else
                            //{
                            var isSave = deviceRepository.SaveDevice(responseAPI);
                            if (responseAPI.AppdeviceAppRefCode != null)
                            {
                                string AppCode = string.Empty;
                                string AppRefCode = string.Empty;
                                if (!string.IsNullOrEmpty(txtAppRefCode.Text))
                                {
                                    AppCode = txtAppRefCode.Text;
                                    AppRefCode = "MTCT-" + txtAppRefCode.Text;
                                }

                                //Eqtp
                                var isEqtpSave = await SaveEqtp(AppCode);
                                if (!isEqtpSave)
                                {
                                    isSuccess = false;
                                    Utilties.DisplayInfo.ErrorMessage("Eqtp not save.");
                                }

                                //Haulers
                                var isHaulerSave = await SaveHaulers(AppRefCode);
                                if (!isHaulerSave)
                                {
                                    isSuccess = false;
                                    Utilties.DisplayInfo.ErrorMessage("Hauler not save.");
                                }

                                // User Table
                                var isUserSave = await SaveUser(AppRefCode);
                                if (!isUserSave)
                                {
                                    isSuccess = false;
                                    Utilties.DisplayInfo.ErrorMessage("User not save.");
                                }

                                // Driver Table
                                var isDriverSave = await SaveDriver(AppRefCode);
                                if (!isDriverSave)
                                {
                                    isSuccess = false;
                                    Utilties.DisplayInfo.ErrorMessage("Driver not save.");
                                }

                                //Equipment Table
                                var isEquipmentSave = await SaveEquipment(AppRefCode);
                                if (!isEquipmentSave)
                                {
                                    isSuccess = false;
                                    Utilties.DisplayInfo.ErrorMessage("Equipment not save.");
                                }

                                //Eqtp Location
                                var isLocationSave = await SaveLocations(AppRefCode);
                                if (!isLocationSave)
                                {
                                    isSuccess = false;
                                    Utilties.DisplayInfo.ErrorMessage("Location not save.");
                                }

                                // Projects and projectActivities
                                var resultPair = await SaveProject(AppRefCode);
                                if (resultPair.Count > 0)
                                {
                                    var result = resultPair.FirstOrDefault();
                                    if (!result.Key)
                                    {
                                        isSuccess = false;
                                        if (!string.IsNullOrEmpty(result.Value))
                                            Utilties.DisplayInfo.ErrorMessage(result.Value);
                                        else
                                            Utilties.DisplayInfo.ErrorMessage("Project not save.");
                                    }
                                }

                                Devices device = new Devices();
                                device.appdeviceCode = responseAPI.AppdeviceCode;
                                device.appdeviceDesc = responseAPI.AppdeviceDesc;
                                device.appdeviceAppRefCode = responseAPI.AppdeviceAppRefCode;
                                device.appdeviceSecurityCheck = responseAPI.AppdeviceSecurityCheck;
                                device.appdeviceInuse = true;

                                List<Devices> deviceListner = new List<Devices>();
                                deviceListner.Add(device);

                                var apiDevice = await deviceAPI.SaveDevices(deviceListner);
                                if (apiDevice.IsSuccess)
                                {
                                    var mAppDevice = deviceRepository.GetDevice();
                                    AppDevice appDevice = new AppDevice();
                                    appDevice.Id = mAppDevice.Id;
                                    appDevice.AppdeviceInuse = true;
                                    deviceRepository.UpdateDevice(appDevice);
                                }
                                if (isSuccess)
                                    Navigation.PushAsync(new MasterPages.LoginPage());
                            }
                            //}
                        }
                        else
                        {
                            Utilties.DisplayInfo.ErrorMessage("Please enter correct device code.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnSave_Tapped/AddDevicePage Exception: " + ex.Message);
            }
            finally
            {
                await Task.Run(() => Utilties.DisplayInfo.HideLoader());
            }
        }

        private void imgPrintFormat_Clicked(object sender, EventArgs e)
        {
            pkPrintFormat.Focus();
        }

        private void btnPrintFormat_Tapped(object sender, EventArgs e)
        {
            pkPrintFormat.Focus();
        }

        private void BtnSalir_Tapped(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var result = await DisplayAlert("Alerta", "Esta seguro de salir?", "Si", "No");
                if (result)
                {
                    if (Device.OS == TargetPlatform.Android)
                    {
                        Xamarin.Forms.DependencyService.Get<ICloseAppOnBackButton>().CloseApp();
                    }
                }
            });
        }

        private void StkSignTrue_Clicked(object sender, EventArgs e)
        {
            imgSignFalse.Source = "iconRadioBlank.png";
            imgSignTrue.Source = "iconRadioFill.png";
            signEnabled = true;
        }

        private void StkSignFalse_Clicked(object sender, EventArgs e)
        {
            imgSignTrue.Source = "iconRadioBlank.png";
            imgSignFalse.Source = "iconRadioFill.png";
            signEnabled = false;
        }
        #endregion
    }
}