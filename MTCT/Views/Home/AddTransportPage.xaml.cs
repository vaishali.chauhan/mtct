﻿using Acr.UserDialogs;
using MTCT.API;
using MTCT.Interface;
using MTCT.Models;
using MTCT.Models.APIModels;
using MTCT.Models.DBModel;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using MTCT.Views.PopUp;
using Plugin.Geolocator;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using ZPLForge;
using ZPLForge.Common;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddTransportPage : ContentPage
    {
        #region Repository
        EquipmentRepository equipmentRepository;
        LocationRepository locationRepository;
        ProjectRepository projectRepository;
        ProjectActivityRepository projectActivityRepository;
        ShiftRepository shiftRepository;
        EqTpSoLnRepository eqTpSoLnRepository;
        EqtpRepository eqtpRepository;
        #endregion

        #region List
        List<Equipment> mEquipments;
        List<Locations> mLocations;
        List<Project> mProjects;
        List<ProjectActivity> mProjectActivitys;
        List<BluetoothList> bluetoothList;
        IEnumerable<string> Rfids;
        #endregion

        #region Objects
        EqTpSo mEqTpSo;
        EqTpSoLnLocal mEqTpSoLn;
        Eqtp mEqtp;
        Equipment equipment;
        Locations locations;
        Project project;
        ProjectActivity projectActivity;
        EqTpSoLnAPI eqTpSoLnAPI;
        Flst mFlst;
        Eqtp mEqtpData;

        Plugin.Geolocator.Abstractions.Position location = null;
        bool isLocation = false;
        string EqTpSoRefno = string.Empty;
        string EqTpSoDeviceCode = string.Empty;
        string EqipmentPlate = string.Empty;
        string mBarcode = string.Empty;
        string mSecurityCode = string.Empty;
        string printTitle = string.Empty;
        bool isEditeable = false;
        bool isEditeable1 = false;
        bool isPrintSuccess = false;

        MediaFile file1;
        private byte[] imageByteArray1;

        MediaFile file2;
        private byte[] imageByteArray2;
        DateTime currentDt;
        byte[] signatureByte;
        byte[] signatureByte1;

        private double? prevLoadLati = null;
        private double? prevLoadLong = null;
        private double? prevUnloadLati = null;
        private double? prevUnloadLong = null;
        #endregion

        #region Constructor
        public AddTransportPage(DateTime openDate, IEnumerable<string> rfids = null)
        {
            try
            {
                InitializeComponent();

                lblLoadingDateTime.MinimumDate = openDate;
                lblLoadingDateTime.MaximumDate = DateTime.Today;
                lblUnLoadingDateTime.MinimumDate = openDate;

                InitializeObject();
                isEditeable = true;
                imgPictureDown.IsVisible = false;

                btnLoadingTransport.IsVisible = true;
                printTitle = "Orden de Transporte";

                Rfids = rfids;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeComponent/AddTransportPage" + ex.Message);
            }
        }

        public AddTransportPage(EqTpSoLnLocal eqTpSoLn)
        {
            try
            {
                InitializeComponent();
                InitializeObject();
                isEditeable = false;
                BindIssueData(eqTpSoLn);
                btnLoadingTransport.IsVisible = false;
                grdUnLoadingTransport.IsVisible = true;
                printTitle = "Orden de Trabajo";
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeComponent/AddTransportPage" + ex.Message);
            }
        }

        public AddTransportPage(EqTpSoLnLocal eqTpSoLn, bool isViewOnly)
        {
            try
            {
                InitializeComponent();
                InitializeObject();
                isEditeable = false;
                BindIssueData(eqTpSoLn);
                grdUnLoadingTransport.IsVisible = true;
                btnLoadingTransport.IsVisible = true;
                printTitle = "Orden de Trabajo";
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeComponent/AddTransportPage" + ex.Message);
            }
        }

        #endregion

        #region Methods
        public void InitializeObject()
        {
            try
            {
                mEqtpData = new Eqtp();
                equipmentRepository = new EquipmentRepository();
                locationRepository = new LocationRepository();
                projectRepository = new ProjectRepository();
                projectActivityRepository = new ProjectActivityRepository();
                eqTpSoLnRepository = new EqTpSoLnRepository();
                shiftRepository = new ShiftRepository();
                eqtpRepository = new EqtpRepository();
                mEquipments = new List<Equipment>();
                mLocations = new List<Locations>();
                mProjects = new List<Project>();
                mProjectActivitys = new List<ProjectActivity>();
                bluetoothList = new List<BluetoothList>();
                mEqTpSo = new EqTpSo();
                mEqTpSoLn = new EqTpSoLnLocal();
                mEqtp = new Eqtp();
                eqTpSoLnAPI = new EqTpSoLnAPI();
                mFlst = new Flst();
                currentDt = DateTime.Now;

                BindHaulerData();
                BindOpenOrderData();
                BindEquipments();
                BindLocations();
                BindProjects();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeObject/AddTransportPage" + ex.Message);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                if (!isLocation)
                {
                    BindMap();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("OnAppearing/AddTransportPage" + ex.Message);
            }
        }

        public void BindHaulerData()
        {
            try
            {
                string customerCode = "000001";
                lblLoadingDateTime.Date = currentDt;
                mEqtp = eqtpRepository.GetEqTp();
                if (mEqtp != null)
                {
                    if (mEqtp.EqtpTransportCounterNo > 0)
                    {
                        customerCode = GetDynamicRefnoCode(mEqtp.EqtpTransportCounterNo + 1);
                    }

                    if (mEqtp.EqtpRfidEnabled)
                    {
                        frmEqipmentCode.IsVisible = false;
                        frmCaptureRFID.IsVisible = true;
                    }
                    else
                    {
                        frmCaptureRFID.IsVisible = false;
                        frmEqipmentCode.IsVisible = true;
                    }
                }
                lblRefno.Text = customerCode;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindHaulerData/AddTransportPage" + ex.Message);
            }
        }

        public string GetDynamicRefnoCode(int TransCounter)
        {
            string RefNoCode = string.Empty;
            try
            {
                var code = "000000";
                if (TransCounter > 0)
                {
                    int counterLenght = Convert.ToString(TransCounter).Length;
                    int refCodeLenght = code.Length;
                    var trimIndex = refCodeLenght - counterLenght;
                    if (trimIndex > 0)
                    {
                        RefNoCode = code.Substring(0, trimIndex);
                        RefNoCode += TransCounter;
                    }
                    else
                    {
                        RefNoCode = Convert.ToString(TransCounter);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetDynamicRefnoCode/AddTransportPage" + ex.Message);
            }
            return RefNoCode;
        }

        public void BindIssueData(EqTpSoLnLocal eqTpSoLn)
        {
            try
            {
                ReadOnlyLoadingControls();
                Utilties.DisplayInfo.ShowLoader();
                if (grdUnLoadingTransport.IsVisible == true)
                {
                    lblUnLoadingDateTime.Date = currentDt.Date;
                    lblUnLoadingDateTime.MinimumDate = currentDt.Date;
                }

                mEqTpSoLn = eqTpSoLnRepository.GetEqTpSoLnByRefno(eqTpSoLn.eqtpsolnRefno);
                if (mEqTpSoLn != null && mEqTpSoLn.Id > 0)
                {
                    frmCaptureRFID.IsVisible = false;
                    frmEqipmentCode.IsVisible = true;

                    EqTpSoDeviceCode = mEqTpSoLn.eqtpsolnDeviceCode;
                    mBarcode = mEqTpSoLn.eqtpsolnBarcode;
                    mSecurityCode = mEqTpSoLn.eqtpsolnSecurityCode;
                    lblHeaderRefno.Text = "Transporte " + mEqTpSoLn.eqtpsolnEqtpCode;
                    lblRefno.Text = mEqTpSoLn.eqtpsolnRefno;
                    if (mEqTpSoLn.eqtpsolnLoadingDatetime != null)
                    {
                        lblLoadingDateTime.Date = mEqTpSoLn.eqtpsolnLoadingDatetime.Value.Date;
                    }
                    else
                    {
                        lblLoadingDateTime.Date = currentDt;
                    }
                    lblHaulerCode.Text = mEqTpSoLn.eqtpsolnEqtphaulerCode;
                    lblTrailerCode.Text = mEqTpSoLn.eqtpsolnEqtptrailerCode;
                    lblRFID.Text = mEqTpSoLn.eqtpsolnEquipmentRfid;
                    lblEqipCode.Text = mEqTpSoLn.eqtpsolnEquipmentCode;
                    lblEqipDesc.Text = mEqTpSoLn.eqtpsolnEquipmentDesc;
                    EqipmentPlate = mEqTpSoLn.eqtpsolnEquipmentLicensePlate;
                    lblProjectCode.Text = mEqTpSoLn.eqtpsolnProjectCode;
                    lblProjectDesc.Text = mEqTpSoLn.eqtpsolnProjectDesc;
                    lblProjactCode.Text = mEqTpSoLn.eqtpsolnProjactCode;
                    lblActDesc.Text = mEqTpSoLn.eqtpsolnProjactDesc;
                    lblLoadingLocation.Text = mEqTpSoLn.eqtpsolnLoadingLocation;
                    txtLoadingOdometer.Text = mEqTpSoLn.eqtpsolnLoadingOdometer.ToDecimalString();
                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLatitude))
                    {
                        prevLoadLati = Convert.ToDouble(mEqTpSoLn.eqtpsolnLoadingLatitude);
                    }

                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLongitude))
                    {
                        prevLoadLong = Convert.ToDouble(mEqTpSoLn.eqtpsolnLoadingLongitude);
                    }

                    txtLoadingNotes.Text = mEqTpSoLn.eqtpsolnLoadingNotes;

                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingOdometerImage))
                    {
                        var stream1 = LoadImage(mEqTpSoLn.eqtpsolnLoadingOdometerImage);
                    }

                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingSignature))
                    {
                        signatureByte = Convert.FromBase64String(mEqTpSoLn.eqtpsolnLoadingSignature);
                        lblLoadingComplete.IsVisible = true;
                    }

                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLocation))
                    {
                        ReadOnlyUnLoadingControls();
                        prevUnloadLati = Convert.ToDouble(mEqTpSoLn.eqtpsolnUnloadingLatitude);
                        prevUnloadLong = Convert.ToDouble(mEqTpSoLn.eqtpsolnUnloadingLongitude);
                        lblUnLoadingLocation.Text = mEqTpSoLn.eqtpsolnUnloadingLocation;
                        lblUnLoadingDateTime.Date = mEqTpSoLn.eqtpsolnUnloadingDatetime.Value.Date;
                        txtUnLoadingNotes.Text = mEqTpSoLn.eqtpsolnUnloadingNotes;
                        txtUnLoadingOdometer.Text = mEqTpSoLn.eqtpsolnUnloadingOdometer.ToDecimalString();
                        signatureByte1 = Convert.FromBase64String(mEqTpSoLn.eqtpsolnUnLoadingSignature);
                        if (mEqTpSoLn.eqtpsolnUnloadingOdometerImage != null)
                        {
                            var stream1 = LoadUnLoadingImage(mEqTpSoLn.eqtpsolnUnloadingOdometerImage);
                        }

                        if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnLoadingSignature))
                            lblUnLoadingComplete.IsVisible = true;

                    }

                    if (mEqTpSoLn.eqtpsolnStatus == EqTpSoLnStatus.Anulado.ToString())
                    {
                        ReadOnlyUnLoadingControls();
                    }
                }
                Utilties.DisplayInfo.HideLoader();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.HideLoader();
                Utilties.DisplayInfo.ErrorMessage("BindIssueData/AddTransportPage Exception: " + ex.Message);
            }
        }

        public void ReadOnlyLoadingControls()
        {
            grdUnLoadingTransport.IsVisible = true;
            grdLoading.IsVisible = false;
            grdUnLoading.IsVisible = true;
            PickerBox.IsVisible = true;
            grdDate.IsEnabled = false;
            imgEqipmentCode.IsEnabled = false;
            grdLoadingimg1.IsEnabled = false;
            txtLoadingOdometer.IsReadOnly = true;
            txtLoadingNotes.IsReadOnly = true;
        }

        public void ReadOnlyUnLoadingControls()
        {
            isEditeable1 = true;
            grdUnLoadingTransport.IsVisible = true;
            grdLoading.IsVisible = true;
            grdUnLoading.IsVisible = true;
            DateBox.IsVisible = true;
            UnLoadingimg1.IsEnabled = false;
            txtUnLoadingOdometer.IsReadOnly = true;
            txtUnLoadingNotes.IsReadOnly = true;

            btnUnLoadingSignature.IsEnabled = false;

            btnSave.IsVisible = false;
            btnSave.IsEnabled = false;

            Grid.SetColumnSpan(btnCancel, 2);
            btnCancel.Margin = new Thickness(20, 10, 20, 0);
        }

        public Image LoadImage(string base64string)
        {
            byte[] bytes = Convert.FromBase64String(base64string);
            imageByteArray1 = bytes;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                LoadingPicture1.Source = ImageSource.FromStream(() => new MemoryStream(bytes));
                LoadingPicture1.Aspect = Aspect.Fill;
            }

            return LoadingPicture1;
        }

        public Image LoadUnLoadingImage(string base64string)
        {
            byte[] bytes = Convert.FromBase64String(base64string);
            imageByteArray2 = bytes;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                UnLoadingPicture1.Source = ImageSource.FromStream(() => new MemoryStream(bytes));
                UnLoadingPicture1.Aspect = Aspect.Fill;
            }

            return UnLoadingPicture1;
        }

        public void BindOpenOrderData()
        {
            try
            {
                mEqTpSo = shiftRepository.GetOpenOrder(Common.OpenStatus.ToString());
                if (mEqTpSo != null)
                {
                    lblHaulerCode.Text = mEqTpSo.eqtpsoEqtphaulerCode;
                    lblTrailerCode.Text = mEqTpSo.eqtpsoEqtptrailerCode;
                    lblHeaderRefno.Text = "Transporte " + mEqTpSo.eqtpsoEqtpCode;
                    EqTpSoRefno = mEqTpSo.eqtpsoRefno;
                    EqTpSoDeviceCode = mEqTpSo.eqtpsoDeviceCode;

                    if (!string.IsNullOrEmpty(EqTpSoDeviceCode) || !string.IsNullOrEmpty(lblRefno.Text) || !string.IsNullOrEmpty(currentDt.ToString()))
                    {
                        string default0 = "";
                        if (EqTpSoDeviceCode.Length == 1)
                        {
                            default0 = "000";
                        }
                        else if (EqTpSoDeviceCode.Length == 2)
                        {
                            default0 = "00";
                        }
                        else if (EqTpSoDeviceCode.Length == 3)
                        {
                            default0 = "0";
                        }

                        mBarcode = default0 + EqTpSoDeviceCode + lblRefno.Text + currentDt.ToString("yyyyMMdd");
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindLocations/OrderPage" + ex.Message);
            }
        }

        public void BindEquipments()
        {
            try
            {
                mEquipments = equipmentRepository.GetEquipments();
                if (mEquipments != null && mEquipments.Count == 1)
                {
                    lblRFID.Text = mEquipments.Select(x => x.EquipmentRFID).FirstOrDefault();
                    lblEqipCode.Text = mEquipments.Select(x => x.EquipmentCode).FirstOrDefault();
                    lblEqipDesc.Text = mEquipments.Select(x => x.EquipmentDesc).FirstOrDefault();
                    EqipmentPlate = mEquipments.Select(x => x.EquipmentLicensePlate).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(lblHaulerCode.Text) && !string.IsNullOrEmpty(EqTpSoDeviceCode) && !string.IsNullOrEmpty(lblEqipCode.Text) && !string.IsNullOrEmpty(lblRefno.Text) && lblLoadingDateTime.Date != null)
                {
                    var mSecurity = lblHaulerCode.Text + EqTpSoDeviceCode + EqTpSoRefno + lblRefno.Text + lblLoadingDateTime.Date.ToString("yyyyMMdd") + lblEqipCode.Text + "AASMTCT";
                    mSecurityCode = MD5Hash(mSecurity);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindEquipments/AddTransportPage" + ex.Message);
            }
        }

        public void BindLocations()
        {
            try
            {
                mLocations = locationRepository.GetLocationss();
                if (mLocations != null && mLocations.Count == 1)
                {
                    if (grdUnLoadingTransport.IsVisible == true)
                    {
                        lblUnLoadingLocation.Text = mLocations.Select(x => x.EqtplocationCode).FirstOrDefault();
                    }
                    else
                    {
                        lblLoadingLocation.Text = mLocations.Select(x => x.EqtplocationCode).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindLocations/OrderPage" + ex.Message);
            }
        }

        public void BindProjects()
        {
            try
            {
                mProjects = projectRepository.GetProjects();
                if (mProjects != null && mProjects.Count == 1)
                {
                    lblProjectCode.Text = mProjects.Select(x => x.ProjectCode).FirstOrDefault();
                    lblProjectDesc.Text = mProjects.Select(x => x.ProjectDesc).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindProjects/AddTransportPage" + ex.Message);
            }
        }

        public void BindMap()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    if (locator != null && locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        locator.DesiredAccuracy = 5;
                        location = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(1000));
                        if (location != null)
                        {
                            if (prevLoadLati != null && prevLoadLong != null && prevUnloadLati != null && prevUnloadLong != null)
                            {
                                BindLoadingMap(prevLoadLati.Value, prevLoadLong.Value);
                                BindUnLoadingMap(prevUnloadLati.Value, prevUnloadLong.Value);
                            }
                            else
                            {
                                if (!grdUnLoading.IsVisible)
                                {
                                    BindLoadingMap(location.Latitude, location.Longitude);
                                }
                                else
                                {
                                    BindUnLoadingMap(location.Latitude, location.Longitude);
                                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLatitude) && !string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLongitude))
                                    {
                                        BindLoadingMap(Convert.ToDouble(mEqTpSoLn.eqtpsolnLoadingLatitude), Convert.ToDouble(mEqTpSoLn.eqtpsolnLoadingLongitude));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var myAction = await DisplayAlert("Ubicación", "Activa la ubicación", "Okay", "Cancelar");
                        if (myAction)
                        {
                            if (Device.RuntimePlatform == global::Xamarin.Forms.Device.Android)
                            {
                                global::Xamarin.Forms.DependencyService.Get<ILocSettings>().OpenSettings();
                            }
                            else
                            {
                                await DisplayAlert("Dispositivo", "Estas usando alguna otra mierda", "SÍ");
                            }
                        }
                        else
                        {
                            isLocation = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utilties.DisplayInfo.ErrorMessage("El proceso no pudo obtener la ubicación actual.");
                    BindMap();
                }
            });
        }

        public async void BindLoadingMap(double latitude, double longitude)
        {
            if (Common.HasInternetConnection())
            {
                try
                {
                    var mAddress = await Geocoding.GetPlacemarksAsync(latitude, longitude);
                    string addressLabel = string.Empty;
                    foreach (var add in mAddress)
                    {
                        addressLabel += add.FeatureName + " ";
                    }
                    var pin = new Pin
                    {
                        Type = PinType.Place,
                        Label = addressLabel,
                        Position = new Position(latitude, longitude),
                    };

                    Loadingmap.Pins.Add(pin);
                    Loadingmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitude, longitude), Distance.FromMeters(150.0)));
                }
                catch (Exception ex)
                {
                    Utilties.DisplayInfo.ErrorMessage("El proceso no pudo obtener la ubicación actual.");
                }
            }
        }

        public async void BindUnLoadingMap(double latitude, double longitude)
        {
            if (Common.HasInternetConnection())
            {
                try
                {
                    var mAddress = await Geocoding.GetPlacemarksAsync(latitude, longitude);
                    string addressLabel = string.Empty;
                    foreach (var add in mAddress)
                    {
                        addressLabel += add.FeatureName + " ";
                    }
                    var pin = new Pin
                    {
                        Type = PinType.Place,
                        Label = addressLabel,
                        Position = new Position(latitude, longitude)
                    };

                    UnLoadingmap.Pins.Add(pin);
                    UnLoadingmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitude, longitude), Distance.FromMeters(150.0)));
                }
                catch (Exception ex)
                {
                    Utilties.DisplayInfo.ErrorMessage("El proceso no pudo obtener la ubicación actual.");
                }
            }
        }

        public async void NavigateToLoctionSelection()
        {
            try
            {
                //if (mLocations != null && mLocations.Count > 1)
                //{
                var locationPopup = new PopUp.LocationPopup();
                locationPopup.RefreshEvent += (s1, e1) =>
                {
                    locations = (Locations)s1;
                    if (locations != null && !string.IsNullOrEmpty(locations.EqtplocationCode))
                    {
                        if (grdUnLoadingTransport.IsVisible == true)
                            lblUnLoadingLocation.Text = locations.EqtplocationCode;
                        else
                            lblLoadingLocation.Text = locations.EqtplocationCode;
                    }
                };
                await PopupNavigation.PushAsync(locationPopup, false);
                //}
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToLoctionSelection/AddTransportPage" + ex.Message);
            }
        }

        public async void NavigateToEquipmentSelection()
        {
            try
            {
                //if (mEquipments != null && mEquipments.Count > 1)
                //{
                var equipmentRFIDPopup = new PopUp.EquipmentRFIDPopup();
                equipmentRFIDPopup.RefreshEvent += (s1, e1) =>
                {
                    equipment = (Equipment)s1;
                    if (equipment != null && !string.IsNullOrEmpty(equipment.EquipmentRFID))
                    {
                        lblRFID.Text = equipment.EquipmentRFID;
                        lblEqipCode.Text = equipment.EquipmentCode;
                        lblEqipDesc.Text = equipment.EquipmentDesc;
                        EqipmentPlate = equipment.EquipmentLicensePlate;
                    }
                };
                await PopupNavigation.PushAsync(equipmentRFIDPopup, false);
                //}
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToEquipmentSelection/AddTransportPage" + ex.Message);
            }
        }

        public async void NavigateToProjectSelection()
        {
            try
            {
                //if (mProjects != null && mProjects.Count > 1)
                //{
                var projectPopup = new PopUp.ProjectPopup();
                projectPopup.RefreshEvent += (s1, e1) =>
                {
                    project = (Project)s1;
                    if (project != null && !string.IsNullOrEmpty(project.ProjectCode))
                    {
                        lblProjectCode.Text = project.ProjectCode;
                        lblProjectDesc.Text = project.ProjectDesc;
                    }
                    mProjectActivitys = projectActivityRepository.GetProjectActivityByProjectId(project.ProjectId);
                    if (mProjectActivitys != null && mProjectActivitys.Count == 1)
                    {
                        lblProjactCode.Text = mProjectActivitys.Select(x => x.projactCode).FirstOrDefault();
                        lblActDesc.Text = mProjectActivitys.Select(x => x.projactDesc).FirstOrDefault();
                    }
                };
                await PopupNavigation.PushAsync(projectPopup, false);
                //}
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToProjectSelection/AddTransportPage" + ex.Message);
            }
        }

        public async void NavigateToProjactSelection()
        {
            try
            {
                if (!string.IsNullOrEmpty(lblProjectCode.Text) && !lblProjectCode.Text.ToLower().Contains("proyecto"))
                {
                    var projactActivityPopup = new PopUp.ProjactActivityPopup(project);
                    projactActivityPopup.RefreshEvent += (s1, e1) =>
                    {
                        projectActivity = (ProjectActivity)s1;
                        if (projectActivity != null && !string.IsNullOrEmpty(projectActivity.projactCode))
                        {
                            lblProjactCode.Text = projectActivity.projactCode;
                            lblActDesc.Text = projectActivity.projactDesc;
                        }
                    };
                    await PopupNavigation.PushAsync(projactActivityPopup, false);
                }
                else
                {
                    await DisplayAlert("", "Seleccione el código del proyecto", "Ok");
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToProjactSelection/AddTransportPage" + ex.Message);
            }
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }

        private byte[] ConvertMediaFileToByteArray(MediaFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                file.GetStream().CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        private byte[] GetImageStreamAsBytes(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public async Task ConnectBluetooth(EqTpSoLn mEqTpSoLn)
        {
            try
            {
                if (mEqTpSoLn != null)
                {
                    Utilties.DisplayInfo.ShowLoader();
                    bluetoothList = await DependencyService.Get<IPrintReceipt>().BluetoothList();
                    Utilties.DisplayInfo.HideLoader();
                    // Issues Print Copy
                    if (bluetoothList.Count > 0)
                    {
                        //Get Configuration Data
                        mEqtpData = eqtpRepository.GetEqTp();
                        if (mEqtpData != null && mEqtpData.EqtpTransportPrint > 0)
                        {
                            if (Common.mAppkey == null || Common.mAppkey.Id == 0)
                            {
                                if (bluetoothList.Count == 1)
                                {
                                    AppKey mAppkey = new AppKey();
                                    mAppkey.Id = 1;
                                    mAppkey.KeyName = bluetoothList[0].Name;
                                    mAppkey.KeyAddress = bluetoothList[0].Address;
                                    Common.mAppkey = mAppkey;

                                    if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                                    {
                                        var result = ZebraTransportPrint(mEqTpSoLn, Common.mAppkey, mEqtpData.EqtpTransportPrint);
                                        if (await result)
                                        {
                                            isPrintSuccess = true;
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < mEqtpData.EqtpTransportPrint; i++)
                                        {
                                            var isSuccsess = await PrintTransportRecipt(mEqTpSoLn, Common.mAppkey);
                                            if (!string.IsNullOrEmpty(isSuccsess) && isSuccsess.ToLower() == "true")
                                                isPrintSuccess = true;
                                            await Task.Delay(2000);
                                        }
                                    }

                                    if (isPrintSuccess)
                                    {
                                        await DisplayAlert("", "Impresión completada", "Ok");
                                    }
                                    else
                                    {
                                        await DisplayAlert("", "No se pudo imprimir", "Ok");
                                    }
                                }
                                else
                                {
                                    BluetoothPopup bluetoothPopup = new BluetoothPopup();
                                    bluetoothPopup.RefreshEvent += async (s11, e11) =>
                                    {
                                        AppKey mAppkey = new AppKey();
                                        var bluetooth = (BluetoothList)s11;
                                        if (bluetooth != null)
                                        {
                                            mAppkey.Id = 1;
                                            mAppkey.KeyName = bluetooth.Name;
                                            mAppkey.KeyAddress = bluetooth.Address;
                                            Common.mAppkey = mAppkey;
                                            if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                                            {
                                                var result = ZebraTransportPrint(mEqTpSoLn, Common.mAppkey, mEqtpData.EqtpTransportPrint);
                                                if (await result)
                                                {
                                                    isPrintSuccess = true;
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i < mEqtpData.EqtpTransportPrint; i++)
                                                {
                                                    var isSuccsess = await PrintTransportRecipt(mEqTpSoLn, Common.mAppkey);
                                                    if (!string.IsNullOrEmpty(isSuccsess) && isSuccsess.ToLower() == "true")
                                                        isPrintSuccess = true;
                                                    await Task.Delay(2000);
                                                }
                                            }
                                        }
                                        await Navigation.PopAsync();
                                    };
                                    await PopupNavigation.PushAsync(bluetoothPopup, false);
                                }
                            }
                            else
                            {
                                if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                                {
                                    var result = ZebraTransportPrint(mEqTpSoLn, Common.mAppkey, mEqtpData.EqtpTransportPrint);
                                    if (await result)
                                    {
                                        isPrintSuccess = true;
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < mEqtpData.EqtpTransportPrint; i++)
                                    {
                                        var isSuccsess = await PrintTransportRecipt(mEqTpSoLn, Common.mAppkey);
                                        if (!string.IsNullOrEmpty(isSuccsess) && isSuccsess.ToLower() == "true")
                                            isPrintSuccess = true;
                                        await Task.Delay(2000);
                                    }
                                }

                                if (isPrintSuccess)
                                {
                                    await DisplayAlert("", "Impresión completada", "Ok");
                                }
                                else
                                {
                                    await DisplayAlert("", "No se pudo imprimir", "Ok");
                                }
                            }
                        }
                    }
                    else
                    {
                        await Navigation.PopAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("Bluetooth/IssuesPage" + ex.Message);
            }
        }
 
        public async Task<string> PrintTransportRecipt(EqTpSoLn mEqTpSoLn, AppKey mAppKey)
        {
            try
            {
                var mHaulerData = eqtpRepository.GetEqTp();

                EqptHaulerRepository eqptHaulerRepository = new EqptHaulerRepository();
                var mHauler = eqptHaulerRepository.GetHaulerByEqtphaulerCode(mEqTpSo.eqtpsoEqtphaulerCode);

                EqptTrailerRepository eqptTrailerRepository = new EqptTrailerRepository();
                var mTrailer = eqptTrailerRepository.GerTrailerByEqtptrailerCode(mEqTpSo.eqtpsoEqtptrailerCode);

                UserDialogs.Instance.ShowLoading("Printing...");
                await Task.Delay(1 * 200);
                string nullString = "N/A";
                Receipt mReceipt = new Receipt();
                StringBuilder sbReceipt1 = new StringBuilder();
                StringBuilder sbReceipt2 = new StringBuilder();
                mReceipt.ReceiptString1 = new StringBuilder();
                mReceipt.ReceiptString2 = new StringBuilder();

                sbReceipt1.Append("\x1B\x21\x1");
                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("\x1b\x40");

                if (mHaulerData != null && mHaulerData.PrintFormat == "Format 1")
                {
                    sbReceipt1.Append("--------------------------------");
                    sbReceipt1.Append(System.Environment.NewLine);
                }

                sbReceipt1.Append("\x1B\x21\x1");
                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("Orden");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("--------------------------------");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                Common.barcode = mEqTpSoLn.eqtpsolnBarcode;
                //
                string key1 = "Orden:";
                string value1 = nullString;
                if (mEqTpSo.eqtpsoRefno != null)
                {
                    value1 = mEqTpSo.eqtpsoRefno + " ";
                }
                sbReceipt1.Append(key1 + GetRightSpace(key1) + value1);
                sbReceipt1.Append(System.Environment.NewLine);

                string key2 = "Fecha:";
                string value2 = nullString;
                if (mEqTpSo.eqtpsoOpeningDate != DateTime.MinValue)
                {
                    value2 = mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy HH:mm:ss tt");
                }
                sbReceipt1.Append(key2 + GetRightSpace(key2) + value2);
                sbReceipt1.Append(System.Environment.NewLine);

                string key23 = "Movil:";
                string value23 = nullString;
                if (mEqTpSo.eqtpsoDeviceCode != null)
                {
                    value23 = mEqTpSo.eqtpsoDeviceCode;
                }
                sbReceipt1.Append(key23 + GetRightSpace(key23) + value23);
                sbReceipt1.Append(System.Environment.NewLine);


                string key3 = "Usuario:";
                string value3 = nullString;
                if (mEqTpSo.eqtpsoUserCode != null)
                {
                    value3 = mEqTpSo.eqtpsoUserCode;
                }
                sbReceipt1.Append(key3 + GetRightSpace(key3) + value3);
                sbReceipt1.Append(System.Environment.NewLine);

                string key15 = "Cód.Seg.:";
                string value15 = nullString;
                if (mEqTpSoLn.eqtpsolnSecurityCode != null)
                {
                    value15 = mEqTpSoLn.eqtpsolnSecurityCode;
                }
                sbReceipt1.Append(key15);
                sbReceipt1.Append(System.Environment.NewLine);
                sbReceipt1.Append(value15);
                sbReceipt1.Append(System.Environment.NewLine);


                string key4 = "Cabezote:"; // Transporte => Cabezote
                string value4 = nullString;
                if (mEqTpSo.eqtpsoEqtphaulerCode != null)
                {
                    value4 = ((mHauler != null && mHauler.Id > 0) ? mHauler.DisplayHauler : mEqTpSo.eqtpsoEqtphaulerCode);
                }
                sbReceipt1.Append(key4 + GetRightSpace(key4) + value4);
                sbReceipt1.Append(System.Environment.NewLine);


                string key5 = "Cola:";//Trailer => Cola
                string value5 = nullString;
                if (mEqTpSo.eqtpsoEqtptrailerCode != null)
                {
                    value5 = ((mTrailer != null && mTrailer.Id > 0) ? mTrailer.DisplayTrailer : mEqTpSo.eqtpsoEqtptrailerCode);
                }
                sbReceipt1.Append(key5 + GetRightSpace(key5) + value5);
                sbReceipt1.Append(System.Environment.NewLine);


                string key6 = "Chofer:";
                string value6 = nullString;
                if (mEqTpSo.eqtpsoDriverCode != null)
                {
                    value6 = mEqTpSo.eqtpsoDriverCode;
                }
                sbReceipt1.Append(key6 + GetRightSpace(key6) + value6);
                sbReceipt1.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1B\x21\x1");
                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("Transporte");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("--------------------------------");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                string key7 = "Refno.:";
                string value7 = nullString;
                if (mEqTpSoLn.eqtpsolnRefno != null)
                {
                    value7 = mEqTpSoLn.eqtpsolnRefno;
                }
                sbReceipt1.Append(key7 + GetRightSpace(key7) + value7);
                sbReceipt1.Append(System.Environment.NewLine);


                string key8 = "RFID:";
                string value8 = nullString;
                if (mEqTpSoLn.eqtpsolnEquipmentRfid != null)
                {
                    value8 = mEqTpSoLn.eqtpsolnEquipmentRfid;
                }
                sbReceipt1.Append(key8 + GetRightSpace(key8) + value8);
                sbReceipt1.Append(System.Environment.NewLine);


                string key9 = "Equipo:";
                string value9 = nullString;
                string value10 = nullString;
                if (mEqTpSoLn.eqtpsolnEquipmentCode != null)
                {
                    value9 = mEqTpSoLn.eqtpsolnEquipmentCode;
                }
                if (mEqTpSoLn.eqtpsolnEquipmentDesc != null)
                {
                    value10 = mEqTpSoLn.eqtpsolnEquipmentDesc;
                }
                sbReceipt1.Append(key9 + GetRightSpace(key9) + value9 + " " + value10);
                sbReceipt1.Append(System.Environment.NewLine);


                sbReceipt1.Append("\x1B\x21\x1");
                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("Cargo");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);


                string key11 = "Proyecto:";
                string value11 = nullString;
                string value12 = nullString;
                if (mEqTpSoLn.eqtpsolnProjectCode != null)
                {
                    value11 = mEqTpSoLn.eqtpsolnProjectCode;
                }
                if (mEqTpSoLn.eqtpsolnProjectDesc != null)
                {
                    value12 = mEqTpSoLn.eqtpsolnProjectDesc;
                }
                sbReceipt1.Append(key11 + GetRightSpace(key11) + value11 + " " + value12);
                sbReceipt1.Append(System.Environment.NewLine);

                string key13 = "Partida:";
                string value13 = nullString;
                string value14 = nullString;
                if (mEqTpSoLn.eqtpsolnProjactCode != null)
                {
                    value13 = mEqTpSoLn.eqtpsolnProjactCode;
                }
                if (mEqTpSoLn.eqtpsolnProjactDesc != null)
                {
                    value14 = mEqTpSoLn.eqtpsolnProjactDesc;
                }
                sbReceipt1.Append(key13 + GetRightSpace(key13) + value13 + " " + value14);
                sbReceipt1.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("Carga");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                string key16 = "Fecha:";
                string value16 = nullString;
                if (mEqTpSoLn.eqtpsolnLoadingDatetime.HasValue && mEqTpSoLn.eqtpsolnLoadingDatetime.Value != DateTime.MinValue)
                {
                    value16 = mEqTpSoLn.eqtpsolnLoadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");
                }
                else
                {
                    value16 = mEqTpSoLn.eqtpsolnInterfaceDate.ToString("dd/MM/yyyy HH:mm:ss tt");
                }
                sbReceipt1.Append(key16 + GetRightSpace(key16) + value16);
                sbReceipt1.Append(System.Environment.NewLine);

                string key17 = "Localización:";
                string value17 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLocation))
                {
                    value17 = mEqTpSoLn.eqtpsolnLoadingLocation;
                }
                sbReceipt1.Append(key17 + GetRightSpace(key17) + value17);
                sbReceipt1.Append(System.Environment.NewLine);


                string key18 = "Kilometraje:";
                string value18 = nullString;
                if (mEqTpSoLn.eqtpsolnLoadingOdometer != null)
                {
                    value18 = mEqTpSoLn.eqtpsolnLoadingOdometer.ToString();
                }
                sbReceipt1.Append(key18 + GetRightSpace(key18) + value18);
                sbReceipt1.Append(System.Environment.NewLine);


                string key19 = "Long./Lat.:";
                string value19 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLongitude) && !string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLatitude))
                {
                    value19 = mEqTpSoLn.eqtpsolnLoadingLongitude + "/" + mEqTpSoLn.eqtpsolnLoadingLatitude;
                }
                sbReceipt1.Append(key19 + GetRightSpace(key19) + value19);
                sbReceipt1.Append(System.Environment.NewLine);


                string key20 = "Notas:";
                string value20 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingNotes))
                {
                    value20 = mEqTpSoLn.eqtpsolnLoadingNotes;
                }
                sbReceipt1.Append(key20 + GetRightSpace(key20) + value20);
                sbReceipt1.Append(System.Environment.NewLine);

                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLocation) && (mEqTpSoLn.eqtpsolnUnloadingDatetime != null && mEqTpSoLn.eqtpsolnUnloadingDatetime != DateTime.MinValue))
                {
                    sbReceipt1.Append("\x1b\x45\x01");
                    sbReceipt1.Append("Descarga");
                    sbReceipt1.Append("\x1b\x45\x00");
                    sbReceipt1.Append(System.Environment.NewLine);

                    string key26 = "Fecha:";
                    string value26 = nullString;
                    if (mEqTpSoLn.eqtpsolnUnloadingDatetime != null
                        && mEqTpSoLn.eqtpsolnUnloadingDatetime.HasValue
                        && mEqTpSoLn.eqtpsolnUnloadingDatetime != DateTime.MinValue)
                    {
                        value26 = mEqTpSoLn.eqtpsolnUnloadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");
                    }
                    sbReceipt1.Append(key26 + GetRightSpace(key26) + value26);
                    sbReceipt1.Append(System.Environment.NewLine);

                    string key27 = "Localización:";
                    string value27 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLocation))
                    {
                        value27 = mEqTpSoLn.eqtpsolnUnloadingLocation;
                    }
                    sbReceipt1.Append(key27 + GetRightSpace(key27) + value27);
                    sbReceipt1.Append(System.Environment.NewLine);


                    string key28 = "Kilometraje:";
                    string value28 = nullString;
                    if (mEqTpSoLn.eqtpsolnUnloadingOdometer != null)
                    {
                        value28 = mEqTpSoLn.eqtpsolnUnloadingOdometer.ToString();
                    }
                    sbReceipt1.Append(key28 + GetRightSpace(key28) + value28);
                    sbReceipt1.Append(System.Environment.NewLine);


                    string key29 = "Long./Lat.:";
                    string value29 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLongitude) && !string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLatitude))
                    {
                        value29 = mEqTpSoLn.eqtpsolnUnloadingLongitude + "/" + mEqTpSoLn.eqtpsolnUnloadingLatitude;
                    }
                    sbReceipt1.Append(key29 + GetRightSpace(key29) + value29);
                    sbReceipt1.Append(System.Environment.NewLine);


                    string key30 = "Notas:";
                    string value30 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingNotes))
                    {
                        value30 = mEqTpSoLn.eqtpsolnUnloadingNotes;
                    }
                    sbReceipt1.Append(key30 + GetRightSpace(key30) + value30);
                    sbReceipt1.Append(System.Environment.NewLine);
                }

                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("--------------------------------");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                string key21 = "Estatus:";
                string value21 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnStatus))
                {
                    value21 = mEqTpSoLn.eqtpsolnStatus;
                }
                sbReceipt1.Append(key21 + GetRightSpace(key21) + value21);
                sbReceipt1.Append(System.Environment.NewLine);
                sbReceipt1.Append(System.Environment.NewLine);

                mReceipt.ReceiptString1 = sbReceipt1;

                if (mHaulerData != null && mHaulerData.PrintFormat == "Format 1")
                {
                    // Barcode
                    sbReceipt2.Append("\x1b\x61\x01");
                    sbReceipt2.Append("\x1b\x40");
                    string value22 = nullString;
                    if (mEqTpSoLn.eqtpsolnBarcode != null)
                    {
                        value22 = mEqTpSoLn.eqtpsolnBarcode;
                        sbReceipt2.Append("\x1b\x61\x01" + value22);
                    }
                    sbReceipt2.Append(System.Environment.NewLine);
                }

                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1B\x21\x1");
                string key24 = "---------------";
                string value24 = "---------------";
                sbReceipt2.Append("\x1B\x21\x1" + key24 + GetSpace(key24, value24) + value24);
                sbReceipt2.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1B\x21\x1");
                string key25 = "  Transportista  ";
                string value25 = "     Supervisor     ";
                sbReceipt2.Append("\x1B\x21\x1" + key25 + GetSpace(key25, value25) + value25);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);

                mReceipt.ReceiptString2 = sbReceipt2;

                var jsonString = await DependencyService.Get<IPrintReceipt>().PrintReceiptMethod(mAppKey.KeyAddress, mReceipt, mEqTpSoLn.eqtpsolnBarcode);
                UserDialogs.Instance.HideLoading();
                //DisplayAlert("", jsonString, "Ok");
                if (jsonString)
                {
                    return "True";
                }
                else
                {
                    return "False";
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                return "False";
            }
        }

        public async Task<bool> ZebraTransportPrint(EqTpSoLn mEqTpSoLn, AppKey mAppKey, int noOfCopies)
        {
            bool isSuccess = false;
            try
            {
                UserDialogs.Instance.ShowLoading("Printing...");
                Xamarin.Forms.DependencyService.Get<IBluetooth>().ConnectBluetooth(mAppKey.KeyName, mAppKey.KeyAddress);

                await Task.Run(async () =>
                {
                    var zpl = await ZebraLabelBuilder(mEqTpSoLn);
                    for (int i = 0; i < noOfCopies; i++)
                    {
                        var result = await Xamarin.Forms.DependencyService.Get<IBluetooth>().Print(zpl);
                        if (result)
                        {
                            isSuccess = true;
                        }
                        await Task.Delay(3000);
                    }
                    Xamarin.Forms.DependencyService.Get<IBluetooth>().DisconnectBluetooth();
                });
            }
            catch (Exception ex)
            {
                isSuccess = false;
                Utilties.DisplayInfo.ErrorMessage("AddTransportPage/ZebraIssuePrint: " + ex.Message);
            }
            finally { UserDialogs.Instance.HideLoading(); }

            return isSuccess;
        }

        //public string GetDate(DateTime dt)
        //{
        //    string Date = "";
        //    if (dt != DateTime.MinValue)
        //        return Date = dt.ToString("dd/MM/yyyy HH:mm:ss tt");
        //    else
        //        return Date;
        //}

        public async Task<byte[]> ZebraLabelBuilder(EqTpSoLn mEqTpSoLn)
        {
            string zpl = string.Empty;
            try
            {
                string base64Image = LogoBase64String.LogoBase64;
                byte[] imageBytes = Convert.FromBase64String(base64Image);
                ZPLImage zplimg = Xamarin.Forms.DependencyService.Get<IBluetooth>().GetZPLImageCommand(imageBytes);

                string dt = "";
                if (mEqTpSoLn.eqtpsolnLoadingDatetime.HasValue && mEqTpSoLn.eqtpsolnLoadingDatetime.Value != DateTime.MinValue)
                    dt = mEqTpSoLn.eqtpsolnLoadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");

                string dt1 = "";
                if (mEqTpSoLn.eqtpsolnUnloadingDatetime.HasValue && mEqTpSoLn.eqtpsolnUnloadingDatetime.Value != DateTime.MinValue)
                    dt1 = mEqTpSoLn.eqtpsolnUnloadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");

                EqptHaulerRepository eqptHaulerRepository = new EqptHaulerRepository();
                var mHauler = eqptHaulerRepository.GetHaulerByEqtphaulerCode(mEqTpSo.eqtpsoEqtphaulerCode);
                var DisplayHauler = ((mHauler != null && mHauler.Id > 0) ? mHauler.DisplayHauler : mEqTpSo.eqtpsoEqtphaulerCode);

                EqptTrailerRepository eqptTrailerRepository = new EqptTrailerRepository();
                var mTrailer = eqptTrailerRepository.GerTrailerByEqtptrailerCode(mEqTpSo.eqtpsoEqtptrailerCode);
                var DisplayTrailer = ((mTrailer != null && mTrailer.Id > 0) ? mTrailer.DisplayTrailer : mEqTpSo.eqtpsoEqtptrailerCode);


                ZPLForge.Label label = new ZPLForge.Label
                {
                    Quantity = 1,
                    PrintWidth = 576,
                    MediaTracking = MediaTracking.ContinuousVariableLength,
                    MediaType = MediaType.ThermalDirect,
                    PrintMode = PrintMode.TearOff,
                    MediaDarknessLevel = 15,
                    Content =
                    {
                        new ImageElement
                    {
                        PositionX = 15,
                        PositionY = 70,
                        Compression = CompressionType.HexASCII,
                        BinaryByteCount = zplimg.ByteCount,
                        GraphicFieldCount = zplimg.ByteCount,
                        BytesPerRow = zplimg.BytesPerRow,
                        Content = zplimg.Data
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 180,
                        Content = "Alba Sanchez & Asociados",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 210,
                        Content = "RNC: 1-01-13267-1",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 240,
                        Content = printTitle,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 270,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 300,
                        Content = "Orden:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 300,
                        Content = mEqTpSo.eqtpsoRefno,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 330,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 330,
                        Content = mEqTpSo.eqtpsoOpeningDate != DateTime.MinValue ? mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy HH:mm:ss tt") : " ",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 360,
                        Content = "Movil:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 360,
                        Content = string.Format("{0:00}", Convert.ToInt32(mEqTpSo.eqtpsoDeviceCode)),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 390,
                        Content = "Usuario:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 390,
                        Content = mEqTpSo.eqtpsoUserCode + " " + mEqTpSo.eqtpsoUserName,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 420,
                        Content = "Cód.Seg.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 420,
                        Content = mEqTpSoLn.eqtpsolnSecurityCode,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 450,
                        Content = "Transportista:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 450,
                        Content = mEqtpData.EqtpCode,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 480,
                        Content = "Cabezote:", // Transporte => Cabezote
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 480,
                        Content = DisplayHauler,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 510,
                        Content = "Cola:", //Trailer => Cola
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 510,
                        Content = DisplayTrailer,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 540,
                        Content = "Chofer:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 540,
                        Content = mEqTpSo.eqtpsoDriverCode + " " + mEqTpSo.eqtpsoDriverName,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 570,
                        Content = "Transporte",//Transporte => Cabezote
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 570,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 630,
                        Content = "Refno.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 630,
                        Content = mEqTpSoLn.eqtpsolnRefno,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 660,
                        Content = "RFID:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 660,
                        Content = mEqTpSoLn.eqtpsolnEquipmentRfid,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 690,
                        Content = "Equipo:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 690,
                        Content = mEqTpSoLn.eqtpsolnEquipmentCode+" "+mEqTpSoLn.eqtpsolnEquipmentDesc,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 720,
                        Content = "Cargo",//Transporte => Cabezote
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 750,
                        Content = "Proyecto:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 750,
                        Content = mEqTpSoLn.eqtpsolnProjectCode + " " + mEqTpSoLn.eqtpsolnProjectDesc,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 780,
                        Content = "Partida:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 780,
                        Content = mEqTpSoLn.eqtpsolnProjactCode + " " + mEqTpSoLn.eqtpsolnProjactDesc,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 810,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 840,
                        Content = "Carga",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 870,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 870,
                        Content = dt,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 900,
                        Content = "Localización:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 900,
                        Content = mEqTpSoLn.eqtpsolnLoadingLocation,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 930,
                        Content = "Kilometraje:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 930,
                        Content = mEqTpSoLn.eqtpsolnLoadingOdometer.ToString(),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 960,
                        Content = "Long./Lat.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 960,
                        Content = mEqTpSoLn.eqtpsolnLoadingLongitude + "/" + mEqTpSoLn.eqtpsolnLoadingLatitude,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 990,
                        Content = "Notas:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 990,
                        Content = mEqTpSoLn.eqtpsolnLoadingNotes,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                    }
                };

                int posY = 1020;

                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLocation) && (mEqTpSoLn.eqtpsolnUnloadingDatetime != null && mEqTpSoLn.eqtpsolnUnloadingDatetime != DateTime.MinValue))
                {
                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Descarga",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = dt1,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Localización:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = mEqTpSoLn.eqtpsolnUnloadingLocation,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Kilometraje:",// Odómetro => Kilometraje
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = mEqTpSoLn.eqtpsolnUnloadingOdometer.ToString(),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Long./Lat.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = mEqTpSoLn.eqtpsolnUnloadingLongitude + "/" + mEqTpSoLn.eqtpsolnUnloadingLatitude,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Notas:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = mEqTpSoLn.eqtpsolnUnloadingNotes,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;
                }

                posY = posY + 30;

                label.Content.Add(new TextElement
                {
                    PositionX = 10,
                    PositionY = posY,
                    Content = "----------------------------------------",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 15
                });

                posY = posY + 30;

                label.Content.Add(new TextElement
                {
                    PositionX = 10,
                    PositionY = posY,
                    Content = "Estatus:",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 25
                });

                label.Content.Add(new TextElement
                {
                    PositionX = 145,
                    PositionY = posY,
                    Content = mEqTpSoLn.eqtpsolnStatus,
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 25
                });

                posY = posY + 100;

                label.Content.Add(new TextElement
                {
                    PositionX = 10,
                    PositionY = posY,
                    Content = "------------------",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 15
                });

                label.Content.Add(new TextElement
                {
                    PositionX = 310,
                    PositionY = posY,
                    Content = "------------------",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 15
                });

                posY += 30;

                label.Content.Add(new TextElement
                {
                    PositionX = 80,
                    PositionY = posY,
                    Content = "Transportista",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 25
                });


                label.Content.Add(new TextElement
                {
                    PositionX = 395,
                    PositionY = posY,
                    Content = "Supervisor",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 25
                });

                posY += 30;

                label.Content.Add(new BarcodeElement
                {
                    BarcodeType = BarcodeType.Code128,
                    PositionX = 15,
                    PositionY = posY,
                    Height = 50,
                    Content = mEqTpSoLn.eqtpsolnBarcode
                });

                label.MediaLength = posY + 150;

                zpl = label.ToString().Insert(3, "^POI");
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                Utilties.DisplayInfo.ErrorMessage("AddTransportPage/ZebraLabelBuilder: " + ex.Message);
            }
            return Encoding.UTF8.GetBytes(zpl);
        }

        string GetSpace(string key, string value)
        {
            string space = "";
            int defaultCount = 42;
            int totalLength = 0;
            try
            {
                if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                {
                    totalLength = key.Length + value.Length;
                }
                else if (!string.IsNullOrEmpty(key))
                {
                    totalLength = key.Length;
                }
                else if (!string.IsNullOrEmpty(value))
                {
                    totalLength = value.Length;
                }

                for (int i = 0; i < (defaultCount - totalLength); i++)
                {
                    space += " ";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetSpace/AddTransportPage" + ex.Message);
            }
            return space;
        }

        string GetRightSpace(string value)
        {
            string space = "";
            try
            {
                int defaultCount = 14;
                int totalLength = (defaultCount - value.Length);
                for (int i = 0; i < totalLength; i++)
                {
                    space += " ";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetRightSpace/AddTransportPage Exception: " + ex.Message);
            }
            return space;
        }

        public async Task ReadUIAndSaveTransporter()
        {
            try
            {
                Utilties.DisplayInfo.ShowLoader("Loading...");
                var eqTpSoLn = new EqTpSoLn();
                if (mEqTpSoLn.Id > 0)
                {
                    eqTpSoLn.Id = mEqTpSoLn.Id;
                }

                #region Loading sequence
                if (!string.IsNullOrEmpty(lblRefno.Text))
                {
                    EqTpSoLnLocal eqTpSoLnRefno = eqTpSoLnRepository.GetEqTpSoLnByRefno(lblRefno.Text);
                    if (eqTpSoLnRefno != null)
                    {
                        eqTpSoLn.EqTpSoLnId = eqTpSoLnRefno.EqTpSoLnId;
                    }
                    else
                    {
                        var eqTpSoLnsList = eqTpSoLnRepository.GetAllEqTpSoLns();
                        if (eqTpSoLnsList.Count() > 0 && eqTpSoLnsList != null)
                        {
                            eqTpSoLn.EqTpSoLnId = eqTpSoLnsList.LastOrDefault().EqTpSoLnId + 1;
                        }
                        else
                        {
                            eqTpSoLn.EqTpSoLnId = 1;
                        }
                    }
                }
                else
                {
                    var eqTpSoLnsList = eqTpSoLnRepository.GetAllEqTpSoLns();
                    if (eqTpSoLnsList.Count() > 0 && eqTpSoLnsList != null)
                    {
                        eqTpSoLn.EqTpSoLnId = eqTpSoLnsList.LastOrDefault().EqTpSoLnId + 1;
                    }
                    else
                    {
                        eqTpSoLn.EqTpSoLnId = 1;
                    }
                }

                if (!string.IsNullOrEmpty(EqTpSoDeviceCode) && !string.IsNullOrEmpty(EqTpSoRefno) && !string.IsNullOrEmpty(lblHaulerCode.Text) && !string.IsNullOrEmpty(lblTrailerCode.Text))
                {
                    eqTpSoLn.eqtpsolnDeviceCode = EqTpSoDeviceCode;
                    eqTpSoLn.eqtpsolnEqtpsoRefno = EqTpSoRefno;
                    eqTpSoLn.eqtpsolnEqtphaulerCode = lblHaulerCode.Text;
                    eqTpSoLn.eqtpsolnEqtptrailerCode = lblTrailerCode.Text;
                }
                eqTpSoLn.eqtpsolnEqtpCode = mEqtp.EqtpCode;
                eqTpSoLn.eqtpsolnRefno = lblRefno.Text;
                if (frmCaptureRFID.IsVisible && lblCaptureRFID.Text != "Captura RFID")
                {
                    eqTpSoLn.eqtpsolnEquipmentRfid = lblCaptureRFID.Text;
                }
                else
                {
                    eqTpSoLn.eqtpsolnEquipmentRfid = lblRFID.Text;
                }
                eqTpSoLn.eqtpsolnEquipmentCode = lblEqipCode.Text;
                eqTpSoLn.eqtpsolnEquipmentDesc = lblEqipDesc.Text;
                eqTpSoLn.eqtpsolnEquipmentLicensePlate = EqipmentPlate;
                eqTpSoLn.eqtpsolnProjectCode = lblProjectCode.Text;
                eqTpSoLn.eqtpsolnProjectDesc = lblProjectDesc.Text;
                eqTpSoLn.eqtpsolnProjactCode = lblProjactCode.Text;
                eqTpSoLn.eqtpsolnProjactDesc = lblActDesc.Text;
                eqTpSoLn.eqtpsolnLoadingLocation = lblLoadingLocation.Text;

                if (location != null && location.Latitude != 0 && location.Longitude != 0)
                {
                    eqTpSoLn.eqtpsolnLoadingLongitude = location.Longitude.ToString();
                    eqTpSoLn.eqtpsolnLoadingLatitude = location.Latitude.ToString();
                }
                else
                {
                    eqTpSoLn.eqtpsolnLoadingLongitude = "0";
                    eqTpSoLn.eqtpsolnLoadingLatitude = "0";
                }

                eqTpSoLn.eqtpsolnLoadingDatetime = mEqTpSoLn.eqtpsolnLoadingDatetime;
                if (eqTpSoLn.eqtpsolnLoadingDatetime == null || eqTpSoLn.eqtpsolnLoadingDatetime == DateTime.MinValue)
                {
                    var currentTime = DateTime.Now.ToString("hh:mm:ss");
                    var currentDate = lblLoadingDateTime.Date.ToString("dd/MM/yyyy");
                    var curDateTime = currentDate + " " + currentTime;
                    DateTime myDate = DateTime.ParseExact(curDateTime, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    eqTpSoLn.eqtpsolnLoadingDatetime = myDate;
                }

                eqTpSoLn.eqtpsolnLoadingNotes = txtLoadingNotes.Text;
                eqTpSoLn.eqtpsolnLoadingOdometer = Convert.ToDecimal(txtLoadingOdometer.Text, CultureInfo.InvariantCulture);
                if (imageByteArray1 != null)
                {
                    eqTpSoLn.eqtpsolnLoadingOdometerImage = Convert.ToBase64String(imageByteArray1);
                }

                if (signatureByte != null)
                {
                    eqTpSoLn.eqtpsolnLoadingSignature = Convert.ToBase64String(signatureByte);
                }

                eqTpSoLn.eqtpsolnStatus = EqTpSoLnStatus.Procesado.ToString();
                eqTpSoLn.eqtpsolnInterfaceStatus = InterfaceStatus.Pendiente.ToString();
                eqTpSoLn.eqtpsolnInterfaceDate = currentDt;
                if (!string.IsNullOrEmpty(mBarcode))
                {
                    eqTpSoLn.eqtpsolnBarcode = mBarcode;
                }

                if (!string.IsNullOrEmpty(mSecurityCode))
                {
                    eqTpSoLn.eqtpsolnSecurityCode = mSecurityCode;
                }
                #endregion

                #region Unloading sequence

                if (grdUnLoadingTransport.IsVisible)
                {
                    if (lblUnLoadingDateTime.Date != null)
                    {
                        var currentTime1 = DateTime.Now.ToString("hh:mm:ss");
                        var currentDate1 = lblUnLoadingDateTime.Date.ToString("dd/MM/yyyy");
                        var curDateTime1 = currentDate1 + " " + currentTime1;
                        DateTime myDate1 = DateTime.ParseExact(curDateTime1, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        eqTpSoLn.eqtpsolnUnloadingDatetime = myDate1;
                    }
                    eqTpSoLn.eqtpsolnUnloadingLocation = lblUnLoadingLocation.Text;
                    if (location != null)
                    {
                        eqTpSoLn.eqtpsolnUnloadingLongitude = location.Longitude.ToString();
                        eqTpSoLn.eqtpsolnUnloadingLatitude = location.Latitude.ToString();
                    }
                    eqTpSoLn.eqtpsolnUnloadingOdometer = Convert.ToDecimal(txtUnLoadingOdometer.Text, CultureInfo.InvariantCulture);
                    if (imageByteArray2 != null)
                    {
                        eqTpSoLn.eqtpsolnUnloadingOdometerImage = Convert.ToBase64String(imageByteArray2);
                    }

                    if (!string.IsNullOrEmpty(txtUnLoadingNotes.Text))
                    {
                        eqTpSoLn.eqtpsolnUnloadingNotes = txtUnLoadingNotes.Text;
                    }

                    if (signatureByte1 != null)
                    {
                        eqTpSoLn.eqtpsolnUnLoadingSignature = Convert.ToBase64String(signatureByte1);
                    }
                }
                #endregion

                var response = eqTpSoLnRepository.SaveEqtpSoLn(eqTpSoLn);
                if (response)
                {
                    if (eqTpSoLn.eqtpsolnEqtpCode == mEqtp.EqtpCode && isEditeable)
                    {
                        string mRefno = eqTpSoLn.eqtpsolnRefno.TrimStart(new Char[] { '0' });
                        if (mRefno != null)
                        {
                            mEqtp.EqtpTransportCounterNo = Convert.ToInt32(mRefno);
                        }
                        var updatemFlst = eqtpRepository.UpdateEqtpTransportCounter(mEqtp);
                    }
                    await SaveTransport(eqTpSoLn);
                }
                else
                {
                    Utilties.DisplayInfo.HideLoader();
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.ShiftTransportError);
                }
            }
            catch (Exception ex)
            {
                var exception = ex.Message;
            }
            finally
            {
                Utilties.DisplayInfo.HideLoader();
            }
        }

        public async Task SaveTransport(EqTpSoLn mEqTpSoLn)
        {
            try
            {
                var eqTpSoLnModel = mEqTpSoLn.ToEqTpSoLnModel();
                List<EqTpSoLnModel> eqTpSoLnModels = new List<EqTpSoLnModel>();
                eqTpSoLnModels.Add(eqTpSoLnModel);

                if (Common.HasInternetConnection())
                {
                    var responseAPIEqTpSoLn = await eqTpSoLnAPI.SaveEqTpSoLn(eqTpSoLnModels);
                    if (responseAPIEqTpSoLn.IsSuccess)
                    {
                        //Update
                        var responseAPI1 = eqTpSoLnRepository.UpdateInterfaceStatus(InterfaceStatus.Completado.ToString(), mEqTpSoLn.EqTpSoLnId);
                        if (!grdUnLoadingTransport.IsVisible)
                            await DisplayAlert("", EndPointsMessage.TransportSuccess, "Ok");
                        else
                            await DisplayAlert("", EndPointsMessage.DownloadSuccess, "Ok");
                        await ConnectBluetooth(mEqTpSoLn);
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await DisplayAlert("", EndPointsMessage.TransportSuccessLocal, "Ok");
                        await ConnectBluetooth(mEqTpSoLn);
                        await Navigation.PopAsync();
                    }
                }
                else
                {
                    await DisplayAlert("", EndPointsMessage.TransportSuccessLocal, "Ok");
                    await ConnectBluetooth(mEqTpSoLn);
                    await Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage(ex.Message);
            }
            finally
            {
                Utilties.DisplayInfo.HideLoader();
            }
        }

        public void CorrectLabel()
        {
            try
            {
                lblHaulerCode.TextColor = Color.Black;
                lblTrailerCode.TextColor = Color.Black;
                lblRFID.TextColor = Color.Black;
                lblEqipCode.TextColor = Color.Black;
                lblEqipDesc.TextColor = Color.Black;
                lblProjectCode.TextColor = Color.Black;
                lblProjectDesc.TextColor = Color.Black;
                lblProjactCode.TextColor = Color.Black;
                lblActDesc.TextColor = Color.Black;
                lblLoadingLocation.TextColor = Color.Black;
                txtLoadingOdometer.PlaceholderColor = Color.Black;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("CorrectLabel/AddTransportPage" + ex.Message);
            }
        }

        public bool ValidatedFields()
        {
            bool isValid = false;
            try
            {
                if (string.IsNullOrEmpty(lblHaulerCode.Text))
                {
                    CorrectLabel();
                    lblHaulerCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Hauler);
                }
                else if (string.IsNullOrEmpty(lblTrailerCode.Text))
                {
                    CorrectLabel();
                    lblTrailerCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Trailer);
                }
                else if (frmCaptureRFID.IsVisible && (string.IsNullOrEmpty(lblCaptureRFID.Text) || lblCaptureRFID.Text == "Captura RFID"))
                {
                    CorrectLabel();
                    lblRFID.TextColor = Color.Red;
                    lblCaptureRFID.TextColor = Color.Red;
                    lblEqipCode.TextColor = Color.Red;
                    lblEqipDesc.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.RFID);
                }
                else if (frmEqipmentCode.IsVisible && (string.IsNullOrEmpty(lblRFID.Text) || lblRFID.Text == "RFID"))
                {
                    CorrectLabel();
                    lblRFID.TextColor = Color.Red;
                    lblEqipCode.TextColor = Color.Red;
                    lblEqipDesc.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.RFID);
                }
                else if (string.IsNullOrEmpty(lblProjectCode.Text) || lblProjectCode.Text == "Proyecto")
                {
                    CorrectLabel();
                    lblProjectCode.TextColor = Color.Red;
                    lblProjectDesc.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.ProjectCode);
                }
                else if (string.IsNullOrEmpty(lblProjactCode.Text) || lblProjactCode.Text == "Partida")
                {
                    CorrectLabel();
                    lblProjactCode.TextColor = Color.Red;
                    lblActDesc.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Projact);
                }
                else if (string.IsNullOrEmpty(lblLoadingLocation.Text) || lblLoadingLocation.Text == "Localización")
                {
                    CorrectLabel();
                    lblLoadingLocation.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Location);
                }
                else if (string.IsNullOrEmpty(txtLoadingOdometer.Text))
                {
                    CorrectLabel();
                    txtLoadingOdometer.PlaceholderColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Odometer);
                }
                else if (imageByteArray1 == null)
                {
                    CorrectLabel();
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Picture);
                }
                else if (mEqtp != null && mEqtp.EqtpSignEnabled && signatureByte == null)
                {
                    CorrectLabel();
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Signature);
                }
                else
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ValidatedFields/AddTransportPage" + ex.Message);
            }
            return isValid;
        }

        public bool UnLoadingValidatedFields()
        {
            bool isValid = false;
            try
            {
                if (lblUnLoadingDateTime.Date == null)
                {
                    CorrectLabel();
                    lblUnLoadingDateTime.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.UnLoadingDate);
                }
                else if (string.IsNullOrEmpty(lblUnLoadingLocation.Text) || lblUnLoadingLocation.Text == "Localización")
                {
                    CorrectLabel();
                    lblUnLoadingLocation.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Location);
                }
                else if (string.IsNullOrEmpty(txtUnLoadingOdometer.Text))
                {
                    CorrectLabel();
                    txtUnLoadingOdometer.PlaceholderColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Odometer);
                }
                else if (imageByteArray2 == null)
                {
                    CorrectLabel();
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Picture);
                }
                else if (mEqtp != null && mEqtp.EqtpSignEnabled && signatureByte1 == null)
                {
                    CorrectLabel();
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Signature);
                }
                else
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("UnLoadingValidatedFields/AddTransportPage" + ex.Message);
            }
            return isValid;
        }
        #endregion

        #region Events
        private async void BtnBack_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private void ImgEqipmentCode_Clicked(object sender, EventArgs e)
        {
            NavigateToEquipmentSelection();
        }

        private void BtnEqipmentCode_Tapped(object sender, EventArgs e)
        {
            NavigateToEquipmentSelection();
        }

        private void ImgProjectCode_Clicked(object sender, EventArgs e)
        {
            NavigateToProjectSelection();
        }

        private void BtnProjectCode_Tapped(object sender, EventArgs e)
        {
            NavigateToProjectSelection();
        }

        private void ImgLoadingLocation_Clicked(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private void BtnLoadingLocation_Tapped(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private async void BtnLoadingDelete_Tapped(object sender, EventArgs e)
        {
            PopUp.DeleteImagePopup deleteImagePopup = new PopUp.DeleteImagePopup();
            try
            {
                deleteImagePopup.isRefresh += (s, e1) =>
                {
                    var isDelete = (bool)s;
                    if (isDelete)
                    {
                        imageByteArray1 = null;
                        btnLoadingDelete.IsVisible = false;
                        LoadingPicture1.Source = "iconGrayNoImage.png";
                    }
                };
                await PopupNavigation.PushAsync(deleteImagePopup, false);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnLoadingDelete_Tapped/AddTransportPage" + ex.Message);
            }
        }

        private async void BtnLoadingUploadPicture1_Tapped(object sender, EventArgs e)
        {
            try
            {
                var noImage = "iconGrayNoImage.png";
                var selectedImage = LoadingPicture1.Source.ToString().Replace("File: ", string.Empty);
                if (selectedImage == noImage)
                {
                    if (LoadingPicture1.Source == null)
                    {
                        btnLoadingDelete.IsVisible = false;
                        LoadingPicture1.Source = "iconGrayNoImage.png";
                    }
                    else
                    {
                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {
                            await DisplayAlert("No Camera", ":( No camera available.", "OK");
                            return;
                        }

                        var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                        var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                        if (cameraStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted || storageStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                            cameraStatus = results[Permission.Camera];
                            storageStatus = results[Permission.Storage];
                        }

                        if (cameraStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted && storageStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            file1 = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                            {
                                Directory = "AlbaSanchez",
                                SaveToAlbum = true,
                                CompressionQuality = 50,
                                CustomPhotoSize = 50,
                                DefaultCamera = CameraDevice.Rear,
                                AllowCropping = true,
                            });

                            if (file1 == null)
                            {
                                btnLoadingDelete.IsVisible = false;
                                LoadingPicture1.Source = "iconGrayNoImage.png";
                                return;
                            }

                            var imagefile = file1.GetStreamWithImageRotatedForExternalStorage();
                            imagefile.Position = 0;
                            if (imagefile != null)
                                imageByteArray1 = GetImageStreamAsBytes(imagefile);

                            LoadingPicture1.Aspect = Aspect.Fill;
                            file1.Dispose();
                            LoadingPicture1.Source = ImageSource.FromStream(() => new MemoryStream(imageByteArray1));

                            btnLoadingDelete.IsVisible = true;
                        }
                        else
                        {
                            await DisplayAlert("Permissions denied", "Unable to take photos.", "OK");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnLoadingUploadPicture1_Tapped/AddTransportPage" + ex.Message);
            }
        }

        private async void BtnLoadingSignature_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (isEditeable)
                {
                    PopUp.SignaturePopup signaturePopup = new PopUp.SignaturePopup(false, string.Empty);
                    signaturePopup.isRefresh += (s, e1) =>
                    {
                        signatureByte = (byte[])s;
                        if (signatureByte == null)
                        {
                            lblLoadingComplete.IsVisible = false;
                        }
                        else
                        {
                            lblLoadingComplete.IsVisible = true;
                        }
                    };
                    await PopupNavigation.PushAsync(signaturePopup, false);
                }
                else
                {
                    lblLoadingComplete.IsVisible = true;
                    await PopupNavigation.PushAsync(new ShowSignaturePopup(true, mEqTpSoLn.eqtpsolnLoadingSignature), false);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnLoadingSignature_Tapped/AddTransportPage" + ex.Message);
            }
        }

        private void BtnLoadingTransport_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (!grdLoading.IsVisible)
                {
                    grdLoading.IsVisible = true;
                    imgPictureDown.Rotation = 180;
                    //btnSave.IsVisible = false;

                    //grdUnLoading.IsVisible = false;
                    //imgPictureDownUn.Rotation = 0;
                }
                else
                {
                    grdLoading.IsVisible = false;
                    imgPictureDown.Rotation = 0;
                    //btnSave.IsVisible = true;

                    //grdUnLoading.IsVisible = true;
                    //imgPictureDownUn.Rotation = 180;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnLoadingTransport_Tapped/AddTransportPage" + ex.Message);
            }
        }

        private void BtnUnLoadingTransport_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (!grdUnLoading.IsVisible)
                {
                    grdUnLoading.IsVisible = true;
                    imgPictureDownUn.Rotation = 180;

                    //if (!isEditeable1)
                    //    btnSave.IsVisible = true;

                    //grdLoading.IsVisible = false;
                    //imgPictureDown.Rotation = 0;
                }
                else
                {
                    grdUnLoading.IsVisible = false;
                    imgPictureDownUn.Rotation = 0;
                    //btnSave.IsVisible = false;

                    //grdLoading.IsVisible = true;
                    //imgPictureDown.Rotation = 180;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnUnLoadingTransport_Tapped/AddTransportPage" + ex.Message);
            }
        }

        private void ImgUnLoadingLocation_Clicked(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private void BtnUnLoadingLocation_Tapped(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private async void BtnUnLoadingDelete_Tapped(object sender, EventArgs e)
        {
            PopUp.DeleteImagePopup deleteImagePopup = new PopUp.DeleteImagePopup();
            try
            {
                deleteImagePopup.isRefresh += (s, e1) =>
                {
                    var isDelete = (bool)s;
                    if (isDelete)
                    {
                        imageByteArray2 = null;
                        btnUnLoadingDelete.IsVisible = false;
                        UnLoadingPicture1.Source = "iconGrayNoImage.png";
                    }
                };
                await PopupNavigation.PushAsync(deleteImagePopup, false);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnUnLoadingDelete_Tapped/AddTransportPage" + ex.Message);
            }
        }

        private async void BtnUnLoadingUploadPicture1_Tapped(object sender, EventArgs e)
        {
            try
            {
                var noImage = "iconGrayNoImage.png";
                var selectedImage = UnLoadingPicture1.Source.ToString().Replace("File: ", string.Empty);
                if (selectedImage == noImage)
                {
                    if (UnLoadingPicture1.Source == null)
                    {
                        btnUnLoadingDelete.IsVisible = false;
                        UnLoadingPicture1.Source = "iconGrayNoImage.png";
                    }
                    else
                    {
                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {
                            await DisplayAlert("No Camera", ":( No camera available.", "OK");
                            return;
                        }

                        var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                        var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                        if (cameraStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted || storageStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                            cameraStatus = results[Permission.Camera];
                            storageStatus = results[Permission.Storage];
                        }

                        if (cameraStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted && storageStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            file2 = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                            {
                                Directory = "MTCT",
                                SaveToAlbum = true,
                                CompressionQuality = 50,
                                CustomPhotoSize = 50,
                                DefaultCamera = CameraDevice.Rear,
                                AllowCropping = true,
                            });

                            if (file2 == null)
                            {
                                btnUnLoadingDelete.IsVisible = false;
                                UnLoadingPicture1.Source = "iconGrayNoImage.png";
                                return;
                            }

                            var imagefile = file2.GetStreamWithImageRotatedForExternalStorage();
                            imagefile.Position = 0;
                            if (imagefile != null)
                                imageByteArray2 = GetImageStreamAsBytes(imagefile);

                            UnLoadingPicture1.Aspect = Aspect.Fill;
                            file2.Dispose();
                            UnLoadingPicture1.Source = ImageSource.FromStream(() => new MemoryStream(imageByteArray2));

                            btnUnLoadingDelete.IsVisible = true;
                        }
                        else
                        {
                            await DisplayAlert("Permissions denied", "Unable to take photos.", "OK");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnUnLoadingUploadPicture1_Tapped/AddTransportPage" + ex.Message);
            }
        }

        private async void BtnUnLoadingSignature_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (!isEditeable1)
                {
                    PopUp.SignaturePopup signaturePopup = new PopUp.SignaturePopup(false, string.Empty);
                    signaturePopup.isRefresh += (s, e1) =>
                    {
                        signatureByte1 = (byte[])s;
                        if (signatureByte1 == null)
                        {
                            lblUnLoadingComplete.IsVisible = false;
                        }
                        else
                        {
                            lblUnLoadingComplete.IsVisible = true;
                        }
                    };
                    await PopupNavigation.PushAsync(signaturePopup, false);
                }
                else
                {
                    lblUnLoadingComplete.IsVisible = true;
                    await PopupNavigation.PushAsync(new ShowSignaturePopup(true, mEqTpSoLn.eqtpsolnUnLoadingSignature), false);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnUnLoadingSignature_Tapped/AddTransportPage" + ex.Message);
            }
        }

        private async void BtnCancel_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private async void BtnSave_Tapped(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool validationResult = false;
            if (!grdUnLoadingTransport.IsVisible)
            {
                validationResult = ValidatedFields();
                message = "Grabar transporte?";
            }
            else
            {
                validationResult = UnLoadingValidatedFields();
                message = "Grabar descarga de equipo?";
            }

            if (validationResult)
            {
                PopUp.ConfirmationPopup confirmationPopup = new PopUp.ConfirmationPopup(message);
                confirmationPopup.isRefresh += async (s, e1) =>
                {
                    var isConfirm = (bool)s;
                    if (isConfirm)
                    {
                        await ReadUIAndSaveTransporter();
                    }
                };
                await PopupNavigation.PushAsync(confirmationPopup, false);
            }
        }

        private void BtnActCode_Tapped(object sender, EventArgs e)
        {
            NavigateToProjactSelection();
        }

        private void ImgActCode_Clicked(object sender, EventArgs e)
        {
            NavigateToProjactSelection();
        }

        private void TxtUnLoadingOdometer_Completed(object sender, EventArgs e)
        {
            if (mEqTpSoLn != null && !string.IsNullOrEmpty(txtUnLoadingOdometer.Text))
            {
                decimal unloadmeter = Convert.ToDecimal(txtUnLoadingOdometer.Text);
                decimal loadmeter = mEqTpSoLn.eqtpsolnLoadingOdometer;
                if (loadmeter > unloadmeter)
                {
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.OdometerNotLess);
                    txtUnLoadingOdometer.Text = string.Empty;
                }
            }
        }

        private void TxtUnLoadingOdometer_Unfocused(object sender, FocusEventArgs e)
        {
            if (mEqTpSoLn != null && !string.IsNullOrEmpty(txtUnLoadingOdometer.Text))
            {
                decimal unloadmeter = Convert.ToDecimal(txtUnLoadingOdometer.Text);
                decimal loadmeter = mEqTpSoLn.eqtpsolnLoadingOdometer;
                if (loadmeter > unloadmeter)
                {
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.OdometerNotLess);
                    txtUnLoadingOdometer.Text = string.Empty;
                }
            }
        }

        private async void FrmScanEquipo_Tapped(object sender, EventArgs e)
        {
            try
            {
                var scanRfidPopup = new PopUp.ScanRfidPopup(Rfids);
                scanRfidPopup.IsRefresh += (s1, e1) =>
                {
                    equipment = (Equipment)s1;
                    if (equipment != null && !string.IsNullOrEmpty(equipment.EquipmentCode))
                    {
                        lblCaptureRFID.Text = equipment.EquipmentRFID;
                        lblRFID.Text = equipment.EquipmentRFID;
                        lblEqipCode.Text = equipment.EquipmentCode;
                        lblEqipDesc.Text = equipment.EquipmentDesc;
                    }
                };
                await PopupNavigation.PushAsync(scanRfidPopup, false);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("FrmScanEquipo_Tapped/AddTransportPage" + ex.Message);
            }
        }
        #endregion
    }
}