﻿using Acr.UserDialogs;
using MTCT.API;
using MTCT.Interface;
using MTCT.Models;
using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using MTCT.Views.PopUp;
using Plugin.Geolocator;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using ZPLForge;
using ZPLForge.Common;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CloseOrderPage : ContentPage
    {
        #region Objects
        DateTime currentDt;
        ShiftRepository shiftRepository;
        EqTpSoLnRepository eqTpSoLnRepository;
        LocationRepository locationRepository;
        EqtpRepository eqtpRepository;
        Eqtp mEqtpData;
        EqTpSo mEqTpSo;
        public List<EqTpSoLnLocal> mEqTpSoLn;
        Locations locations;
        EqTpSoLnAPI eqTpSoLnAPI;
        FlstShiftAPI flstShiftAPI;
        List<Locations> mLocations;
        Plugin.Geolocator.Abstractions.Position location = null;
        bool isLocation = false;
        MediaFile file1;
        private byte[] imageByteArray1;
        private byte[] openingimageByteArray;
        byte[] signatureByte;
        byte[] openingSignatureByte;
        bool isBluetooth = false;
        bool isPrintSuccess = false;
        List<BluetoothList> bluetoothList;
        #endregion

        #region Constructor
        public CloseOrderPage()
        {
            InitializeComponent();
            this.currentDt = DateTime.Now;
            InitializeObject();
            BindOpenOrderData();
            BindLocations();
            lblCloseDateTime.Date = currentDt;

        }
        #endregion

        #region Methods
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (!isLocation)
            {
                BindMap();
            }
        }

        public void InitializeObject()
        {
            mEqtpData = new Eqtp();
            shiftRepository = new ShiftRepository();
            eqTpSoLnRepository = new EqTpSoLnRepository();
            locationRepository = new LocationRepository();
            eqtpRepository = new EqtpRepository();
            mEqTpSo = new EqTpSo();
            mEqTpSoLn = new List<EqTpSoLnLocal>();
            locations = new Locations();
            eqTpSoLnAPI = new EqTpSoLnAPI();
            flstShiftAPI = new FlstShiftAPI();
            mLocations = new List<Locations>();
            bluetoothList = new List<BluetoothList>();
        }

        public void BindOpenOrderData()
        {
            try
            {
                mEqTpSo = shiftRepository.GetOpenOrder(Common.OpenStatus.ToString());
                if (mEqTpSo != null)
                {
                    lblShiftRefno.Text = "Transporte " + mEqTpSo.eqtpsoEqtpCode;
                    lblRefno.Text = mEqTpSo.eqtpsoRefno;
                    lblHaulerCode.Text = mEqTpSo.eqtpsoEqtphaulerCode;
                    lblTrailerCode.Text = mEqTpSo.eqtpsoEqtptrailerCode;
                    lblDriverCode.Text = mEqTpSo.eqtpsoDriverCode;
                    lblDriverName.Text = mEqTpSo.eqtpsoDriverName;
                    lblLocation.Text = mEqTpSo.eqtpsoOpeningLocation;
                    lblKm.Text = mEqTpSo.eqtpsoOpeningOdometer.ToString();
                    lblNotes.Text = mEqTpSo.eqtpsoOpeningNotes;
                    lblDateTime.Text = mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy");
                    if (mEqTpSo.eqtpsoOpeningOdometerImage != null)
                    {
                        var stream1 = LoadImage(mEqTpSo.eqtpsoOpeningOdometerImage);
                    }
                    openingSignatureByte = Convert.FromBase64String(mEqTpSo.eqtpsoOpeningSignature);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage(ex.Message);
            }
        }

        public Image LoadImage(string base64string)
        {
            byte[] bytes = Convert.FromBase64String(base64string);

            using (MemoryStream ms = new MemoryStream(bytes))
            {
                openingimageByteArray = bytes;
                imgPicture1.Source = ImageSource.FromStream(() => new MemoryStream(bytes));
                imgPicture1.Aspect = Aspect.Fill;
            }

            return imgPicture1;
        }

        public void BindMap()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        locator.DesiredAccuracy = 5;
                        location = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(1000));
                        if (grdOpenOrder.IsVisible)
                        {
                            BindOpeningMap(location.Latitude, location.Longitude);
                        }
                        else
                        {
                            BindCloseingMap(location.Latitude, location.Longitude);
                            if (!string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningLongitude) && !string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningLatitude))
                            {
                                BindOpeningMap(Convert.ToDouble(mEqTpSo.eqtpsoOpeningLatitude.Replace(".", ",").Replace("/", "-")), Convert.ToDouble(mEqTpSo.eqtpsoOpeningLongitude.Replace(".", ",").Replace("/", "-")));
                            }
                        }
                    }
                    else
                    {
                        var myAction = await DisplayAlert("Location", "Activa la ubicación", "OK", "CANCELAR");
                        if (myAction)
                        {
                            if (Device.RuntimePlatform == global::Xamarin.Forms.Device.Android)
                            {
                                global::Xamarin.Forms.DependencyService.Get<ILocSettings>().OpenSettings();
                            }
                            else
                            {
                                await DisplayAlert("Device", "Estás usando algún otro turno", "YEP");
                            }
                        }
                        else
                        {
                            isLocation = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utilties.DisplayInfo.ErrorMessage("BindMap/CloseOrderPage" + ex.Message);
                    BindMap();
                }
            });
        }

        public void BindLocations()
        {
            try
            {
                mLocations = locationRepository.GetLocationss();
                if (mLocations != null && mLocations.Count == 1)
                {
                    lblCloseingLocationCode.Text = mLocations.Select(x => x.EqtplocationCode).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindLocations/OrderPage" + ex.Message);
            }
        }

        public async void BindOpeningMap(double latitude, double longitude)
        {
            try
            {
                if (Common.HasInternetConnection())
                {
                    var mAddress = await Geocoding.GetPlacemarksAsync(latitude, longitude);
                    string addressLabel = string.Empty;
                    foreach (var add in mAddress)
                    {
                        addressLabel += add.FeatureName + " ";
                    }
                    var pin = new Pin
                    {
                        Type = PinType.Place,
                        Label = addressLabel,
                        Position = new Position(latitude, longitude),
                        // Address = address
                    };

                    xfmapOpen.Pins.Add(pin);
                    xfmapOpen.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitude, longitude), Distance.FromMeters(150.0)));
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage(ex.Message);
                BindMap();
            }
        }

        public async void BindCloseingMap(double latitude, double longitude)
        {
            try
            {
                if (Common.HasInternetConnection())
                {
                    var mAddress = await Geocoding.GetPlacemarksAsync(latitude, longitude);
                    string addressLabel = string.Empty;
                    foreach (var add in mAddress)
                    {
                        addressLabel += add.FeatureName + " ";
                    }
                    var pin = new Pin
                    {
                        Type = PinType.Place,
                        Label = addressLabel,
                        Position = new Position(latitude, longitude),
                        // Address = address
                    };

                    xfmap.Pins.Add(pin);
                    xfmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitude, longitude), Distance.FromMeters(150.0)));
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage(ex.Message);
                BindMap();
            }
        }

        public async void SaveCloseOrder(string status)
        {
            try
            {
                EqTpSo mEqTpOrder = new EqTpSo();
                if (mEqTpSo.Id > 0)
                    mEqTpOrder.Id = mEqTpSo.Id;

                #region  OpenOrder
                if (mEqTpSo != null)
                {
                    mEqTpOrder.EqTpSoId = mEqTpSo.EqTpSoId;
                    mEqTpOrder.eqtpsoEqtpCode = mEqTpSo.eqtpsoEqtpCode;
                    mEqTpOrder.eqtpsoDeviceCode = mEqTpSo.eqtpsoDeviceCode;
                    mEqTpOrder.eqtpsoRefno = mEqTpSo.eqtpsoRefno;
                    mEqTpOrder.eqtpsoStatus = status;
                    mEqTpOrder.eqtpsoInterfaceStatus = InterfaceStatus.Pendiente.ToString();
                    mEqTpOrder.eqtpsoInterfaceDate = DateTime.Now;
                    mEqTpOrder.eqtpsoEqtphaulerCode = lblHaulerCode.Text;
                    mEqTpOrder.eqtpsoOpeningLocation = lblLocation.Text;
                    mEqTpOrder.eqtpsoDriverCode = lblDriverCode.Text;
                    mEqTpOrder.eqtpsoDriverName = lblDriverName.Text;
                    mEqTpOrder.eqtpsoEqtptrailerCode = lblTrailerCode.Text;
                    if (!string.IsNullOrEmpty(lblDateTime.Text))
                    {
                        mEqTpOrder.eqtpsoOpeningDate = Convert.ToDateTime(lblDateTime.Text);
                    }

                    if (openingimageByteArray != null)
                    {
                        mEqTpOrder.eqtpsoOpeningOdometerImage = Convert.ToBase64String(openingimageByteArray);
                    }

                    mEqTpOrder.eqtpsoOpeningLongitude = mEqTpSo.eqtpsoOpeningLongitude;
                    mEqTpOrder.eqtpsoOpeningLatitude = mEqTpSo.eqtpsoOpeningLatitude;
                    mEqTpOrder.eqtpsoOpeningOdometer = Convert.ToDecimal(lblKm.Text, CultureInfo.InvariantCulture);
                    mEqTpOrder.eqtpsoUserCode = mEqTpSo.eqtpsoUserCode;
                    mEqTpOrder.eqtpsoUserName = mEqTpSo.eqtpsoUserName;
                    mEqTpOrder.eqtpsoOpeningNotes = lblNotes.Text;
                    if (openingSignatureByte != null)
                    {
                        mEqTpOrder.eqtpsoOpeningSignature = Convert.ToBase64String(openingSignatureByte);
                    }
                }
                #endregion

                #region  CloseOrder
                var currentTime = DateTime.Now.ToString("hh:mm:ss");
                var currentDate = lblCloseDateTime.Date.ToString("dd/MM/yyyy");
                var curDateTime = currentDate + " " + currentTime;
                DateTime myDate = DateTime.ParseExact(curDateTime, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                mEqTpOrder.eqtpsoClosingDate = myDate;
                mEqTpOrder.eqtpsoClosingLocation = lblCloseingLocationCode.Text;
                if (location != null && location.Latitude != 0 && location.Longitude != 0)
                {
                    mEqTpOrder.eqtpsoClosingLongitude = location.Longitude.ToString();
                    mEqTpOrder.eqtpsoClosingLatitude = location.Latitude.ToString();
                }
                else
                {
                    mEqTpOrder.eqtpsoClosingLongitude = "0";
                    mEqTpOrder.eqtpsoClosingLatitude = "0";
                }

                decimal eptpsoClosingOdometer = 0;
                decimal.TryParse(txtCloseOdometer.Text, out eptpsoClosingOdometer);
                mEqTpOrder.eptpsoClosingOdometer = eptpsoClosingOdometer;
                // Convert.ToDecimal(txtCloseOdometer.Text, CultureInfo.InvariantCulture);

                if (imageByteArray1 != null)
                    mEqTpOrder.eqtpsoClosingOdometerImage = Convert.ToBase64String(imageByteArray1);
                if (signatureByte != null)
                    mEqTpOrder.eqtpsoClosingSignature = Convert.ToBase64String(signatureByte);
                if (!string.IsNullOrEmpty(txtNotes.Text))
                    mEqTpOrder.eqtpsoClosingNotes = txtNotes.Text;
                #endregion

                var response = shiftRepository.SaveCloseOrder(mEqTpOrder);
                if (response)
                {
                    EqTpSoModel mEqTpSoModel = new EqTpSoModel();

                    #region OpenOrderAPI
                    mEqTpSoModel.eqtpsoEqtpCode = mEqTpOrder.eqtpsoEqtpCode;
                    mEqTpSoModel.eqtpsoRefno = mEqTpOrder.eqtpsoRefno;
                    mEqTpSoModel.eqtpsoStatus = mEqTpOrder.eqtpsoStatus;

                    mEqTpSoModel.eqtpsoInterfaceStatus = Common.ConvertSpainishToEnglish(InterfaceStatus.Completado.ToString());
                    if (mEqTpOrder.eqtpsoInterfaceDate != null && mEqTpOrder.eqtpsoInterfaceDate != DateTime.MinValue)
                    {
                        mEqTpSoModel.eqtpsoInterfaceDate = mEqTpOrder.eqtpsoInterfaceDate;
                    }

                    mEqTpSoModel.eqtpsoEqtphaulerCode = mEqTpOrder.eqtpsoEqtphaulerCode;
                    mEqTpSoModel.eqtpsoDeviceCode = mEqTpOrder.eqtpsoDeviceCode;
                    mEqTpSoModel.eqtpsoOpeningDate = mEqTpOrder.eqtpsoOpeningDate;
                    mEqTpSoModel.eqtpsoOpeningLocation = mEqTpOrder.eqtpsoOpeningLocation;
                    mEqTpSoModel.eqtpsoEqtptrailerCode = mEqTpOrder.eqtpsoEqtptrailerCode;
                    mEqTpSoModel.eqtpsoDriverCode = mEqTpOrder.eqtpsoDriverCode;
                    mEqTpSoModel.eqtpsoDriverName = mEqTpOrder.eqtpsoDriverName;
                    mEqTpSoModel.eqtpsoOpeningOdometer = mEqTpOrder.eqtpsoOpeningOdometer;

                    if (imageByteArray1 != null)
                    {
                        mEqTpSoModel.eqtpsoOpeningOdometerImage = Convert.ToBase64String(imageByteArray1);
                    }

                    if (location != null && mEqTpOrder.eqtpsoOpeningLongitude != "0" && mEqTpOrder.eqtpsoOpeningLongitude != "0")
                    {
                        mEqTpSoModel.eqtpsoOpeningLongitude = mEqTpOrder.eqtpsoOpeningLongitude;
                        mEqTpSoModel.eqtpsoOpeningLatitude = mEqTpOrder.eqtpsoOpeningLatitude;
                    }
                    else
                    {
                        if (Common.HasInternetConnection())
                        {
                            var locator = CrossGeolocator.Current;
                            if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                            {
                                locator.DesiredAccuracy = 5;

                                Device.BeginInvokeOnMainThread(async () =>
                                {
                                    location = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(1000));
                                });
                            }
                            if (location != null && location.Longitude != 0 && location.Latitude != 0)
                            {
                                mEqTpSoModel.eqtpsoOpeningLongitude = location.Longitude.ToString();
                                mEqTpSoModel.eqtpsoOpeningLatitude = location.Latitude.ToString();
                            }
                            else
                            {
                                mEqTpSoModel.eqtpsoOpeningLongitude = "0";
                                mEqTpSoModel.eqtpsoOpeningLatitude = "0";
                            }
                        }
                        else
                        {
                            mEqTpSoModel.eqtpsoOpeningLongitude = "0";
                            mEqTpSoModel.eqtpsoOpeningLatitude = "0";
                        }
                    }

                    mEqTpSoModel.eqtpsoOpeningNotes = mEqTpOrder.eqtpsoOpeningNotes;
                    mEqTpSoModel.eqtpsoUserCode = mEqTpOrder.eqtpsoUserCode;
                    mEqTpSoModel.eqtpsoUserName = mEqTpOrder.eqtpsoUserName;
                    mEqTpSoModel.eqtpsoOpeningSignature = mEqTpOrder.eqtpsoOpeningSignature;
                    #endregion

                    #region CLoseOrderAPI
                    mEqTpSoModel.eqtpsoClosingDate = mEqTpOrder.eqtpsoClosingDate;
                    mEqTpSoModel.eqtpsoClosingLocation = mEqTpOrder.eqtpsoClosingLocation;
                    mEqTpSoModel.eqtpsoClosingLongitude = mEqTpOrder.eqtpsoClosingLongitude;
                    mEqTpSoModel.eqtpsoClosingLatitude = mEqTpOrder.eqtpsoClosingLatitude;

                    mEqTpSoModel.eptpsoClosingOdometer = mEqTpOrder.eptpsoClosingOdometer;
                    mEqTpSoModel.eqtpsoClosingOdometerImage = mEqTpOrder.eqtpsoClosingOdometerImage;
                    mEqTpSoModel.eqtpsoClosingSignature = mEqTpOrder.eqtpsoClosingSignature;
                    mEqTpSoModel.eqtpsoClosingNotes = mEqTpOrder.eqtpsoClosingNotes;
                    #endregion
                    OrderCloseModelDetail orderCloseModelDetail = new OrderCloseModelDetail();
                    List<EqTpSoLnLocal> eqTpSoLns = eqTpSoLnRepository.GetAllEqTpSoLns();
                    if (eqTpSoLns != null && eqTpSoLns.Count > 0)
                    {
                        foreach (var eqTpSoLn in eqTpSoLns.Where(x => x.eqtpsolnEqtpsoRefno == mEqTpSoModel.eqtpsoRefno))
                        {
                            var eqTpSoLnModel = eqTpSoLn.ToEqTpSoLnModel();
                            orderCloseModelDetail.eqTpSoLn.Add(eqTpSoLnModel);
                        }
                    }
                    orderCloseModelDetail.eqTpSo.Add(mEqTpSoModel);

                    OrderCloseModelDetail orderCloseModelDetail1 = new OrderCloseModelDetail();
                    orderCloseModelDetail1 = orderCloseModelDetail;

                    if (await App.CheckInternetConnection())
                    {
                        var responseAPI = await flstShiftAPI.SaveCloseShift(orderCloseModelDetail1);
                        if (responseAPI != null && responseAPI.IsSuccess)
                        {
                            var upDateResponse = shiftRepository.UpdateInterfaceStatus(InterfaceStatus.Completado.ToString(), mEqTpOrder.EqTpSoId, true);
                            await DisplayAlert("", EndPointsMessage.OrderCloseSuccess, "Ok");
                            await ConnectBluetooth();
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            Utilties.DisplayInfo.HideLoader();
                            await DisplayAlert("", EndPointsMessage.OrderCloseSuccess, "Ok");
                            await Navigation.PopAsync();
                        }
                    }
                    Utilties.DisplayInfo.HideLoader();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage(ex.Message);
            }
            finally
            {
                Utilties.DisplayInfo.HideLoader();
            }
        }

        public async Task ConnectBluetooth()
        {
            try
            {
                isBluetooth = true;
                Utilties.DisplayInfo.ShowLoader();
                bluetoothList = await DependencyService.Get<IPrintReceipt>().BluetoothList();
                Utilties.DisplayInfo.HideLoader();
                // Issues Print Copy
                if (bluetoothList != null && bluetoothList.Count > 0)
                {
                    //Get Configuration Data
                    isBluetooth = false;
                    mEqtpData = eqtpRepository.GetEqTp();
                    if (mEqtpData != null && mEqtpData.EqtpOrderPrint > 0)
                    {
                        if (Common.mAppkey == null || Common.mAppkey.Id == 0)
                        {
                            if (bluetoothList.Count == 1)
                            {
                                AppKey mAppkey = new AppKey();
                                mAppkey.Id = 1;
                                mAppkey.KeyName = bluetoothList[0].Name;
                                mAppkey.KeyAddress = bluetoothList[0].Address;
                                Common.mAppkey = mAppkey;

                                if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                                {
                                    var result = ZebraOrderPrint(Common.mAppkey, mEqtpData.EqtpOrderPrint);
                                    if (await result)
                                    {
                                        isPrintSuccess = true;
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < mEqtpData.EqtpOrderPrint; i++)
                                    {
                                        var isSuccsess = await PrintOrderRecipt(Common.mAppkey);
                                        await Task.Delay(3000);
                                        if (!string.IsNullOrEmpty(isSuccsess) && isSuccsess.ToLower() == "true")
                                        {
                                            isPrintSuccess = true;
                                        }
                                    }
                                }

                                if (isPrintSuccess)
                                {
                                    await DisplayAlert("", "Impresión completada", "Ok");
                                }
                                else
                                {
                                    await DisplayAlert("", "No se pudo imprimir", "Ok");
                                }
                            }
                            else
                            {
                                BluetoothPopup bluetoothPopup = new BluetoothPopup();
                                bluetoothPopup.RefreshEvent += async (s11, e11) =>
                                {
                                    AppKey mAppkey = new AppKey();
                                    var bluetooth = (BluetoothList)s11;
                                    if (bluetooth != null)
                                    {
                                        mAppkey.Id = 1;
                                        mAppkey.KeyName = bluetooth.Name;
                                        mAppkey.KeyAddress = bluetooth.Address;
                                        Common.mAppkey = mAppkey;

                                        if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                                        {
                                            var result = ZebraOrderPrint(Common.mAppkey, mEqtpData.EqtpOrderPrint);
                                            if (await result)
                                            {
                                                isPrintSuccess = true;
                                            }
                                        }
                                        else
                                        {
                                            for (int i = 0; i < mEqtpData.EqtpOrderPrint; i++)
                                            {
                                                var isSuccsess = await PrintOrderRecipt(Common.mAppkey);
                                                await Task.Delay(3000);
                                                if (!string.IsNullOrEmpty(isSuccsess) && isSuccsess.ToLower() == "true")
                                                {
                                                    isPrintSuccess = true;
                                                }
                                            }
                                        }

                                        if (isPrintSuccess)
                                        {
                                            await DisplayAlert("", "Impresión completada", "Ok");
                                        }
                                        else
                                        {
                                            await DisplayAlert("", "No se pudo imprimir", "Ok");
                                        }
                                    }
                                    await Navigation.PopAsync();
                                };
                                await PopupNavigation.PushAsync(bluetoothPopup, false);
                            }
                        }
                        else
                        {
                            if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                            {
                                var result = ZebraOrderPrint(Common.mAppkey, mEqtpData.EqtpOrderPrint);
                                if (await result)
                                {
                                    isPrintSuccess = true;
                                }
                            }
                            else
                            {
                                for (int i = 0; i < mEqtpData.EqtpOrderPrint; i++)
                                {
                                    var isSuccsess = await PrintOrderRecipt(Common.mAppkey);
                                    await Task.Delay(3000);
                                    if (!string.IsNullOrEmpty(isSuccsess) && isSuccsess.ToLower() == "true")
                                    {
                                        isPrintSuccess = true;
                                    }
                                }
                            }

                            if (isPrintSuccess)
                            {
                                await DisplayAlert("", "Impresión completada", "Ok");
                            }
                            else
                            {
                                await DisplayAlert("", "No se pudo imprimir", "Ok");
                            }
                        }
                    }
                }
                else
                {
                    await Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ConnectBluetooth/CloseShiftPage Exception: " + ex.Message);
            }
        }

        public async Task<string> PrintOrderRecipt(AppKey mAppKey)
        {
            try
            {
                if (mEqTpSo == null)
                    mEqTpSo = new EqTpSo();

                mEqTpSo = shiftRepository.GetStartShift();
                if (mEqTpSo != null)
                {
                    if (mEqTpSoLn == null)
                        mEqTpSoLn = new List<EqTpSoLnLocal>();

                    mEqTpSoLn = eqTpSoLnRepository.GetAllEqTpSoLns();
                    var mHaulerData = eqtpRepository.GetEqTp();
                    Acr.UserDialogs.UserDialogs.Instance.ShowLoading("Printing...");
                    await Task.Delay(1 * 200);
                    string nullString = "N/A";
                    Receipt mReceipt = new Receipt();
                    List<string> barcodes = new List<string>();
                    StringBuilder orderReceipt = new StringBuilder();

                    orderReceipt.Append("\x1b\x61\x01");
                    orderReceipt.Append("\x1b\x40");
                    orderReceipt.Append("\x1B\x21\x1");
                    if (mHaulerData != null && mHaulerData.PrintFormat == "Format 1")
                    {
                        orderReceipt.Append("------------------------------------------");
                        orderReceipt.Append(System.Environment.NewLine);
                    }

                    orderReceipt.Append("Cierre de Orden de Transporte");
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1b\x45\x01");
                    orderReceipt.Append("------------------------------------------");
                    orderReceipt.Append("\x1b\x45\x00");
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key1 = "Orden:";
                    string value1 = nullString;
                    if (mEqTpSo.eqtpsoRefno != null)
                    {
                        value1 = mEqTpSo.eqtpsoRefno;
                    }
                    orderReceipt.Append(key1 + GetRightSpace(key1) + value1);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key2 = "Movil: ";
                    string value2 = nullString;
                    if (mEqTpSo.eqtpsoDeviceCode != null)
                    {
                        value2 = mEqTpSo.eqtpsoDeviceCode;
                    }
                    orderReceipt.Append(key2 + GetRightSpace(key2) + value2);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key3 = "Usuario: ";
                    string value3 = nullString;
                    if (mEqTpSo.eqtpsoUserCode != null)
                    {
                        value3 = mEqTpSo.eqtpsoUserCode + "-" + mEqTpSo.eqtpsoUserName;
                    }
                    orderReceipt.Append(key3 + GetRightSpace(key3) + value3);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key4 = "Fecha:";
                    string value4 = nullString;
                    if (mEqTpSo.eqtpsoOpeningDate != DateTime.MinValue)
                    {
                        value4 = mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy HH:mm:ss tt");
                    }
                    orderReceipt.Append(key4 + GetRightSpace(key4) + value4);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key5 = ""; // Transporte => Cabezote
                    if (mEqTpSo.eqtpsoEqtphaulerCode != null)
                    {
                        key5 = "Cabezote: " + mEqTpSo.eqtpsoEqtphaulerCode;
                    }
                    string key6 = ""; //Trailer => Cola
                    if (mEqTpSo.eqtpsoEqtptrailerCode != null)
                    {
                        key6 = "Cola: " + mEqTpSo.eqtpsoEqtptrailerCode;
                    }

                    orderReceipt.Append(key5 + "    " + key6);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key7 = "Chofer:";
                    string value7 = nullString;
                    if (mEqTpSo.eqtpsoDriverCode != null)
                    {
                        value7 = mEqTpSo.eqtpsoDriverCode + "-" + mEqTpSo.eqtpsoDriverName;
                    }
                    orderReceipt.Append(key7 + GetRightSpace(key7) + value7);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1b\x45\x01");
                    orderReceipt.Append("------------------------------------------");
                    orderReceipt.Append("\x1b\x45\x00");
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1b\x45\x01");
                    orderReceipt.Append("Apertura");
                    orderReceipt.Append("\x1b\x45\x00");
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1b\x45\x01");
                    orderReceipt.Append("------------------------------------------");
                    orderReceipt.Append("\x1b\x45\x00");
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key8 = "Fecha:";
                    string value8 = nullString;
                    if (mEqTpSo.eqtpsoOpeningDate != DateTime.MinValue)
                    {
                        value8 = mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy HH:mm:ss tt");
                    }
                    orderReceipt.Append(key8 + GetRightSpace(key8) + value8);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key9 = "Localización:";
                    string value9 = nullString;
                    if (mEqTpSo.eqtpsoOpeningLocation != null)
                    {
                        value9 = mEqTpSo.eqtpsoOpeningLocation;
                    }
                    orderReceipt.Append(key9 + GetRightSpace(key9) + value9);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key10 = "Kilometraje:";
                    string value10 = nullString;
                    if (mEqTpSo.eqtpsoOpeningOdometer > 0)
                    {
                        value10 = Convert.ToString(mEqTpSo.eqtpsoOpeningOdometer);
                    }
                    orderReceipt.Append(key10 + GetRightSpace(key10) + value10);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key11 = "Long./Lat.:";
                    string value11 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningLongitude))
                    {
                        value11 = mEqTpSo.eqtpsoOpeningLongitude + "/" + mEqTpSo.eqtpsoOpeningLatitude;
                    }
                    orderReceipt.Append(key11 + GetRightSpace(key11) + value11);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key12 = "Notas:";
                    string value12 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningNotes))
                    {
                        value12 = mEqTpSo.eqtpsoOpeningNotes;
                    }
                    orderReceipt.Append(key12 + GetRightSpace(key12) + value12);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1b\x45\x01");
                    orderReceipt.Append("------------------------------------------");
                    orderReceipt.Append("\x1b\x45\x00");
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1b\x45\x01");
                    orderReceipt.Append("Cierre");
                    orderReceipt.Append("\x1b\x45\x00");
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key13 = "Fecha:";
                    string value13 = nullString;
                    if (mEqTpSo.eqtpsoInterfaceDate != DateTime.MinValue)
                    {
                        value13 = mEqTpSo.eqtpsoClosingDate.ToString("dd/MM/yyyy HH:mm:ss tt");
                    }
                    orderReceipt.Append(key13 + GetRightSpace(key13) + value13);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key14 = "Localización:";
                    string value14 = nullString;
                    if (mEqTpSo.eqtpsoClosingLocation != null)
                    {
                        value14 = mEqTpSo.eqtpsoClosingLocation;
                    }
                    orderReceipt.Append(key14 + GetRightSpace(key14) + value14);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key15 = "Kilometraje:";
                    string value15 = nullString;
                    if (mEqTpSo.eptpsoClosingOdometer > 0)
                    {
                        value15 = Convert.ToString(mEqTpSo.eptpsoClosingOdometer);
                    }
                    orderReceipt.Append(key15 + GetRightSpace(key15) + value15);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key16 = "Long./Lat.:";
                    string value16 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningLongitude))
                    {
                        value16 = mEqTpSo.eqtpsoClosingLongitude + "/" + mEqTpSo.eqtpsoClosingLatitude;
                    }
                    orderReceipt.Append(key16 + GetRightSpace(key16) + value16);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key17 = "Notas:";
                    string value17 = nullString;
                    if (mEqTpSo.eqtpsoClosingNotes != null)
                    {
                        value17 = mEqTpSo.eqtpsoClosingNotes;
                    }
                    orderReceipt.Append(key17 + GetRightSpace(key17) + value17);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1b\x45\x01");
                    orderReceipt.Append("------------------------------------------");
                    orderReceipt.Append("\x1b\x45\x00");
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    string key35 = "Estatus:";
                    string value35 = nullString;
                    if (mEqTpSo.eqtpsoStatus != null)
                    {
                        value35 = mEqTpSo.eqtpsoStatus;
                    }
                    orderReceipt.Append(key35 + GetRightSpace(key35) + value35);
                    orderReceipt.Append(System.Environment.NewLine);
                    orderReceipt.Append(System.Environment.NewLine);
                    orderReceipt.Append(System.Environment.NewLine);

                    orderReceipt.Append("\x1B\x21\x1");
                    orderReceipt.Append("\x1b\x45\x01");
                    orderReceipt.Append("Transportes");
                    orderReceipt.Append("\x1b\x45\x00");
                    orderReceipt.Append(System.Environment.NewLine);

                    mReceipt.OrderReceiptString = orderReceipt;

                    if (mEqTpSoLn != null && mEqTpSoLn.Count > 0)
                    {
                        mReceipt.TransactionReceiptString = new List<StringBuilder>();
                        foreach (var item in mEqTpSoLn.Where(x => x.eqtpsolnEqtpsoRefno == mEqTpSo.eqtpsoRefno))
                        {
                            StringBuilder transactionString = new StringBuilder();
                            transactionString.Append("\x1b\x61\x00");
                            transactionString.Append("\x1B\x21\x1");
                            transactionString.Append("\x1b\x45\x01");
                            transactionString.Append("------------------------------------------");
                            transactionString.Append("\x1b\x45\x00");
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key18 = "Refno.:";
                            string value18 = nullString;
                            if (item.eqtpsolnRefno != null)
                            {
                                value18 = item.eqtpsolnRefno;
                            }
                            transactionString.Append(key18 + GetRightSpace(key18) + value18);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key19 = "";
                            if (item.eqtpsolnEquipmentRfid != null)
                            {
                                key19 = "RFID: " + item.eqtpsolnEquipmentRfid;
                            }
                            string key20 = "";
                            if (item.eqtpsolnEquipmentCode != nullString)
                            {
                                key20 = "Equipo: " + item.eqtpsolnEquipmentCode;
                            }
                            transactionString.Append(key19 + "    " + key20);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key21 = "Proyecto:";
                            string value21 = nullString;
                            if (item.eqtpsolnProjectCode != nullString)
                            {
                                value21 = item.eqtpsolnProjectCode + "-" + item.eqtpsolnProjectDesc;
                            }
                            transactionString.Append(key21 + GetRightSpace(key21) + value21);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key22 = "Partida:";
                            string value22 = nullString;
                            if (!string.IsNullOrEmpty(item.eqtpsolnProjactCode))
                            {
                                value22 = item.eqtpsolnProjactCode + "-" + item.eqtpsolnProjactDesc;
                            }
                            transactionString.Append(key22 + GetRightSpace(key22) + value22);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            transactionString.Append("\x1b\x45\x01");
                            transactionString.Append("Carga");
                            transactionString.Append("\x1b\x45\x00");
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key24 = "Fecha:";
                            string value24 = nullString;
                            if (item.eqtpsolnLoadingDatetime.HasValue && item.eqtpsolnLoadingDatetime != DateTime.MinValue)
                            {
                                value24 = item.eqtpsolnLoadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");
                            }
                            transactionString.Append(key24 + GetRightSpace(key24) + value24);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key25 = "Localización:";
                            string value25 = nullString;
                            if (item.eqtpsolnLoadingLocation != null)
                            {
                                value25 = item.eqtpsolnLoadingLocation;
                            }
                            transactionString.Append(key25 + GetRightSpace(key25) + value25);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key26 = "Kilometraje:";
                            string value26 = nullString;
                            if (item.eqtpsolnLoadingOdometer > 0)
                            {
                                value26 = Convert.ToString(item.eqtpsolnLoadingOdometer);
                            }
                            transactionString.Append(key26 + GetRightSpace(key26) + value26);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key27 = "Long./Lat.:";
                            string value27 = nullString;
                            if (item.eqtpsolnLoadingLongitude != nullString)
                            {
                                value27 = item.eqtpsolnLoadingLongitude + "/" + item.eqtpsolnLoadingLatitude;
                            }
                            transactionString.Append(key27 + GetRightSpace(key27) + value27);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key28 = "Notas:";
                            string value28 = nullString;
                            if (!string.IsNullOrEmpty(item.eqtpsolnLoadingNotes))
                            {
                                value28 = item.eqtpsolnLoadingNotes;
                            }
                            transactionString.Append(key28 + GetRightSpace(key28) + value28);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            transactionString.Append("\x1b\x45\x01");
                            transactionString.Append("Descarga");
                            transactionString.Append("\x1b\x45\x00");
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key29 = "Fecha:";
                            string value29 = nullString;
                            if (item.eqtpsolnUnloadingDatetime.HasValue && item.eqtpsolnUnloadingDatetime != DateTime.MinValue)
                            {
                                value29 = item.eqtpsolnUnloadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");
                            }
                            transactionString.Append(key29 + GetRightSpace(key29) + value29);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key30 = "Localización:";
                            string value30 = nullString;
                            if (item.eqtpsolnUnloadingLocation != null)
                            {
                                value30 = item.eqtpsolnUnloadingLocation;
                            }
                            transactionString.Append(key30 + GetRightSpace(key30) + value30);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key31 = "Kilometraje:";
                            string value31 = nullString;
                            if (item.eqtpsolnUnloadingOdometer > 0)
                            {
                                value31 = Convert.ToString(item.eqtpsolnUnloadingOdometer);
                            }
                            transactionString.Append(key31 + GetRightSpace(key31) + value31);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key32 = "Long./Lat.:";
                            string value32 = nullString;
                            if (item.eqtpsolnUnloadingLongitude != nullString)
                            {
                                value32 = item.eqtpsolnUnloadingLongitude + "/" + item.eqtpsolnUnloadingLatitude;
                            }
                            transactionString.Append(key32 + GetRightSpace(key32) + value32);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key33 = "Notas:";
                            string value33 = nullString;
                            if (!string.IsNullOrEmpty(item.eqtpsolnUnloadingNotes))
                            {
                                value33 = item.eqtpsolnUnloadingNotes;
                            }
                            transactionString.Append(key33 + GetRightSpace(key33) + value33);
                            transactionString.Append(System.Environment.NewLine);
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1b\x45\x01");
                            transactionString.Append("------------------------------------------");
                            transactionString.Append("\x1b\x45\x00");
                            transactionString.Append(System.Environment.NewLine);

                            transactionString.Append("\x1B\x21\x1");
                            string key34 = "Estatus:";
                            string value34 = nullString;
                            if (!string.IsNullOrEmpty(item.eqtpsolnStatus))
                            {
                                value34 = item.eqtpsolnStatus;
                            }
                            transactionString.Append(key34 + GetRightSpace(key34) + value34);
                            transactionString.Append(System.Environment.NewLine);
                            transactionString.Append(System.Environment.NewLine);
                            transactionString.Append(System.Environment.NewLine);
                            mReceipt.TransactionReceiptString.Add(transactionString);
                        }
                    }

                    // Closing Statement
                    StringBuilder closingReceipt = new StringBuilder();

                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);

                    closingReceipt.Append("\x1B\x21\x1");
                    string key36 = "------------------";
                    string value36 = "------------------";
                    closingReceipt.Append(key36 + GetSpace(key36, value36) + value36);
                    closingReceipt.Append(System.Environment.NewLine);

                    closingReceipt.Append("\x1B\x21\x1");
                    string key37 = "   Transportista   ";
                    string value37 = "      Supervisor      ";
                    closingReceipt.Append(key37 + GetSpace(key37, value37) + value37);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    closingReceipt.Append(System.Environment.NewLine);
                    mReceipt.ClosingReceiptString = closingReceipt;
                    //
                    var jsonString = await DependencyService.Get<IPrintReceipt>().PrintOrderReceipt(mAppKey.KeyAddress, mReceipt, barcodes);
                    Acr.UserDialogs.UserDialogs.Instance.HideLoading();
                    //DisplayAlert("", jsonString, "Ok");
                    if (jsonString)
                    {
                        return "True";
                    }
                    else
                        return "False";
                }
                else
                    return "False";
            }
            catch (Exception ex)
            {
                return "False";
            }
        }

        public async Task<bool> ZebraOrderPrint(AppKey mAppKey, int noOfCopies)
        {
            bool isSuccess = false;
            try
            {
                UserDialogs.Instance.ShowLoading("Printing...");
                Xamarin.Forms.DependencyService.Get<IBluetooth>().ConnectBluetooth(mAppKey.KeyName, mAppKey.KeyAddress);

                await Task.Run(async () =>
                {
                    var zpl = await ZebraLabelBuilder();
                    for (int i = 0; i < noOfCopies; i++)
                    {
                        var result = await Xamarin.Forms.DependencyService.Get<IBluetooth>().Print(zpl);
                        if (result)
                        {
                            isSuccess = true;
                        }
                        await Task.Delay(3000);
                    }
                    Xamarin.Forms.DependencyService.Get<IBluetooth>().DisconnectBluetooth();
                });
            }
            catch (Exception ex)
            {
                isSuccess = false;
                Utilties.DisplayInfo.ErrorMessage("CloseOrderPage/ZebraOrderPrint: " + ex.Message);
            }
            finally { UserDialogs.Instance.HideLoading(); }
            return isSuccess;
        }
        //public string GetDate(DateTime dt)
        //{
        //    string Date = "";
        //    if (dt != DateTime.MinValue)
        //        return Date = dt.ToString("dd/MM/yyyy HH:mm:ss tt");
        //    else
        //        return Date;
        //}

        public async Task<byte[]> ZebraLabelBuilder()
        {
            string zpl = string.Empty;
            try
            {
                if (mEqTpSo == null)
                    mEqTpSo = new EqTpSo();

                mEqTpSo = shiftRepository.GetStartShift();

                if (mEqTpSo != null)
                {
                    if (mEqTpSoLn == null)
                        mEqTpSoLn = new List<EqTpSoLnLocal>();

                    mEqTpSoLn = eqTpSoLnRepository.GetAllEqTpSoLns();

                    string base64Image = LogoBase64String.LogoBase64;
                    byte[] imageBytes = Convert.FromBase64String(base64Image);
                    ZPLImage zplimg = Xamarin.Forms.DependencyService.Get<IBluetooth>().GetZPLImageCommand(imageBytes);

                    EqptHaulerRepository eqptHaulerRepository = new EqptHaulerRepository();
                    var mHauler = eqptHaulerRepository.GetHaulerByEqtphaulerCode(mEqTpSo.eqtpsoEqtphaulerCode);

                    var DisplayHauler = ((mHauler != null && mHauler.Id > 0) ? mHauler.DisplayHauler : mEqTpSo.eqtpsoEqtphaulerCode);

                    EqptTrailerRepository eqptTrailerRepository = new EqptTrailerRepository();
                    var mTrailer = eqptTrailerRepository.GerTrailerByEqtptrailerCode(mEqTpSo.eqtpsoEqtptrailerCode);//0730

                    var DisplayTrailer = ((mTrailer != null && mTrailer.Id > 0) ? mTrailer.DisplayTrailer : mEqTpSo.eqtpsoEqtptrailerCode);


                    ZPLForge.Label label = new ZPLForge.Label
                    {
                        Quantity = 1,
                        PrintWidth = 576,
                        MediaTracking = MediaTracking.ContinuousVariableLength,
                        MediaType = MediaType.ThermalDirect,
                        PrintMode = PrintMode.TearOff,
                        MediaDarknessLevel = 15,
                        Content =
                    {
                        new ImageElement
                    {
                        PositionX = 15,
                        PositionY = 70,
                        Compression = CompressionType.HexASCII,
                        BinaryByteCount = zplimg.ByteCount,
                        GraphicFieldCount = zplimg.ByteCount,
                        BytesPerRow = zplimg.BytesPerRow,
                        Content = zplimg.Data
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 180,
                        Content = "Alba Sanchez & Asociados",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 210,
                        Content = "RNC: 1-01-13267-1",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 240,
                        Content = "Cierre de Orden de Transporte",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 270,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 300,
                        Content = "Orden:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 300,
                        Content = mEqTpSo.eqtpsoRefno,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 330,
                        Content = "Movil:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 330,
                        Content = string.Format("{0:00}", Convert.ToInt32(mEqTpSo.eqtpsoDeviceCode)),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 360,
                        Content = "Usuario:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 360,
                        Content = mEqTpSo.eqtpsoUserCode + "-" + mEqTpSo.eqtpsoUserName,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 390,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 390,
                        Content = mEqTpSo.eqtpsoOpeningDate !=DateTime.MinValue ? mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy HH:mm:ss tt") : " ",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 420,
                        Content = "Transportista:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 420,
                        Content = mEqtpData.EqtpCode,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 450,
                        Content = "Cabezote:", // Transporte => Cabezote
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 450,
                        Content = DisplayHauler,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 480,
                        Content = "Cola:", //Trailer => Cola
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 480,
                        Content = DisplayTrailer,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 510,
                        Content = "Chofer:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 510,
                        Content = mEqTpSo.eqtpsoDriverCode + " " + mEqTpSo.eqtpsoDriverName,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 540,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 570,
                        Content = "Apertura",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 600,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 630,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 630,
                        Content = mEqTpSo.eqtpsoOpeningDate !=DateTime.MinValue? mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy HH:mm:ss tt"): " ",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 660,
                        Content = "Localización:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 660,
                        Content = mEqTpSo.eqtpsoOpeningLocation,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 690,
                        Content = "Kilometraje:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 690,
                        Content = Convert.ToString(mEqTpSo.eqtpsoOpeningOdometer),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 720,
                        Content = "Long./Lat.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 720,
                        Content = mEqTpSo.eqtpsoOpeningLongitude + "/" + mEqTpSo.eqtpsoOpeningLatitude,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 750,
                        Content = "Notas:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 750,
                        Content = mEqTpSo.eqtpsoOpeningNotes,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 780,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 810,
                        Content = "Cierre",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 840,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 840,
                        Content = mEqTpSo.eqtpsoClosingDate !=DateTime.MinValue ? mEqTpSo.eqtpsoClosingDate.ToString("dd/MM/yyyy HH:mm:ss tt") : " ",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 870,
                        Content = "Localización:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 870,
                        Content = mEqTpSo.eqtpsoClosingLocation,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 900,
                        Content = "Kilometraje:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 900,
                        Content = Convert.ToString(mEqTpSo.eptpsoClosingOdometer),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 930,
                        Content = "Long./Lat.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 930,
                        Content = mEqTpSo.eqtpsoClosingLongitude + "/" + mEqTpSo.eqtpsoClosingLatitude,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 960,
                        Content = "Notas:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 960,
                        Content = mEqTpSo.eqtpsoClosingNotes,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 990,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 1020,
                        Content = "Estatus:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 1020,
                        Content = mEqTpSo.eqtpsoStatus,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                    }
                    };

                    int posY = 1050;

                    if (mEqTpSoLn != null && mEqTpSoLn.Count > 0)
                    {
                        label.Content.Add(new TextElement
                        {
                            PositionX = 10,
                            PositionY = posY,
                            Content = "Transportes",
                            FontStyle = ZPLForge.Common.Font.Default,
                            CharHeight = 25
                        });

                        posY += 30;

                        foreach (var item in mEqTpSoLn.Where(x => x.eqtpsolnEqtpsoRefno == mEqTpSo.eqtpsoRefno))
                        {
                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "----------------------------------------",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 15
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Refno.:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnRefno,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "RFID:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnEquipmentRfid,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Equipo:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnEquipmentCode + "-" + item.eqtpsolnEquipmentDesc,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Proyecto:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnProjectCode + "-" + item.eqtpsolnProjectDesc,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Partida:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnProjactCode + "-" + item.eqtpsolnProjactDesc,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Cód.Seg.:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnSecurityCode,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "----------------------------------------",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 15
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Carga",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Fecha:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            string dt = "";
                            if (item.eqtpsolnLoadingDatetime.HasValue && item.eqtpsolnLoadingDatetime.Value != DateTime.MinValue)
                                dt = item.eqtpsolnLoadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = dt,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Localización:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnLoadingLocation,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Kilometraje:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = Convert.ToString(item.eqtpsolnLoadingOdometer),
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Long./Lat.:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnLoadingLongitude + "/" + item.eqtpsolnLoadingLatitude,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Notas:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnLoadingNotes,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "----------------------------------------",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 15
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Descarga",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Fecha:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            string dt1 = "";
                            if (item.eqtpsolnUnloadingDatetime.HasValue && item.eqtpsolnUnloadingDatetime.Value != DateTime.MinValue)
                                dt1 = item.eqtpsolnUnloadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = dt1,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Localización:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnUnloadingLocation,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Kilometraje:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = Convert.ToString(item.eqtpsolnUnloadingOdometer),
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Long./Lat.:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnUnloadingLongitude + "/" + item.eqtpsolnUnloadingLatitude,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Notas:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnUnloadingNotes,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "----------------------------------------",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 15
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Estatus:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnStatus,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;
                        }
                    }

                    posY += 100;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 310,
                        PositionY = posY,
                        Content = "------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 80,
                        PositionY = posY,
                        Content = "Transportista",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 395,
                        PositionY = posY,
                        Content = "Supervisor",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.MediaLength = posY + 150;

                    zpl = label.ToString().Insert(3, "^POI");
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                Utilties.DisplayInfo.ErrorMessage("CloseOrderPage/ZebraLabelBuilder: " + ex.Message);
            }
            return Encoding.UTF8.GetBytes(zpl);
        }

        string GetSpace(string key, string value)
        {
            string space = "";
            try
            {
                int defaultCount = 42;
                int totalLength = 0;

                if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                    totalLength = key.Length + value.Length;
                else if (!string.IsNullOrEmpty(key))
                    totalLength = key.Length;
                else if (!string.IsNullOrEmpty(value))
                    totalLength = value.Length;

                for (int i = 0; i < (defaultCount - totalLength); i++)
                {
                    space += " ";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetSpace/CloseShiftPage Exception: " + ex.Message);
            }
            return space;
        }

        string GetRightSpace(string value)
        {
            string space = "";
            try
            {
                int defaultCount = 14;
                int totalLength = (defaultCount - value.Length);
                for (int i = 0; i < totalLength; i++)
                {
                    space += " ";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetSpace/CloseShiftPage Exception: " + ex.Message);
            }
            return space;
        }

        public bool ValidatedFields()
        {
            bool isValid = false;
            try
            {
                if (lblCloseDateTime.Date == null)
                {
                    lblCloseDateTime.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.CloseingDate);
                }
                else if (string.IsNullOrEmpty(lblCloseingLocationCode.Text) || lblCloseingLocationCode.Text == "Localización")
                {
                    lblCloseingLocationCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Location);
                }
                else if (string.IsNullOrEmpty(txtCloseOdometer.Text))
                {
                    txtCloseOdometer.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Odometer);
                }
                else if (img1.IsEnabled && imageByteArray1 == null)
                {
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Picture);
                }
                else if (signatureByte == null)
                {
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Signature);
                }
                else
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ValidatedFields/ShiftPage" + ex.Message);
            }
            return isValid;
        }

        public async void NavigateToLoctionSelection()
        {
            try
            {
                //if (mLocations != null && mLocations.Count > 1)
                //{
                var locationPopup = new PopUp.LocationPopup();
                locationPopup.RefreshEvent += (s1, e1) =>
                {
                    this.locations = (Locations)s1;
                    if (this.locations != null && !string.IsNullOrEmpty(this.locations.EqtplocationCode))
                    {
                        lblCloseingLocationCode.Text = this.locations.EqtplocationCode;
                    }
                };
                await PopupNavigation.PushAsync(locationPopup, false);
                //}
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToLoctionSelection/ShiftPage" + ex.Message);
            }
        }

        private byte[] GetImageStreamAsBytes(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        #endregion

        #region Events
        private async void BtnBack_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private void BtnOpenOrder_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (!grdOpenOrder.IsVisible)
                {
                    grdOpenOrder.IsVisible = true;
                    imgPictureDown.Rotation = 180;
                }
                else
                {
                    grdOpenOrder.IsVisible = false;
                    imgPictureDown.Rotation = 0;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnOpenOrder_Tapped/CloseOrderPage" + ex.Message);
            }
        }

        private void BtnCloseOrder_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (!grdCloseOrder.IsVisible)
                {
                    grdCloseOrder.IsVisible = true;
                    imgPictureDown.Rotation = 180;
                }
                else
                {
                    grdCloseOrder.IsVisible = false;
                    imgPictureDown.Rotation = 0;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnCloseOrder_Tapped/CloseOrderPage" + ex.Message);
            }
        }

        private void ImgLocation_Clicked(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private async void BtnDelete_Tapped(object sender, EventArgs e)
        {
            PopUp.DeleteImagePopup deleteImagePopup = new PopUp.DeleteImagePopup();
            try
            {
                deleteImagePopup.isRefresh += (s, e1) =>
                {
                    var isDelete = (bool)s;
                    if (isDelete)
                    {
                        imageByteArray1 = null;
                        btnDelete.IsVisible = false;
                        imgclosePicture1.Source = "iconGrayNoImage.png";
                    }
                };
                await PopupNavigation.PushAsync(deleteImagePopup, false);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnDelete_Tapped/ShiftPage" + ex.Message);
            }
        }

        private async void BtnUploadPicture1_Tapped(object sender, EventArgs e)
        {
            try
            {
                var noImage = "iconGrayNoImage.png";
                var selectedImage = imgclosePicture1.Source.ToString().Replace("File: ", string.Empty);
                if (selectedImage == noImage)
                {
                    if (imgclosePicture1.Source == null)
                    {
                        btnDelete.IsVisible = false;
                        imgclosePicture1.Source = "iconGrayNoImage.png";
                    }
                    else
                    {
                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {
                            await DisplayAlert("No Camera", ":( No camera available.", "OK");
                            return;
                        }

                        var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                        var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                        if (cameraStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted || storageStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                            cameraStatus = results[Permission.Camera];
                            storageStatus = results[Permission.Storage];
                        }

                        if (cameraStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted && storageStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            file1 = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                            {
                                Directory = "MTCT",
                                SaveToAlbum = true,
                                CompressionQuality = 50,
                                CustomPhotoSize = 50,
                                DefaultCamera = CameraDevice.Rear,
                                AllowCropping = true,
                            });

                            if (file1 == null)
                            {
                                btnDelete.IsVisible = false;
                                imgclosePicture1.Source = "iconGrayNoImage.png";
                                return;
                            }

                            var imagefile = file1.GetStreamWithImageRotatedForExternalStorage();
                            imagefile.Position = 0;
                            if (imagefile != null)
                                imageByteArray1 = GetImageStreamAsBytes(imagefile);

                            imgclosePicture1.Aspect = Aspect.Fill;
                            file1.Dispose();
                            imgclosePicture1.Source = ImageSource.FromStream(() => new MemoryStream(imageByteArray1));

                            btnDelete.IsVisible = true;
                        }
                        else
                        {
                            await DisplayAlert("Permissions denied", "Unable to take photos.", "OK");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnUploadPicture1_Tapped/ShiftPage" + ex.Message);
            }
        }

        private async void BtnSignature_Tapped(object sender, EventArgs e)
        {
            try
            {
                PopUp.SignaturePopup signaturePopup = new PopUp.SignaturePopup(false, string.Empty);
                signaturePopup.isRefresh += (s, e1) =>
                {
                    signatureByte = (byte[])s;
                    if (signatureByte == null)
                    {
                        lblComplete.IsVisible = false;
                    }
                    else
                    {
                        lblComplete.IsVisible = true;
                    }
                };
                await PopupNavigation.PushAsync(signaturePopup, false);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnSignature_Tapped/ShiftPage" + ex.Message);
            }
        }

        private void BtnCancel_Tapped(object sender, EventArgs e)
        {

        }

        private async void BtnSave_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (ValidatedFields())
                {
                    PopUp.ConfirmationPopup confirmationPopup = new PopUp.ConfirmationPopup("Cerrar orden de transporte?");
                    confirmationPopup.isRefresh += async (s, e1) =>
                    {
                        var isConfirm = (bool)s;
                        if (isConfirm)
                        {
                            await Task.Run(() => Utilties.DisplayInfo.ShowLoader());
                            await Task.Delay(1 * 500);

                            List<EqTpSoLnLocal> eqTpSoLns = eqTpSoLnRepository.GetAllEqTpSoLns();
                            if (eqTpSoLns != null && eqTpSoLns.Count > 0)
                            {
                                foreach (var eqTpSoLn in eqTpSoLns)
                                {
                                    if (eqTpSoLn.eqtpsolnInterfaceStatus != InterfaceStatus.Completado.ToString())
                                    {
                                        var eqTpSoLnModel = eqTpSoLn.ToEqTpSoLnModel();
                                        List<EqTpSoLnModel> eqTpSoLnModels = new List<EqTpSoLnModel>();
                                        eqTpSoLnModels.Add(eqTpSoLnModel);

                                        if (Common.HasInternetConnection())
                                        {
                                            var responseAPIEqTpSoLn = await eqTpSoLnAPI.SaveEqTpSoLn(eqTpSoLnModels);
                                            if (responseAPIEqTpSoLn.IsSuccess)
                                            {
                                                //Update
                                                var responseAPI1 = eqTpSoLnRepository.UpdateInterfaceStatus(InterfaceStatus.Completado.ToString(), eqTpSoLn.EqTpSoLnId);
                                            }
                                        }
                                    }
                                }


                                var mUnloadedTransports = eqTpSoLns.Where(x => x.eqtpsolnStatus != EqTpSoLnStatus.Anulado.ToString() && x.eqtpsolnUnloadingLocation == string.Empty).ToList();
                                if (mUnloadedTransports.Count == 0)
                                {
                                    SaveCloseOrder(Common.CloseStatus);
                                }
                                else
                                {
                                    Utilties.DisplayInfo.HideLoader();
                                    await DisplayAlert("", "Para cerrar orden, Todos los transportes deben ser descargados", "Ok");
                                }
                            }
                            else
                            {
                                SaveCloseOrder(Common.VoidStatus);
                            }
                        }
                        else
                        {
                            Utilties.DisplayInfo.HideLoader();
                        }
                    };
                    await PopupNavigation.PushAsync(confirmationPopup, false);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Utilties.DisplayInfo.HideLoader();
            }
        }

        private void BtnLocation_Tapped(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }
        #endregion

        //public async void bindLocation()
        //{
        //    try
        //    {
        //        if (location != null)
        //        {
        //            if (Common.HasInternetConnection())
        //            {
        //                var mAddress = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
        //                string addressLabel = string.Empty;
        //                foreach (var add in mAddress)
        //                {
        //                    addressLabel += add.FeatureName + " ";
        //                }
        //                var pin = new Pin
        //                {
        //                    Type = PinType.Place,
        //                    Label = addressLabel,
        //                    Position = new Position(location.Latitude, location.Longitude),
        //                    // Address = address
        //                };

        //                xfmapOpen.Pins.Add(pin);
        //                xfmapOpen.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(location.Latitude, location.Longitude), Distance.FromMeters(150.0)));
        //                xfmap.Pins.Add(pin);
        //                xfmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(location.Latitude, location.Longitude), Distance.FromMeters(150.0)));
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilties.DisplayInfo.ErrorMessage("bindLocation/CloseOrderPage" + ex.Message);
        //    }
        //}
    }
}