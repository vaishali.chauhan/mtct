﻿using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfigurationPage : ContentPage
    {
        #region Objects
        DeviceRepository deviceRepository;
        EqptHaulerRepository eqptHaulerRepository;
        EqtpRepository eqtpRepository;
        AppConfigRepository appConfigRepository;
        List<string> mFormat = new List<string>();
        private bool signEnabled = true;

        Eqtp mEqtp;
        AppConfig mAppConfig;
        #endregion

        #region Constructor
        public ConfigurationPage()
        {
            InitializeComponent();
            InitializeObject();
            BindPrintFormat();
            pkPrintFormat.SelectedIndex = 0;
        }
        #endregion

        #region Methods
        public void InitializeObject()
        {
            try
            {
                deviceRepository = new DeviceRepository();
                eqtpRepository = new EqtpRepository();
                eqptHaulerRepository = new EqptHaulerRepository();
                appConfigRepository = new AppConfigRepository();

                mEqtp = new Eqtp();
                mAppConfig = new AppConfig();
                BindData();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeObject/ConfigurationPage" + ex.Message);
            }
        }

        public void BindPrintFormat()
        {
            mFormat.Clear();
            mFormat.Add("Imprimir con Logo y Código de Barra (Zebra)");
            mFormat.Add("Imprimir con Logo y Código de Barra");
            mFormat.Add("Imprimir sin Logo y Código de Barra");

            pkPrintFormat.ItemsSource = mFormat.ToList();
            pkPrintFormat.SelectedIndex = 0;
        }

        public async void BindData()
        {
            try
            {
                await Task.Run(() => Utilties.DisplayInfo.ShowLoader());
                await Task.Delay(1 * 500);
                mEqtp = eqtpRepository.GetEqTp();
                if (mEqtp != null)
                {
                    txtAppRefCode.Text = mEqtp.EqtpCode;
                    txtDeviceCode.Text = mEqtp.EqtpDeviceCode.ToString();
                    txtOrderCounter.Text = mEqtp.EqtpOrderCounterNo.ToString();
                    txtTransportCounter.Text = mEqtp.EqtpTransportCounterNo.ToString();
                    txtOrderPrint.Text = mEqtp.EqtpOrderPrint.ToString();
                    txtTransportPrint.Text = mEqtp.EqtpTransportPrint.ToString();

                    if (mEqtp.PrintFormat == "Format Zebra")
                    {
                        pkPrintFormat.SelectedIndex = 0;
                    }
                    else if (mEqtp.PrintFormat == "Format 1")
                    {
                        pkPrintFormat.SelectedIndex = 1;
                    }
                    else if (mEqtp.PrintFormat == "Format 2")
                    {
                        pkPrintFormat.SelectedIndex = 2;
                    }

                    if (mEqtp.EqtpSignEnabled)
                    {
                        imgSignFalse.Source = "iconRadioBlank.png";
                        imgSignTrue.Source = "iconRadioFill.png";
                        signEnabled = true;
                    }
                    else
                    {
                        imgSignTrue.Source = "iconRadioBlank.png";
                        imgSignFalse.Source = "iconRadioFill.png";
                        signEnabled = false;
                    }
                }

                mAppConfig = appConfigRepository.GetAppConfig();
                if (mAppConfig != null)
                {
                    txtURL.Text = mAppConfig.AppConfigApiUrl;
                    txtUserName.Text = mAppConfig.AppConfigUserName;
                    txtPassword.Text = mAppConfig.AppConfigPassword;
                }
                Utilties.DisplayInfo.HideLoader();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindData/ConfigurationPage" + ex.Message);
            }
        }

        public bool ValidatedFields()
        {
            bool isValid = false;
            try
            {
                if (string.IsNullOrEmpty(txtDeviceCode.Text))
                {
                    txtDeviceCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
                }
                else if (string.IsNullOrEmpty(txtOrderCounter.Text))
                {
                    txtOrderCounter.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
                }
                else if (string.IsNullOrEmpty(txtTransportCounter.Text))
                {
                    txtTransportCounter.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
                }
                else if (string.IsNullOrEmpty(txtOrderPrint.Text))
                {
                    txtOrderPrint.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
                }
                else if (string.IsNullOrEmpty(txtTransportPrint.Text))
                {
                    txtTransportPrint.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
                }
                else if (string.IsNullOrEmpty(txtURL.Text))
                {
                    txtURL.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
                }
                else if (string.IsNullOrEmpty(txtUserName.Text))
                {
                    txtUserName.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
                }
                else if (string.IsNullOrEmpty(txtPassword.Text))
                {
                    txtPassword.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
                }
                else
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ValidatedFields/ConfigurationPage Exception: " + ex.Message);
            }
            return isValid;
        } 
        #endregion

        #region Events
        private void btnMenu_Tapped(object sender, EventArgs e)
        {
            Common.masterDetail.IsGestureEnabled = true;
            Common.masterDetail.IsPresented = true;
        }

        private void btnReset_Tapped(object sender, EventArgs e)
        {
            try
            {
                txtAppRefCode.Text = string.Empty;
                txtDeviceCode.Text = string.Empty;
                txtOrderCounter.Text = string.Empty;
                txtTransportCounter.Text = string.Empty;
                txtOrderPrint.Text = string.Empty;
                txtTransportPrint.Text = string.Empty;
                txtURL.Text = string.Empty;
                txtUserName.Text = string.Empty;
                txtPassword.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnReset_Tapped/ConfigurationPage Exception: " + ex.Message);
            }
        }

        private async void btnSave_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (ValidatedFields())
                {
                    await Task.Run(() => Utilties.DisplayInfo.ShowLoader());
                    await Task.Delay(1 * 500);
                    Eqtp eqTpHaulers = new Eqtp();
                    eqTpHaulers.EqtpId = mEqtp.EqtpId;
                    eqTpHaulers.EqtpDeviceCode = Convert.ToInt32(txtDeviceCode.Text);
                    eqTpHaulers.EqtpOrderCounterNo = Convert.ToInt32(txtOrderCounter.Text);
                    eqTpHaulers.EqtpTransportCounterNo = Convert.ToInt32(txtTransportCounter.Text);
                    eqTpHaulers.EqtpOrderPrint = Convert.ToInt32(txtOrderPrint.Text);
                    eqTpHaulers.EqtpTransportPrint = Convert.ToInt32(txtTransportPrint.Text);
                    eqTpHaulers.EqtpSignEnabled = signEnabled;

                    if (pkPrintFormat.SelectedIndex == 0)
                    {
                        eqTpHaulers.PrintFormat = "Format Zebra";
                    }
                    else if (pkPrintFormat.SelectedIndex == 1)
                    {
                        eqTpHaulers.PrintFormat = "Format 1";
                    }
                    else if (pkPrintFormat.SelectedIndex == 2)
                    {
                        eqTpHaulers.PrintFormat = "Format 2";
                    }

                    var mFlst1 = eqtpRepository.UpdateEqTp(eqTpHaulers);
                    // Config
                    AppConfig appConfig = new AppConfig();
                    appConfig.AppConfigId = mAppConfig.AppConfigId;
                    appConfig.AppConfigApiUrl = txtURL.Text;
                    appConfig.AppConfigUserName = txtUserName.Text;
                    appConfig.AppConfigPassword = txtPassword.Text;
                    var mAppConfig1 = appConfigRepository.UpdateAppConfig(appConfig);

                    Common.masterDetail.Detail = new NavigationPage(new Views.Home.DashboardPage());
                }
                Utilties.DisplayInfo.HideLoader();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnSave_Tapped/ConfigurationPage" + ex.Message);
            }
        }

        private void imgPrintFormat_Clicked(object sender, EventArgs e)
        {
            pkPrintFormat.Focus();
        }

        private void btnPrintFormat_Tapped(object sender, EventArgs e)
        {
            pkPrintFormat.Focus();
        }

        private void StkSignTrue_Clicked(object sender, EventArgs e)
        {
            imgSignFalse.Source = "iconRadioBlank.png";
            imgSignTrue.Source = "iconRadioFill.png";
            signEnabled = true;
        }

        private void StkSignFalse_Clicked(object sender, EventArgs e)
        {
            imgSignTrue.Source = "iconRadioBlank.png";
            imgSignFalse.Source = "iconRadioFill.png";
            signEnabled = false;
        }
        #endregion
    }
}