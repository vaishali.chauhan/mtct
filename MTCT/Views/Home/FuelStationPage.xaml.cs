﻿using Acr.UserDialogs;
using MTCT.Interface;
using MTCT.Models;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using MTCT.Views.PopUp;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZPLForge;
using ZPLForge.Common;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FuelStationPage : ContentPage
    {
        #region Objects
        ShiftRepository shiftRepository;
        EqTpSoLnRepository eqTpSoLnRepository;
        EqtpRepository eqtpRepository;
        EqTpSo mEqTpSo;
        EqTpSo mPreviousFlStShift;
        Eqtp mEqtpData;

        public List<EqTpSoLnLocal> mEqTpSoLns;
        List<BluetoothList> bluetoothList;
        DateTime selectedDate;
        List<EqTpSo> shifts;
        #endregion

        #region Constructor
        public FuelStationPage()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            InitializeOject();
        }
        #endregion

        #region Methods
        public void InitializeOject()
        {
            try
            {
                shiftRepository = new ShiftRepository();
                eqTpSoLnRepository = new EqTpSoLnRepository();
                eqtpRepository = new EqtpRepository();

                shifts = new List<EqTpSo>();

                mPreviousFlStShift = new EqTpSo();

                mEqtpData = new Eqtp();
                mEqTpSo = new EqTpSo();
                mEqTpSoLns = new List<EqTpSoLnLocal>();

                selectedDate = DateTime.Now;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeOject/FuelStationPage Exception: " + ex.Message);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                GetShiftStatus();
                if (Common.mEqtp != null && Common.mEqtp.EqtpId > 0)
                {
                    lblTitle.Text = "Transporte " + Common.mEqtp.EqtpCode.ToString();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("OnAppearing/FuelStationPage Exception: " + ex.Message);
            }
        }

        protected override bool OnBackButtonPressed()
        {
            try
            {
                base.OnBackButtonPressed();

                Device.BeginInvokeOnMainThread(async () =>
                {
                    var result = await DisplayAlert("Alerta", "Esta seguro de salir?", "Si", "No");
                    if (result)
                    {
                        if (Device.OS == TargetPlatform.Android)
                        {
                            Xamarin.Forms.DependencyService.Get<ICloseAppOnBackButton>().CloseApp();
                        }
                    }
                });

                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }

        void GetShiftStatus()
        {
            try
            {
                if (mPreviousFlStShift != null && mPreviousFlStShift.Id > 0)
                {
                    mEqTpSo = mPreviousFlStShift;
                    lblShift.Text = " - " + mEqTpSo.eqtpsoRefno;
                    lblDate.IsVisible = true;
                    lblShift.IsVisible = true;
                    lblDate.Text = "Apertura - " + mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyy hh:mm tt");
                }
                else
                {
                    mEqTpSo = shiftRepository.GetOpenOrder(Common.OpenStatus.ToString());
                    if (mEqTpSo != null)
                    {
                        if (mEqTpSo.eqtpsoStatus == Common.OpenStatus.ToString())
                        {
                            btnClose.IsEnabled = true;
                            btnView.IsEnabled = true;
                            btnPrint.IsEnabled = true;
                            btnStart.IsEnabled = false;
                            btnStart.BackgroundColor = Color.FromHex("#DCDDDE");
                            lblShift.IsVisible = true;
                            lblDate.IsVisible = true;
                            lblShift.Text = " - " + mEqTpSo.eqtpsoRefno;
                            lblDate.Text = "Apertura - " + mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyy hh:mm tt");
                            btnTransport.IsEnabled = true;
                            lblTransport.TextColor = Color.Black;
                        }
                        else
                        {
                            btnStart.BackgroundColor = Color.FromHex("#04AD40");
                            btnStart.IsEnabled = true;
                            btnClose.IsEnabled = false;
                            btnView.IsEnabled = false;
                            btnPrint.IsEnabled = false;
                            lblShift.IsVisible = false;
                            lblDate.IsVisible = false;
                            lblShift.Text = string.Empty;
                            lblDate.Text = string.Empty;
                            btnTransport.IsEnabled = false;
                            lblTransport.TextColor = Color.FromHex("#aaaaaa");
                            mEqTpSo = new EqTpSo();
                        }
                    }
                    else
                    {
                        btnStart.BackgroundColor = Color.FromHex("#04AD40");
                        btnStart.IsEnabled = true;
                        lblShift.Text = string.Empty;
                        lblDate.Text = string.Empty;
                        btnClose.IsEnabled = false;
                        btnView.IsEnabled = false;
                        btnPrint.IsEnabled = false;
                        btnTransport.IsEnabled = false;
                        lblTransport.TextColor = Color.FromHex("#aaaaaa");
                    }

                    shifts = shiftRepository.GetAllEqTpSo();
                    if (shifts != null && shifts.Where(x => x.eqtpsoStatus == Common.CloseStatus).Count() > 0)
                    {
                        imgPreviousDate.IsVisible = true;
                    }
                    else
                    {
                        imgPreviousDate.IsVisible = false;
                    }
                    imgNextDate.IsVisible = false;
                    lblShiftStatus.Text = "Abrir Orden";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetShiftStatus/FuelStationPage Exception: " + ex.Message);
            }
        }

        string GetSpace(string key, string value)
        {
            string space = "";
            int defaultCount = 42;
            int totalLength = 0;
            try
            {
                if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                    totalLength = key.Length + value.Length;
                else if (!string.IsNullOrEmpty(key))
                    totalLength = key.Length;
                else if (!string.IsNullOrEmpty(value))
                    totalLength = value.Length;

                for (int i = 0; i < (defaultCount - totalLength); i++)
                {
                    space += " ";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetSpace/FuelStationPage Exception: " + ex.Message);
            }
            return space;
        }

        string GetRightSpace(string value)
        {
            string space = "";
            try
            {
                int defaultCount = 14;
                int totalLength = (defaultCount - value.Length);
                for (int i = 0; i < totalLength; i++)
                {
                    space += " ";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetSpace/CloseShiftPage Exception: " + ex.Message);
            }
            return space;
        }

        public async void ConnectBluetooth(EqTpSo mFlStShift)
        {
            try
            {
                mEqtpData = eqtpRepository.GetEqTp();
                if (mEqTpSoLns == null)
                    mEqTpSoLns = new List<EqTpSoLnLocal>();

                mEqTpSoLns = eqTpSoLnRepository.GetAllEqTpSoLns().Where(x => x.eqtpsolnEqtpsoRefno == mFlStShift.eqtpsoRefno).ToList();
                bluetoothList = await DependencyService.Get<IPrintReceipt>().BluetoothList();
                if (bluetoothList.Count > 0)
                {
                    if (Common.mAppkey == null || Common.mAppkey.Id == 0)
                    {
                        if (bluetoothList.Count == 1)
                        {
                            AppKey mAppkey = new AppKey();
                            mAppkey.Id = 1;
                            mAppkey.KeyName = bluetoothList[0].Name;
                            mAppkey.KeyAddress = bluetoothList[0].Address;
                            Common.mAppkey = mAppkey;
                            if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                            {
                                ZebraOrderPrint(Common.mAppkey, mEqTpSoLns);
                            }
                            else
                            {
                                PrintOrderReceipt(mEqTpSoLns, Common.mAppkey);
                            }
                        }
                        else
                        {
                            AppKey mAppkey = new AppKey();
                            BluetoothPopup bluetoothPopup = new BluetoothPopup();
                            bluetoothPopup.RefreshEvent += (s, e1) =>
                            {
                                var bluetooth = (BluetoothList)s;
                                if (bluetooth != null)
                                {
                                    mAppkey.Id = 1;
                                    mAppkey.KeyName = bluetooth.Name;
                                    mAppkey.KeyAddress = bluetooth.Address;
                                    Common.mAppkey = mAppkey;
                                    if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                                    {
                                        ZebraOrderPrint(Common.mAppkey, mEqTpSoLns);
                                    }
                                    else
                                    {
                                        PrintOrderReceipt(mEqTpSoLns, Common.mAppkey);
                                    }
                                };
                            };
                            PopupNavigation.PushAsync(bluetoothPopup);
                        }
                    }
                    else
                    {
                        if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                        {
                            ZebraOrderPrint(Common.mAppkey, mEqTpSoLns);
                        }
                        else
                        {
                            PrintOrderReceipt(mEqTpSoLns, Common.mAppkey);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ConnectBluetooth/FuelStationPage: " + ex.Message);
            }
        }

        public async void PrintOrderReceipt(List<EqTpSoLnLocal> mEqTpSoLns, AppKey mAppkey)
        {
            try
            {
                var mHaulerData = eqtpRepository.GetEqTp();
                Acr.UserDialogs.UserDialogs.Instance.ShowLoading("Printing...");
                await Task.Delay(1 * 200);
                string nullString = "N/A";
                Receipt mReceipt = new Receipt();
                List<string> barcodes = new List<string>();
                StringBuilder orderReceipt = new StringBuilder();

                orderReceipt.Append("\x1b\x61\x01");
                orderReceipt.Append("\x1b\x40");
                orderReceipt.Append("\x1B\x21\x1");
                if (mHaulerData != null && mHaulerData.PrintFormat == "Format 2")
                {
                    orderReceipt.Append("------------------------------------------");
                    orderReceipt.Append(System.Environment.NewLine);
                }

                orderReceipt.Append("Orden de Transporte");
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1b\x45\x01");
                orderReceipt.Append("------------------------------------------");
                orderReceipt.Append("\x1b\x45\x00");
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key1 = "Orden:";
                string value1 = nullString;
                if (mEqTpSo.eqtpsoRefno != null)
                {
                    value1 = mEqTpSo.eqtpsoRefno;
                }
                orderReceipt.Append(key1 + GetRightSpace(key1) + value1);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key2 = "Movil: ";
                string value2 = nullString;
                if (mEqTpSo.eqtpsoDeviceCode != null)
                {
                    value2 = mEqTpSo.eqtpsoDeviceCode;
                }
                orderReceipt.Append(key2 + GetRightSpace(key2) + value2);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key3 = "Usuario: ";
                string value3 = nullString;
                if (mEqTpSo.eqtpsoUserCode != null)
                {
                    value3 = mEqTpSo.eqtpsoUserCode + "-" + mEqTpSo.eqtpsoUserName;
                }
                orderReceipt.Append(key3 + GetRightSpace(key3) + value3);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key4 = "Fecha:";
                string value4 = nullString;
                if (mEqTpSo.eqtpsoOpeningDate != null)
                {
                    value4 = mEqTpSo.eqtpsoOpeningDate.ToString("MM/dd/yyyy HH:mm:ss tt");
                }
                orderReceipt.Append(key4 + GetRightSpace(key4) + value4);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key5 = "Transporte:";
                string value5 = nullString;
                if (mEqTpSo.eqtpsoEqtphaulerCode != null)
                {
                    value5 = mEqTpSo.eqtpsoEqtphaulerCode;
                }
                orderReceipt.Append(key5 + GetRightSpace(key5) + value5);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key6 = "Trailer:";
                string value6 = nullString;
                if (mEqTpSo.eqtpsoEqtptrailerCode != null)
                {
                    value6 = mEqTpSo.eqtpsoEqtptrailerCode;
                }
                orderReceipt.Append(key6 + GetRightSpace(key6) + value6);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key7 = "Chofer:";
                string value7 = nullString;
                if (mEqTpSo.eqtpsoDriverCode != null)
                {
                    value7 = mEqTpSo.eqtpsoDriverCode + "-" + mEqTpSo.eqtpsoDriverName;
                }
                orderReceipt.Append(key7 + GetRightSpace(key7) + value7);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1b\x45\x01");
                orderReceipt.Append("------------------------------------------");
                orderReceipt.Append("\x1b\x45\x00");
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1b\x45\x01");
                orderReceipt.Append("Apertura");
                orderReceipt.Append("\x1b\x45\x00");
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1b\x45\x01");
                orderReceipt.Append("------------------------------------------");
                orderReceipt.Append("\x1b\x45\x00");
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key8 = "Fecha:";
                string value8 = nullString;
                if (mEqTpSo.eqtpsoOpeningDate != null)
                {
                    value8 = mEqTpSo.eqtpsoOpeningDate.ToString("MM/dd/yyyy HH:mm:ss tt");
                }
                orderReceipt.Append(key8 + GetRightSpace(key8) + value8);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key9 = "Localización:";
                string value9 = nullString;
                if (mEqTpSo.eqtpsoOpeningLocation != null)
                {
                    value9 = mEqTpSo.eqtpsoOpeningLocation;
                }
                orderReceipt.Append(key9 + GetRightSpace(key9) + value9);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key10 = "Odómetro:";
                string value10 = nullString;
                if (mEqTpSo.eqtpsoOpeningOdometer > 0)
                {
                    value10 = Convert.ToString(mEqTpSo.eqtpsoOpeningOdometer);
                }
                orderReceipt.Append(key10 + GetRightSpace(key10) + value10);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key11 = "Long./Lat.:";
                string value11 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningLongitude))
                {
                    value11 = mEqTpSo.eqtpsoOpeningLongitude + "/" + mEqTpSo.eqtpsoOpeningLatitude;
                }
                orderReceipt.Append(key11 + GetRightSpace(key11) + value11);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key12 = "Notas:";
                string value12 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningNotes))
                {
                    value12 = mEqTpSo.eqtpsoOpeningNotes;
                }
                orderReceipt.Append(key12 + GetRightSpace(key12) + value12);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1b\x45\x01");
                orderReceipt.Append("------------------------------------------");
                orderReceipt.Append("\x1b\x45\x00");
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1b\x45\x01");
                orderReceipt.Append("Cierre");
                orderReceipt.Append("\x1b\x45\x00");
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key13 = "Fecha:";
                string value13 = nullString;
                if (mEqTpSo.eqtpsoClosingDate != null && mEqTpSo.eqtpsoInterfaceDate != DateTime.MinValue)
                {
                    value13 = mEqTpSo.eqtpsoClosingDate.ToString("MM/dd/yyyy HH:mm:ss tt");
                }
                orderReceipt.Append(key13 + GetRightSpace(key13) + value13);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key14 = "Localización:";
                string value14 = nullString;
                if (mEqTpSo.eqtpsoClosingLocation != null)
                {
                    value14 = mEqTpSo.eqtpsoClosingLocation;
                }
                orderReceipt.Append(key14 + GetRightSpace(key14) + value14);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key15 = "Odómetro:";
                string value15 = nullString;
                if (mEqTpSo.eptpsoClosingOdometer > 0)
                {
                    value10 = Convert.ToString(mEqTpSo.eptpsoClosingOdometer);
                }
                orderReceipt.Append(key15 + GetRightSpace(key15) + value15);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key16 = "Long./Lat.:";
                string value16 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningLongitude))
                {
                    value16 = mEqTpSo.eqtpsoClosingLongitude + "/" + mEqTpSo.eqtpsoClosingLatitude;
                }
                orderReceipt.Append(key16 + GetRightSpace(key16) + value16);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key17 = "Notas:";
                string value17 = nullString;
                if (mEqTpSo.eqtpsoClosingNotes != null)
                {
                    value17 = mEqTpSo.eqtpsoClosingNotes;
                }
                orderReceipt.Append(key17 + GetRightSpace(key17) + value17);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1b\x45\x01");
                orderReceipt.Append("------------------------------------------");
                orderReceipt.Append("\x1b\x45\x00");
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                string key35 = "Estatus:";
                string value35 = nullString;
                if (mEqTpSo.eqtpsoStatus != null)
                {
                    value35 = mEqTpSo.eqtpsoStatus;
                }
                orderReceipt.Append(key35 + GetRightSpace(key35) + value35);
                orderReceipt.Append(System.Environment.NewLine);
                orderReceipt.Append(System.Environment.NewLine);
                orderReceipt.Append(System.Environment.NewLine);

                orderReceipt.Append("\x1B\x21\x1");
                orderReceipt.Append("\x1b\x45\x01");
                orderReceipt.Append("Transportes");
                orderReceipt.Append("\x1b\x45\x00");
                orderReceipt.Append(System.Environment.NewLine);

                mReceipt.OrderReceiptString = orderReceipt;

                if (mEqTpSoLns != null && mEqTpSoLns.Count > 0)
                {
                    mReceipt.TransactionReceiptString = new List<StringBuilder>();
                    foreach (var item in mEqTpSoLns.Where(x => x.eqtpsolnEqtpsoRefno == mEqTpSo.eqtpsoRefno))
                    {
                        StringBuilder transactionString = new StringBuilder();
                        transactionString.Append("\x1b\x61\x00");
                        transactionString.Append("\x1B\x21\x1");
                        transactionString.Append("\x1b\x45\x01");
                        transactionString.Append("------------------------------------------");
                        transactionString.Append("\x1b\x45\x00");
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key18 = "Refno.:";
                        string value18 = nullString;
                        if (item.eqtpsolnRefno != null)
                        {
                            value18 = item.eqtpsolnRefno;
                        }
                        transactionString.Append(key18 + GetRightSpace(key18) + value18);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key19 = "Rfid:";
                        string value19 = nullString;
                        if (item.eqtpsolnEquipmentRfid != null)
                        {
                            value19 = item.eqtpsolnEquipmentRfid;
                        }
                        transactionString.Append(key19 + GetRightSpace(key19) + value19);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key20 = "Equipo:";
                        string value20 = nullString;
                        if (item.eqtpsolnEquipmentCode != nullString)
                        {
                            value20 = item.eqtpsolnEquipmentCode + "-" + item.eqtpsolnEquipmentDesc;
                        }
                        transactionString.Append(key20 + GetRightSpace(key20) + value20);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key21 = "Proyecto:";
                        string value21 = nullString;
                        if (item.eqtpsolnProjactCode != nullString)
                        {
                            value21 = item.eqtpsolnProjactCode + "-" + item.eqtpsolnProjectDesc;
                        }
                        transactionString.Append(key21 + GetRightSpace(key21) + value21);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key22 = "Partida:";
                        string value22 = nullString;
                        if (!string.IsNullOrEmpty(item.eqtpsolnProjactCode))
                        {
                            value22 = item.eqtpsolnProjactCode + "-" + item.eqtpsolnProjactDesc;
                        }
                        transactionString.Append(key22 + GetRightSpace(key22) + value22);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key23 = "Código Seguridad:";
                        string value23 = nullString;
                        if (!string.IsNullOrEmpty(item.eqtpsolnSecurityCode))
                        {
                            value23 = item.eqtpsolnSecurityCode;
                        }
                        transactionString.Append(key23);
                        transactionString.Append(System.Environment.NewLine);
                        transactionString.Append(value23);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        transactionString.Append("\x1b\x45\x01");
                        transactionString.Append("Carga");
                        transactionString.Append("\x1b\x45\x00");
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key24 = "Fecha:";
                        string value24 = nullString;
                        if (item.eqtpsolnLoadingDatetime != null)
                        {
                            value24 = item.eqtpsolnLoadingDatetime.ToString();
                        }
                        transactionString.Append(key24 + GetRightSpace(key24) + value24);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key25 = "Localización:";
                        string value25 = nullString;
                        if (item.eqtpsolnLoadingLocation != null)
                        {
                            value25 = item.eqtpsolnLoadingLocation;
                        }
                        transactionString.Append(key25 + GetRightSpace(key25) + value25);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key26 = "Odómetro:";
                        string value26 = nullString;
                        if (item.eqtpsolnLoadingOdometer > 0)
                        {
                            value26 = Convert.ToString(item.eqtpsolnLoadingOdometer);
                        }
                        transactionString.Append(key26 + GetRightSpace(key26) + value26);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key27 = "Long./Lat.:";
                        string value27 = nullString;
                        if (item.eqtpsolnLoadingLongitude != nullString)
                        {
                            value27 = item.eqtpsolnLoadingLongitude + "/" + item.eqtpsolnLoadingLatitude;
                        }
                        transactionString.Append(key27 + GetRightSpace(key27) + value27);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key28 = "Notas:";
                        string value28 = nullString;
                        if (!string.IsNullOrEmpty(item.eqtpsolnLoadingNotes))
                        {
                            value28 = item.eqtpsolnLoadingNotes;
                        }
                        transactionString.Append(key28 + GetRightSpace(key28) + value28);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        transactionString.Append("\x1b\x45\x01");
                        transactionString.Append("Descarga");
                        transactionString.Append("\x1b\x45\x00");
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key29 = "Fecha:";
                        string value29 = nullString;
                        if (item.eqtpsolnUnloadingDatetime != null)
                        {
                            value29 = item.eqtpsolnUnloadingDatetime.ToString();
                        }
                        transactionString.Append(key29 + GetRightSpace(key29) + value29);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key30 = "Localización:";
                        string value30 = nullString;
                        if (item.eqtpsolnUnloadingLocation != null)
                        {
                            value30 = item.eqtpsolnUnloadingLocation;
                        }
                        transactionString.Append(key30 + GetRightSpace(key30) + value30);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key31 = "Odómetro:";
                        string value31 = nullString;
                        if (item.eqtpsolnUnloadingOdometer > 0)
                        {
                            value31 = Convert.ToString(item.eqtpsolnUnloadingOdometer);
                        }
                        transactionString.Append(key31 + GetRightSpace(key31) + value31);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key32 = "Long./Lat.:";
                        string value32 = nullString;
                        if (item.eqtpsolnUnloadingLongitude != nullString)
                        {
                            value32 = item.eqtpsolnUnloadingLongitude + "/" + item.eqtpsolnUnloadingLatitude;
                        }
                        transactionString.Append(key32 + GetRightSpace(key32) + value32);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key33 = "Notas:";
                        string value33 = nullString;
                        if (!string.IsNullOrEmpty(item.eqtpsolnUnloadingNotes))
                        {
                            value33 = item.eqtpsolnUnloadingNotes;
                        }
                        transactionString.Append(key33 + GetRightSpace(key33) + value33);
                        transactionString.Append(System.Environment.NewLine);
                        transactionString.Append(System.Environment.NewLine);

                        transactionString.Append("\x1B\x21\x1");
                        string key34 = "Estatus:";
                        string value34 = nullString;
                        if (!string.IsNullOrEmpty(item.eqtpsolnStatus))
                        {
                            value34 = item.eqtpsolnStatus;
                        }
                        transactionString.Append(key34 + GetRightSpace(key34) + value34);
                        transactionString.Append(System.Environment.NewLine);
                        transactionString.Append(System.Environment.NewLine);
                        transactionString.Append(System.Environment.NewLine);

                        if (mHaulerData != null && mHaulerData.PrintFormat == "Format 2")
                        {
                            // Barcode
                            if (item.eqtpsolnBarcode != null)
                            {
                                barcodes.Add(item.eqtpsolnBarcode);
                            }
                            transactionString.Append("\x1b\x40");
                            transactionString.Append("\x1b\x61\x01");
                        }

                        mReceipt.TransactionReceiptString.Add(transactionString);
                    }
                }

                // Closing Statement
                StringBuilder closingReceipt = new StringBuilder();

                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);

                closingReceipt.Append("\x1B\x21\x1");
                string key36 = "------------------";
                string value36 = "------------------";
                closingReceipt.Append(key36 + GetSpace(key36, value36) + value36);
                closingReceipt.Append(System.Environment.NewLine);

                closingReceipt.Append("\x1B\x21\x1");
                string key37 = "   Transportista   ";
                string value37 = "      Supervisor      ";
                closingReceipt.Append(key37 + GetSpace(key37, value37) + value37);
                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);
                closingReceipt.Append(System.Environment.NewLine);
                mReceipt.ClosingReceiptString = closingReceipt;
                //
                var jsonString = await DependencyService.Get<IPrintReceipt>().PrintOrderReceipt(mAppkey.KeyAddress, mReceipt, barcodes);
                Acr.UserDialogs.UserDialogs.Instance.HideLoading();
                if (jsonString)
                {
                    await DisplayAlert("", "Impresión completada", "Ok");
                }
                else
                {
                    await DisplayAlert("", "No se pudo imprimir", "Ok");
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("PrintOrderReceipt/FuelStationPage: " + ex.Message);
            }
        }

        public async void ZebraOrderPrint(AppKey mAppKey, List<EqTpSoLnLocal> mEqTpSoLns)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Printing...");
                Xamarin.Forms.DependencyService.Get<IBluetooth>().ConnectBluetooth(mAppKey.KeyName, mAppKey.KeyAddress);

                await Task.Run(async () =>
                {
                    var zpl = await ZebraLabelBuilder(mEqTpSoLns);
                    var result = await Xamarin.Forms.DependencyService.Get<IBluetooth>().Print(zpl);
                    await Task.Delay(3000);
                    Xamarin.Forms.DependencyService.Get<IBluetooth>().DisconnectBluetooth();
                });
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("FuelStationPage/ZebraOrderPrint: " + ex.Message);
            }
            finally { UserDialogs.Instance.HideLoading(); }
        }

        public async Task<byte[]> ZebraLabelBuilder(List<EqTpSoLnLocal> mEqTpSoLn)
        {
            string zpl = string.Empty;
            try
            {
                if (mEqTpSo != null)
                {
                    string base64Image = LogoBase64String.LogoBase64;
                    byte[] imageBytes = Convert.FromBase64String(base64Image);
                    ZPLImage zplimg = Xamarin.Forms.DependencyService.Get<IBluetooth>().GetZPLImageCommand(imageBytes);

                    ZPLForge.Label label = new ZPLForge.Label
                    {
                        Quantity = 1,
                        PrintWidth = 576,
                        MediaTracking = MediaTracking.ContinuousVariableLength,
                        MediaType = MediaType.ThermalDirect,
                        PrintMode = PrintMode.TearOff,
                        MediaDarknessLevel = 15,
                        Content =
                    {
                        new ImageElement
                    {
                        PositionX = 15,
                        PositionY = 70,
                        Compression = CompressionType.HexASCII,
                        BinaryByteCount = zplimg.ByteCount,
                        GraphicFieldCount = zplimg.ByteCount,
                        BytesPerRow = zplimg.BytesPerRow,
                        Content = zplimg.Data
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 180,
                        Content = "Alba Sanchez & Asociados",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 210,
                        Content = "RNC: 1-01-13267-1",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 240,
                        Content = "Orden de Transporte",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 270,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 300,
                        Content = "Orden:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 300,
                        Content = mEqTpSo.eqtpsoRefno,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 330,
                        Content = "Movil:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 330,
                        Content = string.Format("{0:00}", Convert.ToInt32(mEqTpSo.eqtpsoDeviceCode)),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 360,
                        Content = "Usuario:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 360,
                        Content = mEqTpSo.eqtpsoUserCode + "-" + mEqTpSo.eqtpsoUserName,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 390,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 390,
                        Content = mEqTpSo.eqtpsoOpeningDate.ToString("MM/dd/yyyy HH:mm:ss tt"),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 420,
                        Content = "Transportista:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 420,
                        Content = mEqtpData.EqtpCode,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 450,
                        Content = "Transporte:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 450,
                        Content = mEqTpSo.eqtpsoEqtphaulerCode,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 480,
                        Content = "Trailer:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 480,
                        Content = mEqTpSo.eqtpsoEqtptrailerCode,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 510,
                        Content = "Chofer:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 510,
                        Content = mEqTpSo.eqtpsoDriverCode + " " + mEqTpSo.eqtpsoDriverName,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 540,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 570,
                        Content = "Apertura",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 600,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 630,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 630,
                        Content = mEqTpSo.eqtpsoOpeningDate.ToString("MM/dd/yyyy HH:mm:ss tt"),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 660,
                        Content = "Localización:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 660,
                        Content = mEqTpSo.eqtpsoOpeningLocation,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 690,
                        Content = "Odómetro:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 690,
                        Content = Convert.ToString(mEqTpSo.eqtpsoOpeningOdometer),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 720,
                        Content = "Long./Lat.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 720,
                        Content = mEqTpSo.eqtpsoOpeningLongitude + "/" + mEqTpSo.eqtpsoOpeningLatitude,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 750,
                        Content = "Notas:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 750,
                        Content = mEqTpSo.eqtpsoOpeningNotes,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 780,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 810,
                        Content = "Cierre",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 840,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 840,
                        Content = mEqTpSo.eqtpsoClosingDate.ToString("MM/dd/yyyy HH:mm:ss tt"),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 870,
                        Content = "Localización:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 870,
                        Content = mEqTpSo.eqtpsoClosingLocation,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 900,
                        Content = "Odómetro:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 900,
                        Content = Convert.ToString(mEqTpSo.eptpsoClosingOdometer),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 930,
                        Content = "Long./Lat.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 930,
                        Content = mEqTpSo.eqtpsoClosingLongitude + "/" + mEqTpSo.eqtpsoClosingLatitude,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 960,
                        Content = "Notas:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 960,
                        Content = mEqTpSo.eqtpsoClosingNotes,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 990,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 1020,
                        Content = "Estatus:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 1020,
                        Content = mEqTpSo.eqtpsoStatus,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                    }
                    };

                    int posY = 1050;

                    if (mEqTpSoLn != null && mEqTpSoLn.Count > 0)
                    {
                        label.Content.Add(new TextElement
                        {
                            PositionX = 10,
                            PositionY = posY,
                            Content = "Transportes",
                            FontStyle = ZPLForge.Common.Font.Default,
                            CharHeight = 25
                        });

                        posY += 30;

                        foreach (var item in mEqTpSoLn.Where(x => x.eqtpsolnEqtpsoRefno == mEqTpSo.eqtpsoRefno))
                        {
                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "----------------------------------------",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 15
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Refno.:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnRefno,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Rfid:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnEquipmentRfid,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Equipo:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnEquipmentCode + "-" + item.eqtpsolnEquipmentDesc,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Proyecto:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnProjectCode + "-" + item.eqtpsolnProjectDesc,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Partida:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnProjactCode + "-" + item.eqtpsolnProjactDesc,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Cod.Seg.:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnSecurityCode,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "----------------------------------------",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 15
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Carga",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Fecha:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnLoadingDatetime.ToString(),
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Localización:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnLoadingLocation,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Odómetro:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = Convert.ToString(item.eqtpsolnLoadingOdometer),
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Long./Lat.:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnLoadingLongitude + "/" + item.eqtpsolnLoadingLatitude,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Notas:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnLoadingNotes,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "----------------------------------------",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 15
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Descarga",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Fecha:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnUnloadingDatetime.ToString(),
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Localización:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnUnloadingLocation,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Odómetro:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = Convert.ToString(item.eqtpsolnUnloadingOdometer),
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Long./Lat.:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnUnloadingLongitude + "/" + item.eqtpsolnUnloadingLatitude,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Notas:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnUnloadingNotes,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;

                            label.Content.Add(new TextElement
                            {
                                PositionX = 10,
                                PositionY = posY,
                                Content = "Estatus:",
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            label.Content.Add(new TextElement
                            {
                                PositionX = 145,
                                PositionY = posY,
                                Content = item.eqtpsolnStatus,
                                FontStyle = ZPLForge.Common.Font.Default,
                                CharHeight = 25
                            });

                            posY += 30;
                        }
                    }

                    posY += 100;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 310,
                        PositionY = posY,
                        Content = "------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 80,
                        PositionY = posY,
                        Content = "Transportista",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 395,
                        PositionY = posY,
                        Content = "Supervisor",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.MediaLength = posY + 150;

                    zpl = label.ToString().Insert(3, "^POI");
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                Utilties.DisplayInfo.ErrorMessage("CloseOrderPage/ZebraLabelBuilder: " + ex.Message);
            }
            return Encoding.UTF8.GetBytes(zpl);
        }
        #endregion

        #region Events
        // Previous Shift
        private void BtnPrevious_Tapped(object sender, EventArgs e)
        {
            try
            {
                imgPreviousDate.IsVisible = false;
                imgNextDate.IsVisible = true;
                lblShiftStatus.Text = "Previous Orden";
                btnStart.IsEnabled = false;
                btnClose.IsEnabled = false;
                btnView.IsEnabled = true;
                btnPrint.IsEnabled = true;

                mEqTpSo = shifts.Where(x => x.eqtpsoStatus == Common.CloseStatus).LastOrDefault();
                if (mEqTpSo != null)
                {
                    mPreviousFlStShift = mEqTpSo;
                    lblShift.Text = " - " + mEqTpSo.eqtpsoRefno;
                    lblDate.IsVisible = true;
                    lblShift.IsVisible = true;
                    lblDate.Text = "Apertura - " + mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyy hh:mm tt");
                }
                else
                {
                    lblDate.IsVisible = false;
                    lblShift.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnPrevious_Tapped/FuelStationPage Exception: " + ex.Message);
            }
        }

        // Current Shift
        private void BtnNext_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (shifts != null && shifts.Where(x => x.eqtpsoStatus == Common.CloseStatus).Count() > 0)
                    imgPreviousDate.IsVisible = true;
                else
                    imgPreviousDate.IsVisible = false;

                imgNextDate.IsVisible = false;
                lblShiftStatus.Text = "Abrir Orden";

                mPreviousFlStShift = new EqTpSo();
                GetShiftStatus();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnNext_Tapped/FuelStationPage Exception: " + ex.Message);
            }
        }

        private void btnStart_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Home.ShiftPage(selectedDate));
        }

        private void BtnView_Clicked(object sender, EventArgs e)
        {
            if (mEqTpSo != null)
                Navigation.PushAsync(new Home.ViewTransportesPage(mEqTpSo.eqtpsoRefno));
            else
            {
                //
            }
            // Navigation.PushAsync(new Home.ViewTransportesPage());
        }

        private void BtnPrint_Clicked(object sender, EventArgs e)
        {
            try
            {
                EqTpSo mShift = new EqTpSo();

                if (mEqTpSo != null && !string.IsNullOrEmpty(mEqTpSo.eqtpsoRefno))
                {
                    mShift = shiftRepository.GetAllEqTpSo().Where(x => x.eqtpsoRefno == mEqTpSo.eqtpsoRefno).LastOrDefault();
                }

                if (mShift != null && mShift.Id > 0)
                    ConnectBluetooth(mShift);
                else
                {
                    var message = DisplayAlert("", "No shift is open", "Ok");
                }

            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnPrint_Clicked/FuelStationPage Exception: " + ex.Message);
            }
        }

        private void BtnClose_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Home.CloseOrderPage());
        }

        private void BtnIssues_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (mEqTpSo == null)
                {
                    Utilties.DisplayInfo.InfoMessage(EndPointsMessage.IssueInfo);
                }
                else
                {
                    //Navigation.PushAsync(new Home.IssuesPage(new Driver(), new Equipment()));
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnIssues_Tapped/FuelStationPage Exception: " + ex.Message);
            }
        }

        private void BtnTransfers_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Home.AddTransportPage(selectedDate));
        }

        private void BtnReceipt_Tapped(object sender, EventArgs e)
        {

        }

        private void BtnAdjustment_Tapped(object sender, EventArgs e)
        {

        }

        private void BtnMenu_Tapped(object sender, EventArgs e)
        {
            Common.masterDetail.IsGestureEnabled = true;
            Common.masterDetail.IsPresented = true;
        }
        #endregion
    }
}