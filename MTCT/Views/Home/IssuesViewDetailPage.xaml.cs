﻿using MTCT.Models.LocalModels;
using MTCT.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IssuesViewDetailPage : ContentPage
    {
        #region Objects
        EqTpSoLnRepository flstIsRepository;
        FlstIs mFlstIs;
        #endregion

        #region Constructor
        public IssuesViewDetailPage(FlstIs mFlstIs)
        {
            InitializeComponent();
            flstIsRepository = new EqTpSoLnRepository();
            this.mFlstIs = new FlstIs();

            this.mFlstIs = mFlstIs;
            BindData(this.mFlstIs);
        }
        #endregion

        #region Methods
        public void BindData(FlstIs flstIs)
        {
            //try
            //{
            //    Utilties.DisplayInfo.ShowLoader();
            //    var mFlstIs = flstIsRepository.GetFlstIsById1(flstIs.FlstisRefno);
            //    if (mFlstIs != null)
            //    {
            //        lblHeader.Text = "Issue - " + mFlstIs.FlstisRefno;
            //        lblFlstCode.Text = mFlstIs.FlstisFlstCode;
            //        lblShiftRefno.Text = mFlstIs.FlstisShiftRefno;
            //        lblRFID.Text = mFlstIs.FlstisEquipmentRfid;
            //        lblRFIDCode.Text = mFlstIs.FlstisEquipmentCode;
            //        lblRFIDDesc.Text = mFlstIs.FlstisEquipmentDesc;
            //        lblDriverCode.Text = mFlstIs.FlstisDriverCode;
            //        lblDriverName.Text = mFlstIs.FlstisDriverName;
            //        lblDateTime.Text = mFlstIs.FlstisDate.ToString();
            //        lblKmMeter.Text = mFlstIs.FlstisOdhrmeter.ToString();
            //        lblLongitude.Text = mFlstIs.FlstisLongitude;
            //        lblLatitude.Text = mFlstIs.FlstisLatitude;
            //        lblQty.Text = mFlstIs.FlstisQuantity.ToString();
            //        lblPumpCode.Text = mFlstIs.FlstisPumpCode;
            //        lblFuelCode.Text = mFlstIs.FlstisFuelCode;

            //        if (mFlstIs.FlstisPumpImage != null)
            //        {
            //            var stream1 = LoadImage(mFlstIs.FlstisPumpImage);
            //        }
            //        // imgPicture1.Source = mFlstIs.FlstisPumpImage;

            //        //  imgPicture1.Source = mFlstIs.FlstisPumpImage;
            //        lblNotes.Text = mFlstIs.FlstisNote;
            //    }
            //    Utilties.DisplayInfo.HideLoader();
            //}
            //catch (Exception ex)
            //{
            //    Utilties.DisplayInfo.ErrorMessage("BindData/IssuesViewDetailPage Exception: " + ex.Message);
            //}
        }

        public Image LoadImage(string base64string)
        {
            byte[] bytes = Convert.FromBase64String(base64string);

            using (MemoryStream ms = new MemoryStream(bytes))
            {
                imgPicture1.Source = ImageSource.FromStream(() => new MemoryStream(bytes));
            }

            return imgPicture1;
        } 
        #endregion

        #region Events
        private async void btnBack_Tapped(object sender, EventArgs e)
        {
          await  Navigation.PopAsync();
        } 
        #endregion
    }
}