﻿using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProjectViewPage : ContentPage
    {
        #region Objects
        ProjectRepository projectRepository;
        List<Project> mProjects;
        #endregion

        #region Constructor
        public ProjectViewPage()
        {
            InitializeComponent();
            InitializeObjects();
            BindProjectData();
        }
        #endregion

        #region Methods
        public void InitializeObjects()
        {
            projectRepository = new ProjectRepository();
            mProjects = new List<Project>();
        }

        public void BindProjectData()
        {
            mProjects = projectRepository.GetProjects();
            if (mProjects != null)
            {
                lstProjects.ItemsSource = mProjects.ToList();
            }
        }
        #endregion

        #region Events
        private void BtnMenu_Tapped(object sender, EventArgs e)
        {
            Common.masterDetail.IsGestureEnabled = true;
            Common.masterDetail.IsPresented = true;
        }

        private void lstProjects_ItemTapped(object sender, ItemTappedEventArgs e)
        {

        } 
        #endregion
    }
}