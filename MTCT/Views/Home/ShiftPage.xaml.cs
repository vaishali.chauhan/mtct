﻿using MTCT.API;
using MTCT.Interface;
using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Plugin.Geolocator;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShiftPage : ContentPage
    {
        #region Objects
        ShiftRepository shiftRepository;
        EqptTrailerRepository eqptTrailerRepository;
        LocationRepository locationRepository;
        DriverRepository driverRepository;
        EqptHaulerRepository eqptHaulerRepository;
        EqtpRepository eqtpRepository;
        List<Driver> mDrivers;
        List<Locations> mLocations;
        Driver driver;
        EqTpTrailers eqTpTrailers;
        EqTpHaulers eqTpHaulers;
        Locations locations;
        Eqtp mEqtp;
        public List<FlstFuel> mPumpCounters;
        Plugin.Geolocator.Abstractions.Position location = null;

        MediaFile file1;
        private byte[] imageByteArray1;

        DateTime currentDt;
        byte[] signatureByte;
        bool isLocation = false;

        FlstShiftAPI flstShiftAPI;
        #endregion

        #region Constructor
        public ShiftPage(DateTime currentDt)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            this.currentDt = currentDt;
            InitializeOject();
        }
        #endregion

        #region Methods
        public void InitializeOject()
        {
            try
            {
                shiftRepository = new ShiftRepository();
                eqptTrailerRepository = new EqptTrailerRepository();
                locationRepository = new LocationRepository();
                driverRepository = new DriverRepository();
                eqptHaulerRepository = new EqptHaulerRepository();
                eqtpRepository = new EqtpRepository();
                driver = new Driver();
                locations = new Locations();
                mEqtp = new Eqtp();
                eqTpTrailers = new EqTpTrailers();
                eqTpHaulers = new EqTpHaulers();
                mDrivers = new List<Driver>();
                mLocations = new List<Locations>();
                mPumpCounters = new List<FlstFuel>();

                flstShiftAPI = new FlstShiftAPI();
                file1 = null;
                signatureByte = null;

                BindUserStartShiftDetails();
                BindDrivers();
                BindHaulers();
                BindTrailers();
                BindLocations();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeOject/OrderPage" + ex.Message);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (!isLocation)
            {
                BindMap();
            }
        }

        public void BindUserStartShiftDetails()
        {
            try
            {
                string customerCode = "00001";
                txtDateTime.Date = currentDt;
                mEqtp = eqtpRepository.GetEqTp();
                if (mEqtp != null)
                {
                    if (mEqtp.EqtpOrderCounterNo > 0)
                    {
                        customerCode = GetDynamicRefnoCode(mEqtp.EqtpOrderCounterNo + 1);
                    }
                }
                lblShiftRefno.Text = "Abrir Orden : " + customerCode;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindUserStartShiftDetails/OrderPage" + ex.Message);
            }
        }

        public string GetDynamicRefnoCode(int OrderCounter)
        {
            string RefNoCode = string.Empty;
            try
            {
                var code = "00000";
                if (OrderCounter > 0)
                {
                    int counterLenght = Convert.ToString(OrderCounter).Length;
                    int refCodeLenght = code.Length;
                    var trimIndex = refCodeLenght - counterLenght;
                    if (trimIndex > 0)
                    {
                        RefNoCode = code.Substring(0, trimIndex);
                        RefNoCode += OrderCounter;
                    }
                    else
                    {
                        RefNoCode = Convert.ToString(OrderCounter);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetDynamicRefnoCode/OrderPage" + ex.Message);
            }
            return RefNoCode;
        }

        public void BindMap()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    if (locator != null && locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        locator.DesiredAccuracy = 5;
                        location = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(1000));
                        if (location != null)
                        {
                            if (Common.HasInternetConnection())
                            {
                                try
                                {
                                    var mAddress = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
                                    if (mAddress != null && mAddress.Count() > 0)
                                    {
                                        string addressLabel = string.Empty;
                                        foreach (var add in mAddress)
                                        {
                                            addressLabel += add.FeatureName + " ";
                                        }
                                        var pin = new Pin
                                        {
                                            Type = PinType.Place,
                                            Label = addressLabel,
                                            Position = new Position(location.Latitude, location.Longitude)
                                        };

                                        xfmap.Pins.Add(pin);
                                        xfmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(location.Latitude, location.Longitude), Distance.FromMeters(150.0)));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Utilties.DisplayInfo.ErrorMessage("El proceso no pudo obtener la ubicación actual.");
                                }
                            }
                        }

                    }
                    else
                    {
                        var myAction = await DisplayAlert("Ubicación", "Activa la ubicación", "Okay", "Cancelar");
                        if (myAction)
                        {
                            if (Device.RuntimePlatform == global::Xamarin.Forms.Device.Android)
                            {
                                global::Xamarin.Forms.DependencyService.Get<ILocSettings>().OpenSettings();
                            }
                            else
                            {
                                await DisplayAlert("Dispositivo", "Estas usando alguna otra mierda", "SÍ");
                            }
                        }
                        else
                        {
                            isLocation = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utilties.DisplayInfo.ErrorMessage("El proceso no pudo obtener la ubicación actual.");
                }
            });
        }

        public void BindDrivers()
        {
            try
            {
                mDrivers = driverRepository.GetDrivers();
                if (mDrivers != null && mDrivers.Count == 1)
                {
                    lblDriverCode.Text = mDrivers.Select(x => x.DriverCode).FirstOrDefault();
                    lblDriverName.Text = mDrivers.Select(x => x.DriverName).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindDrivers/OrderPage" + ex.Message);
            }
        }

        public void BindHaulers()
        {
            try
            {
                var mHaulers = eqptHaulerRepository.GetEqTpHaulers();
                if (mHaulers != null && mHaulers.Count == 1)
                {
                    lblHaulerCode.Text = mHaulers.Select(x => x.EqtphaulerCode).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindHaulers/OrderPage" + ex.Message);
            }
        }

        public void BindTrailers()
        {
            try
            {
                var mTrailers = eqptTrailerRepository.GetEqTpTrailers();
                if (mTrailers != null && mTrailers.Count == 1)
                {
                    lblTrailerCode.Text = mTrailers.Select(x => x.EqtptrailerCode).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindTrailers/OrderPage" + ex.Message);
            }
        }

        public void BindLocations()
        {
            try
            {
                mLocations = locationRepository.GetLocationss();
                if (mLocations != null && mLocations.Count == 1)
                {
                    lblLocationCode.Text = mLocations.Select(x => x.EqtplocationCode).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindLocations/OrderPage" + ex.Message);
            }
        }

        public void NavigateToDriverSelection()
        {
            try
            {
                var driverSearchPopup = new PopUp.SearchDriverNamePopup();
                driverSearchPopup.RefreshEvent += (s1, e1) =>
                {
                    driver = (Driver)s1;
                    if (driver != null && !string.IsNullOrEmpty(driver.DriverName))
                    {
                        lblDriverCode.Text = driver.DriverCode;
                        lblDriverCode.TextColor = Color.FromHex("#000000");
                        lblDriverName.Text = driver.DriverName;
                        lblDriverName.TextColor = Color.FromHex("#000000");
                    }
                };
                PopupNavigation.PushAsync(driverSearchPopup);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnDriverCode_Tapped/OrderPage" + ex.Message);
            }
        }

        public void NavigateToHaulerSelection()
        {
            try
            {
                var haulerPopup = new PopUp.HaulersPopup();
                haulerPopup.RefreshEvent += (s1, e1) =>
                {
                    eqTpHaulers = (EqTpHaulers)s1;
                    if (eqTpHaulers != null && !string.IsNullOrEmpty(eqTpHaulers.EqtphaulerCode))
                    {
                        lblHaulerCode.Text = eqTpHaulers.EqtphaulerCode;
                        lblHaulerCode.TextColor = Color.FromHex("#000000");
                        lblTrailerCode.Text = "Trailer";
                    }
                };
                PopupNavigation.PushAsync(haulerPopup);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToHaulerSelection/OrderPage" + ex.Message);
            }
        }

        public void NavigateToTrailerSelection()
        {
            try
            {
                if (!string.IsNullOrEmpty(lblHaulerCode.Text) && lblHaulerCode.Text != "Transporte")
                {
                    var trailerPopup = new PopUp.TrailerPopup(eqTpHaulers.EqtphaulerId);
                    trailerPopup.RefreshEvent += (s1, e1) =>
                    {
                        eqTpTrailers = (EqTpTrailers)s1;
                        if (eqTpTrailers != null && !string.IsNullOrEmpty(eqTpTrailers.EqtptrailerCode))
                        {
                            lblTrailerCode.Text = eqTpTrailers.EqtptrailerCode;
                            lblTrailerCode.TextColor = Color.FromHex("#000000");
                        }
                    };
                    PopupNavigation.PushAsync(trailerPopup);
                }
                else
                {
                    Utilties.DisplayInfo.ErrorMessage("Primero seleccione transportista.");
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToTrailerSelection/OrderPage" + ex.Message);
            }
        }

        public void NavigateToLoctionSelection()
        {
            try
            {
                var locationPopup = new PopUp.LocationPopup();
                locationPopup.RefreshEvent += (s1, e1) =>
                {
                    locations = (Locations)s1;
                    if (locations != null && !string.IsNullOrEmpty(locations.EqtplocationCode))
                    {
                        lblLocationCode.Text = locations.EqtplocationCode;
                        lblLocationCode.TextColor = Color.FromHex("#000000");
                    }
                };
                PopupNavigation.PushAsync(locationPopup);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToLoctionSelection/OrderPage" + ex.Message);
            }
        }

        public byte[] fileToByte(MediaFile file)
        {
            byte[] ImageBytes = null;
            try
            {
                var memoryStream = new MemoryStream();
                file.GetStream().CopyTo(memoryStream);
                ImageBytes = memoryStream.ToArray();
                ImageBytes = CompressImage(ImageBytes);
                memoryStream.Dispose();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("fileToByte/OrderPage" + ex.Message);
            }
            return ImageBytes;
        }

        public byte[] CompressImage(byte[] imgBytes)
        {

            byte[] CompressimageBytes = null;
            try
            {
                var image = SKImage.FromEncodedData(imgBytes);
                var data = image.Encode(SKEncodedImageFormat.Jpeg, 30);
                CompressimageBytes = data.ToArray();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("CompressImage/OrderPage" + ex.Message);
            }
            return CompressimageBytes;
        }

        public bool ValidatedFields()
        {
            bool isValid = false;
            try
            {
                if (string.IsNullOrEmpty(lblHaulerCode.Text))
                {
                    lblHaulerCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Hauler);
                }
                else if (string.IsNullOrEmpty(lblTrailerCode.Text) || lblTrailerCode.Text == "Trailer")
                {
                    lblTrailerCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Trailer);
                }
                else if (txtDateTime.Date == null)
                {
                    txtDateTime.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.OpeningDate);
                }
                else if (string.IsNullOrEmpty(lblDriverCode.Text) || string.IsNullOrEmpty(lblDriverName.Text) || lblDriverCode.Text == "Código del Chofer")
                {
                    lblDriverCode.TextColor = Color.Red;
                    lblDriverName.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Driver);
                }
                else if (string.IsNullOrEmpty(txtOdometer.Text))
                {
                    //txtOdometer.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Odometer);
                }
                else if (string.IsNullOrEmpty(lblLocationCode.Text) || lblLocationCode.Text == "Localización")
                {
                    lblLocationCode.TextColor = Color.Red;
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Location);
                }
                else if (img1.IsEnabled && imageByteArray1 == null)
                {
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Picture);
                }
                else if (signatureByte == null)
                {
                    Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Signature);
                }
                else
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ValidatedFields/OrderPage" + ex.Message);
            }
            return isValid;
        }

        private byte[] ConvertMediaFileToByteArray(MediaFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                file.GetStream().CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        private byte[] GetImageStreamAsBytes(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        #endregion

        #region Events
        private void BtnBack_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void BtnSignature_Tapped(object sender, EventArgs e)
        {
            try
            {
                PopUp.SignaturePopup signaturePopup = new PopUp.SignaturePopup(false, string.Empty);
                signaturePopup.isRefresh += (s, e1) =>
                  {
                      signatureByte = (byte[])s;
                      if (signatureByte == null)
                      {
                          lblComplete.IsVisible = false;
                      }
                      else
                      {
                          lblComplete.IsVisible = true;
                      }
                  };
                PopupNavigation.PushAsync(signaturePopup);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnSignature_Tapped/OrderPage" + ex.Message);
            }
        }

        private async void BtnUploadPicture1_Tapped(object sender, EventArgs e)
        {
            try
            {
                var noImage = "iconGrayNoImage.png";
                var selectedImage = imgPicture1.Source.ToString().Replace("File: ", string.Empty);
                if (selectedImage == noImage)
                {
                    if (imgPicture1.Source == null)
                    {
                        btnDelete.IsVisible = false;
                        imgPicture1.Source = "iconGrayNoImage.png";
                    }
                    else
                    {
                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {
                            await DisplayAlert("No Camera", ":( No camera available.", "OK");
                            return;
                        }

                        var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                        var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                        if (cameraStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted || storageStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                            cameraStatus = results[Permission.Camera];
                            storageStatus = results[Permission.Storage];
                        }

                        if (cameraStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted && storageStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                        {
                            file1 = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                            {
                                Directory = "AlbaSanchez",
                                SaveToAlbum = true,
                                CompressionQuality = 50,
                                CustomPhotoSize = 50,
                                DefaultCamera = CameraDevice.Rear,
                                AllowCropping = true,
                            });

                            if (file1 == null)
                            {
                                btnDelete.IsVisible = false;
                                imgPicture1.Source = "iconGrayNoImage.png";
                                return;
                            }

                            var imagefile = file1.GetStreamWithImageRotatedForExternalStorage();
                            imagefile.Position = 0;
                            if (imagefile != null)
                                imageByteArray1 = GetImageStreamAsBytes(imagefile);

                            imgPicture1.Aspect = Aspect.Fill;
                            file1.Dispose();
                            imgPicture1.Source = ImageSource.FromStream(() => new MemoryStream(imageByteArray1));

                            btnDelete.IsVisible = true;
                        }
                        else
                        {
                            await DisplayAlert("Permissions denied", "Unable to take photos.", "OK");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnUploadPicture1_Tapped/OrderPage" + ex.Message);
            }
        }

        private void BtnDelete_Tapped(object sender, EventArgs e)
        {
            PopUp.DeleteImagePopup deleteImagePopup = new PopUp.DeleteImagePopup();
            try
            {
                deleteImagePopup.isRefresh += (s, e1) =>
                   {
                       var isDelete = (bool)s;
                       if (isDelete)
                       {
                           imageByteArray1 = null;
                           btnDelete.IsVisible = false;
                           imgPicture1.Source = "iconGrayNoImage.png";
                       }
                   };
                PopupNavigation.PushAsync(deleteImagePopup);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnDelete_Tapped/OrderPage" + ex.Message);
            }
        }

        private void BtnCancel_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void BtnSave_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (ValidatedFields())
                {
                    PopUp.ConfirmationPopup confirmationPopup = new PopUp.ConfirmationPopup("Abrir orden de transporte?");
                    confirmationPopup.isRefresh += async (s, e1) =>
                    {
                        var isConfirm = (bool)s;
                        if (isConfirm)
                        {
                            await Task.Run(() => Utilties.DisplayInfo.ShowLoader());
                            await Task.Delay(1 * 500);

                            // Local
                            EqTpSo mEqTpOrder = new EqTpSo();
                            var mShiftList = shiftRepository.GetAllEqTpSo();
                            if (mShiftList.Count() > 0 && mShiftList != null)
                            {
                                mEqTpOrder.EqTpSoId = mShiftList.LastOrDefault().EqTpSoId + 1;
                            }
                            else
                            {
                                mEqTpOrder.EqTpSoId = 1;
                            }
                            mEqTpOrder.eqtpsoEqtpCode = mEqtp.EqtpCode;
                            mEqTpOrder.eqtpsoDeviceCode = Convert.ToString(mEqtp.EqtpDeviceCode);
                            mEqTpOrder.eqtpsoRefno = lblShiftRefno.Text.Split(':').LastOrDefault().Trim();
                            mEqTpOrder.eqtpsoStatus = Common.OpenStatus;
                            mEqTpOrder.eqtpsoInterfaceStatus = InterfaceStatus.Pendiente.ToString();
                            mEqTpOrder.eqtpsoInterfaceDate = DateTime.Now;

                            if (mEqtp != null)
                            {
                                mEqTpOrder.eqtpsoEqtphaulerCode = lblHaulerCode.Text;
                            }
                            mEqTpOrder.eqtpsoOpeningLocation = lblLocationCode.Text;
                            mEqTpOrder.eqtpsoDriverCode = lblDriverCode.Text;
                            mEqTpOrder.eqtpsoDriverName = lblDriverName.Text;
                            mEqTpOrder.eqtpsoEqtptrailerCode = lblTrailerCode.Text;

                            var currentTime = DateTime.Now.ToString("hh:mm:ss");
                            var currentDate = txtDateTime.Date.ToString("dd/MM/yyyy");
                            var curDateTime = currentDate + " " + currentTime;
                            DateTime myDate = DateTime.ParseExact(curDateTime, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            mEqTpOrder.eqtpsoOpeningDate = myDate;

                            if (file1 != null)
                            {
                                mEqTpOrder.eqtpsoOpeningOdometerImage = Convert.ToBase64String(imageByteArray1);
                            }

                            if (location != null && location.Latitude != 0 && location.Longitude != 0)
                            {
                                mEqTpOrder.eqtpsoOpeningLongitude = location.Longitude.ToString();
                                mEqTpOrder.eqtpsoOpeningLatitude = location.Latitude.ToString();
                            }
                            else
                            {
                                mEqTpOrder.eqtpsoOpeningLongitude = "0";
                                mEqTpOrder.eqtpsoOpeningLatitude = "0";
                            }

                            mEqTpOrder.eqtpsoOpeningOdometer = Convert.ToDecimal(txtOdometer.Text, CultureInfo.InvariantCulture);


                            if (Common.mUser != null && Common.mUser.UserId > 0)
                            {
                                mEqTpOrder.eqtpsoUserCode = Common.mUser.UserCode;
                                mEqTpOrder.eqtpsoUserName = Common.mUser.UserName;
                            }

                            mEqTpOrder.eqtpsoOpeningNotes = txtNotes.Text;
                            if (signatureByte != null)
                                mEqTpOrder.eqtpsoOpeningSignature = Convert.ToBase64String(signatureByte);

                            mEqTpOrder.IsServer = false;

                            var response = shiftRepository.SaveStartShift(mEqTpOrder);
                            if (response)
                            {
                                mEqTpOrder = shiftRepository.GetStartShift();
                                if (mEqTpOrder.eqtpsoEqtpCode == mEqtp.EqtpCode)
                                {
                                    string mRefno = mEqTpOrder.eqtpsoRefno.TrimStart(new Char[] { '0' });
                                    if (mRefno != null)
                                        mEqtp.EqtpOrderCounterNo = Convert.ToInt32(mRefno);
                                    var updatemFlst = eqtpRepository.UpdateEqtpCounter(mEqtp);
                                }

                                // API
                                EqTpSoModel mEqTpSoModel = new EqTpSoModel();
                                mEqTpSoModel.eqtpsoEqtpCode = mEqtp.EqtpCode;
                                mEqTpSoModel.eqtpsoRefno = mEqTpOrder.eqtpsoRefno;
                                mEqTpSoModel.eqtpsoStatus = mEqTpOrder.eqtpsoStatus;

                                mEqTpSoModel.eqtpsoInterfaceStatus = Common.ConvertSpainishToEnglish(InterfaceStatus.Completado.ToString());
                                if (mEqTpOrder.eqtpsoInterfaceDate != null && mEqTpOrder.eqtpsoInterfaceDate != DateTime.MinValue)
                                {
                                    mEqTpSoModel.eqtpsoInterfaceDate = mEqTpOrder.eqtpsoInterfaceDate;
                                }

                                mEqTpSoModel.eqtpsoEqtphaulerCode = mEqTpOrder.eqtpsoEqtphaulerCode;
                                mEqTpSoModel.eqtpsoDeviceCode = mEqTpOrder.eqtpsoDeviceCode;
                                mEqTpSoModel.eqtpsoOpeningDate = mEqTpOrder.eqtpsoOpeningDate;
                                mEqTpSoModel.eqtpsoOpeningLocation = mEqTpOrder.eqtpsoOpeningLocation;
                                mEqTpSoModel.eqtpsoEqtptrailerCode = mEqTpOrder.eqtpsoEqtptrailerCode;
                                mEqTpSoModel.eqtpsoDriverCode = mEqTpOrder.eqtpsoDriverCode;
                                mEqTpSoModel.eqtpsoDriverName = mEqTpOrder.eqtpsoDriverName;
                                mEqTpSoModel.eqtpsoOpeningOdometer = mEqTpOrder.eqtpsoOpeningOdometer;

                                if (imageByteArray1 != null)
                                    mEqTpSoModel.eqtpsoOpeningOdometerImage = Convert.ToBase64String(imageByteArray1);

                                if (location != null && mEqTpOrder.eqtpsoOpeningLongitude != "0" && mEqTpOrder.eqtpsoOpeningLongitude != "0")
                                {
                                    mEqTpSoModel.eqtpsoOpeningLongitude = mEqTpOrder.eqtpsoOpeningLongitude;
                                    mEqTpSoModel.eqtpsoOpeningLatitude = mEqTpOrder.eqtpsoOpeningLatitude;
                                }
                                else
                                {
                                    if (Common.HasInternetConnection())
                                    {
                                        var locator = CrossGeolocator.Current;
                                        if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                                        {
                                            locator.DesiredAccuracy = 5;

                                            Device.BeginInvokeOnMainThread(async () =>
                                            {
                                                location = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(1000));
                                            });
                                        }
                                        if (location != null && location.Longitude != 0 && location.Latitude != 0)
                                        {
                                            mEqTpSoModel.eqtpsoOpeningLongitude = location.Longitude.ToString();
                                            mEqTpSoModel.eqtpsoOpeningLatitude = location.Latitude.ToString();
                                        }
                                        else
                                        {
                                            mEqTpSoModel.eqtpsoOpeningLongitude = "0";
                                            mEqTpSoModel.eqtpsoOpeningLatitude = "0";
                                        }
                                    }
                                    else
                                    {
                                        mEqTpSoModel.eqtpsoOpeningLongitude = "0";
                                        mEqTpSoModel.eqtpsoOpeningLatitude = "0";
                                    }
                                }

                                mEqTpSoModel.eqtpsoOpeningNotes = mEqTpOrder.eqtpsoOpeningNotes;
                                mEqTpSoModel.eqtpsoUserCode = mEqTpOrder.eqtpsoUserCode;
                                mEqTpSoModel.eqtpsoUserName = mEqTpOrder.eqtpsoUserName;
                                mEqTpSoModel.eqtpsoOpeningSignature = mEqTpOrder.eqtpsoOpeningSignature;

                                List<EqTpSoModel> eqTpSoModels = new List<EqTpSoModel>();
                                eqTpSoModels.Add(mEqTpSoModel);

                                if (Common.HasInternetConnection())
                                {
                                    var responseAPI = await flstShiftAPI.SaveOpenOrder(eqTpSoModels);
                                    if (responseAPI.IsSuccess)
                                    {
                                        if (responseAPI.totalRecordsCount > 0)
                                        {
                                            var upDateResponse = shiftRepository.UpdateInterfaceStatus(InterfaceStatus.Completado.ToString(), mEqTpOrder.EqTpSoId, true);
                                            mEqTpOrder = shiftRepository.GetStartShift();
                                        }
                                        Utilties.DisplayInfo.SuccessMessage(EndPointsMessage.OrderSuccess);
                                        Navigation.PopAsync();
                                    }
                                    else
                                    {
                                        Utilties.DisplayInfo.SuccessMessage(EndPointsMessage.OrderSuccessLocal);
                                        Navigation.PopAsync();
                                    }
                                }
                                else
                                {
                                    Utilties.DisplayInfo.SuccessMessage(EndPointsMessage.OrderSuccessLocal);
                                    Navigation.PopAsync();
                                }
                            }
                            else
                            {
                                Utilties.DisplayInfo.HideLoader();
                                Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.StartOrderError);
                            }
                            Utilties.DisplayInfo.HideLoader();
                        }
                    };
                    PopupNavigation.PushAsync(confirmationPopup);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnSave_Tapped/OrderPage" + ex.Message);
            }
        }

        private void BtnTrailerCode_Tapped(object sender, EventArgs e)
        {
            NavigateToTrailerSelection();
        }

        private void ImgTrailerCode_Clicked(object sender, EventArgs e)
        {
            NavigateToTrailerSelection();
        }

        private void ImgDriverCode_Clicked(object sender, EventArgs e)
        {
            NavigateToDriverSelection();
        }

        private void BtnDriverCode_Tapped(object sender, EventArgs e)
        {
            NavigateToDriverSelection();
        }

        private void ImgLocation_Clicked(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private void BtnLocation_Tapped(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private void ImgHaulerCode_Clicked(object sender, EventArgs e)
        {
            NavigateToHaulerSelection();
        }

        private void BtnHaulerCode_Tapped(object sender, EventArgs e)
        {
            NavigateToHaulerSelection();
        }
        #endregion
    }
}