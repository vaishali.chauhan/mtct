﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Plugin.Geolocator;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Services;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewOrderPage : ContentPage
    {
        #region Objects
        ShiftRepository shiftRepository;
        EqtpRepository eqtpRepository;
        Driver driver;
        EqTpTrailers eqTpTrailers;
        EqTpHaulers eqTpHaulers;
        Locations locations;
        Eqtp mEqtp;
        DateTime currentDt;
        string orederRefno;
        #endregion

        #region Constructor
        public ViewOrderPage(string orederRefno)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            this.orederRefno = orederRefno;
            this.currentDt = DateTime.Now;
            InitializeOject();
        }
        #endregion

        #region Methods
        public void InitializeOject()
        {
            try
            {
                shiftRepository = new ShiftRepository();
                eqtpRepository = new EqtpRepository();

                driver = new Driver();
                locations = new Locations();
                mEqtp = new Eqtp();
                eqTpTrailers = new EqTpTrailers();
                eqTpHaulers = new EqTpHaulers();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("InitializeOject/OrderPage" + ex.Message);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindEqTpSoLnData();
        }

        public void BindEqTpSoLnData()
        {
            try
            {
                lblRefno.Text = orederRefno;

                var mEqTpSo = shiftRepository.GetStartShiftByFlstshiftRefno(orederRefno);
                if (mEqTpSo != null)
                {
                    lblShiftCode.Text = "Transporte " + mEqTpSo.eqtpsoEqtpCode;
                    lblHaulerCode.Text = mEqTpSo.eqtpsoEqtphaulerCode;
                    lblTrailerCode.Text = mEqTpSo.eqtpsoEqtptrailerCode;
                    lblDriverCode.Text = mEqTpSo.eqtpsoDriverCode;
                    lblDriverName.Text = mEqTpSo.eqtpsoDriverName;

                    if (mEqTpSo.eqtpsoOpeningOdometerImage != null)
                    {
                        var stream1 = LoadImage(mEqTpSo.eqtpsoOpeningOdometerImage);
                        imgPicture1.Aspect = Aspect.Fill;
                        imgPicture1.Source = stream1;
                    }

                    if (mEqTpSo.eqtpsoOpeningSignature != null)
                    {
                        var stream2 = LoadImage(mEqTpSo.eqtpsoOpeningSignature);
                        imgPicture2.Aspect = Aspect.Fill;
                        imgPicture2.Source = stream2;
                    }

                    if (string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningNotes))
                        txtNotes.Text = EndPointsMessage.Notes;
                    else
                        txtNotes.Text = mEqTpSo.eqtpsoOpeningNotes;

                    lblLocationCode.Text = mEqTpSo.eqtpsoOpeningLocation;
                    txtOdometer.Text = mEqTpSo.eqtpsoOpeningOdometer.ToString();

                    txtDateTime.Text = mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy");

                    if (string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningLongitude) && string.IsNullOrEmpty(mEqTpSo.eqtpsoOpeningLatitude))
                        BindMap(0, 0);
                    else
                    {
                        var lat = mEqTpSo.eqtpsoOpeningLatitude.Replace(".", ",").Replace("-", "/");
                        var lng = mEqTpSo.eqtpsoOpeningLongitude.Replace(".", ",").Replace("-", "/");

                        double Latitude = 0;
                        double Longitude = 0;

                        double.TryParse(lat, out Latitude);
                        double.TryParse(lng, out Longitude);

                        BindMap(Latitude, Longitude);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public ImageSource LoadImage(string base64string)
        {
            byte[] bytes = Convert.FromBase64String(base64string);
            ImageSource Source = "iconGrayNoImage.png";

            using (MemoryStream ms = new MemoryStream(bytes))
            {
                Source = ImageSource.FromStream(() => new MemoryStream(bytes));
            }
            return Source;
        }

        public void BindMap(double Latitude, double Longitude)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    if (locator != null && locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        locator.DesiredAccuracy = 5;
                        if (Latitude > 0 && Longitude > 0)
                        {

                        }
                        else
                        {
                            var location = await locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(1000));
                            Latitude = location.Latitude;
                            Longitude = location.Longitude;
                        }

                        if (Common.HasInternetConnection())
                        {
                            try
                            {
                                var mAddress = await Geocoding.GetPlacemarksAsync(Latitude, Longitude);
                                if (mAddress != null && mAddress.Count() > 0)
                                {
                                    string addressLabel = string.Empty;
                                    foreach (var add in mAddress)
                                    {
                                        addressLabel += add.FeatureName + " ";
                                    }
                                    var pin = new Pin
                                    {
                                        Type = PinType.Place,
                                        Label = addressLabel,
                                        Position = new Position(Latitude, Longitude)
                                    };

                                    xfmap.Pins.Add(pin);
                                    xfmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(Latitude, Longitude), Distance.FromMeters(150.0)));
                                }
                            }
                            catch (Exception ex)
                            {
                                Utilties.DisplayInfo.ErrorMessage("El proceso no pudo obtener la ubicación actual.");
                            }
                        }


                    }
                    else
                    {
                        var myAction = await DisplayAlert("Ubicación", "Activa la ubicación", "Okay", "Cancelar");
                        if (myAction)
                        {
                            if (Device.RuntimePlatform == global::Xamarin.Forms.Device.Android)
                            {
                                global::Xamarin.Forms.DependencyService.Get<ILocSettings>().OpenSettings();
                            }
                            else
                            {
                                await DisplayAlert("Dispositivo", "Estas usando alguna otra mierda", "SÍ");
                            }
                        }
                        else
                        {
                            //isLocation = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utilties.DisplayInfo.ErrorMessage("El proceso no pudo obtener la ubicación actual.");
                }
            });
        }

        //public void BindDrivers()
        //{
        //    try
        //    {
        //        mDrivers = driverRepository.GetDrivers();
        //        if (mDrivers != null && mDrivers.Count == 1)
        //        {
        //            lblDriverCode.Text = mDrivers.Select(x => x.DriverCode).FirstOrDefault();
        //            lblDriverName.Text = mDrivers.Select(x => x.DriverName).FirstOrDefault();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilties.DisplayInfo.ErrorMessage("BindDrivers/OrderPage" + ex.Message);
        //    }
        //}

        //public void BindHaulers()
        //{
        //    try
        //    {
        //        var mHaulers = eqptHaulerRepository.GetEqTpHaulers();
        //        if (mHaulers != null && mHaulers.Count == 1)
        //        {
        //            eqTpHaulers = mHaulers.FirstOrDefault();
        //            lblHaulerCode.Text = mHaulers.Select(x => x.EqtphaulerCode).FirstOrDefault();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilties.DisplayInfo.ErrorMessage("BindHaulers/OrderPage" + ex.Message);
        //    }
        //}

        public async void NavigateToDriverSelection()
        {
            try
            {
                var driverSearchPopup = new PopUp.SearchDriverNamePopup();
                driverSearchPopup.RefreshEvent += (s1, e1) =>
                {
                    driver = (Driver)s1;
                    if (driver != null && !string.IsNullOrEmpty(driver.DriverName))
                    {
                        lblDriverCode.Text = driver.DriverCode;
                        lblDriverCode.TextColor = Color.FromHex("#000000");
                        lblDriverName.Text = driver.DriverName;
                        lblDriverName.TextColor = Color.FromHex("#000000");
                    }
                };
                await PopupNavigation.PushAsync(driverSearchPopup, false);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnDriverCode_Tapped/OrderPage" + ex.Message);
            }
        }

        public async void NavigateToHaulerSelection()
        {
            try
            {
                var haulerPopup = new PopUp.HaulersPopup();
                haulerPopup.RefreshEvent += (s1, e1) =>
                {
                    eqTpHaulers = (EqTpHaulers)s1;
                    if (eqTpHaulers != null && !string.IsNullOrEmpty(eqTpHaulers.EqtphaulerCode))
                    {
                        lblHaulerCode.Text = eqTpHaulers.EqtphaulerCode;
                        lblHaulerCode.TextColor = Color.FromHex("#000000");
                        lblTrailerCode.Text = "Cola";
                    }
                };
                await PopupNavigation.PushAsync(haulerPopup, false);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToHaulerSelection/OrderPage" + ex.Message);
            }
        }

        public async void NavigateToTrailerSelection()
        {
            try
            {
                if (!string.IsNullOrEmpty(lblHaulerCode.Text) && lblHaulerCode.Text != "Cabezote")
                {
                    var trailerPopup = new PopUp.TrailerPopup(eqTpHaulers.EqtphaulerId);
                    trailerPopup.RefreshEvent += (s1, e1) =>
                    {
                        eqTpTrailers = (EqTpTrailers)s1;
                        if (eqTpTrailers != null && !string.IsNullOrEmpty(eqTpTrailers.EqtptrailerCode))
                        {
                            lblTrailerCode.Text = eqTpTrailers.EqtptrailerCode;
                            lblTrailerCode.TextColor = Color.FromHex("#000000");
                        }
                    };
                    await PopupNavigation.PushAsync(trailerPopup, false);
                }
                else
                {
                    Utilties.DisplayInfo.ErrorMessage("Primero seleccione transportista.");
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToTrailerSelection/OrderPage" + ex.Message);
            }
        }

        public async void NavigateToLoctionSelection()
        {
            try
            {
                var locationPopup = new PopUp.LocationPopup();
                locationPopup.RefreshEvent += (s1, e1) =>
                {
                    locations = (Locations)s1;
                    if (locations != null && !string.IsNullOrEmpty(locations.EqtplocationCode))
                    {
                        lblLocationCode.Text = locations.EqtplocationCode;
                        lblLocationCode.TextColor = Color.FromHex("#000000");
                    }
                };
                await PopupNavigation.PushAsync(locationPopup, false);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("NavigateToLoctionSelection/OrderPage" + ex.Message);
            }
        }

        public byte[] fileToByte(MediaFile file)
        {
            byte[] ImageBytes = null;
            try
            {
                var memoryStream = new MemoryStream();
                file.GetStream().CopyTo(memoryStream);
                ImageBytes = memoryStream.ToArray();
                ImageBytes = CompressImage(ImageBytes);
                memoryStream.Dispose();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("fileToByte/OrderPage" + ex.Message);
            }
            return ImageBytes;
        }

        public byte[] CompressImage(byte[] imgBytes)
        {

            byte[] CompressimageBytes = null;
            try
            {
                var image = SKImage.FromEncodedData(imgBytes);
                var data = image.Encode(SKEncodedImageFormat.Jpeg, 30);
                CompressimageBytes = data.ToArray();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("CompressImage/OrderPage" + ex.Message);
            }
            return CompressimageBytes;
        }

        private byte[] GetImageStreamAsBytes(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        #endregion

        #region Events
        private async void BtnBack_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private void BtnTrailerCode_Tapped(object sender, EventArgs e)
        {
            NavigateToTrailerSelection();
        }

        private void ImgTrailerCode_Clicked(object sender, EventArgs e)
        {
            NavigateToTrailerSelection();
        }

        private void ImgDriverCode_Clicked(object sender, EventArgs e)
        {
            NavigateToDriverSelection();
        }

        private void BtnDriverCode_Tapped(object sender, EventArgs e)
        {
            NavigateToDriverSelection();
        }

        private void ImgLocation_Clicked(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private void BtnLocation_Tapped(object sender, EventArgs e)
        {
            NavigateToLoctionSelection();
        }

        private void ImgHaulerCode_Clicked(object sender, EventArgs e)
        {
            NavigateToHaulerSelection();
        }

        private void BtnHaulerCode_Tapped(object sender, EventArgs e)
        {
            NavigateToHaulerSelection();
        }
        #endregion
    }
}