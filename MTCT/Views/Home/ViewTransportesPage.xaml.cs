﻿using Acr.UserDialogs;
using MTCT.API;
using MTCT.Interface;
using MTCT.Models;
using MTCT.Models.APIModels;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using MTCT.Views.PopUp;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZPLForge;
using ZPLForge.Common;

namespace MTCT.Views.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewTransportesPage : ContentPage
    {
        #region Objects
        EqTpSoLnRepository eqTpSoLnRepository;
        ShiftRepository shiftRepository;
        EqtpRepository eqtpRepository;

        Eqtp mEqtpData;
        EqTpSo mEqTpSo;
        EqTpSoLnLocal mEqTpSoLn;
        List<EqTpSoLnLocal> mEqTpSoLns;
        List<BluetoothList> bluetoothList;
        EqTpSoLnAPI eqTpSoLnAPI;
        string orederRefno;
        string printTitle = string.Empty;
        bool IsClick = false;
        #endregion

        #region Constructor
        public ViewTransportesPage(string orederRefno)
        {
            InitializeComponent();
            InitializeObjects();
            this.orederRefno = orederRefno;
        }

        #endregion

        #region Methods
        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindEqTpSoLnData();
            IsClick = false;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            IsClick = true;
        }

        public void InitializeObjects()
        {
            mEqtpData = new Eqtp();
            eqTpSoLnRepository = new EqTpSoLnRepository();
            shiftRepository = new ShiftRepository();
            eqtpRepository = new EqtpRepository();
            mEqTpSo = new EqTpSo();
            mEqTpSoLns = new List<EqTpSoLnLocal>();
            bluetoothList = new List<BluetoothList>();
            eqTpSoLnAPI = new EqTpSoLnAPI();
        }

        public void BindEqTpSoLnData()
        {
            try
            {
                Utilties.DisplayInfo.ShowLoader();
                mEqTpSo = shiftRepository.GetStartShiftByFlstshiftRefno(orederRefno);
                if (mEqTpSo != null)
                {
                    lblHeader.Text = "Transporte " + mEqTpSo.eqtpsoEqtpCode;
                    mEqTpSoLns = eqTpSoLnRepository.GetAllIssuesByShift(mEqTpSo.eqtpsoRefno);
                    if (mEqTpSoLns != null && mEqTpSoLns.Count > 0)
                    {
                        TransportReport.IsVisible = true;
                        lblRecord.IsVisible = false;
                        foreach (var item in mEqTpSoLns)
                        {
                            if (item.eqtpsolnStatus == EqTpSoLnStatus.Anulado.ToString())
                            {
                                item.IsVoided = false;
                                item.IsVoidImage = "iconNonGray.png";
                            }
                            else
                            {
                                item.IsVoided = true;
                                item.IsVoidImage = "iconNon.png";
                            }

                            if (mEqTpSo.eqtpsoStatus == Common.CloseStatus)
                            {
                                item.IsVoided = false;
                                item.IsVoidImage = "iconNonGray.png";
                            }

                            string mRefNo = mEqTpSo.eqtpsoRefno;
                            if (item.eqtpsolnEqtpsoRefno == mRefNo)
                            {
                                TransportReport.ItemsSource = mEqTpSoLns.ToList();
                            }
                        }
                    }
                    else
                    {
                        TransportReport.IsVisible = false;
                        lblRecord.IsVisible = true;
                    }
                    Utilties.DisplayInfo.HideLoader();
                }
                else
                    lblRecord.IsVisible = true;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindData/ViewTransportesPage" + ex.Message);
            }
            finally
            {
                Utilties.DisplayInfo.HideLoader();
            }
        }

        public async Task ConnectBluetooth(EqTpSoLnLocal mEqTpSoLn)
        {
            try
            {
                Utilties.DisplayInfo.ShowLoader();
                bluetoothList = await DependencyService.Get<IPrintReceipt>().BluetoothList();
                Utilties.DisplayInfo.HideLoader();
                if (bluetoothList.Count > 0)
                {
                    PrintTranportRecipt(mEqTpSoLn);
                }
                else
                {
                    await DisplayAlert("", "Not able to print", "Ok");
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ConnectBluetooth/ViewShiftPage" + ex.Message);
            }
        }

        public async void PrintTranportRecipt(EqTpSoLnLocal mEqTpSoLn)
        {
            try
            {
                mEqtpData = eqtpRepository.GetEqTp();
                if (Common.mAppkey == null || Common.mAppkey.Id == 0)
                {
                    if (bluetoothList.Count == 1)
                    {
                        AppKey mAppkey = new AppKey();
                        mAppkey.Id = 1;
                        mAppkey.KeyName = bluetoothList[0].Name;
                        mAppkey.KeyAddress = bluetoothList[0].Address;
                        Common.mAppkey = mAppkey;
                        if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                        {
                            ZebraIssuePrint(mEqTpSoLn, Common.mAppkey);
                        }
                        else
                        {
                            PrintTransportFormat(mEqTpSoLn, Common.mAppkey);
                        }
                    }
                    else
                    {
                        AppKey mAppkey = new AppKey();
                        BluetoothPopup bluetoothPopup = new BluetoothPopup();
                        bluetoothPopup.RefreshEvent += async (s, e1) =>
                        {
                            var bluetooth = (BluetoothList)s;
                            if (bluetooth != null)
                            {
                                mAppkey.Id = 1;
                                mAppkey.KeyName = bluetooth.Name;
                                mAppkey.KeyAddress = bluetooth.Address;
                                Common.mAppkey = mAppkey;
                                if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                                {
                                    ZebraIssuePrint(mEqTpSoLn, Common.mAppkey);
                                }
                                else
                                {
                                    PrintTransportFormat(mEqTpSoLn, Common.mAppkey);
                                }
                            }
                        };
                        await PopupNavigation.PushAsync(bluetoothPopup, false);
                    }
                }
                else
                {
                    if (mEqtpData != null && mEqtpData.PrintFormat == "Format Zebra")
                    {
                        ZebraIssuePrint(mEqTpSoLn, Common.mAppkey);
                    }
                    else
                    {
                        PrintTransportFormat(mEqTpSoLn, Common.mAppkey);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("PrintTranportRecipt/ViewShiftPage" + ex.Message);
            }
        }

        public async Task<string> PrintTransportFormat(EqTpSoLnLocal mEqTpSoLn, AppKey mAppKey)
        {
            try
            {
                var mHaulerData = eqtpRepository.GetEqTp();

                EqptHaulerRepository eqptHaulerRepository = new EqptHaulerRepository();
                var mHauler = eqptHaulerRepository.GetHaulerByEqtphaulerCode(mEqTpSo.eqtpsoEqtphaulerCode);

                EqptTrailerRepository eqptTrailerRepository = new EqptTrailerRepository();
                var mTrailer = eqptTrailerRepository.GerTrailerByEqtptrailerCode(mEqTpSo.eqtpsoEqtptrailerCode);

                Acr.UserDialogs.UserDialogs.Instance.ShowLoading("Printing...");
                await Task.Delay(1 * 200);
                string nullString = "N/A";
                Receipt mReceipt = new Receipt();
                StringBuilder sbReceipt1 = new StringBuilder();
                StringBuilder sbReceipt2 = new StringBuilder();
                mReceipt.ReceiptString1 = new StringBuilder();
                mReceipt.ReceiptString2 = new StringBuilder();

                sbReceipt1.Append("\x1B\x21\x1");
                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("\x1b\x40");

                if (mHaulerData != null && mHaulerData.PrintFormat == "Format 1")
                {
                    sbReceipt1.Append("--------------------------------");
                    sbReceipt1.Append(System.Environment.NewLine);
                }

                sbReceipt1.Append("\x1B\x21\x1");
                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("Orden");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("--------------------------------");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                Common.barcode = mEqTpSoLn.eqtpsolnBarcode;
                //
                string key1 = "Orden:";
                string value1 = nullString;
                if (mEqTpSo.eqtpsoRefno != null)
                {
                    value1 = mEqTpSo.eqtpsoRefno + " ";
                }
                sbReceipt1.Append(key1 + GetRightSpace(key1) + value1);
                sbReceipt1.Append(System.Environment.NewLine);

                string key2 = "Fecha:";
                string value2 = nullString;
                if (mEqTpSo.eqtpsoOpeningDate != DateTime.MinValue)
                {
                    value2 = mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy HH:mm:ss tt");
                }
                sbReceipt1.Append(key2 + GetRightSpace(key2) + value2);
                sbReceipt1.Append(System.Environment.NewLine);

                string key23 = "Movil:";
                string value23 = nullString;
                if (mEqTpSo.eqtpsoDeviceCode != null)
                {
                    value23 = mEqTpSo.eqtpsoDeviceCode;
                }
                sbReceipt1.Append(key23 + GetRightSpace(key23) + value23);
                sbReceipt1.Append(System.Environment.NewLine);


                string key3 = "Usuario:";
                string value3 = nullString;
                if (mEqTpSo.eqtpsoUserCode != null)
                {
                    value3 = mEqTpSo.eqtpsoUserCode;
                }
                sbReceipt1.Append(key3 + GetRightSpace(key3) + value3);
                sbReceipt1.Append(System.Environment.NewLine);

                string key15 = "Cód.Seg.:";
                string value15 = nullString;
                if (mEqTpSoLn.eqtpsolnSecurityCode != null)
                {
                    value15 = mEqTpSoLn.eqtpsolnSecurityCode;
                }
                sbReceipt1.Append(key15);
                sbReceipt1.Append(System.Environment.NewLine);
                sbReceipt1.Append(value15);
                sbReceipt1.Append(System.Environment.NewLine);


                string key4 = "Cabezote:"; // Transporte => Cabezote
                string value4 = nullString;
                if (mEqTpSo.eqtpsoEqtphaulerCode != null)
                {
                    var DisplayHauler = ((mHauler != null && mHauler.Id > 0) ? mHauler.DisplayHauler : mEqTpSo.eqtpsoEqtphaulerCode);
                    value4 = DisplayHauler;
                }
                sbReceipt1.Append(key4 + GetRightSpace(key4) + value4);
                sbReceipt1.Append(System.Environment.NewLine);


                string key5 = "Cola:"; //Trailer => Cola
                string value5 = nullString;
                if (mEqTpSo.eqtpsoEqtptrailerCode != null)
                {
                    var DisplayTrailer = ((mTrailer != null && mTrailer.Id > 0) ? mTrailer.DisplayTrailer : mEqTpSo.eqtpsoEqtptrailerCode);
                    value5 = DisplayTrailer;
                }
                sbReceipt1.Append(key5 + GetRightSpace(key5) + value5);
                sbReceipt1.Append(System.Environment.NewLine);


                string key6 = "Chofer:";
                string value6 = nullString;
                if (mEqTpSo.eqtpsoDriverCode != null)
                {
                    value6 = mEqTpSo.eqtpsoDriverCode;
                }
                sbReceipt1.Append(key6 + GetRightSpace(key6) + value6);
                sbReceipt1.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1B\x21\x1");
                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("Transporte");//Transporte => Cabezote
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("--------------------------------");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);


                string key7 = "Refno.:";
                string value7 = nullString;
                if (mEqTpSoLn.eqtpsolnRefno != null)
                {
                    value7 = mEqTpSoLn.eqtpsolnRefno;
                }
                sbReceipt1.Append(key7 + GetRightSpace(key7) + value7);
                sbReceipt1.Append(System.Environment.NewLine);


                string key8 = "RFID:";
                string value8 = nullString;
                if (mEqTpSoLn.eqtpsolnEquipmentRfid != null)
                {
                    value8 = mEqTpSoLn.eqtpsolnEquipmentRfid;
                }
                sbReceipt1.Append(key8 + GetRightSpace(key8) + value8);
                sbReceipt1.Append(System.Environment.NewLine);


                string key9 = "Equipo:";
                string value9 = nullString;
                string value10 = nullString;
                if (mEqTpSoLn.eqtpsolnEquipmentCode != null)
                {
                    value9 = mEqTpSoLn.eqtpsolnEquipmentCode;
                }
                if (mEqTpSoLn.eqtpsolnEquipmentDesc != null)
                {
                    value10 = mEqTpSoLn.eqtpsolnEquipmentDesc;
                }
                sbReceipt1.Append(key9 + GetRightSpace(key9) + value9 + " " + value10);
                sbReceipt1.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1B\x21\x1");
                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("Cargo");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                string key11 = "Proyecto:";
                string value11 = nullString;
                string value12 = nullString;
                if (mEqTpSoLn.eqtpsolnProjectCode != null)
                {
                    value11 = mEqTpSoLn.eqtpsolnProjectCode;
                }
                if (mEqTpSoLn.eqtpsolnProjectDesc != null)
                {
                    value12 = mEqTpSoLn.eqtpsolnProjectDesc;
                }
                sbReceipt1.Append(key11 + GetRightSpace(key11) + value11 + " " + value12);
                sbReceipt1.Append(System.Environment.NewLine);

                string key13 = "Partida:";
                string value13 = nullString;
                string value14 = nullString;
                if (mEqTpSoLn.eqtpsolnProjactCode != null)
                {
                    value13 = mEqTpSoLn.eqtpsolnProjactCode;
                }
                if (mEqTpSoLn.eqtpsolnProjactDesc != null)
                {
                    value14 = mEqTpSoLn.eqtpsolnProjactDesc;
                }
                sbReceipt1.Append(key13 + GetRightSpace(key13) + value13 + " " + value14);
                sbReceipt1.Append(System.Environment.NewLine);


                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("Carga");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                string key16 = "Fecha:";

                string value16 = nullString;
                if (mEqTpSoLn.eqtpsolnLoadingDatetime.HasValue && mEqTpSoLn.eqtpsolnLoadingDatetime != DateTime.MinValue)
                {
                    value16 = mEqTpSoLn.eqtpsolnLoadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");
                }

                sbReceipt1.Append(key16 + GetRightSpace(key16) + value16);
                sbReceipt1.Append(System.Environment.NewLine);

                string key17 = "Localización:";
                string value17 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLocation))
                {
                    value17 = mEqTpSoLn.eqtpsolnLoadingLocation;
                }
                sbReceipt1.Append(key17 + GetRightSpace(key17) + value17);
                sbReceipt1.Append(System.Environment.NewLine);


                string key18 = "Kilometraje:";
                string value18 = nullString;
                if (mEqTpSoLn.eqtpsolnLoadingOdometer != null)
                {
                    value18 = mEqTpSoLn.eqtpsolnLoadingOdometer.ToString();
                }
                sbReceipt1.Append(key18 + GetRightSpace(key18) + value18);
                sbReceipt1.Append(System.Environment.NewLine);


                string key19 = "Long./Lat.:";
                string value19 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLongitude) && !string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingLatitude))
                {
                    value19 = mEqTpSoLn.eqtpsolnLoadingLongitude + "/" + mEqTpSoLn.eqtpsolnLoadingLatitude;
                }
                sbReceipt1.Append(key19 + GetRightSpace(key19) + value19);
                sbReceipt1.Append(System.Environment.NewLine);


                string key20 = "Notas:";
                string value20 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnLoadingNotes))
                {
                    value20 = mEqTpSoLn.eqtpsolnLoadingNotes;
                }
                sbReceipt1.Append(key20 + GetRightSpace(key20) + value20);
                sbReceipt1.Append(System.Environment.NewLine);

                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLocation)
                    && (mEqTpSoLn.eqtpsolnUnloadingDatetime != null && mEqTpSoLn.eqtpsolnUnloadingDatetime != DateTime.MinValue))
                {
                    sbReceipt1.Append("\x1b\x45\x01");
                    sbReceipt1.Append("Descarga");
                    sbReceipt1.Append("\x1b\x45\x00");
                    sbReceipt1.Append(System.Environment.NewLine);

                    string key26 = "Fecha:";
                    string value26 = nullString;
                    if (mEqTpSoLn.eqtpsolnUnloadingDatetime != null
                        && mEqTpSoLn.eqtpsolnUnloadingDatetime.HasValue
                        && mEqTpSoLn.eqtpsolnUnloadingDatetime != DateTime.MinValue)
                    {
                        value26 = mEqTpSoLn.eqtpsolnUnloadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");
                    }
                    sbReceipt1.Append(key26 + GetRightSpace(key26) + value26);
                    sbReceipt1.Append(System.Environment.NewLine);

                    string key27 = "Localización:";
                    string value27 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLocation))
                    {
                        value27 = mEqTpSoLn.eqtpsolnUnloadingLocation;
                    }
                    sbReceipt1.Append(key27 + GetRightSpace(key27) + value27);
                    sbReceipt1.Append(System.Environment.NewLine);


                    string key28 = "Kilometraje:";
                    string value28 = nullString;
                    if (mEqTpSoLn.eqtpsolnUnloadingOdometer != null)
                    {
                        value28 = mEqTpSoLn.eqtpsolnUnloadingOdometer.ToString();
                    }
                    sbReceipt1.Append(key28 + GetRightSpace(key28) + value28);
                    sbReceipt1.Append(System.Environment.NewLine);


                    string key29 = "Long./Lat.:";
                    string value29 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLongitude) && !string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLatitude))
                    {
                        value29 = mEqTpSoLn.eqtpsolnUnloadingLongitude + "/" + mEqTpSoLn.eqtpsolnUnloadingLatitude;
                    }
                    sbReceipt1.Append(key29 + GetRightSpace(key29) + value29);
                    sbReceipt1.Append(System.Environment.NewLine);


                    string key30 = "Notas:";
                    string value30 = nullString;
                    if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingNotes))
                    {
                        value30 = mEqTpSoLn.eqtpsolnUnloadingNotes;
                    }
                    sbReceipt1.Append(key30 + GetRightSpace(key30) + value30);
                    sbReceipt1.Append(System.Environment.NewLine);
                }

                sbReceipt1.Append("\x1b\x45\x01");
                sbReceipt1.Append("--------------------------------");
                sbReceipt1.Append("\x1b\x45\x00");
                sbReceipt1.Append(System.Environment.NewLine);

                string key21 = "Estatus:";
                string value21 = nullString;
                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnStatus))
                {
                    value21 = mEqTpSoLn.eqtpsolnStatus;
                }
                sbReceipt1.Append(key21 + GetRightSpace(key21) + value21);
                sbReceipt1.Append(System.Environment.NewLine);
                sbReceipt1.Append(System.Environment.NewLine);

                mReceipt.ReceiptString1 = sbReceipt1;

                if (mHaulerData != null && mHaulerData.PrintFormat == "Format 1")
                {
                    // Barcode
                    sbReceipt2.Append("\x1b\x61\x01");
                    sbReceipt2.Append("\x1b\x40");
                    string value22 = nullString;
                    if (mEqTpSoLn.eqtpsolnBarcode != null)
                    {
                        value22 = mEqTpSoLn.eqtpsolnBarcode;
                        sbReceipt2.Append("\x1b\x61\x01" + value22);
                    }
                    sbReceipt2.Append(System.Environment.NewLine);
                }

                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1B\x21\x1");
                string key24 = "---------------";
                string value24 = "---------------";
                sbReceipt2.Append("\x1B\x21\x1" + key24 + GetSpace(key24, value24) + value24);
                sbReceipt2.Append(System.Environment.NewLine);

                sbReceipt1.Append("\x1B\x21\x1");
                string key25 = "  Transportista  ";
                string value25 = "     Supervisor     ";
                sbReceipt2.Append("\x1B\x21\x1" + key25 + GetSpace(key25, value25) + value25);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);
                sbReceipt2.Append(System.Environment.NewLine);

                mReceipt.ReceiptString2 = sbReceipt2;

                var jsonString = await DependencyService.Get<IPrintReceipt>().PrintReceiptMethod(mAppKey.KeyAddress, mReceipt, mEqTpSoLn.eqtpsolnBarcode);
                Acr.UserDialogs.UserDialogs.Instance.HideLoading();
                if (jsonString)
                {
                    await DisplayAlert("", "Impresión completada", "Ok");
                    return "True";
                }
                else
                {
                    await DisplayAlert("", "No se pudo imprimir", "Ok");
                    return "False";
                }
            }
            catch (Exception ex)
            {
                return "False";
            }
        }

        public async void ZebraIssuePrint(EqTpSoLnLocal mEqTpSoLn, AppKey mAppKey)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Printing...");
                Xamarin.Forms.DependencyService.Get<IBluetooth>().ConnectBluetooth(mAppKey.KeyName, mAppKey.KeyAddress);

                await Task.Run(async () =>
                {
                    var zpl = await ZebraLabelBuilder(mEqTpSoLn);
                    var result = await Xamarin.Forms.DependencyService.Get<IBluetooth>().Print(zpl);
                    await Task.Delay(3000);
                    Xamarin.Forms.DependencyService.Get<IBluetooth>().DisconnectBluetooth();
                });
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ViewShiftPage/ZebraIssuePrint: " + ex.Message);
            }
            finally { UserDialogs.Instance.HideLoading(); }
        }

        public async Task<byte[]> ZebraLabelBuilder(EqTpSoLnLocal mEqTpSoLn)
        {
            if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLocation))
            {
                printTitle = "Orden de Trabajo";
            }
            else
            {
                printTitle = "Orden de Transporte";
            }
            string zpl = string.Empty;
            try
            {
                string base64Image = LogoBase64String.LogoBase64;
                byte[] imageBytes = Convert.FromBase64String(base64Image);
                ZPLImage zplimg = Xamarin.Forms.DependencyService.Get<IBluetooth>().GetZPLImageCommand(imageBytes);

                string dt = "";
                if (mEqTpSoLn.eqtpsolnLoadingDatetime.HasValue && mEqTpSoLn.eqtpsolnLoadingDatetime.Value != DateTime.MinValue)
                    dt = mEqTpSoLn.eqtpsolnLoadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");

                string dt1 = "";
                if (mEqTpSoLn.eqtpsolnUnloadingDatetime.HasValue && mEqTpSoLn.eqtpsolnUnloadingDatetime.Value != DateTime.MinValue)
                    dt1 = mEqTpSoLn.eqtpsolnUnloadingDatetime.Value.ToString("dd/MM/yyyy HH:mm:ss tt");

                EqptHaulerRepository eqptHaulerRepository = new EqptHaulerRepository();
                var mHauler = eqptHaulerRepository.GetHaulerByEqtphaulerCode(mEqTpSo.eqtpsoEqtphaulerCode);
                var DisplayHauler = ((mHauler != null && mHauler.Id > 0) ? mHauler.DisplayHauler : mEqTpSo.eqtpsoEqtphaulerCode);

                EqptTrailerRepository eqptTrailerRepository = new EqptTrailerRepository();
                var mTrailer = eqptTrailerRepository.GerTrailerByEqtptrailerCode(mEqTpSo.eqtpsoEqtptrailerCode);
                var DisplayTrailer = ((mTrailer != null && mTrailer.Id > 0) ? mTrailer.DisplayTrailer : mEqTpSo.eqtpsoEqtptrailerCode);

                ZPLForge.Label label = new ZPLForge.Label
                {
                    Quantity = 1,
                    PrintWidth = 576,
                    MediaTracking = MediaTracking.ContinuousVariableLength,
                    MediaType = MediaType.ThermalDirect,
                    PrintMode = PrintMode.TearOff,
                    MediaDarknessLevel = 15,
                    Content =
                    {
                        new ImageElement
                    {
                        PositionX = 15,
                        PositionY = 70,
                        Compression = CompressionType.HexASCII,
                        BinaryByteCount = zplimg.ByteCount,
                        GraphicFieldCount = zplimg.ByteCount,
                        BytesPerRow = zplimg.BytesPerRow,
                        Content = zplimg.Data
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 180,
                        Content = "Alba Sanchez & Asociados",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 210,
                        Content = "RNC: 1-01-13267-1",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 240,
                        Content = printTitle,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 270,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 300,
                        Content = "Orden:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 300,
                        Content = mEqTpSo.eqtpsoRefno,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 330,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 330,
                        Content = mEqTpSo.eqtpsoOpeningDate != DateTime.MinValue ? mEqTpSo.eqtpsoOpeningDate.ToString("dd/MM/yyyy HH:mm:ss tt") : " ",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 360,
                        Content = "Movil:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 360,
                        Content = string.Format("{0:00}", Convert.ToInt32(mEqTpSo.eqtpsoDeviceCode)),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 390,
                        Content = "Usuario:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 390,
                        Content = mEqTpSo.eqtpsoUserCode + " " + mEqTpSo.eqtpsoUserName,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 420,
                        Content = "Cod.Seg.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 420,
                        Content = mEqTpSoLn.eqtpsolnSecurityCode,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 450,
                        Content = "Transportista:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 450,
                        Content = mEqtpData.EqtpCode,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 480,
                        Content = "Cabezote:", // Transporte => Cabezote
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },

                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 480,
                        Content = DisplayHauler,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 510,
                        Content = "Cola:", //Trailer => Cola
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 510,
                        Content = DisplayTrailer,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 540,
                        Content = "Chofer:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 540,
                        Content = mEqTpSo.eqtpsoDriverCode + " " + mEqTpSo.eqtpsoDriverName,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 570,
                        Content = "Transporte",//Transporte => Cabezote
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                    new TextElement
                    {
                        PositionX = 10,
                        PositionY = 600,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 630,
                        Content = "Refno.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 630,
                        Content = mEqTpSoLn.eqtpsolnRefno,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 660,
                        Content = "RFID:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 660,
                        Content = mEqTpSoLn.eqtpsolnEquipmentRfid,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 690,
                        Content = "Equipo:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 690,
                        Content = mEqTpSoLn.eqtpsolnEquipmentCode+" "+mEqTpSoLn.eqtpsolnEquipmentDesc,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 720,
                        Content = "Cargo",//Transporte => Cabezote
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 750,
                        Content = "Proyecto:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 750,
                        Content = mEqTpSoLn.eqtpsolnProjectCode + " " + mEqTpSoLn.eqtpsolnProjectDesc,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 780,
                        Content = "Partida:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 780,
                        Content = mEqTpSoLn.eqtpsolnProjactCode + " " + mEqTpSoLn.eqtpsolnProjactDesc,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 810,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 840,
                        Content = "Carga",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 870,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 870,
                        Content = dt,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 900,
                        Content = "Localización:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 900,
                        Content = mEqTpSoLn.eqtpsolnLoadingLocation,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 930,
                        Content = "Kilometraje:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 930,
                        Content = mEqTpSoLn.eqtpsolnLoadingOdometer.ToString(),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 960,
                        Content = "Long./Lat.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 960,
                        Content = mEqTpSoLn.eqtpsolnLoadingLongitude + "/" + mEqTpSoLn.eqtpsolnLoadingLatitude,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 10,
                        PositionY = 990,
                        Content = "Notas:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                        new TextElement
                    {
                        PositionX = 145,
                        PositionY = 990,
                        Content = mEqTpSoLn.eqtpsolnLoadingNotes,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    },
                    }
                };

                int posY = 1020;

                if (!string.IsNullOrEmpty(mEqTpSoLn.eqtpsolnUnloadingLocation) && (mEqTpSoLn.eqtpsolnUnloadingDatetime != null && mEqTpSoLn.eqtpsolnUnloadingDatetime != DateTime.MinValue))
                {
                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "----------------------------------------",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 15
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Descarga",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Fecha:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = dt1,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Localización:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = mEqTpSoLn.eqtpsolnUnloadingLocation,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Kilometraje:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = mEqTpSoLn.eqtpsolnUnloadingOdometer.ToString(),
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Long./Lat.:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = mEqTpSoLn.eqtpsolnUnloadingLongitude + "/" + mEqTpSoLn.eqtpsolnUnloadingLatitude,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;

                    label.Content.Add(new TextElement
                    {
                        PositionX = 10,
                        PositionY = posY,
                        Content = "Notas:",
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    label.Content.Add(new TextElement
                    {
                        PositionX = 145,
                        PositionY = posY,
                        Content = mEqTpSoLn.eqtpsolnUnloadingNotes,
                        FontStyle = ZPLForge.Common.Font.Default,
                        CharHeight = 25
                    });

                    posY += 30;
                }

                posY = posY + 30;

                label.Content.Add(new TextElement
                {
                    PositionX = 10,
                    PositionY = posY,
                    Content = "----------------------------------------",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 15
                });

                posY = posY + 30;
                label.Content.Add(new TextElement
                {
                    PositionX = 10,
                    PositionY = posY,
                    Content = "Estatus:",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 25
                });

                label.Content.Add(new TextElement
                {
                    PositionX = 145,
                    PositionY = posY,
                    Content = mEqTpSoLn.eqtpsolnStatus,
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 25
                });

                posY = posY + 100;

                label.Content.Add(new TextElement
                {
                    PositionX = 10,
                    PositionY = posY,
                    Content = "------------------",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 15
                });

                label.Content.Add(new TextElement
                {
                    PositionX = 310,
                    PositionY = posY,
                    Content = "------------------",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 15
                });

                posY += 30;

                label.Content.Add(new TextElement
                {
                    PositionX = 80,
                    PositionY = posY,
                    Content = "Transportista",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 25
                });


                label.Content.Add(new TextElement
                {
                    PositionX = 395,
                    PositionY = posY,
                    Content = "Supervisor",
                    FontStyle = ZPLForge.Common.Font.Default,
                    CharHeight = 25
                });

                posY += 30;

                label.Content.Add(new BarcodeElement
                {
                    BarcodeType = BarcodeType.Code128,
                    PositionX = 15,
                    PositionY = posY,
                    Height = 50,
                    Content = mEqTpSoLn.eqtpsolnBarcode
                });

                label.MediaLength = posY + 150;

                zpl = label.ToString().Insert(3, "^POI");
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                Utilties.DisplayInfo.ErrorMessage("ViewTransportesPage/ZebraLabelBuilder: " + ex.Message);
            }
            return Encoding.UTF8.GetBytes(zpl);
        }

        string GetSpace(string key, string value)
        {
            string space = "";
            int defaultCount = 42;
            int totalLength = 0;
            try
            {
                if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                    totalLength = key.Length + value.Length;
                else if (!string.IsNullOrEmpty(key))
                    totalLength = key.Length;
                else if (!string.IsNullOrEmpty(value))
                    totalLength = value.Length;

                for (int i = 0; i < (defaultCount - totalLength); i++)
                {
                    space += " ";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetSpace/ViewTransportesPage" + ex.Message);
            }
            return space;
        }

        string GetRightSpace(string value)
        {
            string space = "";
            try
            {
                int defaultCount = 14;
                int totalLength = (defaultCount - value.Length);
                for (int i = 0; i < totalLength; i++)
                {
                    space += " ";
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetRightSpace/ViewTransportesPage Exception: " + ex.Message);
            }
            return space;
        }
        #endregion

        #region Events
        private async void BtnBack_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private async void BtnTransport_Clicked(object sender, EventArgs e)
        {
            try
            {
                var obj = (ImageButton)sender;
                var selectedItem = obj.BindingContext as EqTpSoLnLocal;
                if (selectedItem != null)
                {

                    if (string.IsNullOrEmpty(selectedItem.eqtpsolnUnloadingLocation))
                    {
                        if (selectedItem.eqtpsolnStatus == EqTpSoLnStatus.Anulado.ToString())
                        {
                            Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.StatusVoid);
                        }
                        else
                        {
                            await Navigation.PushAsync(new Home.AddTransportPage(selectedItem), false);
                        }
                    }
                    else
                    {
                        DisplayAlert("", "Equipo ha sido descargado!!!", "Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnDetails_Clicked/ViewTransportesPage" + ex.Message);
            }
        }

        private async void BtnDetails_Clicked(object sender, EventArgs e)
        {
            try
            {
                var obj = (ImageButton)sender;
                var selectedItem = obj.BindingContext as EqTpSoLnLocal;
                if (selectedItem != null)
                {
                    await Navigation.PushAsync(new Home.AddTransportPage(selectedItem, true), false);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnDetails_Clicked/ViewTransportesPage" + ex.Message);
            }
        }

        private async void BtnPrint_Clicked(object sender, EventArgs e)
        {
            var obj = (ImageButton)sender;
            mEqTpSoLn = obj.BindingContext as EqTpSoLnLocal;
            if (mEqTpSoLn != null)
            {
                await ConnectBluetooth(mEqTpSoLn);
            }
        }

        private async void BtnVoid_Clicked(object sender, EventArgs e)
        {
            try
            {
                var obj = (ImageButton)sender;
                var selectedItem = obj.BindingContext as EqTpSoLnLocal;
                if (selectedItem != null)
                {
                    if (selectedItem.eqtpsolnStatus == EqTpSoLnStatus.Procesado.ToString())
                    {
                        PopUp.VoidPopup voidPopup = new PopUp.VoidPopup();
                        voidPopup.isVoidRefresh += async (s, e1) =>
                        {
                            var isConfirm = (bool)s;
                            if (isConfirm)
                            {
                                selectedItem.eqtpsolnStatus = EqTpSoLnStatus.Anulado.ToString();
                                var isUpdate = eqTpSoLnRepository.UpdateStatus(selectedItem);
                                if (isUpdate)
                                {
                                    EqTpSoLnModel eqTpSoLnModel = new EqTpSoLnModel();
                                    eqTpSoLnModel.eqtpsolnDeviceCode = selectedItem.eqtpsolnDeviceCode;
                                    eqTpSoLnModel.eqtpsolnEqtphaulerCode = selectedItem.eqtpsolnEqtphaulerCode;
                                    eqTpSoLnModel.eqtpsolnEqtptrailerCode = selectedItem.eqtpsolnEqtptrailerCode;
                                    eqTpSoLnModel.eqtpsolnEqtpsoRefno = selectedItem.eqtpsolnEqtpsoRefno;
                                    eqTpSoLnModel.eqtpsolnRefno = selectedItem.eqtpsolnRefno;
                                    eqTpSoLnModel.eqtpsolnBarcode = selectedItem.eqtpsolnBarcode;
                                    eqTpSoLnModel.eqtpsolnSecurityCode = selectedItem.eqtpsolnSecurityCode;
                                    eqTpSoLnModel.eqtpsolnEquipmentRfid = selectedItem.eqtpsolnEquipmentRfid;
                                    eqTpSoLnModel.eqtpsolnEquipmentCode = selectedItem.eqtpsolnEquipmentCode;
                                    eqTpSoLnModel.eqtpsolnEquipmentDesc = selectedItem.eqtpsolnEquipmentDesc;
                                    eqTpSoLnModel.eqtpsolnEquipmentLicensePlate = selectedItem.eqtpsolnEquipmentLicensePlate;
                                    eqTpSoLnModel.eqtpsolnProjectCode = selectedItem.eqtpsolnProjectCode;
                                    eqTpSoLnModel.eqtpsolnProjectDesc = selectedItem.eqtpsolnProjectDesc;
                                    eqTpSoLnModel.eqtpsolnProjactCode = selectedItem.eqtpsolnProjactCode;
                                    eqTpSoLnModel.eqtpsolnProjactDesc = selectedItem.eqtpsolnProjactDesc;
                                    eqTpSoLnModel.eqtpsolnLoadingDatetime = Convert.ToDateTime(selectedItem.eqtpsolnLoadingDatetime);
                                    eqTpSoLnModel.eqtpsolnLoadingLocation = selectedItem.eqtpsolnLoadingLocation;
                                    eqTpSoLnModel.eqtpsolnLoadingLongitude = selectedItem.eqtpsolnLoadingLongitude;
                                    eqTpSoLnModel.eqtpsolnLoadingLatitude = selectedItem.eqtpsolnLoadingLatitude;
                                    eqTpSoLnModel.eqtpsolnLoadingOdometer = selectedItem.eqtpsolnLoadingOdometer;
                                    eqTpSoLnModel.eqtpsolnLoadingOdometerImage = selectedItem.eqtpsolnLoadingOdometerImage;
                                    eqTpSoLnModel.eqtpsolnLoadingNotes = selectedItem.eqtpsolnLoadingNotes;
                                    eqTpSoLnModel.eqtpsolnLoadingSignature = selectedItem.eqtpsolnLoadingSignature;
                                    eqTpSoLnModel.eqtpsolnStatus = Common.ConvertSpainishToEnglish(EqTpSoLnStatus.Anulado.ToString());
                                    eqTpSoLnModel.eqtpsolnInterfaceStatus = Common.ConvertSpainishToEnglish(InterfaceStatus.Completado.ToString());
                                    eqTpSoLnModel.eqtpsolnInterfaceDate = selectedItem.eqtpsolnInterfaceDate;
                                    if (selectedItem.eqtpsolnUnloadingDatetime != null && selectedItem.eqtpsolnUnloadingDatetime != DateTime.MinValue)
                                    {
                                        eqTpSoLnModel.eqtpsolnUnloadingDatetime = Convert.ToDateTime(selectedItem.eqtpsolnUnloadingDatetime);
                                    }

                                    eqTpSoLnModel.eqtpsolnUnloadingLocation = selectedItem.eqtpsolnUnloadingLocation;
                                    eqTpSoLnModel.eqtpsolnUnloadingLongitude = selectedItem.eqtpsolnUnloadingLongitude;
                                    eqTpSoLnModel.eqtpsolnUnloadingLatitude = selectedItem.eqtpsolnUnloadingLatitude;
                                    eqTpSoLnModel.eqtpsolnUnloadingOdometer = selectedItem.eqtpsolnUnloadingOdometer;
                                    eqTpSoLnModel.eqtpsolnUnloadingOdometerImage = selectedItem.eqtpsolnUnloadingOdometerImage;
                                    eqTpSoLnModel.eqtpsolnUnloadingNotes = selectedItem.eqtpsolnUnloadingNotes;
                                    eqTpSoLnModel.eqtpsolnUnLoadingSignature = selectedItem.eqtpsolnUnLoadingSignature;

                                    List<EqTpSoLnModel> eqTpSoLnModels = new List<EqTpSoLnModel>();
                                    eqTpSoLnModels.Add(eqTpSoLnModel);

                                    if (Common.HasInternetConnection())
                                    {
                                        var responseAPIEqTpSoLn = await eqTpSoLnAPI.SaveEqTpSoLn(eqTpSoLnModels);
                                        var responseAPI1 = eqTpSoLnRepository.UpdateInterfaceStatus(InterfaceStatus.Completado.ToString(), selectedItem.EqTpSoLnId);
                                    }
                                }
                                else
                                {
                                    selectedItem.IsVoided = true;
                                    selectedItem.IsVoidImage = "iconNon.png";
                                }
                            }
                            else
                            {
                                selectedItem.IsVoided = true;
                                selectedItem.IsVoidImage = "iconNon.png";
                            }
                            BindEqTpSoLnData();
                        };
                        await PopupNavigation.PushAsync(voidPopup, false);
                    }
                    else
                    {
                        selectedItem.IsVoided = true;
                        selectedItem.IsVoidImage = "iconNonGray.png";
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnVoid_Clicked/ViewTransportesPage" + ex.Message);
            }
        }

        private async void BtnAdd_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (mEqTpSo != null && mEqTpSo.eqtpsoStatus == Common.OpenStatus.ToString())
                {
                    if (!IsClick)
                    {
                        IsClick = true;
                        var rfids = mEqTpSoLns?.Select(x => x.eqtpsolnEquipmentRfid);

                        await Navigation.PushAsync(new Home.AddTransportPage(mEqTpSo.eqtpsoOpeningDate, rfids), false);
                    }
                }
                else
                {
                    DisplayAlert("", "Turno cerrado. No se pueden registrar despachos", "Ok");
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void TransportReport_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            TransportReport.SelectedItem = null;
        }
        #endregion
    }
}