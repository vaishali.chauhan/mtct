﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.MasterPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        #region Object
        public string isPassShow = "iconShowPass.png";
        public string isPassHide = "iconHidePass.png";
        Eqtp mEqtp;
        User mUser = new User();
        public static string key { get; set; } = "A!9HHhi%XjjYY4YP2@Nob009X";
        #endregion

        #region Repository
        FuelStationRepository fuelStationRepository;
        UserRepository userRepository;
        EqptHaulerRepository eqptHaulerRepository;
        EqtpRepository eqtpRepository;
        #endregion

        public LoginPage()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            InitializeObject();
        }

        public void InitializeObject()
        {
            fuelStationRepository = new FuelStationRepository();
            userRepository = new UserRepository();
            eqptHaulerRepository = new EqptHaulerRepository();
            eqtpRepository = new EqtpRepository();

            mEqtp = new Eqtp();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetFuelStation();
        }

        public void GetFuelStation()
        {
            mEqtp = eqtpRepository.GetEqTp();
            if (mEqtp != null)
            {
                lblFuelStationName.Text = mEqtp.EqtpCode;
                Common.mEqtp = mEqtp;
            }
        }

        bool ValidateControls()
        {
            bool isValidate = false;
            if (string.IsNullOrEmpty(txtUser.Text) && string.IsNullOrEmpty(txtPassword.Text))
            {
                txtUser.PlaceholderColor = Color.Red;
                txtPassword.PlaceholderColor = Color.Red;
                DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
            }
            else if (string.IsNullOrEmpty(txtUser.Text))
            {
                txtUser.PlaceholderColor = Color.Red;
                DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
            }
            else if (string.IsNullOrEmpty(txtPassword.Text))
            {
                txtPassword.PlaceholderColor = Color.Red;
                DisplayInfo.ErrorMessage(EndPointsMessage.Validation);
            }
            else
                isValidate = true;
            return isValidate;
        }

        public static string Encrypt(string text)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }

        public static string Decrypt(string cipher)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateDecryptor())
                    {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }

        private async void btnLogin_Tapped(object sender, EventArgs e)
        {
            try
            {
                //var mUsers = userRepository.GetAllUser();
                if (ValidateControls())
                {
                    Utilties.DisplayInfo.ShowLoader();

                    if (mUser == null)
                        mUser = new User();


                    mUser.UserCode = txtUser.Text.Trim();
                    var mPassword = txtPassword.Text;
                    mUser.UserPassword = MD5Hash(mPassword); // 123456

                    mUser = userRepository.Login(mUser.UserCode, mUser.UserPassword);
                    if (mUser != null && mUser.UserId > 0)
                    {
                        Common.userRole = mUser.UserRole;
                        Common.mUser = mUser;
                      await  Navigation.PushAsync(new MasterPages.MasterPage(), false);
                    }
                    else
                    {
                        DisplayInfo.ErrorMessage(EndPointsMessage.LoginError);
                    }

                    Utilties.DisplayInfo.HideLoader();
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("LoginPage/Login_Click: " + ex.Message);
            }
            finally
            {
                Utilties.DisplayInfo.HideLoader();
            }
        }

        protected override bool OnBackButtonPressed()
        {
            try
            {
                base.OnBackButtonPressed();

                Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
                {
                    var result = await DisplayAlert("Alerta", "Esta seguro de salir?", "Si", "No");
                    if (result)
                    {
                        if (Device.OS == TargetPlatform.Android)
                        {
                            Xamarin.Forms.DependencyService.Get<ICloseAppOnBackButton>().CloseApp();
                        }
                    }
                });

                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
    }
}