﻿using MTCT.API;
using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.MasterPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterMenuPage : ContentPage
    {
        #region API
        EqptAPI eqptAPI;
        DriverAPI driverAPI;
        DeviceAPI deviceAPI;
        EqtpHaulerAPI eqtpHaulerAPI;
        LocationsAPI locationsAPI;
        ProjectAPI projectAPI;
        EquipmentAPI equipmentAPI;
        UserAPI userAPI;
        #endregion

        #region Repository
        DeviceRepository deviceRepository;
        DriverRepository driverRepository;
        EqptHaulerRepository eqptHaulerRepository;
        EqptTrailerRepository eqptTrailerRepository;
        EquipmentRepository equipmentRepository;
        ProjectRepository projectRepository;
        ProjectActivityRepository projectActivityRepository;
        LocationRepository locationRepository;
        UserRepository userRepository;
        EqtpRepository eqtpRepository;
        AppConfigRepository appConfigRepository;

        List<string> DeviceList;
        Flst mFuelStations;
        #endregion

        #region Constructor
        public MasterMenuPage()
        {
            try
            {
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
                Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
                InitializeComponent();
                InitializeObject();
                ClearMenuBackGround();
                ftrans.BackgroundColor = Color.FromHex("#004311");
                lblftrans.TextColor = Color.White;
                imgftrans.Source = "iconTransactionW.png";
                if (!string.IsNullOrEmpty(Common.userRole))
                {
                    if (Common.userRole.ToLower() == UserRole.Admin.ToString().ToLower())
                    {
                        btnAdmin.IsVisible = true;
                        bxAdmin.IsVisible = true;
                    }
                    else
                    {
                        btnAdmin.IsVisible = false;
                        bxAdmin.IsVisible = false;
                    }
                }

                if (Common.mUser != null && Common.mUser.UserId > 0)
                    lblUserName.Text = "Bienvenido " + Common.mUser.UserName;
                else
                    lblUserName.Text = "Bienvenido Guest";
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("Constructor/AddDevicePage Exception: " + ex.Message);
            }
        }
        #endregion

        #region Methods
        public void InitializeObject()
        {
            eqptAPI = new EqptAPI();
            driverAPI = new DriverAPI();
            deviceAPI = new DeviceAPI();
            equipmentAPI = new EquipmentAPI();
            eqtpHaulerAPI = new EqtpHaulerAPI();
            projectAPI = new ProjectAPI();
            userAPI = new UserAPI();
            locationsAPI = new LocationsAPI();

            deviceRepository = new DeviceRepository();
            eqptHaulerRepository = new EqptHaulerRepository();
            eqptTrailerRepository = new EqptTrailerRepository();
            driverRepository = new DriverRepository();
            equipmentRepository = new EquipmentRepository();
            userRepository = new UserRepository();
            appConfigRepository = new AppConfigRepository();
            projectRepository = new ProjectRepository();
            projectActivityRepository = new ProjectActivityRepository();
            eqtpRepository = new EqtpRepository();
            locationRepository = new LocationRepository();

            DeviceList = new List<string>();
            mFuelStations = new Flst();
        }

        public void ClearMenuBackGround()
        {
            ftrans.BackgroundColor = Color.Transparent;
            lblftrans.TextColor = Color.Black;
            imgftrans.Source = "iconTransaction.png";

            frmData.BackgroundColor = Color.Transparent;
            lbldata.TextColor = Color.Black;
            imgdata.Source = "iconTransaction.png";

            fsync.BackgroundColor = Color.Transparent;
            lblfsync.TextColor = Color.Black;
            imgfsync.Source = "iconSync.png";

            btnAdmin.BackgroundColor = Color.Transparent;
            lblfadmin.TextColor = Color.Black;
            imgfadmin.Source = "iconAdmin.png";

            frmConnection.BackgroundColor = Color.Transparent;
            lblConnection.TextColor = Color.Black;
            imgConnection.Source = "iconConnection.png";

            flogout.BackgroundColor = Color.Transparent;
            lblflogout.TextColor = Color.Black;
            imgflogout.Source = "iconLogout.png";
        }

        public async Task SaveEqtp()
        {
            bool isSuccess = true;
            try
            {
                var mEqtp = eqtpRepository.GetEqTp();
                if (mEqtp != null)
                {
                    var apiEqtp = await eqptAPI.GetEqtpByEqtpCode(mEqtp.EqtpCode);
                    if (apiEqtp != null)
                    {
                        var mDevice = deviceRepository.GetDevice();
                        if (mDevice != null)
                        {
                            string deviceCode = string.Empty;
                            var deviceValue = mDevice.AppdeviceAppRefCode.Split('-');
                            if (deviceValue != null && deviceValue.Length > 1)
                                deviceCode = deviceValue[1];
                            var AppRefCode = "MTCT-" + apiEqtp.eqtpCode;

                            //Haulers
                            var isHaulerSave = await SaveHaulers(AppRefCode);
                            if (!isHaulerSave)
                            {
                                isSuccess = false;
                                Utilties.DisplayInfo.ErrorMessage("Hauler not save.");
                            }


                            // User Table
                            var isUserSave = await SaveUser(AppRefCode);
                            if (!isUserSave)
                            {
                                isSuccess = false;
                                Utilties.DisplayInfo.ErrorMessage("User not save.");
                            }

                            // Driver Table
                            var isDriverSave = await SaveDriver(AppRefCode);
                            if (!isDriverSave)
                            {
                                isSuccess = false;
                                Utilties.DisplayInfo.ErrorMessage("Driver not save.");
                            }

                            //Equipment Table
                            var isEquipmentSave = await SaveEquipment(AppRefCode);
                            if (!isEquipmentSave)
                            {
                                isSuccess = false;
                                Utilties.DisplayInfo.ErrorMessage("Equipment not save.");
                            }

                            //Eqtp Location
                            var isLocationSave = await SaveLocations(AppRefCode);
                            if (!isLocationSave)
                            {
                                isSuccess = false;
                                Utilties.DisplayInfo.ErrorMessage("Location not save.");
                            }

                            // Projects and projectActivities
                            var isProjectSave = await SaveProject(AppRefCode);
                            if (!isProjectSave)
                            {
                                isSuccess = false;
                                Utilties.DisplayInfo.ErrorMessage("Project not save.");
                            }

                            if (isUserSave && isDriverSave && isEquipmentSave && isLocationSave && isProjectSave)
                            {
                                Utilties.DisplayInfo.SuccessMessage(EndPointsMessage.SyncronizationSuccess);
                                Common.masterDetail.Detail = new NavigationPage(new Views.Home.DashboardPage());
                                Common.masterDetail.IsPresented = false;
                            }
                            else
                                Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.SyncronizationError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> SaveHaulers(string AppRefCode)
        {
            bool isSave = false;
            try
            {
                var allEqtpHauler = await eqtpHaulerAPI.GetAllEqtpHaulers(AppRefCode);
                if (allEqtpHauler != null && allEqtpHauler.Count > 0)
                {
                    var mEqtpHaulers = eqptHaulerRepository.GetEqTpHaulers();
                    if (mEqtpHaulers != null && mEqtpHaulers.Count() > 0)
                    {
                        foreach (var mHauler in mEqtpHaulers)
                        {
                            var result = eqptHaulerRepository.DeleteEqTpHaulers(mHauler.Id);
                        }
                    }

                    var mEqTpTrailers = eqptTrailerRepository.GetEqTpTrailers();
                    if (mEqTpTrailers != null && mEqTpTrailers.Count() > 0)
                    {
                        foreach (var mTrailer in mEqTpTrailers)
                        {
                            var result = eqptTrailerRepository.DeleteEqTpTrailers(mTrailer.Id);
                        }
                    }

                    foreach (var eqtpHaulers in allEqtpHauler)
                    {
                        var apiEqtpHaulers = await eqtpHaulerAPI.GetEqtpHaulersByCode(eqtpHaulers.eqtphaulerCode);
                        if (apiEqtpHaulers != null && apiEqtpHaulers.EqTpHaulers != null && apiEqtpHaulers.EqTpHaulers.EqtphaulerId > 0)
                        {
                            if (apiEqtpHaulers.EqTpHaulers != null && apiEqtpHaulers.EqTpHaulers.EqtphaulerId > 0)
                            {
                                var result = eqptHaulerRepository.SaveEqTpHaulers(apiEqtpHaulers.EqTpHaulers);
                            }

                            if (apiEqtpHaulers.EqTpTrailers != null && apiEqtpHaulers.EqTpTrailers.Count > 0)
                            {
                                foreach (var eqTpTrailers in apiEqtpHaulers.EqTpTrailers.Where(x => x.EqtptrailerActiveStatus == true))
                                {
                                    var result = eqptTrailerRepository.SaveEqTpTrailers(eqTpTrailers);
                                }
                            }
                            isSave = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSave;
        }

        public async Task<bool> SaveUser(string AppdeviceAppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiUser = await userAPI.GetuserByAppRefCode(AppdeviceAppRefCode);
                if (apiUser != null)
                {
                    var mUsers = userRepository.GetAllUser();
                    if (mUsers != null && mUsers.Count() > 0)
                    {
                        foreach (var mUser in mUsers)
                        {
                            userRepository.DeleteUser(mUser.Id);
                        }
                    }

                    if (apiUser.mUserModels != null)
                    {
                        foreach (var mUserModel in apiUser.mUserModels.Where(x => x.appuserActiveStatus == true))
                        {
                            User mUser = new User()
                            {
                                UserId = mUserModel.appuserId,
                                UserName = mUserModel.appuserName,
                                UserEmail = mUserModel.appuserEmail,
                                UserPassword = mUserModel.appuserPassword,
                                UserRole = mUserModel.appuserRole,
                                UserCode = mUserModel.appuserCode,
                                appuserActiveStatus = mUserModel.appuserActiveStatus,
                                appuserAppRefCode = mUserModel.appuserAppRefCode,
                            };
                            var mUserInsert = userRepository.SaveUser(mUser);
                        }
                    }
                    isSave = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSave;
        }

        public async Task<bool> SaveDriver(string AppdeviceAppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiDriver = await driverAPI.GetDriverByAppRefCode(AppdeviceAppRefCode);
                if (apiDriver != null)
                {
                    var mDrivers = driverRepository.GetDrivers();
                    if (mDrivers != null && mDrivers.Count() > 0)
                    {
                        foreach (var mDriver in mDrivers)
                        {
                            driverRepository.DeleteDriver(mDriver.Id);
                        }
                    }

                    foreach (var mDriverModel in apiDriver.mDriverModels.Where(x => x.driverActiveStatus == true))
                    {
                        Driver mDriver = new Driver()
                        {
                            DriverId = mDriverModel.driverId,
                            DriverCode = mDriverModel.driverCode,
                            DriverName = mDriverModel.driverName,
                            DriverActiveStatus = mDriverModel.driverActiveStatus,
                            driverAppRefCode = mDriverModel.driverAppRefCode,
                        };
                        driverRepository.SaveDriver(mDriver);
                    }
                    isSave = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSave;
        }

        public async Task<bool> SaveEquipment(string AppdeviceAppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiEquipment = await equipmentAPI.GetEquipmentByRefCode(AppdeviceAppRefCode);
                if (apiEquipment != null)
                {
                    var mEquipments = equipmentRepository.GetEquipments();
                    if (mEquipments != null && mEquipments.Count() > 0)
                    {
                        foreach (var mEquipment in mEquipments)
                        {
                            equipmentRepository.DeleteEquipment(mEquipment.Id);
                        }
                    }
                }
                foreach (var mEquipmentModel in apiEquipment.mEquipmentModels.Where(x => x.equipmentActiveStatus == true))
                {
                    Equipment mEquipments = new Equipment()
                    {
                        EquipmentId = mEquipmentModel.equipmentId,
                        EquipmentCode = mEquipmentModel.equipmentCode,
                        EquipmentDesc = mEquipmentModel.equipmentDesc,
                        EquipmentLicensePlate = mEquipmentModel.equipmentLicensePlate,
                        EquipmentRFID = mEquipmentModel.equipmentRfid,
                        EquipmentFltankCapacity = mEquipmentModel.equipmentFltankCapacity,
                        EquipmentActiveStatus = mEquipmentModel.equipmentActiveStatus,
                        equipmentAppRefCode = mEquipmentModel.equipmentAppRefCode,
                    };
                    equipmentRepository.SaveEquipment(mEquipments);
                }
                isSave = true;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("SaveEquipment/AddDevicePage Exception: " + ex.Message);
            }
            return isSave;
        }

        public async Task<bool> SaveLocations(string eqTpLocationAppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiLocationDetail = await locationsAPI.GetEqtLocations(eqTpLocationAppRefCode);
                if (apiLocationDetail.IsSuccess)
                {
                    isSave = true;
                    if (apiLocationDetail != null
                    && apiLocationDetail.Locations.Count > 0)
                    {
                        isSave = true;
                        var mLocationss = locationRepository.GetLocationss();
                        if (mLocationss != null && mLocationss.Count() > 0)
                        {
                            foreach (var mLocations in mLocationss)
                            {
                                locationRepository.DeleteLocations(mLocations.Id);
                            }
                        }

                        foreach (var item in apiLocationDetail.Locations.Where(x => x.EqtplocationActiveStatus == true))
                        {
                            locationRepository.SaveLocations(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("SaveEquipment/AddDevicePage Exception: " + ex.Message);
            }
            return isSave;
        }

        public async Task<bool> SaveProject(string eqTpLocationAppRefCode)
        {
            bool isSave = false;
            try
            {
                var apiProjectDetails = await projectAPI.GetProjects(eqTpLocationAppRefCode);
                if (apiProjectDetails != null
                    && apiProjectDetails.ProjectDetail.Count > 0)
                {
                    var mProjects = projectRepository.GetProjects();
                    if (mProjects != null && mProjects.Count() > 0)
                    {
                        foreach (var mProject in mProjects)
                        {
                            projectRepository.DeleteProjects(mProject.Id);
                        }
                    }

                    var mProjectActivitys = projectActivityRepository.GetProjectActivity();
                    if (mProjectActivitys != null && mProjectActivitys.Count() > 0)
                    {
                        foreach (var mProjectActivity in mProjectActivitys)
                        {
                            projectActivityRepository.DeleteProjectActivity(mProjectActivity.Id);
                        }
                    }

                    foreach (var ProjectDetail in apiProjectDetails.ProjectDetail.Where(x => x.project.ProjectActiveStatus == true))
                    {
                        projectRepository.SaveProjects(ProjectDetail.project);
                        if (ProjectDetail.projAct != null)
                        {
                            foreach (var projectActivity in ProjectDetail.projAct.Where(x => x.projactActiveStatus == true))
                            {
                                projectActivityRepository.SaveProjectActivity(projectActivity);
                            }
                        }
                    }
                    isSave = true;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("SaveEquipment/AddDevicePage Exception: " + ex.Message);
            }
            return isSave;
        }

        public async Task GetDeviceList()
        {
            try
            {
                var deviceList = DependencyService.Get<IRfidScanning>().GetDevices();
                DeviceList.Clear();
                if (deviceList != null)
                {
                    foreach (var item in deviceList)
                    {
                        DeviceList.Add(item);
                    }
                    if (DeviceList.Count > 0)
                    {
                        string CurrentDevice = DeviceList.FirstOrDefault();
                        if (!string.IsNullOrEmpty(CurrentDevice))
                        {
                            await ConnectDevice(CurrentDevice);
                        }
                        else
                        {
                            await DisplayAlert("", "Ningún dispositivo encontrado", "OK");
                        }
                    }
                    else
                    {
                        await DisplayAlert("", "Ningún dispositivo encontrado", "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetDeviceList/MasterMenuPage" + ex.Message);
            }
        }

        public async Task ConnectDevice(string DeviceName)
        {
            try
            {
                if (!string.IsNullOrEmpty(DeviceName))
                {
                    bool status = DependencyService.Get<IRfidScanning>().ConnectBarcodeDevice(DeviceName);
                    if (status)
                    {
                        await DisplayAlert("", "Conectado", "OK");
                    }
                    else
                    {
                        await DisplayAlert("", "Sin conexión", "OK");
                    }
                }
                else
                {
                    await DisplayAlert("", "Ningún dispositivo encontrado", "OK");
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ConnectDevice/MasterMenuPage" + ex.Message);
            }
        }
        #endregion

        #region Events
        private void Trans_Click(object sender, EventArgs e)
        {
            ClearMenuBackGround();
            ftrans.BackgroundColor = Color.FromHex("#004311");
            lblftrans.TextColor = Color.White;
            imgftrans.Source = "iconTransactionW.png";
            Common.masterDetail.Detail = new NavigationPage(new Views.Home.DashboardPage());
            Common.masterDetail.IsPresented = false;
        }

        private async void Sync_Click(object sender, EventArgs e)
        {
            try
            {
                ClearMenuBackGround();
                fsync.BackgroundColor = Color.FromHex("#004311");
                lblfsync.TextColor = Color.White;
                imgfsync.Source = "iconSyncW.png";
                Common.masterDetail.IsPresented = false;
                var isClose = await DisplayAlert("", "Esta seguro de sincronizar los datos?", "Si", "No");
                if (isClose)
                {
                    await Task.Run(() => Utilties.DisplayInfo.ShowLoader());
                    await Task.Delay(1 * 500);
                    await SaveEqtp();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage(EndPointsMessage.Error);
            }
            finally
            {
                Utilties.DisplayInfo.HideLoader();
            }
        }

        private async void Logout_Click(object sender, EventArgs e)
        {
            ClearMenuBackGround();
            flogout.BackgroundColor = Color.FromHex("#004311");
            lblflogout.TextColor = Color.White;
            imgflogout.Source = "iconLogoutW.png";
            Common.masterDetail.IsPresented = false;
            var isClose = await DisplayAlert("", "Esta seguro de salir?", "Si", "No");
            if (isClose)
            {
                await Navigation.PushAsync(new MasterPages.LoginPage(), false);
            }
        }

        private void Configuration_Click(object sender, EventArgs e)
        {
            ClearMenuBackGround();
            btnAdmin.BackgroundColor = Color.FromHex("#004311");
            lblfadmin.TextColor = Color.White;
            imgfadmin.Source = "iconAdminW.png";
            Common.masterDetail.Detail = new NavigationPage(new Views.Home.ConfigurationPage());
            Common.masterDetail.IsPresented = false;
        }

        private void FrmData_Tapped(object sender, EventArgs e)
        {
            ClearMenuBackGround();
            frmData.BackgroundColor = Color.FromHex("#004311");
            lbldata.TextColor = Color.White;
            imgdata.Source = "iconTransactionW.png";
            Common.masterDetail.Detail = new NavigationPage(new Views.Tabbed.DataTabbedPage());
            Common.masterDetail.IsPresented = false;
        }

        private async void Connection_Click(object sender, EventArgs e)
        {
            ClearMenuBackGround();
            frmConnection.BackgroundColor = Color.FromHex("#004311");
            lblConnection.TextColor = Color.White;
            imgConnection.Source = "iconConnectionW.png";
            await GetDeviceList();
        }
        #endregion
    }
}