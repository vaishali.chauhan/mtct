﻿using MTCT.Utilties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.MasterPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : MasterDetailPage
    {
        public MasterPage()
        {
            try
            {
                InitializeComponent();
                Common.masterDetail = this;
                Common.masterDetail.Master = new MasterMenuPage();
                var DashboardPage = new Views.Home.DashboardPage();
                Common.masterDetail.Detail = new Xamarin.Forms.NavigationPage(DashboardPage);
                MasterBehavior = MasterBehavior.Popover;
                Common.masterDetail.IsGestureEnabled = true;
                Common.masterDetail.IsPresented = false;
            }
            catch (Exception ex)
            {

            }
        }
    }
}