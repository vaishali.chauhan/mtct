﻿using MTCT.DatabaseService;
using MTCT.Interface;
using MTCT.Models;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BluetoothPopup : PopupPage
    {
        DatabaseHelper databaseService = new DatabaseHelper();

        List<BluetoothList> bluetoothList = new List<BluetoothList>();
        BluetoothList bluetooth = new BluetoothList();
        public event EventHandler RefreshEvent;
        // private string title;
        //  private Color bgColor;

        public BluetoothPopup()
        {
            InitializeComponent();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            FillBluetooth();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
        }

        public async void FillBluetooth()
        {
            try
            {
                Utilties.DisplayInfo.ShowLoader();
                bluetoothList = await DependencyService.Get<IPrintReceipt>().BluetoothList();
                Utilties.DisplayInfo.HideLoader();
                if (bluetoothList.Count > 0)
                {
                    flvBlueTooth.IsVisible = true;
                    flvBlueTooth.ItemsSource = bluetoothList.ToList();
                    flvBlueTooth.HeightRequest = (bluetoothList.Count() * 50) + 10;
                    slNotFound.IsVisible = false;
                }
                else
                {
                    //PopupNavigation.PopAsync();
                    //BluetoothPermission();
                    flvBlueTooth.ItemsSource = bluetoothList.ToList();
                    flvBlueTooth.IsVisible = false;
                    slNotFound.IsVisible = true;

                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("FillBluetooth/BluetoothPopup" + ex.Message);
            }
        }

        private async void BtnCancel_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync(false);
        }

        private async void BtnDone_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (bluetooth != null)
                {
                    if (!string.IsNullOrEmpty(bluetooth.Address))
                    {
                        Common.Bluetoothaddress = bluetooth.Address;
                        RefreshEvent(bluetooth, EventArgs.Empty);
                        await PopupNavigation.PopAsync(false);
                    }
                    else
                    {
                        await PopupNavigation.PopAsync(false);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BtnDone_Clicked/BluetoothPopup" + ex.Message);
            }
        }

        private void FlvBlueTooth_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                bluetooth = e.Item as BluetoothList;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("FlvBlueTooth_ItemTapped/BluetoothPopup" + ex.Message);
            }
        }

        private void flvBlueTooth_ItemTapped_1(object sender, ItemTappedEventArgs e)
        {

        }
    }
}