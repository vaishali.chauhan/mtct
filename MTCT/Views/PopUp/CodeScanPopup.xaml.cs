﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CodeScanPopup : PopupPage
    {
        private ZXingScannerView zxing;
        private ZXingDefaultOverlay overlay;
        public EventHandler isRefresh;

        public CodeScanPopup()
        {
            try
            {
                InitializeComponent(); zxing = new ZXingScannerView
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };

                zxing.OnScanResult += (result) =>
                Device.BeginInvokeOnMainThread(async () =>
                {
                    zxing.IsAnalyzing = false;
                    await PopupNavigation.PopAsync();
                    isRefresh(result.Text, EventArgs.Empty);
                });

                overlay = new ZXingDefaultOverlay
                {
                    TopText = "Hold your phone up to the barcode",
                    BottomText = "Scanning will happen automatically",
                    ShowFlashButton = zxing.HasTorch,
                };
                overlay.FlashButtonClicked += (sender, e) =>
                {
                    zxing.IsTorchOn = !zxing.IsTorchOn;
                };
                var grid = new Grid
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };
                grid.Children.Add(zxing);
                grid.Children.Add(overlay);

                Content = grid;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("CodeScanPage/Constructor" + ex.Message);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            zxing.IsScanning = true;
        }

        protected override void OnDisappearing()
        {
            zxing.IsScanning = false;
            base.OnDisappearing();
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
        }
    }
}