﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfirmationPopup : PopupPage
    {
        bool isShow = false;
        public EventHandler isRefresh;
        bool isClick = false;

        public ConfirmationPopup(string title)
        {
            InitializeComponent();
            lblHeader.Text = title;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            isClick = false;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            isClick = true;
        }


        private async void imgCancel_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync(false);
        }

        private void btnYes_Tapped(object sender, EventArgs e)
        {
            imgYes.Source = "iconRadioFill.png";
            imgNo.Source = "iconRadioBlank.png";
        }

        private void btnNo_Tapped(object sender, EventArgs e)
        {
            imgNo.Source = "iconRadioFill.png";
            imgYes.Source = "iconRadioBlank.png";
        }

        private async void btnOk_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (!isClick)
                {
                    isClick = true;

                    if (imgYes.Source.ToString().Replace("File: ", "") == "iconRadioFill.png")
                    {
                        await PopupNavigation.PopAsync(false);
                        isRefresh(true, EventArgs.Empty);
                    }
                    else
                    {
                        await PopupNavigation.PopAsync(false);
                        isRefresh(false, EventArgs.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnOk_Tapped/ConfirmationPopup Exception: " + ex.Message);
            }
        }
    }
}