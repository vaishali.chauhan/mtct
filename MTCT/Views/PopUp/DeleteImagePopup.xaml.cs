﻿using MTCT.Interface;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeleteImagePopup : PopupPage
    {
        public EventHandler isRefresh;
        public DeleteImagePopup()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
        }

        private async void btnNo_Tapped(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync(false);
        }

        private async void btnYes_Tapped(object sender, EventArgs e)
        {
            try
            {
                await PopupNavigation.PopAsync(false);
                isRefresh(true, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnYes_Tapped/DeleteImagePopup Exception: " + ex.Message);
            }
        }
    }
}