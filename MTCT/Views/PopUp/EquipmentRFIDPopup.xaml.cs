﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EquipmentRFIDPopup : PopupPage
    {
        EquipmentRepository equipmentRepository;
        public event EventHandler RefreshEvent;
        public EquipmentRFIDPopup()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            InitializeObject();
        }

        public void InitializeObject()
        {
            equipmentRepository = new EquipmentRepository();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindEquipments();
        }

        public void BindEquipments()
        {
            try
            {
                var mEquipments = equipmentRepository.GetEquipments().OrderBy(x=>x.EquipmentCode);
                if (mEquipments != null)
                {
                    lstDetails.ItemsSource = mEquipments.ToList();
                    lstDetails.HeightRequest = mEquipments.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindEquipments/EquipmentRFIDPopup" + ex.Message);
            }
        }

        private async void lstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedEquipment = e.Item as Equipment;
                if (selectedEquipment != null)
                {
                    await PopupNavigation.PopAsync(false);
                    RefreshEvent?.Invoke(selectedEquipment, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("lstDetails_ItemTapped/EquipmentRFIDPopup" + ex.Message);
            }
        }

        private void txtRFID_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mEquipments = equipmentRepository.GetEquipmentRFID(txtRFID.Text).OrderBy(x=>x.EquipmentCode);
                if (mEquipments != null)
                {
                    lstDetails.ItemsSource = mEquipments.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("txtRFID_TextChanged/EquipmentRFIDPopup" + ex.Message);
            }
        }
    }
}