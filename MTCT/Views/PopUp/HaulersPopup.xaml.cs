﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HaulersPopup : PopupPage
    {
        public event EventHandler RefreshEvent;
        EqptHaulerRepository eqptHaulerRepository;

        public HaulersPopup()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            eqptHaulerRepository = new EqptHaulerRepository();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindHaulers();
        }

        public void BindHaulers()
        {
            try
            {
                var mHaulers = eqptHaulerRepository.GetEqTpHaulers().OrderBy(x => x.EqtphaulerCode);
                if (mHaulers != null)
                {
                    lstDetails.ItemsSource = mHaulers.ToList();
                    lstDetails.HeightRequest = mHaulers.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindHaulers/HaulersPopup" + ex.Message);
            }
        }

        private void TxtHaulerCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mHaulers = eqptHaulerRepository.GetEqTpHaulersbyName(txtHaulerCode.Text);
                if (mHaulers != null)
                {
                    lstDetails.ItemsSource = mHaulers.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("TxtHaulerCode_TextChanged/HaulersPopup" + ex.Message);
            }
        }

        private async void LstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedTrailer = e.Item as EqTpHaulers;
                if (selectedTrailer != null)
                {
                    await PopupNavigation.PopAsync(false);
                    RefreshEvent?.Invoke(selectedTrailer, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("LstDetails_ItemTapped/HaulersPopup" + ex.Message);
            }
        }
    }
}