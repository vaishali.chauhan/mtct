﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocationPopup : PopupPage
    {
        public event EventHandler RefreshEvent;
        LocationRepository locationRepository;
        public LocationPopup()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            locationRepository = new LocationRepository();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindLocations();
        }

        public void BindLocations()
        {
            try
            {
                var mLocations = locationRepository.GetLocationss().OrderBy(x=>x.EqtplocationCode);
                if (mLocations != null)
                {
                    lstDetails.ItemsSource = mLocations.ToList();
                    lstDetails.HeightRequest = mLocations.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindLocations/LocationPopup" + ex.Message);
            }
        }

        private void txtLocationCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mLocations = locationRepository.GetLocationsbyName(txtLocationCode.Text);
                if (mLocations != null)
                {
                    lstDetails.ItemsSource = mLocations.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("txtLocationCode_TextChanged/LocationPopup" + ex.Message);
            }
        }

        private async void lstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedLocation = e.Item as Locations;
                if (selectedLocation != null)
                {
                    await PopupNavigation.PopAsync(false);
                    if (RefreshEvent != null)
                    {
                        RefreshEvent(selectedLocation, EventArgs.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("lstDetails_ItemTapped/LocationPopup" + ex.Message);
            }
        }
    }
}