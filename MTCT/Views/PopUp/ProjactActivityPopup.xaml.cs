﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProjactActivityPopup : PopupPage
    {
        public event EventHandler RefreshEvent;
        ProjectActivityRepository projectActivityRepository;
        Project mProject;
        public ProjactActivityPopup(Project project)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            projectActivityRepository = new ProjectActivityRepository();
            mProject = new Project();
            this.mProject = project;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindProjacts();
        }
        public void BindProjacts()
        {
            try
            {
                var mProjacts = projectActivityRepository.GetProjectActivityByProjectId(this.mProject.ProjectId).OrderBy(x => x.projactCode);
                if (mProjacts != null)
                {
                    lstDetails.ItemsSource = mProjacts.ToList();
                    lstDetails.HeightRequest = mProjacts.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindProjacts/ProjactActivityPopup" + ex.Message);
            }
        }

        private async void lstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedProjact = e.Item as ProjectActivity;
                if (selectedProjact != null)
                {
                    await PopupNavigation.PopAsync(false);
                    if (RefreshEvent != null)
                    {
                        RefreshEvent(selectedProjact, EventArgs.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("lstDetails_ItemTapped/ProjactActivityPopup" + ex.Message);
            }
        }

        private void txtProjactCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mProjacts = projectActivityRepository.GetProjectActivitybyCode(txtProjactCode.Text);
                if (mProjacts != null)
                {
                    lstDetails.ItemsSource = mProjacts.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("txtProjactCode_TextChanged/ProjactActivityPopup" + ex.Message);
            }
        }
    }
}