﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProjectPopup : PopupPage
    {
        public event EventHandler RefreshEvent;
        ProjectRepository projectRepository;
        public ProjectPopup()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            projectRepository = new ProjectRepository();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindProjects();
        }

        public void BindProjects()
        {
            try
            {
                var mProjects = projectRepository.GetProjects().OrderBy(x=>x.ProjectCode);
                if (mProjects != null)
                {
                    lstDetails.ItemsSource = mProjects.ToList();
                    lstDetails.HeightRequest = mProjects.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindProjects/ProjectPopup" + ex.Message);
            }
        }

        private async void lstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedProject = e.Item as Project;
                if (selectedProject != null)
                {
                    await PopupNavigation.PopAsync(false);
                    RefreshEvent?.Invoke(selectedProject, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("lstDetails_ItemTapped/ProjectPopup" + ex.Message);
            }
        }

        private void txtProjectCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mProjects = projectRepository.GetProjectbyName(txtProjectCode.Text);
                if (mProjects != null)
                {
                    lstDetails.ItemsSource = mProjects.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("txtProjectCode_TextChanged/ProjectPopup" + ex.Message);
            }
        }
    }
}