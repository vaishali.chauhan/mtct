﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScanRfidPopup : PopupPage
    {
        #region Objects
        public event EventHandler IsRefresh;
        private Equipment mEquipment;
        private List<Equipment> mEquipments;
        private EquipmentRepository equipmentRepository;
        List<string> DeviceList;
        string CurrentDevice = string.Empty;
        IEnumerable<string> RfidList;
        #endregion

        #region Consructor
        public ScanRfidPopup(IEnumerable<string> rfids)
        {
            InitializeComponent();
            CloseWhenBackgroundIsClicked = false;
            mEquipment = new Equipment();
            mEquipments = new List<Equipment>();
            equipmentRepository = new EquipmentRepository();
            DeviceList = new List<string>();
            RfidList = new List<string>(rfids);
        }
        #endregion

        #region Methods
        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindEquipments();
            //var rfidCode = "900034";  //Sahadev 17/5/23 : Need to bypass don't have device
            MessagingCenter.Subscribe<App, string>(this, "ScanBarcode", (arg1, rfidCode) =>
            {
                if (!string.IsNullOrEmpty(rfidCode) && !RfidList.Contains(rfidCode))
                {
                    GetEquipRfid(rfidCode);
                }
            });
        }

        public void BindEquipments()
        {
            try
            {
                mEquipments = equipmentRepository.GetEquipments();
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindEquipments/ScanRfidPopup" + ex.Message);
            }
        }

        public async Task GetDeviceList()
        {
            try
            {
                var deviceList = DependencyService.Get<IRfidScanning>().GetDevices();
                DeviceList.Clear();
                if (deviceList != null && deviceList.Count > 0)
                {
                    foreach (var item in deviceList)
                    {
                        DeviceList.Add(item);
                    }
                    if (DeviceList.Count > 0)
                    {
                        CurrentDevice = DeviceList.FirstOrDefault();
                        if (!string.IsNullOrEmpty(CurrentDevice))
                        {
                            await ConnectDevice(CurrentDevice);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetDeviceList/ScanRfidPopup" + ex.Message);
            }
        }

        public async Task ConnectDevice(string DeviceName)
        {
            try
            {
                if (!string.IsNullOrEmpty(DeviceName))
                {
                    bool status = DependencyService.Get<IRfidScanning>().ConnectBarcodeDevice(DeviceName);
                    if (status)
                    {
                        ScanRfidCode(DeviceName);
                    }
                    else
                    {
                        Utilties.DisplayInfo.ErrorMessage("Device is Disconnected");
                    }

                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ConnectDevice/ScanRfidPopup" + ex.Message);
            }
        }

        public void ScanRfidCode(string DeviceName)
        {
            try
            {
                if (!string.IsNullOrEmpty(DeviceName))
                {
                    DependencyService.Get<IRfidScanning>().ScanDevice(DeviceName);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("ScanRfidCode/ScanRfidPopup" + ex.Message);
            }
        }

        public void GetEquipRfid(string rfidCode)
        {
            try
            {
                string equipRfid = string.Empty;
                bool isNotZero = false;
                for (int i = 0; i < rfidCode.Length; i++)
                {
                    if (isNotZero)
                    {
                        equipRfid += rfidCode[i];
                    }
                    if (rfidCode[i] != '0' && !isNotZero)
                    {
                        equipRfid += rfidCode[i];
                        isNotZero = true;
                    }
                }

                if (!string.IsNullOrEmpty(equipRfid))
                {
                    if (mEquipments == null || (mEquipments != null && mEquipments.Count == 0))
                    {
                        BindEquipments();
                    }

                    mEquipment = mEquipments.Where(x => x.EquipmentRFID == equipRfid).FirstOrDefault();
                    if (mEquipment != null && mEquipment.Id > 0)
                    {
                        lblRfid.Text = mEquipment.EquipmentRFID;
                        lblEquipo.Text = mEquipment.EquipmentCode;
                        lblDescription.Text = mEquipment.EquipmentDesc;
                    }
                    else
                    {
                        DisplayInfo.ErrorMessage1("Etiqueta RFID para equipo " + equipRfid + " no encontrada");
                    }
                }
                else
                {
                    DisplayInfo.ErrorMessage("RFID no encontrada!");
                }
            }
            catch (Exception ex)
            {
                DisplayInfo.ErrorMessage("GetEquipRfid/ScanRfidPopup" + ex.Message);
            }
        }
        #endregion

        #region Events
        private async void BtnScan_Tapped(object sender, EventArgs e)
        {
            try
            {
                await GetDeviceList();
                //GetEquipRfid("137036");
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BtnScan_Tapped/ScanRfidPopup" + ex.Message);
            }
        }

        private async void BtnCancel_Tapped(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync(false);
            IsRefresh?.Invoke(mEquipment, null);
        }
        #endregion
    }
}