﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchDriverNamePopup : PopupPage
    {
        DriverRepository driverRepository;
        public event EventHandler RefreshEvent;

        public SearchDriverNamePopup()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            InitializeObject();
        }

        public void InitializeObject()
        {
            driverRepository = new DriverRepository();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindDrivers();
        }

        public void BindDrivers()
        {
            try
            {
                var mDrivers = driverRepository.GetDrivers().OrderBy(x=>x.DriverName);
                if (mDrivers != null)
                {
                    lstDetails.ItemsSource = mDrivers.ToList();
                    lstDetails.HeightRequest = mDrivers.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindDrivers/SearchDriverNamePopup" + ex.Message);
            }
        }

        private async void lstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedDriver = e.Item as Driver;
                if (selectedDriver != null)
                {
                    await PopupNavigation.PopAsync(false);
                    if (RefreshEvent != null)
                    {
                        RefreshEvent(selectedDriver, EventArgs.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("lstDetails_ItemTapped/SearchDriverNamePopup" + ex.Message);
            }
        }

        private void txtDriverName_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mDrivers = driverRepository.GetDriverName(txtDriverName.Text);
                if (mDrivers != null)
                {
                    lstDetails.ItemsSource = mDrivers.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("txtDriverName_TextChanged/SearchDriverNamePopup" + ex.Message);
            }
        }
    }
}