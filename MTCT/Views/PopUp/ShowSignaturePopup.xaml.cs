﻿using MTCT.Models.LocalModels;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShowSignaturePopup : PopupPage
    {
        bool isShow = false;
        public ShowSignaturePopup(bool isShow, string base64)
        {
            try
            {
                InitializeComponent();
                this.isShow = isShow;
                if (isShow)
                {
                    lblName.IsVisible = true;
                }
                else
                {
                    lblName.IsVisible = false;
                }

                if (!string.IsNullOrEmpty(base64) && !string.IsNullOrWhiteSpace(base64))
                {
                    LoadImage(base64);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public Image LoadImage(string base64string)
        {
            byte[] bytes = Convert.FromBase64String(base64string);

            using (MemoryStream ms = new MemoryStream(bytes))
            {
                imgSignature.Source = ImageSource.FromStream(() => new MemoryStream(bytes));
            }

            return imgSignature;
        }

        private async void imgCancel_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync(false);
        }
    }
}