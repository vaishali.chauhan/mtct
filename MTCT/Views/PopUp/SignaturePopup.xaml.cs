﻿using MTCT.Interface;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using SignaturePad.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignaturePopup : PopupPage
    {
        bool isShow = false;
       
        public EventHandler isRefresh;
        public SignaturePopup(bool isShow, string driverName)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            this.isShow = isShow;
            if (isShow)
            {
                lblHeader.IsVisible = false;
                grdDriverName.IsVisible = true;
                lblName.Text = driverName;
            }
            else
            {
                lblHeader.IsVisible = true;
                grdDriverName.IsVisible = false;
                lblName.Text = string.Empty;
            }
        }

        private void btnClose_Tapped(object sender, EventArgs e)
        {
            PopupNavigation.PopAllAsync();
        }

        private async void btnConfirm_Tapped(object sender, EventArgs e)
        {
            try
            {
                PopupNavigation.PopAllAsync();
                Byte[] sign = null;
                Stream mStream = await signatureView.GetImageStreamAsync(SignatureImageFormat.Png);
                if (mStream != null)
                {
                    signatureView.IsEnabled = false;
                    BinaryReader br = new BinaryReader(mStream);
                    br.BaseStream.Position = 0;
                    sign = br.ReadBytes((int)mStream.Length);

                    isRefresh(sign, EventArgs.Empty);

                    //string strBase64 = Convert.ToBase64String(All);
                    //var base64Img = strBase64;
                }
                else
                    isRefresh(sign, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnConfirm_Tapped/SignaturePopup Exception: " + ex.Message);
            }
        }
    }
}