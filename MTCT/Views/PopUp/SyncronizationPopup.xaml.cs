﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SyncronizationPopup : ContentPage
    {
        public SyncronizationPopup()
        {
            InitializeComponent();
        }
    }
}