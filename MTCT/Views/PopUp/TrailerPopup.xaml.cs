﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrailerPopup : PopupPage
    {
        public event EventHandler RefreshEvent;
        EqptTrailerRepository eqptTrailerRepository;
        int haulerId = 0;

        public TrailerPopup(int haulerId)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            this.haulerId = haulerId;
            eqptTrailerRepository = new EqptTrailerRepository();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindTrailers();
        }

        public void BindTrailers()
        {
            try
            {
                var mTrailers = eqptTrailerRepository.GetEqTpTrailersByHaulerId(haulerId).OrderBy(x=>x.EqtptrailerCode);
                if (mTrailers != null)
                {
                    lstDetails.ItemsSource = mTrailers.ToList();
                    lstDetails.HeightRequest = mTrailers.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindTrailers/TrailerPopup" + ex.Message);
            }
        }

        private void txtTrailerCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mTrailers = eqptTrailerRepository.GetEqTpTrailersbyName(txtTrailerCode.Text);
                if (mTrailers != null)
                {
                    lstDetails.ItemsSource = mTrailers.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("txtTrailerCode_TextChanged/TrailerPopup" + ex.Message);
            }
        }

        private async void lstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedTrailer = e.Item as EqTpTrailers;
                if (selectedTrailer != null)
                {
                    await PopupNavigation.PopAsync(false);
                    if (RefreshEvent != null)
                    {
                        RefreshEvent(selectedTrailer, EventArgs.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("lstDetails_ItemTapped/TrailerPopup" + ex.Message);
            }
        }
    }
}