﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Utilties;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VoidPopup : PopupPage
    {
        public EventHandler isVoidRefresh;

        public VoidPopup()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
        }

        private void btnNo_Tapped(object sender, EventArgs e)
        {
            imgNo.Source = "iconRadioFill.png";
            imgYes.Source = "iconRadioBlank.png";
        }

        private void btnYes_Tapped(object sender, EventArgs e)
        {
            imgYes.Source = "iconRadioFill.png";
            imgNo.Source = "iconRadioBlank.png";
        }

        private async void btnOk_Tapped(object sender, EventArgs e)
        {
            try
            {
                await PopupNavigation.PopAsync(false);
                if (isVoidRefresh != null)
                {
                    if (imgYes.Source.ToString().Replace("File: ", "") == "iconRadioFill.png")
                        isVoidRefresh(true, EventArgs.Empty);
                    else
                        isVoidRefresh(false, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("btnOk_Tapped/VoidPopup Exception: " + ex.Message);
            }
        }
    }
}