﻿using MTCT.Interface;
using MTCT.Services;
using MTCT.Utilties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.SplashScreen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SplashScreenPage : ContentPage
    {
        EqptHaulerRepository eqptHaulerRepository;

        public SplashScreenPage()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            eqptHaulerRepository = new EqptHaulerRepository();
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }

        protected async override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                await Task.Delay(3 * 1000);
                if (GetEqTpHaulers())
                {
                    App.Current.MainPage = new Xamarin.Forms.NavigationPage(new Views.MasterPages.LoginPage());
                }
                else
                {
                    App.Current.MainPage = new Xamarin.Forms.NavigationPage(new Views.Home.AddDevicePage());
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("OnAppearing/SplashScreenPage Exception: " + ex.Message);
            }
        }

        public bool GetEqTpHaulers()
        {
            return eqptHaulerRepository.GetEqTpHaulers().Count() > 0 ? true : false;
        }
    }
}