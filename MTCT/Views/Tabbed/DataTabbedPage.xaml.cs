﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DataTabbedPage : TabbedPage
    {
        public DataTabbedPage()
        {
            InitializeComponent();
        }
    }
}