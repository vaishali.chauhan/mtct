﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DriverPage : ContentPage
    {
        DriverRepository driverRepository;
        public event EventHandler RefreshEvent;
        public DriverPage()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            InitializeObject();
        }
        public void InitializeObject()
        {
            driverRepository = new DriverRepository();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindDrivers();
        }

        public void BindDrivers()
        {
            try
            {
                var mDrivers = driverRepository.GetDrivers();
                if (mDrivers != null)
                {
                    lstDetails.ItemsSource = mDrivers.OrderBy(x=>x.DriverName).ToList();
                    lstDetails.HeightRequest = mDrivers.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindDrivers/SearchDriverNamePopup" + ex.Message);
            }
        }

        private void lstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedDriver = e.Item as Driver;
                if (selectedDriver != null)
                {
                    if (RefreshEvent != null)
                    {
                        RefreshEvent(selectedDriver, EventArgs.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("lstDetails_ItemTapped/SearchDriverNamePopup" + ex.Message);
            }
        }

        private void txtDriverName_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mDrivers = driverRepository.GetDriverName(txtDriverName.Text);
                if (mDrivers != null)
                {
                    lstDetails.ItemsSource = mDrivers.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("txtDriverName_TextChanged/SearchDriverNamePopup" + ex.Message);
            }
        }
    }
}