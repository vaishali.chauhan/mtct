﻿using MTCT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EqtpHaulersPage : ContentPage
    {
        EqtpHaulersPageViewModel _viewModel;
        public EqtpHaulersPage()
        {
            InitializeComponent();

            _viewModel = new EqtpHaulersPageViewModel();

            BindingContext = _viewModel;
            _viewModel.Navigation = Navigation;
        }
    }
}