﻿using MTCT.Interface;
using MTCT.Models.LocalModels;
using MTCT.Services;
using MTCT.Utilties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EquipmentPage : ContentPage
    {
        EquipmentRepository equipmentRepository;
        public event EventHandler RefreshEvent;
        public EquipmentPage()
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Common.LanguageCode);
            Xamarin.Forms.DependencyService.Get<ILocalize>().SetLocale(ci);
            InitializeComponent();
            InitializeObject();
            BindEquipments();
        }

        public void InitializeObject()
        {
            equipmentRepository = new EquipmentRepository();
        }

        public void BindEquipments()
        {
            try
            {
                var mEquipments = equipmentRepository.GetEquipments();
                if (mEquipments != null)
                {
                    lstDetails.ItemsSource = mEquipments.OrderBy(x=>x.EquipmentCode).ToList();
                    lstDetails.HeightRequest = mEquipments.Count() * 50;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("BindEquipments/EquipmentRFIDPopup" + ex.Message);
            }
        }

        private void lstDetails_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                lstDetails.SelectedItem = null;
                var selectedEquipment = e.Item as Equipment;
                if (selectedEquipment != null)
                {
                    RefreshEvent?.Invoke(selectedEquipment, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("lstDetails_ItemTapped/EquipmentRFIDPopup" + ex.Message);
            }
        }

        private void txtRFID_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var mEquipments = equipmentRepository.GetEquipmentRFID(txtRFID.Text);
                if (mEquipments != null)
                {
                    lstDetails.ItemsSource = mEquipments.ToList();
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("txtRFID_TextChanged/EquipmentRFIDPopup" + ex.Message);
            }
        }
    }
}