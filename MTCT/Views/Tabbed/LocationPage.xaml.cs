﻿using MTCT.Models.LocalModels;
using MTCT.Services;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocationPage : ContentPage
    {
        public LocationPage()
        {
            InitializeComponent();
            GetAllLocation();
        }

        void GetAllLocation(string search = "")
        {
            try
            {
                List<Locations> locations = new List<Locations>();
                LocationRepository locationRepository = new LocationRepository();
                if (!string.IsNullOrEmpty(search))
                    locations = locationRepository.GetLocationsbyName(search);
                else
                    locations = locationRepository.GetLocationss();
                lstLocations.ItemsSource = locations;
            }
            catch (System.Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetAllLocation/Exception" + ex.Message);
            }
        }

        private void txtLocation_TextChanged(object sender, TextChangedEventArgs e)
        {
            GetAllLocation(txtLocation.Text);
        }

        private void lstLocations_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            lstLocations.SelectedItem = null;
        }
    }
}