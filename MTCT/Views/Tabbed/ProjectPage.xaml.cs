﻿using MTCT.Models.APIModels;
using MTCT.Services;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MTCT.Views.Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProjectPage : ContentPage
    {
        ProjectDetails mProjectDetails = new ProjectDetails();

        public ProjectPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetAllProject();
        }

        void GetAllProject()
        {
            try
            {
                mProjectDetails.ProjectDetail.Clear();

                ProjectDetail mProjectDetail = new ProjectDetail();
                ProjectRepository projectRepository = new ProjectRepository();
                ProjectActivityRepository projectActivityRepository = new ProjectActivityRepository();

                foreach (var mProject in projectRepository.GetProjects())
                {
                    var projAct = projectActivityRepository.GetProjectActivityByProjectId(mProject.ProjectId);
                    double projActHeight = (projAct.Count * 65);

                    mProjectDetails.ProjectDetail.Add(new ProjectDetail()
                    {
                        project = mProject,
                        projAct = projAct,
                        projActHeight = projActHeight
                    });
                }
                lstProject.ItemsSource = mProjectDetails.ProjectDetail;
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetAllProject Exception: " + ex.Message);
            }
        }

        private void lstProject_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            lstProject.SelectedItem = null;
        }

        private void txtProject_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtProject.Text))
                {
                    lstProject.ItemsSource = mProjectDetails.ProjectDetail.Where(x => x.project.ProjectDesc.ToLower().Contains(txtProject.Text) ||
                                                                                      x.project.ProjectCode.ToLower().Contains(txtProject.Text));
                }
                else
                {
                    lstProject.ItemsSource = mProjectDetails.ProjectDetail;
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("Search Exception: " + ex.Message);
            }
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                var grd = (Grid)sender;
                var img = (Image)grd.Children.LastOrDefault();

                var mProjectDetail = grd.BindingContext as ProjectDetail;
                if (mProjectDetail != null)
                {
                    if (mProjectDetail.IsDisplayprojAct)
                    {
                        mProjectDetail.IsDisplayprojAct = false;
                        img.Rotation = 0;
                    }
                    else
                    {
                        mProjectDetail.IsDisplayprojAct = true;
                        img.Rotation = 180;
                    }
                }
            }
            catch (Exception ex)
            {
                Utilties.DisplayInfo.ErrorMessage("GetAllProject Exception: " + ex.Message);
            }
        }
    }
}